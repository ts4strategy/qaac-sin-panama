/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_EmptyBalanceTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_EmptyBalanceTriggerHandler extends TriggerHandlerCustom{
    
    private Map<Id, ONTAP__EmptyBalance__c> newMap;
    private Map<Id, ONTAP__EmptyBalance__c> oldMap;
    private List<ONTAP__EmptyBalance__c> newList;
    private List<ONTAP__EmptyBalance__c> oldList;
    
    public V360_EmptyBalanceTriggerHandler() {
        
        this.newMap = (Map<Id, ONTAP__EmptyBalance__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__EmptyBalance__c>) Trigger.oldMap;
        this.newList = (List<ONTAP__EmptyBalance__c>) Trigger.new;
        this.oldList = (List<ONTAP__EmptyBalance__c>) Trigger.old;
    }
    
    /**
    * method wich start every time after record is inserted 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public override void afterInsert(){
        // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
		// Using ONTAP__Codigo_del_cliente__c instead 
		// drs@avx        	
		/*
        List<ONTAP__EmptyBalance__c> listEmp = PreventExecutionUtil.validateEmptyWithId(this.newMap);
        Set<Id> setIdsEmpties = new Set<Id>();
        for(ONTAP__EmptyBalance__c emp : listEmp){
            if(!setIdsEmpties.contains(emp.Id) && emp.ONTAP__Account__c == null ){setIdsEmpties.add(emp.Id);}
        }
         if(!setIdsEmpties.isEmpty()){
             relateAccountsToEmpties(setIdsEmpties);
         }
        */
    }
     
    /**
    * method wich start every time after record is updated 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */    
     public override void afterUpdate(){
         // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
		// Using ONTAP__Codigo_del_cliente__c instead 
		// drs@avx        	
		/*
        List<ONTAP__EmptyBalance__c> listEmp = PreventExecutionUtil.validateEmptyWithId(this.newMap);
        Set<Id> setIdsEmpties = new Set<Id>();
        for(ONTAP__EmptyBalance__c emp : listEmp){
            if(!setIdsEmpties.contains(emp.Id) && emp.ONTAP__Account__c == null ){setIdsEmpties.add(emp.Id);}
        }
         if(!setIdsEmpties.isEmpty()){
             relateAccountsToEmpties(setIdsEmpties);
         }
		*/
    }
    
    /**
   	* Method for relate an Account to Empty Balance.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    
    /*
	 * No need for code if accounts are linked using an External Id-Unique
 	* To relate the account use instead ONTAP__Codigo_del_cliente__c
 	* ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
 	* drs@avx
 	*/

    /*
    @future
    public static void relateAccountsToEmpties(Set<Id> newSetEmpty){
        List<ONTAP__EmptyBalance__c> newlist = [SELECT Id, ONTAP__SAPCustomerId__c, RecordTypeId, ONTAP__Account__c FROM ONTAP__EmptyBalance__c WHERE Id IN: newSetEmpty];
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        
        Id V360_EmptyBalanceRT = Schema.SObjectType.ONTAP__EmptyBalance__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.EMPTY_BALANCE_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        for (ONTAP__EmptyBalance__c accEmp:newlist){
            if(accEmp.ONTAP__SAPCustomerId__c !=NULL&&accEmp.RecordTypeId==V360_EmptyBalanceRT){
                SAPNumsToRelate.add(accEmp.ONTAP__SAPCustomerId__c );
            }
        }
        
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN:SAPNumsToRelate]){
            for (Account acc : accs){
                accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }
        
        List<ONTAP__EmptyBalance__c> listAccEmp = new List<ONTAP__EmptyBalance__c>();
        for (ONTAP__EmptyBalance__c finalEmpties:newlist){
           Account acc = accReference.get(finalEmpties.ONTAP__SAPCustomerId__c);
            if(acc!= NULL){           
                finalEmpties.ONTAP__Account__c = acc.Id;
                listAccEmp.add(finalEmpties);
			}
        }
        
        if(!listAccEmp.isEmpty()){
        	update listAccEmp;    
        }
    }
    */
}