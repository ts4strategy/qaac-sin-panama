/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: ItemObj.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 06/01/2019           Heron Zurita           Creation of methods.
*/

public class ItemObj {
    
    
    /**
    * Methods getters and setters for entity ItemObj
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
	public String sku{get; set;}
    public Boolean freeGood{get; set;}
    public Double price{get; set;}
    public Double unitPrice{get; set;}
    public String unit{get; set;}
    public Integer quantity{get; set;}
    public Double subtotal{get; set;}
    public Double discount{get; set;}
    public Double tax{get; set;}
    public Double total{get; set;}
    public List<Applieddeals> applieddeals{get; set;}
    

}