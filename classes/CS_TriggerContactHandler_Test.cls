/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_TriggerContactHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * 
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 07/02/2019           Gabriel E Garcia       Test class of CS_TriggerContactHandler 
 */
@isTest
private class CS_TriggerContactHandler_Test {
	
    /**
    * Method test TriggerContactHandler_after Insert and update
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
	@isTest static void test_TriggerContactHandler_afterIU() {
		Account cuenta = new Account(
			Name = 'TestCuenta');
		insert cuenta;

		Contact contacto = new Contact(
			LastName = 'Testcontact', 
			AccountId = cuenta.Id,
			CS_Contacto_Principal__c = true);
		insert contacto;

		Contact contacto2 = new Contact(
			LastName = 'Testcontact2', 
			AccountId = cuenta.Id,
			CS_Contacto_Principal__c = true);
		insert contacto2; 

		contacto.CS_Contacto_Principal__c = true;
		update contacto;

	}
	
}