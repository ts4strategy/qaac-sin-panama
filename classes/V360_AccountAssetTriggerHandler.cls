/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_AccountAssetTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_AccountAssetTriggerHandler extends TriggerHandlerCustom {

    private Map<Id, ONTAP__Account_Asset__c> newMap;
    private Map<Id, ONTAP__Account_Asset__c> oldMap;
    private List<ONTAP__Account_Asset__c> newList;
    private List<ONTAP__Account_Asset__c> oldList;
    
    public V360_AccountAssetTriggerHandler() {
        this.newMap = (Map<Id, ONTAP__Account_Asset__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__Account_Asset__c>) Trigger.oldMap;
        this.newList = (List<ONTAP__Account_Asset__c>) Trigger.new;
        this.oldList = (List<ONTAP__Account_Asset__c>) Trigger.old;
    }
    
    /**
    * Method for after insert
    * Created By: g.martinez.cabral@accenture.com
    * @param Void
    * @return Void
    */    
    public override void afterInsert(){
        
        // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
		// Using ONTAP__Codigo_del_cliente__c instead 
		// drs@avx        	
		
        /*
        List<ONTAP__Account_Asset__c> objList = PreventExecutionUtil.validateAccountAssetWithId(this.newMap);
        Set<Id> setIdsAsset = new Set<Id>();
        for(ONTAP__Account_Asset__c aass : objList){
            if(!setIdsAsset.contains(aass.Id) && aass.ONTAP__Account__c == null){setIdsAsset.add(aass.Id);}
        }
        if(!setIdsAsset.isEmpty()){
        	relateAccountsToAssetsAfter(setIdsAsset);
        }
        */
    }
    
    /**
    * Method for after update
    * Created By: g.martinez.cabral@accenture.com
    * @param Void
    * @return Void 
    */    
     public override void afterUpdate(){
         
        // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
		// Using ONTAP__Codigo_del_cliente__c instead 
		// drs@avx
		      	

        /*
 		
        List<ONTAP__Account_Asset__c> objList = PreventExecutionUtil.validateAccountAssetWithId(this.newMap);
        Set<Id> setIdsAsset = new Set<Id>();
        for(ONTAP__Account_Asset__c aass : objList){
            if(!setIdsAsset.contains(aass.Id) && aass.ONTAP__Account__c == null ){setIdsAsset.add(aass.Id);}
        }
         System.debug('setIdsAsset ' + setIdsAsset);
         if(!setIdsAsset.isEmpty()){
             relateAccountsToAssetsAfter(setIdsAsset);
         }
         */
    }
    
    /**
    * Method for relate Accounts by Sap Customer Id
    * Created By: g.martinez.cabral@accenture.com
    * @param List<ONTAP__Account_Asset__c> newlist
    * @return Void
    */  
    /*
	 * No need for code if accounts are linked using an External Id-Unique
 	* To relate the account use instead ONTAP__Codigo_del_cliente__c
 	* ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
 	* drs@avx
 	*/

    /*
    @future
    public static void relateAccountsToAssetsAfter(Set<Id> newSetAsset){
        List<ONTAP__Account_Asset__c> newlist = [SELECT Id, V360_SAPCustomerId__c, RecordTypeId, ONTAP__Account__c FROM ONTAP__Account_Asset__c WHERE Id IN: newSetAsset];
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        system.debug('Valor ASSET RECORD TYPE : '+GlobalStrings.ASSETS_V360_RECORDTYPE_NAME);
        Id V360_Account_AssetRT = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ASSETS_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        for (ONTAP__Account_Asset__c accAsset:newlist){
            if(accAsset.V360_SAPCustomerId__c != NULL && accAsset.RecordTypeId == V360_Account_AssetRT){
                SAPNumsToRelate.add(accAsset.V360_SAPCustomerId__c);
            }
        }
        
        
        System.debug('SAPNumsToRelate ' + SAPNumsToRelate);
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN: SAPNumsToRelate]){
            for (Account acc : accs){
                 accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }
        
        System.debug('accReference'+accReference);
      
        List<ONTAP__Account_Asset__c> listAccAss = new List<ONTAP__Account_Asset__c>();
        for (ONTAP__Account_Asset__c finalAsstet:newlist){
           Account acc = accReference.get(finalAsstet.V360_SAPCustomerId__c);
            if(acc!= NULL){           
                finalAsstet.ONTAP__Account__c = acc.Id;
                listAccAss.add(finalAsstet);
			}
           System.debug('Asset: '+finalAsstet);
        }
        
        if(!listAccAss.isEmpty()){
        	update listAccAss;    
        }
        
    }
    */
}