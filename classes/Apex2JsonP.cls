/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: Apex2JsonP.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class Apex2JsonP {
    
  
		public String token;
		public String accountId;
		public String salesOrg;
		public String paymentMethod;
		public String orderType;
    	
    	public List<json_items> items;
    	public List<json_deals> selectedDeals;
    
        public Apex2JsonP(){
        }

}