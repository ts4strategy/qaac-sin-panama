/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Class: CS_CASEASSETACCOUNT_CLS_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 11/02/2019           Debbie Zacarias        Creation of the test class for CS_CASEASSETACCOUNT_CLS
*/

@isTest
private class CS_CaseAssetAccount_Class_Test
{   
    /**
    * Method for create data 
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void setupTestData()
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ISSM_TypificationMatrix__c typMat = new ISSM_TypificationMatrix__c();
        typMat.CS_Days_to_End__c = 1;
        insert typMat;
        
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
        
        RecordType rectype = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtInstallationCooler]);
        RecordType rectypeSV = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtRetirementCooler]);
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Subject';
        caseTest.ISSM_TypificationNumber__c = typMat.Id;
        caseTest.HONES_Case_Country__c = 'Honduras';
        caseTest.CS_Send_To_Mule__c = false;
        caseTest.RecordTypeId = rectype.id;
        caseTest.ISSM_BrandProduct__c = 'Brand';
        caseTest.ISSM_CaseReason__c = 'Description';
        caseTest.Equipment_model__c = 'model';
        caseTest.ISSM_SerialNumber__c = '0987654321';
        insert caseTest;
        
        ONTAP__Account_Asset__c assetAcc = new ONTAP__Account_Asset__c();
        assetAcc.ONTAP__Asset_Description__c = 'Description';
        assetAcc.HONES_EquipmentNumber__c='1234567890';
        assetAcc.HONES_Asset_Model__c='model';
        assetAcc.ONTAP__Serial_Number__c='0987654321';
        assetAcc.V360_MaterialNumber__c='1029384756';
        assetAcc.ONTAP__Brand__c='Brand';
        assetAcc.ONTAP__Account__c = accountTest.id;
        Insert assetAcc;
        
    }
    
    /**
    * Method for test the method getSerialNumber
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getSerialNumber()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        CS_CaseAssetAccount_Class.getSerialNumber(caso.Id);
    }
    
    /**
    * Method for test the method getDescription
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_getAssetInfo()
    {
        Case caso = ([SELECT id, ISSM_SerialNumber__c FROM Case LIMIT 1]);        
        CS_CaseAssetAccount_Class.getAssetInfo(caso.Id, caso.ISSM_SerialNumber__c);
    }
    
    /**
    * Method for test the method SetAssetCaseInfo
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_SetAssetCaseInfo()
    {
        Case caso = ([SELECT id FROM Case LIMIT 1]);
        CS_CaseAssetAccount_Class.SetAssetCaseInfo(caso.Id);
    }
    
    /**
    * Method for test the method UpdateCase
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_UpdateCase()
    {     
        Case caso = ([SELECT id, ISSM_SerialNumber__c FROM Case LIMIT 1]);        
        CS_CaseAssetAccount_Class.UpdateCase(caso.Id, caso.ISSM_SerialNumber__c, 1);
    }
    
    /**
    * Method for test the method UpdateCase
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_UpdateCaseAsset()
    {
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;        
        Case caso = ([SELECT id, ISSM_SerialNumber__c FROM Case LIMIT 1]);
        
        CS_CaseAssetAccount_Class.UpdateCaseAsset(caso.Id, caso.ISSM_SerialNumber__c);
        
    }
}