/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: JSON2Apex.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Heron Zurita           Creation of methods.
*/

public class JSON2Apex {
	/**
    * Methods class and method for model JSON2Apex
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
	public class Order {
		public String country;
		public String accountId;
		public String salesOrg;
		public String paymentMethod;
        public String orderType;
		public Double total;
		public Double subtotal; 
		public Double tax;
		public Double discount;
		public Empties empties;
		public List<Items> items;
	}

	public List<OutputMessages> outputMessages;
	public Order order;

	public class Empties {
		public String totalAmount;
		public String minimumRequired;
		public Integer extraAmount;
	}

	public class Items {
		public String sku;
		public String quantity;
		public Boolean freeGood;
        public String unit;
		public Double discount;
		public Double tax;
		public Double subtotal;
		public Double total;
		public Double price;
		public Double unitPrice;
        public List<applied_Deals> applieddeals;
	}
    
    public class applied_Deals{
        public String pronr;
        public String condcode;
        public Decimal amount;
        public Decimal percentage;
        public String posex;
        public String scaleid;
    }

	public class OutputMessages {
        public String text;
        public String type;
        public String key;
	}

	
	public static JSON2Apex parse(String json) {

		return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
	}
}