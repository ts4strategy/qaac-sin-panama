public class ONCALL_QueueableSendOrder implements Queueable  {
    Integer size = 100;
 
	public void execute(QueueableContext context) {        
        System.debug('count batch' + [SELECT COUNT() FROM AsyncApexJob WHERE JobType='BatchApex' AND Status IN ('Processing','Preparing','Queued')]);
        if([SELECT COUNT() FROM AsyncApexJob WHERE JobType='BatchApex' AND Status IN ('Processing','Preparing','Queued')] >= 5){
			SettingsBatchSendOrder__c sbs = SettingsBatchSendOrder__c.getOrgDefaults();            	            
                        
            Integer stophour = 20; 
            Integer stopminute = 59;
            Integer nextExecute = 2;
            if(sbs.Hour_Stop__c !=null && sbs.Hour_Stop__c < 25 && sbs.Hour_Stop__c > -1) stophour = Integer.valueOf(sbs.Hour_Stop__c);
            if(sbs.Minute_Stop__c !=null && sbs.Minute_Stop__c < 60 && sbs.Minute_Stop__c > -1) stopminute = Integer.valueOf(sbs.Minute_Stop__c);
            if(sbs.Next_Execute_Extended__c !=null && sbs.Next_Execute_Extended__c < 14001 && sbs.Next_Execute_Extended__c > -1) nextExecute = Integer.valueOf(sbs.Next_Execute_Extended__c);
            
            Datetime hoy =  System.now();            
            hoy = hoy.addMinutes(nextExecute);		
            String cronExpression = '0 ' + String.valueOf(hoy.minute()) + ' ' + String.valueOf(hoy.hour()) + ' ' + String.valueOf(hoy.day()) + ' ' + String.valueOf(hoy.month()) + ' ? ' + String.valueOf(hoy.year());       
            System.debug('cronExpression ' + cronExpression);
          
            System.Debug('hour ' + stophour + ' minute '+ stopminute );
            System.debug('hora ' + hoy.hour() + ' minute ' + hoy.minute());
            if(hoy.hour() <= stophour && hoy.minute() <= stopminute ){
                String SCHEDULE_NAME = 'SendOrder' + String.valueOf(hoy.getTime());
                System.schedule(SCHEDULE_NAME, cronExpression, new ONCALL_SchedulerSendOrder()); 
            }                
        }else{        
            SettingsBatchSendOrder__c sbs = SettingsBatchSendOrder__c.getOrgDefaults();
            if(sbs.Batch_size__c !=null && sbs.Batch_size__c < 101 && sbs.Batch_size__c > 0) size = Integer.valueOf(sbs.Batch_size__c);
            
            ONCALL_SendOrder_Batch b = new ONCALL_SendOrder_Batch();
            Database.executeBatch(b, size);   
        }
    }
}