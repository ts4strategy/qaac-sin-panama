/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CaseProspect_Class.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 23/04/2019      Jose Luis Vargas        Creation of the class for re-send prospect to mulesoft
 *                                             
*/

public class CS_CaseProspect_Class 
{
    /**
    * method for send to mulesoft the prospect 
    * @author: jose.l.vargas.lara@accenture.com
    * @param IdCase to get Account Id
    * @return Boolean if process is correct
    */ 
    @AuraEnabled
    public static boolean SendCaseProspect(string IdCase)
    {
        List<Id> lstProspect = new List<Id>();
        Case oProspectCase = new Case();
        try
        {
            /* Se obtiene el Id de Prospecto del caso */
            oProspectCase = [SELECT AccountId FROM Case WHERE Id =: IdCase];
            lstProspect.add(oProspectCase.AccountId);
            
            /* Se ejecuta el Callout para envio a MuleSoft*/
            CS_Prospect_Callout_Class.SendProspectMuleSoft(lstProspect);
            
            return true;
        }
        catch(Exception ex)
        {
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Message: ' + ex.getMessage());   
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Cause: ' + ex.getCause());   
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CaseProspect_Class.SendCaseProspect  Stack trace: ' + ex.getStackTraceString());
            return false;
        }
    }
    
    /**
    * method to get the flag to send prospect 
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Boolean
    */ 
    @AuraEnabled
    public static boolean SendProspectMdWeb()
    {
        CS_Prospect_Settings__c prospectSetting = CS_Prospect_Settings__c.getOrgDefaults();
        return prospectSetting.CS_Send_MdWeb__c;
    }
}