/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_DeleteRouteBatch_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
public class VisitControl_DeleteRouteBatch_Test {
    
   /**
   * Method test for delete all the assets in VisitControl_DeleteRouteBatch.
   * @author: g.martinez.cabral@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testBatch(){
        
        VisitControl_RetraceAndAnticipateRoute__c ob = new VisitControl_RetraceAndAnticipateRoute__c(VisitControl_Deleted__c=TRUE,VisitControl_DateToSkip__c=System.today(), VisitControl_RetraceOrAnticipate__c='Anticipate');
        insert ob;    
        VisitControl_DeleteRouteBatch obj01 = new VisitControl_DeleteRouteBatch();
        Database.executeBatch(obj01, 200);
    }
}