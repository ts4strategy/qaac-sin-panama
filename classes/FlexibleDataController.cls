/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: FlexibleDataController.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 * 24/jun/2019 		Daniel Rosales			Modified "genOrder" method to accept new response from Deal Conditions webservice
 */
public class FlexibleDataController {
    
    /**
    * Method to get all the promos for a user by Id  
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<Id>
    */
    @AuraEnabled
    public static List<Id> getpromosId(String recordId){
        
        List<Id> listcall=new List<Id>();
        try{
        ONCALL__Call__c callobj = [SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId];
        listcall.add(callobj.ONCALL__POC__c);
         } catch(exception e) {
       	system.debug(e);
        }             
        return listcall;  
        
    }
    
    /**
    * Method to get a list of promotions  fron a recordId 
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<ONTAP__Promociones__c>
    */
    @AuraEnabled
    public static List<ONTAP__Promociones__c> getpromos(String recordId){
        List <ONTAP__Promociones__c> listpromoid = new List <ONTAP__Promociones__C>();
        try{
		 listpromoid=new List<ONTAP__Promociones__c> ([SELECT Id,Name,ONTAP__Type_of_promotion__c FROM ONTAP__Promociones__c WHERE ONTAP__Customer__c =:recordId]);
         }
        catch(exception e) {
       	system.debug(e);
        }             
        return listpromoid;
    }
    
    /**
    * Method to get id of POC from recordId type 1  
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<Id>
    */
    @AuraEnabled
    public static List<Id> getAccountFromCall(String recordId){
        List<Id> listcall=new List<Id>();
        try{
        ONCALL__Call__c callobj = [SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId];
        listcall.add(callobj.ONCALL__POC__c);
              } catch(exception e) {
       	system.debug(e);
        } 
        return listcall; 
    }
    
    /**
    * Method to get id of POC from recordId type 2  
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<Id>
    */
    @AuraEnabled
    public static List<Id> getAccountFromCall2(String recordId){
        List<Id> listcall=new List<Id>();
        try{
        ONCALL__Call__c callobj = [SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId];
        listcall.add(callobj.ONCALL__POC__c);
              } catch(exception e) {
       	system.debug(e);
        } 
        return listcall; 
    }
    
    /**
 	* Method from flexible data Account 
  	* Created By: g.martinez.cabral@accenture.com
  	* @param String recordId
  	* @return List<List<V360_FlexibleData__c>>
  	*/
    @AuraEnabled
    public static  List<List<V360_FlexibleData__c>> getFlexibleIdAccount (String recordId){
        String types;
        String country;
        
        List<Account> account = new List<Account>();
        account = [Select Id, ONTAP__ExternalKey__c From Account Where Id =: recordId Limit 1];
        
        country = account[0].ONTAP__ExternalKey__c.left(2);
        
        List<List<V360_FlexibleData__c>> general = new List<List<V360_FlexibleData__c>>();
        List<String> daataType = new List<String>();
        try{
        
        //List<V360_FlexibleData__c> data = [SELECT Name,V360_Account__c , V360_Type__c,V360_Value__c FROM V360_FlexibleData__c WHERE ( V360_Account__c =:recordId AND V360_ValidTo__c >=: System.TODAY()) OR V360_Account__c=''  ORDER BY V360_Sort__c ASC LIMIT 2000]; 
        List<V360_FlexibleData__c> data = [SELECT  Name,
                                                   V360_Account__c , 
                                                   V360_Type__c,
                                                   V360_Value__c 
                                           FROM V360_FlexibleData__c 
                                           WHERE ((V360_Account__c =:recordId) OR V360_Account__c='') AND
                                           V360_Country_Code__c =: country
                                           ORDER BY V360_Sort__c ASC LIMIT 2000]; 
        
        //fill list with types
            for (V360_FlexibleData__c dta : data){
            types=dta.V360_Type__c.toLowerCase();
            if(!daataType.contains(types))
            {daataType.add(types);}
            
        }
        //separate list by types
        for(String b:daataType){
            List<V360_FlexibleData__c> general2 = new List<V360_FlexibleData__c>();
            for(V360_FlexibleData__c a:data) {
                if(a.V360_Type__c == b ){
                    general2.add(a);
                }
            }
            general.add(general2);
            
        }    } catch(exception e) {
       	system.debug(e);
        }  
        return general;
    }
    
    /**
    * Method to get a specific flexible data  from an account
    * Created By: g.martinez.cabral@accenture.com
    * @param String recordId
    * @return List<List<V360_FlexibleData__c>>
    */
    @AuraEnabled
    public static  List<List<V360_FlexibleData__c>> getFlexibleData (String recordId){
        String types;
        
        List<List<V360_FlexibleData__c>> general = new List<List<V360_FlexibleData__c>>();
        List<String> daataType = new List<String>();
        try{
        List<V360_FlexibleData__c> data = [SELECT Name, V360_Type__c,V360_Value__c FROM V360_FlexibleData__c WHERE V360_Account__c !=:recordId  ORDER BY V360_Sort__c ASC LIMIT 2000];
        //fill list with types
        for (V360_FlexibleData__c dta :data){
            types=dta.V360_Type__c.toLowerCase(); 
            if(!daataType.contains(types))
            {daataType.add(types);}
            
        }
        //separate list by types
        for(String b:daataType)
        {
            List<V360_FlexibleData__c> general2 = new List<V360_FlexibleData__c>();
            for(V360_FlexibleData__c a:data) {
                if(a.V360_Type__c == b ){
                    general2.add(a);
                }
            }
            general.add(general2);
            
        }  } catch(exception e) {
       	system.debug(e);
        } 
        return general;
    }
    
    /**
  	* Method from list of all flexible data
  	* Created By: g.martinez.cabral@accenture.com
  	* @param List<V360_FlexibleData__c> dataQueryConsult
  	* @return List<List<V360_FlexibleData__c>>
  	*/
    public static List<List<V360_FlexibleData__c>> getGroupedLists(List<V360_FlexibleData__c> dataQueryConsult){
        //Declarated Variables
        List<List<V360_FlexibleData__c>> groupedLists = new List<List<V360_FlexibleData__c>>();        
        List<String> dataType = new List<String>();
        try{ 
        for(V360_FlexibleData__c flexibleDataObject : dataQueryConsult){
            if(!dataType.contains(flexibleDataObject.V360_Type__c.toLowerCase())){
                dataType.add(flexibleDataObject.V360_Type__c.toLowerCase());
            }
        }
          } catch(exception e) {
       	system.debug(e);
        } 
        return groupedLists;
    }   
    
    /**
  	* Method to test the creation of an order from an account
  	* Created By: g.martinez.cabral@accenture.com
  	* @param String recordId
  	* @return List<wrapperClass>
  	*/
    @AuraEnabled
    
    public static List<wrapperClass> genOrder(String recordId) {
  	
        Account 
            c =
            [SELECT ONTAP__SAP_Number__c, ONTAP__ExternalKey__c, ONTAP__Credit_Condition__c, ONTAP__SalesOgId__c 
             FROM Account 
             WHERE id =:recordId Limit 1];
		String account_country = c.ONTAP__ExternalKey__c.left(2);
        End_Point_Get_Deals__mdt 
            endpoint =
            [SELECT Token__c, URL__c, Country__c 
             FROM End_Point_Get_Deals__mdt 
             WHERE Token__c != null 
             AND Country__c =: account_country Limit 1];
  		
        List<wrapperClass> wrapobj=new List<wrapperClass>();
        
  		Apex2JsonDeals deals_call = new Apex2JsonDeals();        
        deals_call.token = endpoint.Token__c;
        deals_call.salesOrg = c.ONTAP__SalesOgId__c;
        deals_call.accountId = String.valueOf(c.ONTAP__SAP_Number__c);

		JSONGenerator generator = JSON.createGenerator(true);
		generator.writeObject(deals_call);
		
        //////////////////////////////////////////////////
  		Http http = new Http();
  		HttpRequest request = new HttpRequest();
  		request.setEndpoint(endpoint.URL__c);
  		request.setMethod('POST');
  		request.setHeader('Content-Type', 'application/json;charset=utf-8');
        System.debug(generator.getAsString()); //drosales@avx.com
  		request.setBody(generator.getAsString());
		system.debug('request.getbody: ' + request.getBody()); //drosales@avx.com 
        HttpResponse response = http.send(request);
        	
        if (response.getStatusCode()== 200) {
            system.debug('response.getbody: ' + response.getBody()); 
            //--- New parsing for new response structure drs@avx
            List<JSON2apexDeals> jsonresDeals = (List<JSON2apexDeals>) system.JSON.deserialize(response.getBody(), List<JSON2apexDeals>.class);
            
            for(JSON2apexDeals D : jsonresDeals){
        		system.debug('Deal Condition: ' + D);
                wrapperClass wrap=new wrapperClass();
                wrap.optional=true;
           	 	wrap.description = D.title; 
            	wrap.pronr = D.promotionNumber; 
                wrapobj.add	(wrap);
    		}    
            //---
            // removed code from old structure parsing
            /*
            List<wrapperClass> wrapobjfalso=new List<wrapperClass>();
        	List<wrapperClass> wrapobjVer=new List<wrapperClass>();
        
        	for(Integer i=0;i<jsonresDeals.deals.size();i++){
            	wrapperClass wrap=new wrapperClass();
            	wrap.optional=jsonresDeals.deals[i].optional;
           	 	wrap.description=String.valueOf(jsonresDeals.deals[i].description); // changed from description to title // drs@avx
            	wrap.pronr=String.valueOf(jsonresDeals.deals[i].pronr); // changed from pronr to promotionNumber // drs@avx
            
                if(wrap.optional ==false){
                    wrapobjfalso.add(wrap);  
                }
                else if(wrap.optional ==true){
                    wrapobjVer.add(wrap);
                }
        	}
            wrapobj.addall(wrapobjfalso);
            wrapobj.addall(wrapobjVer);
            */
            //----
		}
        return wrapobj;
    }    
}