/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: Oncall_JsonPriceConversionWrapper.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 06/01/2019           Heron Zurita           Creation of methods.
*/
public class Oncall_JsonPriceConversionWrapper {
    
    /**
    * Methods getters and setters for entity Oncall_JsonPriceConversionWrapper
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
   @AuraEnabled public OrderTestJSON order{set;get;}
    
   @AuraEnabled public List<ItemTestJson> Items{set;get;}
}