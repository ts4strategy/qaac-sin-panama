/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Class with Event operations

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       26-07-2017        Andrés Garrido               Creation Class
	2.0		  19.12.2018		WiiT						 Commented for Wiit
****************************************************************************************************/
global class EventOperations_cls {

    /**
    * Method that synchronize the Event object in SFDC with Event object in Heroku, occurs when a record is insert
    * @params:
      1.-mapNewTour: map with all new even records
    * @return void
    **/
    /*public static void syncInsertEventToExternalObject(map<Id, Event> mapEvent){
        set<Id> setIds = new set<Id>();
        for(Event objEvent : mapEvent.values()){
            if(objEvent.VisitList__c != null && !objEvent.OffRoute__c)
                setIds.add(objEvent.Id);
        }
        if(!setIds.isEmpty())
            EventOperations_cls.insertEventToExternalObjectWS(setIds);
    }*/

    /**
    * Method that synchronize the Event object in SFDC with Event object in Heroku, occurs when a record is delete
    * @params:
      1.-mapEvent: map with all event records
    * @return void
    **/
    /*public static void syncDeleteEventToExternalObject(map<Id, Event> mapEvent){
        //EventOperations_cls.deleteEventToExternalObject(mapEvent.keySet());
        EventOperations_cls.deleteEventToExternalObjectWS(mapEvent.keySet());
    }*/

     /**
    * Future method that synchronize the Event object in SFDC with Event object in Heroku, occurs when a record is insert
    * @params:
      1.-setIds: set with all event Ids
    * @return void
    **/
    //@future(callout=true)
    /*public static void insertEventToExternalObjectWS(set<Id> setIds){
        EventOperations_cls.insertEventToExternalObject(setIds, true);
    }*/

     /**
    * Future method that synchronize the Event object in SFDC with Event object in Heroku, occurs when a record is insert
    * @params:
      1.-setIds: set with all event Ids
    * @return void
    **/
    /*public static Boolean insertEventToExternalObject(set<Id> setIds, Boolean blnUpdate){
        //Obtain the record types that will be sinchronize with heroku
        list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');

        list<Event> lstEvent = [
            Select  Id, VisitList__c, VisitList__r.RecordType.DeveloperName, ONTAP__Estado_de_visita__c, OwnerId, Owner.UserName,
                    Sequence__c, StartDateTime, EndDateTime, VisitList__r.Route__r.ServiceModel__c, CustomerId__c,
                    VisitList__r.ONTAP__TourId__c, EventSubestatus__c, SynchronizedHeroku__c
            From    Event
            Where   Id = :setIds And VisitList__r.RecordType.DeveloperName = :lstRecordType And OffRoute__c = false
                    And SynchronizedHeroku__c = false
        ];

        //list with the external records in the even external object
        SyncObjects.SyncEventObject objSync = new SyncObjects.SyncEventObject();

        for(Event objEvent :lstEvent){
            SyncObjects.EventObject objExtEvent     = new SyncObjects.EventObject();
            objExtEvent.salesforceid                = objEvent.Id;
            objExtEvent.customerid                  = objEvent.CustomerId__c;
            objExtEvent.sequence                    = objEvent.Sequence__c;
            objExtEvent.tourid                      = objEvent.VisitList__r.ONTAP__TourId__c;
            objExtEvent.visitid                     = objEvent.VisitList__c;
            objExtEvent.visitstatus                 = objEvent.ONTAP__Estado_de_visita__c;
            objExtEvent.visitsubestatus             = objEvent.EventSubestatus__c;
            objExtEvent.starttime                   = objEvent.StartDateTime;
            objExtEvent.endtime                     = objEvent.EndDateTime;
            objExtEvent.salesagent                  = objEvent.Owner.UserName;
            objSync.events.add(objExtEvent);

            objEvent.SynchronizedHeroku__c          = true;
        }

        //if not is empty callout heroku web service
        if(!objSync.events.isEmpty()){
            String strStatusCode = SyncObjects.calloutInsertHerokuEvents(objSync);
            if(strStatusCode == '200'){
                if(blnUpdate){
                    TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterUpdateSort');
                    update lstEvent;
                }
                return true;
            }
        }
        return false;
    }*/
    
    /**
    * Future method that synchronize the Event object in SFDC with Event object in Heroku, occurs when a record is delete
    * @params:
      1.-setIds: set with all event Ids
    * @return void
    **/
    /*@future(callout=true)
    public static void deleteEventToExternalObjectWS(set<Id> setIds){
       // EventOperations_cls.deleteEventToExternalObject(setIds);

    }*/
    
    /**
    * Method that synchronize the Event object in SFDC with Event object in Heroku, occurs when a record is delete
    * @params:
      1.-setIds: set with all event Ids
    * @return void
    **/
    /**public static void deleteEventToExternalObject(set<Id> setIds){
        SyncObjects.DeleteEventObject objSync = new SyncObjects.DeleteEventObject();
        for(String strId :setIds){
            SyncObjects.ObjEventDelete objDel = new SyncObjects.ObjEventDelete();
            objDel.salesforceid = strId;

            objSync.events.add(objDel);
        }

        if(!objSync.events.isEmpty()){
            String strStatusCode = SyncObjects.calloutDeleteHerokuEvents(objSync);
            if(strStatusCode == '200'){
                System.debug('\nDELETE SUCCESS');
            }

        }

    }*/

    /**
    * Method that creates a order detail when a orderdetailtext field changes in a event record, the field is fill from heroku
    * @params:
      1.-lstNewEvent: list of the new events
      2.-lstOldEvent: list of the old events
    * @return void
    **/
    /*public static void manageOrderDetail(list<Event> lstNewEvent, list<Event> lstOldEvent){
        map<String, Event> mapEvent = new map<String, Event>();
        map<String, VisitDetail__c> mapVD = new map<String, VisitDetail__c>();
        set<String> setIdToDel = new set<String>();

        list<VisitOrder__c> lstAllVO = new list<VisitOrder__c>();

        //For each event, validate if the orderdetailtext field is changed
        for(Integer i=0; i<lstNewEvent.size(); i++){
            if( lstNewEvent[i].VisitList__c != null && ((lstOldEvent==null && lstNewEvent[i].OrderDetailText__c != null) ||
                (lstOldEvent!=null && lstNewEvent[i].OrderDetailText__c != lstOldEvent[i].OrderDetailText__c))
            ){
                //Generate the visit detail for each event
                String strOrders = lstNewEvent[i].OrderDetailText__c;
                //Key for the map
                String strKey = strOrders + '$' + lstNewEvent[i].VisitList__c + '$' + lstNewEvent[i].Sequence__c;
                //set the event map
                mapEvent.put(strKey, lstNewEvent[i]);
                //set the visit detail map
                mapVD.put(strKey, new VisitDetail__c(OwnerId=lstNewEvent[i].OwnerId));
                //if the visit detail record is related with the event, delete the record
                if(lstNewEvent[i].OrderDetail__c != null)
                    setIdToDel.add(lstNewEvent[i].OrderDetail__c);
            }
        }

        if(!mapVD.isEmpty()){
            //Insert the visit detail records
            insert mapVD.values();

            //Relate the visit detail record with each event depending the value in orderdetailtext field
            for(String strKey : mapVD.keySet()){
                list<String> lstKey = strKey.split('$');
                String strOrders = lstKey[0];
                
                list<String> lstDetails = strOrders.split('&');
                System.debug('\nDetails ==> '+lstDetails);
                //Relate the record with the event
                mapEvent.get(strKey).OrderDetail__c = mapVD.get(strKey).Id;
                for(String detail : lstDetails){
                    list<String> lstData = detail.split('-');
                    if(lstData.size() == 3){
                        if(lstData[0]!=null && lstData[0]!='' && lstData[1]!=null && lstData[1]!='' && lstData[2]!=null && lstData[2]!=''){
                            VisitOrder__c objVO = new VisitOrder__c();
                            objVO.DocumentType__c = lstData[0];
                            objVO.NoOrderSAP__c = lstData[1];
                            objVO.Action__c = lstData[2];
                            objVO.VisitDetail__c = mapVD.get(strOrders).Id;
                            lstAllVO.add(objVO);
                        }
                    }
                }
            }

            if(!lstAllVO.isEmpty())
                insert lstAllVO;

            if(!setIdToDel.isEmpty())
                EventOperations_cls.deleteVisitDetail(setIdToDel);
        }
    }*/

    /**
    * Future method to delete the visit detail related with the Events
    * @params:
      1.-setIds: set with all visit detail Ids
    * @return void
    **/
    @future(callout=true)
    public static void deleteVisitDetail(set<String> setIds){
        delete([Select Id From VisitDetail__c Where Id = :setIds]);
    }

    /**
     * Validate Event tour status and substatus to prevent or allow event deletion
     * @param lstEvents List of events to validate
     */
    public static void validateTourStatusForDelete(Event[] lstEvents) {
        TourStatusSettings__mdt[] lstTourStatusSettings = DevUtils_cls.getTourStatusSettings();
        Map<String, RecordType> mapTourRecordTypes = DevUtils_cls.getRecordTypes('ONTAP__Tour__c', 'DeveloperName');

        // Get list of Tour Ids to validate
        Set<Id> setTourIds = new Set<Id>();
        for (Event evt: lstEvents) {
            setTourIds.add(evt.VisitList__c);
        }

        // Get Tour records
        Map<Id, ONTAP__Tour__c> mapTours = new Map<Id, ONTAP__Tour__c>([
            SELECT Id, ONTAP__TourStatus__c, TourSubStatus__c, RecordTypeId
            FROM ONTAP__Tour__c
            WHERE Id IN :setTourIds
        ]);

        if (mapTours.isEmpty()) {
            return;
        }

        // Validate Tour Status and Substatus for each Event record to prevent or allow deletion
        for (Event evt: lstEvents) {
            ONTAP__Tour__c objTour = mapTours.get(evt.VisitList__c);
            // Validate Tour
            if (objTour == null) {
                continue;
            }

            Boolean isDeletable = false;

            // Iterate Tour Status Settings items to validate
            for (TourStatusSettings__mdt tourStatus: lstTourStatusSettings) {
                if (!tourStatus.AllowsEventDeletion__c) {
                    continue;
                }

                // Get Tour record types to exclude from validation
                Set<Id> setRecordTypesToExclude = new Set<Id>();
                if (String.isNotBlank(tourStatus.RecordTypesToExclude__c)) {
                    String[] lstRecordTypeDevNames = tourStatus.RecordTypesToExclude__c.split(',');

                    for (String recTypeDevName : lstRecordTypeDevNames) {
                        RecordType recType = mapTourRecordTypes.get(recTypeDevName.trim());
                        if (recType != null) {
                            setRecordTypesToExclude.add(recType.Id);
                        }
                    }
                }

                // Compare tour status and sub-status and check if it permits Event deletion
                Boolean equalStatus = (tourStatus.Status__c == objTour.ONTAP__TourStatus__c)
                                   && (tourStatus.SubStatus__c == objTour.TourSubStatus__c);
                Boolean recordTypeIsExcluded = setRecordTypesToExclude.contains(objTour.RecordTypeId);

                if (equalStatus && !recordTypeIsExcluded) {
                    isDeletable = true;
                }
            }

            // Validate if Event is Deletable
            if (!isDeletable) {
                evt.addError(Label.EventDeleteError + '. TOUR STATUS: ' +
                    objTour.ONTAP__TourStatus__c + '. TOUR SUBSTATUS: ' + objTour.TourSubStatus__c);
            }
        }
    }
    
    /**
    * Method to sort the event list according to its sequence
    * @params:
      1.-lstEvent: list with the events to be sort
    * @return lstAux Auxiliar list sorted by Sequence
    **/
    public static list<Event> sortListBySequence(list<Event> lstEvent){
        Event objAux = new Event();
    
        Integer intMaxSecuence = 100000;
        for(Integer i=0; i<lstEvent.size(); i++){
            
            for(Integer j=i; j<lstEvent.size(); j++){
                
                if(lstEvent[j].Sequence__c < intMaxSecuence){
                    objAux = lstEvent[j];
                    lstEvent[j] = lstEvent[i];
                    lstEvent[i] = objAux;
                     
                    intMaxSecuence = Integer.valueOf(lstEvent[j].Sequence__c); 
                }
            }
            
            //System.debug('\nPos '+i+' ===> '+lstEvent[i]);
            intMaxSecuence = 100000;
        }
        
        for(Integer i=0; i<lstEvent.size(); i++)
            lstEvent[i].Sequence__c = i+1;
            
        return lstEvent;
    }
    
    /**
    * Method to validate the events that will be sorted
    * @params:
      1.-lstNewEvent: list with the new Events
      1.-lstOldEvent: list with the old Events
      1.-isDelete: indicates if a process is a delete process
    * @return void
    **/
    public static void updateSortListBySequence(list<Event> lstNewEvent, list<Event> lstOldEvent, Boolean isDelete, Boolean isInsert){
        set<String> setIdsVisitList = new set<String>();
        
        //Obtain the related visit plan and accounts when is delete or is update
        if(isDelete || !isInsert){
            for(Integer i=0; i<lstOldEvent.size(); i++){
                if(isDelete || (!isDelete && (lstOldEvent[i].Sequence__c != lstNewEvent[i].Sequence__c))){
                    if(lstOldEvent[i].VisitList__c != null){
                        setIdsVisitList.add(lstOldEvent[i].VisitList__c);
                    }
                }
            }
        }
        //Obtain the related visit plan and accounts when is insert
        if(isInsert){
            for(Integer i=0; i<lstNewEvent.size(); i++){
                if(lstNewEvent[i].VisitList__c != null){
                    setIdsVisitList.add(lstNewEvent[i].VisitList__c);
                }
            }
        }
        
        System.debug('\nsetIdsVisitList==>'+setIdsVisitList);
        //call a future method to update the sequence in event object
        if(!setIdsVisitList.isEmpty()){
            sortListBySequenceFuture(setIdsVisitList);
        }
    }
    
    /**
    * Future Method to sort and update the list of visits palan according to its sequence
    * @params:
      1.-setIdsAccounts: list with the account Ids
      1.-setIdsVisitPlan: list with the visit plan Ids
    * @return lstAux Auxiliar list sorted by Sequence
    **/
    
    @future(callout=true)
    public static void sortListBySequenceFuture(set<String> setIdsVisitList){
        list<Event> lstEvent = [
            Select  Id, Sequence__c, WhatId, VisitList__c, LastModifiedDate 
            From    Event
            Where   VisitList__c = :setIdsVisitList And OffRoute__c = false
            Order By Sequence__c asc, LastModifiedDate desc];
        
        map<String, list<Event>> mapTours = new map<String, list<Event>>();
        
        //Obtain a map with the tours and events related
        for(Event objEvent :lstEvent){
            list<Event> lstAuxEvent;
            if(mapTours.containsKey(objEvent.VisitList__c))
                lstAuxEvent = mapTours.get(objEvent.VisitList__c);
            else
                lstAuxEvent = new list<Event>();
                
            lstAuxEvent.add(objEvent);
            mapTours.put(objEvent.VisitList__c, lstAuxEvent);
        }
        
        //sort the sequence in all events
        list<Event> lstAllEvent = new list<Event>();
        for(String strVisitId :mapTours.keySet()){
            lstAllEvent.addAll(EventOperations_cls.sortListBySequence(mapTours.get(strVisitId)));
        }
        
        //update the events
        /** Wiit if(!lstAllEvent.isEmpty()){
            list<SyncObjects.ObjEventUpdate> lstObjExtEvent = new list<SyncObjects.ObjEventUpdate>();
            for(Event objEvent : lstAllEvent){
                SyncObjects.ObjEventUpdate objUpdte = new SyncObjects.ObjEventUpdate();
                objUpdte.salesforceid = objEvent.Id;
                objUpdte.sequence = Integer.valueOf(objEvent.Sequence__c);
                lstObjExtEvent.add(objUpdte);
            }
            if(!lstObjExtEvent.isEmpty()){       
                SyncObjects.UpdateEventObject objUpdate = new SyncObjects.UpdateEventObject();
                objUpdate.events = lstObjExtEvent;
                
                //Callout the heroku web service to update the events
                /*if(!Test.isRunningTest()) String res = SyncObjects.calloutUpdateHerokuEvents(objUpdate);
            }   
           /*wiit TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterUpdateSort');
            update lstAllEvent;
        }*/
    }
    
    /**
     * @description Method to send tours an events to Heroku
     * @param 
     * @return Boolean
     * @version 1.0
     **/
   /* @RemoteAction
    webservice static void sendEventsToHeroku(){  
        //Obtain the record types that will be sinchronize with heroku
        list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');
        
        map<Id, Event> mapEvents = new map<Id, Event>([
            Select  Id
            From    Event
            Where   VisitList__c != null And VisitList__r.ONTAP__IsActive__c = true And SynchronizedHeroku__c = false And
                    VisitList__r.RecordType.DeveloperName = :lstRecordType
        ]);
        
        EventOperations_cls.insertEventToExternalObject(mapEvents.keySet(), true); 
        
    }*/
    
    /**
     * @description Method to fill the Sequence field
     * @param lstNewEvent list of the new events
     * @return void
     * @version 1.0
     **/
    public static void fillSequence(list<Event> lstNewEvent){
        set<Id> setIds = new set<Id>();
        for(Event e :lstNewEvent){
            if(e.VisitList__c != null && e.Sequence__c == null)
                setIds.add(e.VisitList__c);
        }
        
        if(!setIds.isEmpty()){
            map<String, Integer> mapSize = new map<String, Integer>();
            
            list<ONTAP__Tour__c> lstT = [SELECT Id, (Select Id From Actividades__r) FROM ONTAP__Tour__c Where Id = :setIds];
            
            for(ONTAP__Tour__c t :lstT){
                Integer sizeA = t.Actividades__r != null ? t.Actividades__r.size() : 0;
                mapSize.put(t.Id, sizeA);
            }
            
            for(Event e :lstNewEvent){
                if(e.VisitList__c != null && e.Sequence__c == null && mapSize.containsKey(e.VisitList__c)){
                    e.Sequence__c = mapSize.get(e.VisitList__c) + 1;
                    mapSize.put(e.VisitList__c, Integer.ValueOf(e.Sequence__c));
                }
            }
        }
    }    
    
}