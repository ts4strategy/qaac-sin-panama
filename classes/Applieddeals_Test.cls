/* ----------------------------------------------------------------------------
 * AB InBev :: Oncall
 * ----------------------------------------------------------------------------
 * Clase: Applieddeals_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 10/01/2018        Luis Parra            Creation of methods.
 */
@isTest
public class Applieddeals_Test {
    
    /**
    * Test method for applied deals
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
    @isTest static void Applieddeals_Test(){
    Test.startTest();
		Applieddeals apex2json = new Applieddeals();
        apex2json.condcode = 'Test';
        apex2json.pronr = 'Test';
        apex2json.amount = 0.0;
        apex2json.percentage = 0.0;
    Test.stopTest();
    }
}