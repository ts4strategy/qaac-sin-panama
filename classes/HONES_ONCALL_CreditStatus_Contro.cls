/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_CreditStatus_Contro.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class HONES_ONCALL_CreditStatus_Contro {
    /**
* Method to get limit of credit By userId  
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return ISSM_CreditLimit__c
*/
    
    @AuraEnabled
    public static Double creditLimit(String recordId){
        Double limite = 0.0;
        
        List<Account> lim = new List<Account>();
        try{
            lim = [SELECT ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Id =: recordId];
            if(lim != null){
                for(Account i : lim){
                    limite = i.ISSM_CreditLimit__c;
                }
            }
            else
            {
                limite = 0.0; 
            }
            
        }catch (exception e) {
            system.debug(e);
        }
        if(limite == null){limite = 0.0;}
        System.debug('limite' + limite);
        return limite;  
        
        /**
* Method to get calls By userId  
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return List<Id>
*/
    }
    @AuraEnabled
    public static List<Id> getById(String recordId){
        
        List<Id> AccId = new List<Id>();
        try{
            ONCALL__Call__c acc=[SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId ];    
            AccId.add(acc.ONCALL__POC__c);
        }catch (exception e) {
            system.debug(e);
        }
        return AccId;
    }
    
    /**
* Method to get FlexibleData By userId  
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return List<Id>
*/
    @AuraEnabled
    public static List<Id> getFlexibleDataById(String recordId){
        
        List<Id> AccId = new List<Id>();
        try{
            ONCALL__Call__c acc=[SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE ONCALL__POC__c =:recordId LIMIT 1];    
            AccId.add(acc.ONCALL__POC__c);
        }catch (exception e) {
            system.debug(e);
        }
        return AccId;
    }
    /**
* Method to set product finished avalible   
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ISSM_TotalAmount__c)
*/
    @AuraEnabled
    public static Double disponible(String recordId){
        
        //get account country
        List<Account> lim = new List<Account>([SELECT ISSM_CreditLimit__c, ONTAP__ExternalKey__c FROM Account WHERE Id =:recordId]);
        String account_country = !lim.isEmpty() ? lim[0].ONTAP__ExternalKey__c.left(2) : '';        
        
        //get recordtypes of orders that affect credit
        List<String> orders_mdt_sum = new List<String>();
        List<String> orders_mdt_min = new List<String>();
        List<String> orders_rt_sum = new List<String>();
        List<String> orders_rt_min = new List<String>();
        
        List<Orders_Bh__mdt> orders_mtd = new List<Orders_Bh__mdt>();
        orders_mtd = [Select DeveloperName, Min__c, Sum__c, Country__c  
                      From Orders_Bh__mdt 
                      Where Country__c =: account_country                       		
                      Limit 1];
        orders_mdt_sum = orders_mtd[0].Sum__c.split(',');
        orders_mdt_min = orders_mtd[0].Min__c.split(',');
        
        for(String aux1 : orders_mdt_sum){
            orders_rt_sum.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux1).getRecordTypeId());
        }
        
        for(String aux2 : orders_mdt_min){
            orders_rt_min.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux2).getRecordTypeId());
        }
        System.debug('orders_rt_sum ' +orders_rt_sum);
        System.debug('orders_rt_min ' +orders_rt_min);
      
        Double creditLimit = 0.0;
        Double amount = 0.0;
        Double sumaAmount = 0.0;
        Double totalDisponible = 0.0;
        Double orderAmount= 0.0;
        Double orderAmount_sum= 0.0;
        Double orderAmount_min= 0.0;  
        //get account credit limit
        if(lim != null){
            for(Account i : lim){
                if(i.ISSM_CreditLimit__c != null) {
                    creditLimit = i.ISSM_CreditLimit__c;
                }
                else{
                    creditLimit = 0.0;
                }
            }
        }
        else
        {
            creditLimit = 0.0;  
        }
        
        //total amount of open items
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        /*
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) FROM ONTAP__OpenItem__c WHERE ONTAP__Account__c =:recordId]; 
        if(Amou != null){
            for(AggregateResult i : Amou){
                sumaAmount = (Decimal)i.get('expr0');
                if(sumaAmount == null){
                    sumaAmount=0.0;
                }            
            }  
        }
        else{
            sumaAmount=0.0;
        }*/
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        System.debug('recordId: ' + recordId);
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =: recordId AND
                              		RecordTypeId =: rt_open_items
                             ];
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    sumaAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    sumaAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        if(sumaAmount == null) sumaAmount = 0;
        
        Date hoy = System.today();
        System.debug('hoy: ' + hoy);    
		//total of todays orders, this will affect the sumaAmount        
        AggregateResult[] itemAmount_sum = [SELECT SUM(ONTAP__Amount_Total__c) 
                                         FROM ONTAP__Order__c 
                                         WHERE 
                                         	 (ONTAP__OrderAccount__c =:recordId 
                                              AND CreatedDate >=: hoy 
                                              AND ISSM_PaymentMethod__c = 'Credit'
                                              AND RecordTypeId in: orders_rt_sum) ];
        System.debug('total orders: ' + itemAmount_sum);
        if(itemAmount_sum != null){
            for(AggregateResult i : itemAmount_sum){
                orderAmount_sum = (Decimal)i.get('expr0');
                if(orderAmount_sum == null){ 
                    orderAmount_sum=0.0;
                }  
            }
        }
        else{
            orderAmount_sum=0.0;
        }
        
        AggregateResult[] itemAmount_min = [SELECT SUM(ONTAP__Amount_Total__c) 
                                         FROM ONTAP__Order__c 
                                         WHERE 
                                         	 (ONTAP__OrderAccount__c =:recordId AND 
                                             CreatedDate >=: hoy AND
                                             RecordTypeId in: orders_rt_min AND 
                                             ISSM_PaymentMethod__c = 'Credit') ];                  
        if(itemAmount_min != null){
            for(AggregateResult i : itemAmount_min){
                orderAmount_min = (Decimal)i.get('expr0');
                if(orderAmount_min == null){ 
                    orderAmount_min=0.0;
                }  
            }
        }
        else{
            orderAmount_min=0.0;
        }
        
        orderAmount = orderAmount_sum - orderAmount_min;
               
        if(creditLimit == null){creditLimit = 0.0;}
        if(sumaAmount == null){sumaAmount = 0.0;}
        if(totalDisponible == null){totalDisponible = 0.0;}
        if(orderAmount == null){orderAmount = 0.0;}
        
        totalDisponible = creditLimit - sumaAmount; 
        
        totalDisponible = totalDisponible - orderAmount;
        System.debug('totalDisponible' + totalDisponible);
        return totalDisponible;
        
    }
    /**
* Method to get outOfDateCredits 
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ONTAP__Amount__c)
*/
    
    
    @AuraEnabled
    public static Double outOfDateCredits(String recordId){
        /*
        Double totalAmount = 0.0;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE ONTAP__Account__c =:recordId AND 
                                  ONTAP__DueDate__c <: System.today()];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
            }
        }
        else
        {
            totalAmount = 0.0;
        }
        if(totalAmount == null){totalAmount = 0.0;}
        System.debug('totalAmount' + totalAmount);
        return totalAmount;*/
        Double totalAmount = 0.0;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		V360_ExpirationDate__c <: System.today()	AND
                              		RecordTypeId =: rt_open_items
                             ];
        
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        if(totalAmount == null) totalAmount = 0;
        
        return totalAmount;
        
    }
    
    /**
* Method to get outOfDateCreditsLiquid 
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ONTAP__Amount__c)
*/
    
    
    @AuraEnabled
    public static Double outOfDateCreditsLiquid(String recordId){
        /*       
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        
        String liquid = openItems_identifier[0].Liquid__c;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE (ONTAP__Account__c =:recordId AND 
                                         ONTAP__DueDate__c <: System.today()) AND 
                                  		 V360_Reference__c =: liquid];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else{
            totalAmount = 0.0;
        }
        if(totalAmount == null){totalAmount = 0.0;}
        System.debug('totalAmount' + totalAmount);
        return totalAmount;*/
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String liquid = openItems_identifier[0].Liquid__c;
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c <: System.today()	AND
                              		RecordTypeId =: rt_open_items AND 
                              		V360_Reference__c =: liquid
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        if(totalAmount == null) totalAmount = 0;
        
        return totalAmount;         
    }
    
    /**
* Method to get outOfDateCreditsContainer 
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ONTAP__Amount__c)
*/
    
    @AuraEnabled
    public static Double outOfDateCreditsContainer(String recordId){
        /*        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        
        String empties = openItems_identifier[0].Empties__c;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE (ONTAP__Account__c =:recordId AND 
                                         ONTAP__DueDate__c <: System.today()) AND 
                                  		 V360_Reference__c =: empties];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else
        {
            totalAmount = 0.0; 
        }
        if(totalAmount == null){totalAmount = 0.0;}
        System.debug('totalAmount' + totalAmount);
        return totalAmount;*/
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String empties = openItems_identifier[0].Empties__c;
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c <: System.today()	AND
                              		RecordTypeId =: rt_open_items AND 
                              		V360_Reference__c =: empties
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        if(totalAmount == null) totalAmount = 0;
        
        return totalAmount;        
    }
    
    
    /**
* Method to get dateCredits 
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ONTAP__Amount__c)
*/
    
    @AuraEnabled
    public static Double dateCredits(String recordId){
       /*         
        Double totalAmount = 0.0;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE ONTAP__Account__c =:recordId AND 
                                  ONTAP__DueDate__c >=: System.today()];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else
        {
            totalAmount = 0.0;
        }*/
         Double totalAmount = 0.0;
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
		
		List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >=: System.today() AND
                              		RecordTypeId =: rt_open_items
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        if(totalAmount == null){totalAmount = 0.0;}
        System.debug('totalAmount' + totalAmount);
        return totalAmount;
        
    }
    
    /**
* Method to get total 
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ONTAP__Amount__c)
*/
    
    @AuraEnabled
    public static Double total(String recordId){
        
        Double outAmount = 0.0;
        Double inAmount = 0.0;
        Double sumTotal = 0.0;
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
		
        /*
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE ONTAP__Account__c =:recordId AND 
                                  ONTAP__DueDate__c >=: System.today()];
        for(AggregateResult i : Amou){
            outAmount = (Decimal)i.get('expr0');
            
        }*/
        List<ONTAP__OpenItem__c> account_open_items_out = new List<ONTAP__OpenItem__c>();
        
        account_open_items_out = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c <: System.today() AND
                              		RecordTypeId =: rt_open_items
                             ];
         
        if(account_open_items_out.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items_out){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    outAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    outAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        if(outAmount == null) outAmount=0.0;
        /*
        AggregateResult[] AmountIn = [SELECT SUM(ONTAP__Amount__c) 
                                      FROM ONTAP__OpenItem__c 
                                      WHERE ONTAP__Account__c =:recordId AND 
                                      ONTAP__DueDate__c <: System.today()];
        
        for(AggregateResult i : AmountIn){
            inAmount = (Decimal)i.get('expr0');
            
        }
        if (outAmount== null) outAmount = 0;
        if (inAmount== null) inAmount = 0;*/
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >=: System.today() AND
                              		RecordTypeId =: rt_open_items
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    inAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    inAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        if(inAmount == null) inAmount=0.0;
        
        sumTotal = outAmount + inAmount;
        
        return sumTotal;    
        
    }    
    
    
    /**
* Method to get onTimeDateCreditsLiquid 
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ONTAP__Amount__c)
*/
    
    @AuraEnabled
    public static Double onTimeDateCreditsLiquid(String recordId){
        /*        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        
        String liquid = openItems_identifier[0].Liquid__c;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE (ONTAP__Account__c =:recordId 
                                         AND ONTAP__DueDate__c >: System.today()) AND 
                                  		 V360_Reference__c =: liquid];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else
        {
            totalAmount = 0.0;
        }
        if(totalAmount == null){totalAmount = 0.0;}
        System.debug('totalAmount' + totalAmount);
        return totalAmount;*/
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        System.debug('openItems_identifier ' + openItems_identifier);
        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String liquid = openItems_identifier[0].Liquid__c;
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >: System.today()	AND
                              		RecordTypeId =: rt_open_items AND 
                              		V360_Reference__c =: liquid
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        return totalAmount;
        
    }
    
    
    /**
* Method to get onTimeDateCreditsContainer 
* Contact suport By: g.martinez.cabral@accenture.com
* Created By: Luis Arturo Parra
* @param String recordId
* @return SUM(ONTAP__Amount__c)
*/
    
    @AuraEnabled
    public static Double onTimeDateCreditsContainer(String recordId){
        
        /*
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        
        String empties = openItems_identifier[0].Empties__c;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE (ONTAP__Account__c =:recordId AND 
                                         ONTAP__DueDate__c >: System.today()) AND 
                                  		 V360_Reference__c =: empties];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else{
            totalAmount = 0.0;                
        }
        if(totalAmount == null){totalAmount = 0.0;}
        System.debug('totalAmount' + totalAmount);
        return totalAmount;*/
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String empties = openItems_identifier[0].Empties__c;
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >: System.today()	AND
                              		RecordTypeId =: rt_open_items AND 
                              		V360_Reference__c =: empties
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        return totalAmount;
        
    }
    
}