/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_SyncProspect_Scheduler_Class.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 29/04/2019     Jose Luis Vargas          Creation of methods.
*/

global class CS_SyncProspect_Scheduler_Class implements schedulable 
{
    /**
    * method to execute the prospect sync
    * @author: jose.l.vargas.lara@accenture.com
    * @param sc to execute the process
    * @return Void
    */
    global void execute(SchedulableContext sc)
    {
        CS_SyncProspect_Class oSyncProspect = new CS_SyncProspect_Class();
        oSyncProspect.SyncProspect();
    }
}