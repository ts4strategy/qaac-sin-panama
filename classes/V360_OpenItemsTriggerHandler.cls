/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_OpenItemsTriggerHandler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public class V360_OpenItemsTriggerHandler extends TriggerHandlerCustom{
	
    private Map<Id, ONTAP__OpenItem__c> newMap;
    private Map<Id, ONTAP__OpenItem__c> oldMap;
    private List<ONTAP__OpenItem__c> newList;
    private List<ONTAP__OpenItem__c> oldList;
    
    /**
    * Constructor of the class
    * Created By: g.martinez.cabral@accenture.com
    * @param New values of Product modified
    * @return void
    */
    public V360_OpenItemsTriggerHandler() {
        this.newMap = (Map<Id, ONTAP__OpenItem__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__OpenItem__c>) Trigger.oldMap;
        this.newList = (List<ONTAP__OpenItem__c>) Trigger.new;
        this.oldList = (List<ONTAP__OpenItem__c>) Trigger.old;
    }
    
     /**
    * Method wich is executed every time is inserted
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public override void afterInsert(){
        // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
		// Using ONTAP__Codigo_del_cliente__c instead 
		// drs@avx        	
		/*
        List<ONTAP__OpenItem__c> validops = PreventExecutionUtil.validateOpenItemWithId(this.newMap);
        Set<Id> setIdsOpen = new Set<Id>();
        
        for(ONTAP__OpenItem__c open : validops){
            if(!setIdsOpen.contains(open.Id) && open.ONTAP__Account__c == null){setIdsOpen.add(open.Id);}
        }
        if(!setIdsOpen.isEmpty()){
        	relateAccountsToOpenItems(setIdsOpen);
        }
		*/
    }
    
     /**
    * Method wich is executed every time is updated
    * Created By: g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    public override void afterUpdate(){
        // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
		// Using ONTAP__Codigo_del_cliente__c instead 
		// drs@avx        	
        /*
        List<ONTAP__OpenItem__c> validops = PreventExecutionUtil.validateOpenItemWithId(this.newMap);
        Set<Id> setIdsOpen = new Set<Id>();
        
        for(ONTAP__OpenItem__c open : validops){
            if(!setIdsOpen.contains(open.Id) && open.ONTAP__Account__c == null){setIdsOpen.add(open.Id);}
        }
        if(!setIdsOpen.isEmpty()){
        	relateAccountsToOpenItems(setIdsOpen);
        }
        */
    }
    
     /**
    * Method for relate openitems to accounts
    * Created By: g.martinez.cabral@accenture.com
    * @param NList<ONTAP__OpenItem__c> newlist
    * @return void
    */
    
    /*
	 * No need for code if accounts are linked using an External Id-Unique
 	* To relate the account use instead ONTAP__Codigo_del_cliente__c
 	* ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
 	* drs@avx
 	*/

    /*
    @future
    public static void relateAccountsToOpenItems(Set<Id> newSetOpen){
        List<ONTAP__OpenItem__c> newlist = [SELECT Id, ONTAP__SAPCustomerId__c, RecordTypeId, ONTAP__Account__c FROM ONTAP__OpenItem__c WHERE Id IN: newSetOpen];
        Set<String> SAPNumsToRelate = new Set<String>();
        Map<String,Account> accReference = new Map<String,Account>();
        
        Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();

        
        for (ONTAP__OpenItem__c accOpenItem:newlist){
            if(accOpenItem.ONTAP__SAPCustomerId__c !=NULL&&accOpenItem.RecordTypeId==V360_OpenItemRT){
                SAPNumsToRelate.add(accOpenItem.ONTAP__SAPCustomerId__c );
            }
        }
        
        for (List<Account> accs : [SELECT Id, ONTAP__SAP_Number__c FROM Account WHERE ONTAP__SAP_Number__c IN:SAPNumsToRelate]){
            for (Account acc : accs){
                 accReference.put(acc.ONTAP__SAP_Number__c,acc);
            }            
        }
        
        System.debug('accReference'+accReference);
        
        List<ONTAP__OpenItem__c> listOpenItems = new List<ONTAP__OpenItem__c>();
        for (ONTAP__OpenItem__c finalItem:newlist){
           Account acc = accReference.get(finalItem.ONTAP__SAPCustomerId__c);
            if(acc!= NULL){           
                finalItem.ONTAP__Account__c = acc.Id;
                listOpenItems.add(finalItem);
			}
           System.debug('Asset: '+finalItem);
        }
        
        if(!listOpenItems.isEmpty()){
        	update listOpenItems;    
        }
    }
    */
}