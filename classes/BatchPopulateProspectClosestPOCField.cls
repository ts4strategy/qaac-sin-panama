global class BatchPopulateProspectClosestPOCField implements Database.Stateful, Database.AllowsCallouts, Schedulable, Database.Batchable<sObject>  {
    public BatchPopulateProspectClosestPOCField(){        
    }
 
    global map<id,account> mProspect = new map<id,account>();
    global map<id,list<account>> mAccounts = new map<id,list<account>>();
    public static id Schedule(){
        String cron_exp = '0 0 1 ? * MON,TUE,WED,THU,FRI,SAT,SUN *'; //Weekly at 1AM
        string strnjobame = 'Populate Prospect Closest POC Field';
        List<CronTrigger> surveyAutoStatusJobs = [SELECT CronJobDetail.Name, TimesTriggered, NextFireTime 
                                                  FROM CronTrigger 
                                                  WHERE CronJobDetail.Name =:strnjobame];
        if (surveyAutoStatusJobs.isEmpty()) {
            return System.schedule(strnjobame, cron_exp, new BatchPopulateProspectClosestPOCField());
        }else{
            return null;
        }
    }
    public void execute(SchedulableContext SC) {        
        database.executeBatch(new BatchPopulateProspectClosestPOCField());
    }
     
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id, Closest_POC_Code__c,ownerid,owner.name,
                                         ONTAP__Longitude__c, ONTAP__Latitude__c, Recordtype.DeveloperName 
                                         from Account                                          
                                         where (ONTAP__Longitude__c != null and ONTAP__Latitude__c != null)
                                         order by ownerid,Recordtypeid,ONTAP__Longitude__c, ONTAP__Latitude__c
                                        ]);
    }
    
    public void execute(Database.BatchableContext BC, List<Account> scope){
        
        for(Account a : scope){
           
            if(!mProspect.containskey(a.id) && a.Recordtype.DeveloperName =='Prospect_DO'){
                mProspect.put(a.id,a);
            }
            if(!mAccounts.containskey(a.ownerid) && a.Recordtype.DeveloperName !='Prospect_DO'){
                mAccounts.put(a.ownerid,new list<account>{a});
            }else{
                if(mAccounts.containskey(a.ownerid) && a.Recordtype.DeveloperName !='Prospect_DO'){
                    mAccounts.get(a.ownerid).add(a);
                }
            }      
        }
    }
    
    public void finish(Database.BatchableContext BC){       
        //https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_class_system_Location.htm
        for(account prospect : mProspect.values()){
            //if(mAccounts.containsKey(prospect.ownerid)){
            Location locProspect = Location.newInstance(decimal.valueof(prospect.ONTAP__Latitude__c) ,decimal.valueof(prospect.ONTAP__Longitude__c));
            Double dist_temp = null;
            for(id acid : mAccounts.keyset()    ){
                for(account accClosest : mAccounts.get(acid)){
                    Location locClosest = Location.newInstance(decimal.valueof(accClosest.ONTAP__Latitude__c) ,decimal.valueof(accClosest.ONTAP__Longitude__c));
                    Double dist = Location.getDistance(locProspect, locClosest, 'mi');
                    if(dist_temp == null){
                        dist_temp=dist;
                    }
                    if(dist <= dist_temp){
                        prospect.Closest_POC_Code__c = accClosest.id; 
                        prospect.ontap__geolocation__Latitude__s = locClosest.getLatitude();
                        prospect.ontap__geolocation__Longitude__s = locClosest.getLongitude();
                        dist_temp = dist;
                    }
                    
                }
            }   
        }
        
        update mProspect.values();
    }
    
    public static id getRecordtype(string devname){
        Schema.DescribeSObjectResult resSchema = account.sObjectType.getDescribe();        
        Map<String,Schema.RecordTypeInfo>  recordTypeInfo = resSchema.getRecordTypeInfosByDeveloperName(); 
        
        if(recordTypeInfo.containskey(devname) ){
             return recordTypeInfo.get(devname).getRecordTypeId();
        }else{
            return null;
        }
        

    }

}