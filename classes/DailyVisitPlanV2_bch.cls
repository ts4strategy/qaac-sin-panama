/****************************************************************************************************
   General Information
   -------------------
   author: Andrés Garrido
   email: agarrido@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Batch for generate visit plans

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       25-10-2017        Andres Garrido (AG)          Creation Class
   1.2       04-06-2018        Rodrigo Resendiz             Dynamic Visit Period
****************************************************************************************************/
global with sharing class DailyVisitPlanV2_bch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

    global string  queryVisitPlanByAcc = '';

    // Visit Plan Settings
    private static VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
    private final Integer VISIT_PERIOD;//1.2       = Integer.valueOf(SyncHerokuParams__c.getAll().get('SyncToursEvents').VisitPeriodConfig__c);
    private final Integer VISIT_PERIOD_CONF;//1.2 = Test.isRunningTest() ? Integer.valueOf(visitPlanSettings.VisitPeriodConfig__c) : VISIT_PERIOD;

    private DateTime dtToday;//1.2         =   DateTime.now().addDays(VISIT_PERIOD_CONF);
    private String strDtvisit;//1.2        =   dtToday.format('yyyy-MM-dd');
    private Datetime dtIni;//1.2           =   DateTime.newInstance(Date.today().addDays(VISIT_PERIOD_CONF), Time.newInstance(0, 0, 0, 0));
    private Datetime dtFin;//1.2           =   DateTime.newInstance(Date.today().addDays(VISIT_PERIOD_CONF+1), Time.newInstance(0, 0, 0, 0));
    private String dayOfWeek;//1.2         =   dtIni.format('EEEE');
    private string strFielddayOfWeek;//1.2 = ''+ dayOfWeek + '__c';
    public Date dtNextVisit;//1.2 = Date.Today().addDays(VISIT_PERIOD_CONF);

    public list<AccountByVisitPlan__c> lstAccountByVisitPlan   = new list<AccountByVisitPlan__c>();
    public set<Id> setEventToDel;

    global map<string, string> mapstrModelWhoPrev;
    global String strServiceModel;
    global map<Id, Date> mapIdPlVsDate;
    global Integer intStep;

    // Record types for ONTAP__Tour__c, Event and ONCALL__Call__c
    global Map<String, RecordType> mapTourRecordTypes = DevUtils_cls.getRecordTypes('ONTAP__Tour__c', 'DeveloperName');
    global Map<String, RecordType> mapEventRecordTypes = DevUtils_cls.getRecordTypes('Event', 'DeveloperName');
    global Map<String, RecordType> mapCallRecordTypes = DevUtils_cls.getRecordTypes('ONCALL__Call__c', 'DeveloperName');

    /**
        * @description Batch builder arms dynamic query to query visits and accounts configured for a given day
    **/
    global DailyVisitPlanV2_bch(String strServiceModel, Integer intStep){
        VISIT_PERIOD       = 2;//Integer.valueOf(SyncHerokuParams__c.getAll().get('SyncToursEvents').VisitPeriodConfig__c);//1.2
        VISIT_PERIOD_CONF = Test.isRunningTest() ? Integer.valueOf(visitPlanSettings.VisitPeriodConfig__c) : VISIT_PERIOD;//1.2
        dtToday         =   DateTime.now().addDays(VISIT_PERIOD_CONF);//1.2
        strDtvisit      =   dtToday.format('yyyy-MM-dd');//1.2
        dtIni           =   DateTime.newInstance(Date.today().addDays(VISIT_PERIOD_CONF), Time.newInstance(0, 0, 0, 0));//1.2
    	dtFin           =   DateTime.newInstance(Date.today().addDays(VISIT_PERIOD_CONF+1), Time.newInstance(0, 0, 0, 0));//1.2
        dayOfWeek         =   dtIni.format('EEEE');//1.2
        strFielddayOfWeek = ''+ dayOfWeek + '__c';//1.2
        dtNextVisit = Date.Today().addDays(VISIT_PERIOD_CONF);//1.2

        String fieldsQuery      =   'Id, Route__r.RouteManager__c, Route__r.OwnerId, Route__r.SalesOrg__c, Route__r.Id, ';
        fieldsQuery             +=  'Route__r.ONTAP__SalesOffice__r.StartTime__c, Route__r.ONTAP__SalesOffice__r.EndTime__c, VisitPlanType__c, Route__c, ';
        fieldsQuery             +=  'Route__r.ServiceModel__c';

        queryVisitPlanByAcc     +=  'SELECT '+fieldsQuery+ ' FROM VisitPlan__c ';
        queryVisitPlanByAcc     +=      ' WHERE ' + strFielddayOfWeek + ' = true';
        queryVisitPlanByAcc     +=      ' AND ExecutionDate__c <= ' + strDtvisit;
        queryVisitPlanByAcc     +=      ' AND EffectiveDate__c >= ' + strDtvisit;
        queryVisitPlanByAcc     +=      ' AND Route__r.ServiceModel__c = \'' + strServiceModel + '\'';

        setEventToDel = new set<Id>();
        this.strServiceModel = strServiceModel;
        this.intStep = intStep;

        System.debug('strServiceModel: ' + strServiceModel);
        System.debug('intStep: ' + intStep);
        System.debug('VISIT_PERIOD: ' + VISIT_PERIOD);
        System.debug('VISIT_PERIOD_CONF: ' + VISIT_PERIOD_CONF);
        System.debug('dtToday: ' + dtToday);
        System.debug('strDtvisit: ' + strDtvisit);
        System.debug('dtIni: ' + dtIni);
        System.debug('dtFin: ' + dtFin);
        System.debug('dayOfWeek: ' + dayOfWeek);
        System.debug('strFielddayOfWeek: ' + strFielddayOfWeek);
        System.debug('dtNextVisit: ' + dtNextVisit);
    }

    global DailyVisitPlanV2_bch(String strServiceModel, Integer intStep, Integer VisitPeriodConfig){//1.2
        VISIT_PERIOD       = VisitPeriodConfig;//1.2
        VISIT_PERIOD_CONF = Test.isRunningTest() ? Integer.valueOf(visitPlanSettings.VisitPeriodConfig__c) : VISIT_PERIOD;//1.2
        dtToday         =   DateTime.now().addDays(VISIT_PERIOD_CONF);//1.2
        strDtvisit        =   dtToday.format('yyyy-MM-dd');//1.2
        dtIni           =   DateTime.newInstance(Date.today().addDays(VISIT_PERIOD_CONF), Time.newInstance(0, 0, 0, 0));//1.2
    	dtFin           =   DateTime.newInstance(Date.today().addDays(VISIT_PERIOD_CONF+1), Time.newInstance(0, 0, 0, 0));//1.2
        dayOfWeek         =   dtIni.format('EEEE');//1.2
        strFielddayOfWeek = ''+ dayOfWeek + '__c';//1.2
        dtNextVisit = Date.Today().addDays(VISIT_PERIOD_CONF);//1.2

        String fieldsQuery      =   'Id, Route__r.RouteManager__c, Route__r.OwnerId, Route__r.SalesOrg__c, Route__r.Id, ';
        fieldsQuery             +=  'Route__r.ONTAP__SalesOffice__r.StartTime__c, Route__r.ONTAP__SalesOffice__r.EndTime__c, VisitPlanType__c, Route__c, ';
        fieldsQuery             +=  'Route__r.ServiceModel__c';

        queryVisitPlanByAcc     +=  'SELECT '+fieldsQuery+ ' FROM VisitPlan__c ';
        queryVisitPlanByAcc     +=      ' WHERE ' + strFielddayOfWeek + ' = true';
        queryVisitPlanByAcc     +=      ' AND ExecutionDate__c <= ' + strDtvisit;
        queryVisitPlanByAcc     +=      ' AND EffectiveDate__c >= ' + strDtvisit;
        queryVisitPlanByAcc     +=      ' AND Route__r.ServiceModel__c = \'' + strServiceModel + '\'';
        queryVisitPlanByAcc     +=      ' AND Route__r.ISSM_SkipVisitListCreation__c = false';

        setEventToDel = new set<Id>();
        this.strServiceModel = strServiceModel;
        this.intStep = intStep;

        System.debug('strServiceModel: ' + strServiceModel);
        System.debug('intStep: ' + intStep);
        System.debug('VISIT_PERIOD: ' + VISIT_PERIOD);
        System.debug('VISIT_PERIOD_CONF: ' + VISIT_PERIOD_CONF);
        System.debug('dtToday: ' + dtToday);
        System.debug('strDtvisit: ' + strDtvisit);
        System.debug('dtIni: ' + dtIni);
        System.debug('dtFin: ' + dtFin);
        System.debug('dayOfWeek: ' + dayOfWeek);
        System.debug('strFielddayOfWeek: ' + strFielddayOfWeek);
        System.debug('dtNextVisit: ' + dtNextVisit);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('\n*************** Start Daily Visit Plan '+this.strServiceModel+' ********************************');
        list<ParametersVisitControl__mdt> lstParamVsControl = [
            SELECT  CanCoexist__c, DeveloperName, Id, Label,
                    Language, MasterLabel, NamespacePrefix,
                    QualifiedApiName, SameFrequency__c, SameWeeklyPeriod__c,
                    ServiceModel2__c, ServiceModel__c, WhoPrevails__c
            FROM    ParametersVisitControl__mdt
            WHERE   CanCoexist__c = true AND SameFrequency__c = true
        ];
        mapstrModelWhoPrev  = new map<string, string>();
        for(ParametersVisitControl__mdt objParamVCtrl : lstParamVsControl)
            mapstrModelWhoPrev.put(objParamVCtrl.ServiceModel__c +','+objParamVCtrl.ServiceModel2__c, objParamVCtrl.WhoPrevails__c);

        System.debug('queryVisitPlanByAcc===>>>'+queryVisitPlanByAcc);
        return Database.getQueryLocator(queryVisitPlanByAcc);
    }

    /**
        * @description Get all service models from the list of visits found
        * @author Nelson Sáenz Leal
    **/
    global void execute(Database.BatchableContext BC, list<VisitPlan__c> lstVisitPlan){
        System.debug('\n*************** Excecute Daily Visit Plan '+this.strServiceModel+' ********************************');

        if(lstVisitPlan != null && !lstVisitPlan.isEmpty()){
            System.debug('\n****lstVisitPlan.size=> '+lstVisitPlan.size());
            String ownerField = visitPlanSettings.OwnerField__r.DeveloperName;
            ownerField += (ownerField == 'Owner') ? 'Id' : '__c';

            set<Id> setIdsAcc = new set<Id>();          //set to fill the Ids of the accounts
            set<Id> setIdsRoute = new set<Id>();        //set to fill the Ids of the Routes
            set<String> setIdsOrg = new set<String>();  //set to fill the Sales Org Ids

            set<Id> setIdsValidsVP = new set<Id>();

            map<Id, Event> mapEventsXAcc = new map<Id, Event>();
            map<Id, OnCall__Call__c> mapCallsXAcc = new map<Id, OnCall__Call__c>();
            map<Id, ONTAP__Tour__c> mapTourXRoute = new map<Id, ONTAP__Tour__c>();
            map<Id, ONTAP__Route__c> mapRoutes = new map<Id, ONTAP__Route__c>();
            map<String, BusinessHours> mapBH = new map<String, BusinessHours>();

            map<Id, list<AccountByVisitPlan__c>> mapAccByVisitPlan = new map<Id, list<AccountByVisitPlan__c>>();

            lstAccountByVisitPlan   = new list<AccountByVisitPlan__c>();

            list<AccountByVisitPlan__c> lstVisitPlanByAcc = getAccountByVisitPlan(lstVisitPlan);
            map<Id, Id> mapExistAcc = new map<Id, Id>();


            if(lstVisitPlanByAcc != null && !lstVisitPlanByAcc.isEmpty()){
                for(AccountByVisitPlan__c objAccByVP : lstVisitPlanByAcc){

                    //Validate if the weekly period is matching with the process date
                    if(isValidToGenerateVisit(objAccByVP)){
                        setIdsAcc.add(objAccByVP.Account__c);
                        setIdsRoute.add(objAccByVP.VisitPlan__r.Route__c);
                        setIdsOrg.add(objAccByVP.VisitPlan__r.Route__r.SalesOrg__c);

                        list<AccountByVisitPlan__c> lstAccByVPAux;

                        if(mapAccByVisitPlan.containsKey(objAccByVP.VisitPlan__c))
                            lstAccByVPAux = mapAccByVisitPlan.get(objAccByVP.VisitPlan__c);
                        else
                            lstAccByVPAux = new list<AccountByVisitPlan__c>();

                        lstAccByVPAux.add(objAccByVP);

                        mapAccByVisitPlan.put(objAccByVP.VisitPlan__c, lstAccByVPAux);
                    }
                }

                //Obtain a map with the events and calls related with accounts
                if(!setIdsAcc.isEmpty()){
                    mapEventsXAcc = getEventsXAccount(setIdsAcc);
                    mapCallsXAcc = getCallsXAccount(setIdsAcc);
                }
                //Obtain the tours related with the routes
                if(!setIdsRoute.isEmpty()){
                    mapTourXRoute = getToursByRouteDate(setIdsRoute);
                    mapRoutes = getToursByRoute(setIdsRoute);
                }
                //Obtain the bussiness hours related with the sales orgs
                if(!setIdsOrg.isEmpty()){
                    mapBH = getMapBussinessHour(setIdsOrg);
                    //Validate Business Hours
                    setIdsValidsVP = validateBusinessHours(setIdsOrg, lstVisitPlan);
                }

                System.debug('\n***mapEventsXAcc= '+mapEventsXAcc);
                System.debug('\n***mapCallsXAcc= '+mapCallsXAcc);
                System.debug('\n***mapTourXRoute= '+mapTourXRoute);
                System.debug('\n***mapRoutes= '+mapRoutes);
                System.debug('\n***mapBH= '+mapBH);
            }


            map<Id, ONTAP__Tour__c> mapNewTours = new map<Id, ONTAP__Tour__c>();
            map<Id, list<sObject>> mapEventCallXRoute = new map<Id, list<sObject>>();

            list<ONCALL__Call__c> lstCallToDel = new list<ONCALL__Call__c>();
            System.debug('\n****setIdsValidsVP = '+setIdsValidsVP);

            //Process de visit plans
            for(VisitPlan__c objVisitPlan :lstVisitPlan){

                //System.debug('\n****objVisitPlan.Id = '+objVisitPlan);
                //System.debug('\n****mapAccByVisitPlan.containsKey(objVisitPlan.Id) = '+mapAccByVisitPlan.containsKey(objVisitPlan.Id));


                list<sObject> lstEventOnCall = new list<sObject>();
                Time dTimeOrgSales = Time.newInstance(8,0,0,0);

                if(objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c != null && objVisitPlan.Route__r.ONTAP__SalesOffice__r.EndTime__c != null)
                    dTimeOrgSales = Time.newInstance(Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringBefore(':')), Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.EndTime__c.substringAfter(':')), 0, 0);

                if(setIdsValidsVP.contains(objVisitPlan.Id) && mapAccByVisitPlan.containsKey(objVisitPlan.Id)){

                    //Created a new Record in Tour Object, if does't exist
                    if(!mapTourXRoute.containsKey(objVisitPlan.Route__c)){
                        //System.debug('\n****strServiceModel==> '+strServiceModel);
                        //System.debug('\n****mapTourRecordTypes==> '+mapTourRecordTypes);
                        Id idRecordType = mapTourRecordTypes.get(strServiceModel).Id;
                        ONTAP__Tour__c objTour = new ONTAP__Tour__c(
                            ONTAP__TourId__c         = visitPlanSettings.NameTourId__c,
                            RecordTypeId             = idRecordType,
                            ONTAP__TourDate__c       = System.today().addDays(VISIT_PERIOD_CONF),
                            AlternateName__c         = ' ',
                            VisitPlan__c             = objVisitPlan.Id,
                            ONTAP__TourStatus__c     = mapRoutes.get(objVisitPlan.Route__c).Tours__r.isEmpty() ? visitPlanSettings.AssignedTourStatus__c : visitPlanSettings.CreatedTourStatus__c,
                            TourSubStatus__c         = mapRoutes.get(objVisitPlan.Route__c).Tours__r.isEmpty() ? visitPlanSettings.AssignedTourSubStatus__c : visitPlanSettings.CreatedTourSubStatus__c,
                            ONTAP__IsActive__c       = mapRoutes.get(objVisitPlan.Route__c).Tours__r.isEmpty() ? true : false,
                            OwnerId                  = (Id) objVisitPlan.Route__r.get(ownerField),
                            Route__c                 = objVisitPlan.Route__c,
                            EstimatedDeliveryDate__c = mapIdPlVsDate.get(objVisitPlan.Id)
                        );
                        mapNewTours.put(objVisitPlan.Route__c, objTour);
                    }


                    //Vars to controlate the sequence by plan
		            String strPlanId = '';
		            Integer intSequence = 0;

                    //For each account by visit plan related with the visit plan
                    for(AccountByVisitPlan__c objAccByVP : mapAccByVisitPlan.get(objVisitPlan.Id)){
                        Boolean blnCreate = false;
                        Boolean blnCreateEvent = true;
                        Boolean blnCreateCall = true;

                        String strKey = '', strKey1 = '';
                        String strWho = '';

                        Event objTmpEvent;
                        ONCALL__Call__c objTmpCall;

                        /****Validate if exist an other service model to compare******/
                        //if exist an event for the same client
                        if(mapCallsXAcc.containsKey(objAccByVP.Account__c)){
                            objTmpCall = mapCallsXAcc.get(objAccByVP.Account__c);
                            strKey = objTmpCall.CallList__r.Route__r.ServiceModel__c+','+objAccByVP.VisitPlan__r.Route__r.ServiceModel__c;
                            strKey1 = objAccByVP.VisitPlan__r.Route__r.ServiceModel__c+','+objTmpCall.CallList__r.Route__r.ServiceModel__c;
                            lstAccountByVisitPlan.add(new AccountByVisitPlan__c(Id = objAccByVP.Id, LastVisitDate__c = System.today().addDays(VISIT_PERIOD_CONF)));
                            mapExistAcc.put(objAccByVP.Id, objAccByVP.Id);
                        }
                        //if exist a call for the same client
                        else if(mapEventsXAcc.containsKey(objAccByVP.Account__c)){
                            objTmpEvent = mapEventsXAcc.get(objAccByVP.Account__c);
                            strKey = objTmpEvent.VisitList__r.Route__r.ServiceModel__c+','+objAccByVP.VisitPlan__r.Route__r.ServiceModel__c;
                            strKey1 = objAccByVP.VisitPlan__r.Route__r.ServiceModel__c+','+objTmpEvent.VisitList__r.Route__r.ServiceModel__c;
                            lstAccountByVisitPlan.add(new AccountByVisitPlan__c(Id = objAccByVP.Id, LastVisitDate__c = System.today().addDays(VISIT_PERIOD_CONF)));
                            mapExistAcc.put(objAccByVP.Id, objAccByVP.Id);
                        }
                        else
                            blnCreate = true;

                        if(mapCallsXAcc.containsKey(objAccByVP.Account__c) && mapCallsXAcc.get(objAccByVP.Account__c).CallList__r.Route__r.ServiceModel__c == objAccByVP.VisitPlan__r.Route__r.ServiceModel__c)
                            blnCreateCall = false;
                       	if(mapEventsXAcc.containsKey(objAccByVP.Account__c) && mapEventsXAcc.get(objAccByVP.Account__c).VisitList__r.Route__r.ServiceModel__c == objAccByVP.VisitPlan__r.Route__r.ServiceModel__c)
                            blnCreateEvent = false;

                        //Validate which service model prevalece
                        if(mapstrModelWhoPrev.containsKey(strKey))
                            strWho = mapstrModelWhoPrev.get(strKey);
                        else if(mapstrModelWhoPrev.containsKey(strKey1))
                            strWho = mapstrModelWhoPrev.get(strKey1);

                        //System.debug('\n****strWho = '+strWho);

                        //if the actual service model prevalece, create a event or call, and delete de other records
                        if(strWho == objVisitPlan.Route__r.ServiceModel__c){
                            blnCreate = true;
                            if(objTmpEvent != null)
                                setEventToDel.add(objTmpEvent.Id);
                            if(objTmpCall != null)
                                lstCallToDel.add(objTmpCall);
                        }

                        //if no prevalece any service model, create the visit or call
                        if(strWho == 'N/A')
                            blnCreate = true;
						
                        String strAuxPlan2 = objAccByVP.VisitPlan__c;
                        //System.debug('\n######blnCreate = '+blnCreate);
                        if(strPlanId != strAuxPlan2){
                            strPlanId = strAuxPlan2;
                            intSequence = 1;
                        }
                        //if the service model is a Telesales, create a Call
                        if(blnCreate && blnCreateCall && objVisitPlan.Route__r.ServiceModel__c  == visitPlanSettings.RTTelesales__c){
                            lstEventOnCall.add(new ONCALL__Call__c(
                                ONCALL__POC__c         = objAccByVP.Account__c,
                                ONCALL__Date__c        = DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringBefore(':')), Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringAfter(':')), 0),
                                ONCALL__Call_Status__c = visitPlanSettings.InitialCallStatus__c,
                                OwnerId                = (Id) objVisitPlan.Route__r.get('RouteManager__c'),
                                Name                   = visitPlanSettings.NameOncall__c + ': ' + objAccByVP.Account__r.ONTAP__SAP_Number__c,
                                ONCALL__Call_Time__c   = string.valueOf(dTimeOrgSales).substringBeforeLast(':'),
                                RecordTypeId           = mapCallRecordTypes.get('ISSM_TelesalesCall').Id,
                                ISSM_ISAPDate__c	   = String.valueOf(objAccByVP.Account__c) + String.valueOf(dtNextVisit),
                                HONES_CallSequence__c  = String.valueOf(intSequence)

                            ));
                            dTimeOrgSales = dTimeOrgSales.addMinutes(15);
                            intSequence++;
                            //System.debug('dTimeOrgSales===>>'+dTimeOrgSales);
                            //System.debug('*__* =====> '+string.valueOf(dTimeOrgSales).substringBeforeLast(':'));
                        }
                        //else create a Event
                        else{
                            if(blnCreate && blnCreateEvent){
                            	String strAuxPlan = objAccByVP.VisitPlan__c;
                            	//Validate the sequence for the visit plan
                            	if(strPlanId != strAuxPlan){
                            		strPlanId = strAuxPlan;
                            		intSequence = 1;
                            	}
                                lstEventOnCall.add(new Event(
                                    WhatId                     = objAccByVP.Account__c,
                                    Subject                    = visitPlanSettings.VisitSubject__c,
                                    StartDateTime              = DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringBefore(':')), Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.StartTime__c.substringAfter(':')), 0),
                                    EndDateTime                = DateTime.newInstance(dtNextVisit.year(), dtNextVisit.month(), dtNextVisit.day(), Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.EndTime__c.substringBefore(':')), Integer.valueOf(objVisitPlan.Route__r.ONTAP__SalesOffice__r.EndTime__c.substringAfter(':')), 0),
                                    ONTAP__Estado_de_visita__c = visitPlanSettings.InitialVisitStatus__c,
                                    EventSubestatus__c         = visitPlanSettings.InitialVisitSubStatus__c,
                                    Sequence__c                = intSequence,
                                    CustomerId__c              = objAccByVP.Account__r.ONTAP__SAP_Number__c,
                                    OwnerId                    = (Id) objVisitPlan.Route__r.get('RouteManager__c'),
                                    RecordTypeId               = mapEventRecordTypes.get('Sales').Id,
                                    KeyTourCustomer__c         = String.valueOf(objAccByVP.Account__c) + String.valueOf(dtNextVisit)
                                    
                                ));
																// System.debug(mapEventRecordTypes.get('Sales').Id);
                                intSequence++;
                            }
                        }
                        //Update the last visit date in account by visit plan record
                        if(blnCreate && !mapExistAcc.containsKey(objAccByVP.Id)){
                            lstAccountByVisitPlan.add(new AccountByVisitPlan__c(Id = objAccByVP.Id, LastVisitDate__c = System.today().addDays(VISIT_PERIOD_CONF)));
                            mapExistAcc.put(objAccByVP.Id, objAccByVP.Id);
                        }

                    }
                    //add the event or call list to the map
                    mapEventCallXRoute.put(objVisitPlan.Route__c,lstEventOnCall);
                }
            }

            //set flag to not execute future methods
            TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterInsertSync');
            TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterUpdateSync');


            //insert the new tours
            if(!mapNewTours.isEmpty())
                insert mapNewTours.values();

						  System.debug('\n#####mapNewTours.values() = '+mapNewTours.values().size()+' mapNewTours'+mapNewTours);
            list<SObject> lstSobjEventOncall = new list<SObject>();

            //Associate the events or calls with the tour
            for(String strKey : mapEventCallXRoute.keySet()){
                ONTAP__Tour__c objAuxTour = new ONTAP__Tour__c();
                if(mapTourXRoute.containsKey(strKey))
                    objAuxTour = mapTourXRoute.get(strKey);

                if(mapNewTours.containsKey(strKey))
                    objAuxTour = mapNewTours.get(strKey);

                for(sObject  objSobject : mapEventCallXRoute.get(strKey)){
                    if(objSobject instanceof Event){
                        objSobject.put(Schema.Event.VisitList__c, objAuxTour.Id);
                    }
                    if(objSobject instanceof ONCALL__Call__c){
                        objSobject.put(Schema.ONCALL__Call__c.CallList__c, objAuxTour.Id);
                        objSobject.put(Schema.ONCALL__Call__c.ONCALL__Delivery_Date__c, mapIdPlVsDate.get(objAuxTour.VisitPlan__c));
                    }
										System.System.debug('\n\n *** objSobject: ' + objSobject+'\n\n');
                    lstSobjEventOncall.add(objSobject);
                }

            }

            //Set flags to not call a future methods
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterUpdateSort');
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSync');
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterInsertSort');
            System.debug('\n#####lstSobjEventOncall = '+lstSobjEventOncall.size());

            // Insert Events or On calls
            if(lstSobjEventOncall != null &&  !lstSobjEventOncall.isEmpty())
                Database.insert(lstSobjEventOncall,false);

            System.debug('\n#####lstAccountByVisitPlan = '+lstAccountByVisitPlan.size());

            // Update lastVisitDate to accounts valids
            if(lstAccountByVisitPlan != null &&  !lstAccountByVisitPlan.isEmpty()){
                Database.update(lstAccountByVisitPlan, false);
            }

            //delete existed calls
            if(lstCallToDel != null && !lstCallToDel.isEmpty())
                delete lstCallToDel;
        }
    }

    /**
        * @description Call method to validate service models, generate visits and schedule the batch for the next day at the same time
    **/
    global void finish(Database.BatchableContext BC){
        System.debug('\n*************** Finish Daily Visit Plan '+this.strServiceModel+' ********************************');

        System.debug('\n####setEventToDel ==> '+setEventToDel);
        if(!setEventToDel.isEmpty()){
                /*EventOperations_cls.deleteEventToExternalObject(setEventToDel);*/
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterDeleteSync');	
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterUpdateSort');
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterDeleteEventsSync');
            delete([Select Id From Event Where Id = :setEventToDel]);
        }
       programBatch();
    }


    public list<AccountByVisitPlan__c> getAccountByVisitPlan(list<VisitPlan__c> lstVisitPlan){
        // Ejecutar consulta de Cuentas por Plan de Visita
        String strAccsByPlanQuery = 'SELECT Id, Account__c, EffectiveDate__c, Account__r.ONTAP__SAPCustomerId__c, ExecutionDate__c, ';
        strAccsByPlanQuery += 'Friday__c, Monday__c, PlanType__c, Saturday__c, Sequence__c, Sunday__c, Thursday__c, Wednesday__c, Tuesday__c,';
        strAccsByPlanQuery += 'VisitDays__c, VisitPlan__c, Account__r.ONTAP__SAP_Number__c, WeeklyPeriod__c, LastVisitDate__c, ';
        strAccsByPlanQuery += 'VisitPlan__r.Route__r.RouteManager__c, VisitPlan__r.Route__r.OwnerId, VisitPlan__r.Route__c, ';
        strAccsByPlanQuery += 'VisitPlan__r.Route__r.SalesOrg__c, VisitPlan__r.Route__r.Id, ';
        strAccsByPlanQuery += 'VisitPlan__r.Route__r.ONTAP__SalesOffice__r.StartTime__c, VisitPlan__r.Route__r.ONTAP__SalesOffice__r.EndTime__c, ';
        strAccsByPlanQuery += 'VisitPlan__r.VisitPlanType__c, VisitPlan__r.Route__r.ServiceModel__c ';
        strAccsByPlanQuery += 'FROM AccountByVisitPlan__c';
        strAccsByPlanQuery +=  ' WHERE VisitPlan__c IN :lstVisitPlan';
        strAccsByPlanQuery +=  ' AND ' + strFielddayOfWeek + ' = true';
        strAccsByPlanQuery +=  ' AND Account__r.CustomerFlagLegal__c = false ';   // Cliente en Jurídico
        strAccsByPlanQuery +=  ' AND Account__r.OrderBlockFlag__c = false ';      // Bloqueo Pedido
        strAccsByPlanQuery +=  ' AND Account__r.DeletionRequestFlag__c = false '; // Petición de Borrado
        strAccsByPlanQuery +=  ' AND ExecutionDate__c <= ' + strDtvisit;
        strAccsByPlanQuery +=  ' AND EffectiveDate__c >= ' + strDtvisit;
        strAccsByPlanQuery +=  ' ORDER BY VisitPlan__c, Sequence__c asc ';

        System.debug('\nstrAccsByPlanQuery => '+strAccsByPlanQuery);

        AccountByVisitPlan__c[] lstAccsByVisitPlan = Database.query(strAccsByPlanQuery);
        System.debug('\nstrAccsByPlanQuery => '+'['+lstAccsByVisitPlan.size()+']' +lstAccsByVisitPlan);

        return lstAccsByVisitPlan;
    }


    public Boolean isValidToGenerateVisit(AccountByVisitPlan__c objAccByVP){
        System.debug('\n####objAccByVP.Id '+objAccByVP.Id);
        System.debug('\n####objAccByVP.LastVisitDate__c '+objAccByVP.LastVisitDate__c);
        Decimal visitPeriod;
        if(objAccByVP.LastVisitDate__c != null)
            visitPeriod = getVisitPeriod(objAccByVP.LastVisitDate__c);

        System.debug('\n####visitPeriod '+visitPeriod);
        if(objAccByVP.LastVisitDate__c == null || visitPeriod == Integer.valueOf(objAccByVP.WeeklyPeriod__c) || visitPeriod == 0){
            System.debug('\nIs Valid visitPeriod ');

            return true;
        }
        return false;
    }


    /**
        * @description Get all events related with the accounts in the same date that the process excecute
    **/
    public map<Id, Event> getEventsXAccount(set<Id> setAcc){

        map<Id, Event> mapEventsXAcc = new map<Id, Event>();
        Date theDate = dtIni.date();
        //System.debug('\n===>theDate: '+ theDate);
        list<Event> lstEvents = [
            Select  Id, VisitList__c, WhatId, VisitList__r.ONTAP__TourDate__c, VisitList__r.Route__r.ServiceModel__c
            From    Event
            Where   VisitList__c != null And WhatId = :setAcc And
                    VisitList__r.ONTAP__TourDate__c = :theDate
        ];
        for(Event objEvent : lstEvents)
            mapEventsXAcc.put(objEvent.WhatId, objEvent);

        return mapEventsXAcc;
    }

    /**
        * @description Get all calls related with the accounts in the same date that the process excecute
    **/
    public map<Id, ONCALL__Call__c> getCallsXAccount(set<Id> setAcc){

        map<Id, ONCALL__Call__c> mapCallsXAcc = new map<Id, ONCALL__Call__c>();
        list<ONCALL__Call__c> lstCalls = [
            Select  Id, CallList__c, ONCALL__POC__c, ONCALL__Date__c, CallList__r.Route__r.ServiceModel__c
            From    ONCALL__Call__c
            Where   ONCALL__POC__c = :setAcc And
                    ONCALL__Date__c >= :dtIni And ONCALL__Date__c <= :dtFin
        ];

        for(ONCALL__Call__c objCall : lstCalls)
            mapCallsXAcc.put(objCall.ONCALL__POC__c, objCall);

        return mapCallsXAcc;
    }

    /**
        * @description Get all tours related with the routes in the same date that the process excecute
    **/
    public map<Id, ONTAP__Tour__c> getToursByRouteDate(set<Id> setIdsRoute){
        map<Id, ONTAP__Tour__c> mapTourByRoute = new map<Id, ONTAP__Tour__c>();
        Date theDate = dtIni.date();
        //System.debug('\n===>theDate: '+ theDate);
        list<ONTAP__Tour__c> lstTours = [
            Select  Id, Route__c, VisitPlan__c, ONTAP__TourDate__c
            From    ONTAP__Tour__c
            Where   Route__c != null And Route__c = :setIdsRoute And
                    ONTAP__TourDate__c = :theDate
        ];
        
        /**START DDED TO FIX RETRACE ROUTE**/
        List<VisitControl_RetraceAndAnticipateRoute__c> routesToAnticipate = [SELECT Id, VisitControl_Route__c, VisitControl_RetraceOrAnticipate__c, VisitControl_DateToSkip__c FROM VisitControl_RetraceAndAnticipateRoute__c  WHERE 
                                                                 VisitControl_Route__c IN: setIdsRoute AND 
                                                                 VisitControl_RetraceOrAnticipate__c =: LABEL.VisitControl_Retrace]; 
        /**END ADDED TO FIX RETRACE ROUTE**/
        
        for(ONTAP__Tour__c objTour : lstTours){
             /**START ADDED TO FIX RETRACE ROUTE**/
            Boolean skip = false;
            for(VisitControl_RetraceAndAnticipateRoute__c rt : routesToAnticipate){
                
                if(rt.VisitControl_Route__c==objTour.Route__c){
                    skip = true;
                }

            }
             /**END ADDED TO FIX RETRACE ROUTE**/   
            
            if (!skip){ mapTourByRoute.put(objTour.Route__c, objTour);}
            
        }
            

        return mapTourByRoute;
    }

    /**
        * @description Get all tours related with the routes in the same date that the process excecute
    **/
    public map<Id, ONTAP__Route__c> getToursByRoute(set<Id> setIdsRoute){
        map<Id, ONTAP__Route__c> mapTourByRoute = new map<Id, ONTAP__Route__c>([
            Select  Id, (Select Id From Tours__r Where ONTAP__IsActive__c = true)
            From    ONTAP__Route__c
            Where   Id = :setIdsRoute
        ]);

        return mapTourByRoute;
    }

    /**
        * @description Get a Bussiness hours related with the sales orgs
    **/
    public map<String, BusinessHours> getMapBussinessHour(set<String> setIdsOrg){
        map<String, BusinessHours> mapBH = new map<String, BusinessHours>();

        list<BusinessHours> lstBuHours = [
            Select  Id, Name
            From    BusinessHours
            Where   Name =: setIdsOrg And isActive = true
        ];

        for(BusinessHours objBU : lstBuHours)
            mapBH.put(objBU.Name, objBU);

        return mapBH;
    }

    /**
        * @description Method in charge of validating the visit period configured for the accounts
        * @author Nelson Sáenz Leal
    **/
    public Decimal getVisitPeriod(Date lastVisitDate)
    {
        Date dtStartWeek        = lastVisitDate.toStartOfWeek();
        Decimal intVisitPeriod  = Math.floor(Decimal.valueOf(dtStartWeek.daysBetween(System.today().addDays(VISIT_PERIOD_CONF)) / 7));

        System.debug('dtStartWeek=====>>'+dtStartWeek);
        System.debug('intVisitPeriod==>>>'+intVisitPeriod);
        return intVisitPeriod;
    }

    /**
    * Method in charge of validating if there is a valid business hours for the date of the visit
    * @params: - setIdAccValids Accounts Valids
    * @return void
    **/
    public set<id> validateBusinessHours(set<String> idsOrg, list<VisitPlan__c> lstVisitPlan){
        set<Id> setIdAccValids    = new set<id>();
        list<BusinessHours> lstBuHours = [SELECT Id, Name FROM BusinessHours
                                        WHERE Name =: idsOrg
                                        AND isActive = true];
        System.debug('lstBuHours====>>>>>'+lstBuHours);
        mapIdPlVsDate = new map<Id, Date>();
        Date dtVisit = System.today().addDays(VISIT_PERIOD_CONF);
        Time tmHours = Time.newInstance(17, 0, 0, 0);
        DateTime dtmVisit = DateTime.valueOfGMT(String.valueOf(DateTime.newInstance(dtVisit, tmHours)));
        DateTime dtmEstimatedDate = DateTime.newInstance(System.today().addDays(VISIT_PERIOD_CONF + 1), tmHours);

        for(BusinessHours objBh : lstBuHours){
            for(VisitPlan__c objVsplan : lstVisitPlan){
                if(objBh.Name == objVsplan.Route__r.SalesOrg__c){

                    /*System.debug('objBh====>>>>'+objBh);
                    System.debug('BusinessHours.isWithin(objBh.id, dtmVisit)===>>'+BusinessHours.isWithin(objBh.id, dtmVisit));
                    System.debug('dtmVisit===>>'+dtmVisit);
                    System.debug('dtEstimatedDate===>>'+dtmEstimatedDate);*/
                    if(BusinessHours.isWithin(objBh.id, dtmVisit)){
                        setIdAccValids.add(objVsplan.Id);
                        mapIdPlVsDate.put(objVsplan.Id, Date.valueOf(BusinessHours.nextStartDate(objBh.Id, dtmEstimatedDate)));
                    }
                }
            }
        }
        return setIdAccValids;
    }

    /**
        * @description Method to automatically program the batch
        * @author Nelson Sáenz Leal
    **/
    public void programBatch(){
        Datetime dtHoraActual       =   System.now();
        Datetime dtNextExecution    =   dtHoraActual.addSeconds(10);

        this.intStep += 1;

        String strTime  =   dtNextExecution.second() +' ';
        strTime         +=  dtNextExecution.minute() +' ';
        strTime         +=  dtNextExecution.hour() +' ';
        strTime         +=  dtNextExecution.day() +' ';
        strTime         +=  dtNextExecution.month()+' ';
        strTime         +=  '? ';
        strTime         +=  dtNextExecution.year()+' ';

        String strDailyVisitplan    =   'DailyVisitPlanV2_sch-'+this.intStep;

        try{

            for ( CronTrigger ct : [SELECT  Id
                FROM    CronTrigger
                WHERE   CronJobDetail.Name =: strDailyVisitplan] ) {
                System.abortJob(ct.Id);
            }
            if(!Test.isRunningTest()) String jobId = System.schedule(strDailyVisitplan, strTime, new DailyVisitPlanV2_sch(this.intStep, VISIT_PERIOD));

        }catch(Exception e){
            System.debug('\n ERROR SYNC TOURS EVENTS ===========>>>> '+e.getMessage());
        }
    }
}