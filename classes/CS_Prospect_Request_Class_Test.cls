/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_Prospect_Request_Class_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 24/04/2019      Jose Luis Vargas        Creation of the class with test CS_Prospect_Request_Class class
 *                                             
*/

@isTest
public class CS_Prospect_Request_Class_Test 
{
    /**
    * Method for test the class CS_Prospect_Request_Class
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest
    public static void test_Prospect_Request()
    {
        CS_Prospect_Request_Class oProspect = new CS_Prospect_Request_Class();
        
        oProspect.Header_Id = '1';
        oProspect.Country = '1';
        oProspect.Employee_Id_Create = '1';
        oProspect.Employee_Name_Create = '1';
        oProspect.Employee_Email_Create = '1';
        oProspect.Survey_Date = '1';
        oProspect.Survey_Type = '1';
        oProspect.Survey_Type_Id = '1';
        oProspect.Latitude = '1';
        oProspect.Longitude = '1';
        oProspect.Customer_referee = '1';
        oProspect.Status = '1';
        oProspect.AC_CambioRazonSocial = '1';
        oProspect.AC_TipoIdentificacion = '1';
        oProspect.AC_TipoPersona = '1';
        oProspect.AC_NumeroIdentificacion = '1';
        oProspect.AC_NumeroIdentificacion2 = '1';
        oProspect.AC_NumeroIdentificacion3 = '1';
        oProspect.AC_NombreComercial = '1';
        oProspect.AC_NombreCliente_PrimerNombre = '1';
        oProspect.AC_NombreCliente_SegundoNombre = '1';
        oProspect.AC_NombreCliente_ApPaterno = '1';
        oProspect.AC_NombreCliente_ApMaterno = '1';
        oProspect.AC_TipoVia = '1';
        oProspect.AC_NombreVia = '1';
        oProspect.AC_Nro = '1';
        oProspect.AC_Poblacion = '1';
        oProspect.AC_Distrito = '1';
        oProspect.AC_TipoPoblacion = '1';
        oProspect.AC_TelefonoMovil = '1';
        oProspect.AC_TelefonoFijo = '1';
        oProspect.AC_FechaNacimiento = '1';
        oProspect.AC_External_Id = '1';
        oProspect.AC_Clase_Cliente = '1';
        oProspect.AC_Canal_Subcanal_Local = '1';
    }
}