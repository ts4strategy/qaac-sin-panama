/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_PartnerList.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class HONES_ONCALL_PartnerList {
    
    /**
    * Methods getters and setters for entity HONES_ONCALL_PartnerList
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    public String role{get; set;}
    public String numb{get; set;}
    public String itmNumber{get; set;}
}