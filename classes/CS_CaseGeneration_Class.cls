/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASEGENERATION_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 07/11/ 2018     Jose Luis Vargas         Creacion de la clase para la generacion de casos
 * 29/11/2018      Jose Luis Vargas         Se agrega metodo para la generacion de un nuevo caso.
 */

public class CS_CaseGeneration_Class 
{
    public CS_CaseGeneration_Class() {}
    
    @TestVisible public CS_CASEASSIGNMENT_CLASS oAssignmentInterlocutor = new CS_CASEASSIGNMENT_CLASS();
    @TestVisible public CS_CASE_USER_ESCALATION_CLASS oAssignmentUser = new CS_CASE_USER_ESCALATION_CLASS();
    
    @TestVisible public string IdOwnerStd {get; set;}
    @TestVisible public string IdOwnerForce {get; set;}
    @TestVisible public boolean isQueueAssigned {get; set;}
    @TestVisible public string filteredQuery {get; set;}
    @TestVisible public boolean isInterlocutor {get; set;}
    @TestVisible public boolean isUser {get; set;}
    @TestVisible public Integer INDEX0 = 0;
    @TestVisible public string userCountry {get; set;}
    
    /**
    * Method for generate a new standard case
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id of account, typification level 1, typification level 2, typification level 3, typification level 4, detalle del caso to generate
    * @return String with the id of the new case
    */   
    public String GenerateNewCase(string IdAccount, string typificationN1, string typificationN2, string typificationN3, string typificationN4, string CaseDetail)
    {
        ISSM_TypificationMatrix__c oDetailTypification = new ISSM_TypificationMatrix__c();
        string idNewCase = '' ;
        string idRecordType = '';
        string idContact = '';
        DateTime fechaCierre = Date.today();
        string hnCountry = Label.CS_Country_Honduras;
        string svCountry = Label.CS_Country_El_Salvador;
        string paCountry = Label.CS_Country_Panama;
        
        string subjectCase = '';
        
        CS_CaseForce_RT__c mc = CS_CaseForce_RT__c.getOrgDefaults();        
        String recType = ''; 
        
        try
        {
             /* Se realiza la consulta del detalle de la tipificacion seleccionada en pantalla */
             userCountry = GetUserCountry(); 
             oDetailTypification = GetTypificationDetail(typificationN1, typificationN2, typificationN3, typificationN4, userCountry);

             /* Se realiza la consulta de la cuenta  a la que se generara el caso */
             Account oDetailAccount = GetAccountDetail(IdAccount);
            
             /* Se obtiene el Id de Contacto Principal */
             idContact = GetContactAccount(IdAccount);
        
             /* Se obtiene el id de Record Type */
             string recordTypeName = oDetailTypification.ISSM_CaseRecordType__c;
             idRecordType = GetRecordTypeId(recordTypeName);

             /* Se obtiene el owner del caso */
             GetOwnerCase(oDetailTypification, oDetailAccount);
           
            /* Se genera la fecha esperada de cierre */
            integer daysToAdd = (Integer)oDetailTypification.CS_Days_to_End__c;
            fechaCierre = fechaCierre.addDays(daysToAdd);
            
            /*Obtiene record type de casos force*/
            if(recordTypeName.equals(mc.CS_Cooler_Installation__c) || recordTypeName.equals(mc.CS_Cooler_Repair__c) || recordTypeName.equals(mc.CS_Cooler_Retirement__c))
            {
                if(userCountry.equals(hnCountry))
                    recType = GetRecordTypeId(mc.Ontap_HN_Cooler__c);
                else if(userCountry.equals(svCountry))
                    recType = GetRecordTypeId(mc.Ontap_SV_Cooler__c);
                else if(userCountry.equals(paCountry))
                    recType = GetRecordTypeId(mc.Ontap_PA_Cooler__c);
            }
            else
                recType = GetRecordTypeId(mc.CS_CF_General_Case__c);
            
            subjectCase = 'Caso: ' + typificationN1 + ' | ' + typificationN2 + ' | ' + typificationN3 + ' | ' + typificationN4;
            subjectCase = subjectCase.remove('null');
            subjectCase = subjectCase.removeEnd(' | ');
            
            /* Se genara la instancia del Objeto Case para llenar las propiedades y poder insertar el caso nuevo */
            Case oNewCase = new Case
            (
                ISSM_TypificationLevel1__c = typificationN1,
                ISSM_TypificationLevel2__c = typificationN2,
                ISSM_TypificationLevel3__c = typificationN3,
                ISSM_TypificationLevel4__c = typificationN4,
                ISSM_UpdateAccountTeam__c = true,
                RecordTypeID = idRecordType,
                ISSM_TypificationNumber__c = oDetailTypification.Id,
                EntitlementId = oDetailTypification.ISSM_Entitlement__c,
                Priority = oDetailTypification.ISSM_Priority__c,
                SuppliedEmail = oDetailAccount.ONTAP__Email__c,
                AccountID = IdAccount,
                Description = CaseDetail,
                OwnerId = IdOwnerStd,
                ISSM_OwnerCaseForce__c = IdOwnerForce,
                ISSM_FirstUserOwner__c = IdOwnerForce,
                ISSM_LocalStreet__c = oDetailAccount.ONTAP__Street__c,
                ISSM_LocalColony__c = oDetailAccount.ONTAP__Neighborhood__c,
                ISSM_LocalMunicpality__c = oDetailAccount.ONTAP__Municipality__c,
                ISSM_TypeSponsorship__c = oDetailTypification.ISSM_AssignedTo__c,
                Subject = subjectCase,
                HONES_Case_Country__c = oDetailTypification.ISSM_Countries_ABInBev__c,
                ISSM_CaseClosedSendLetter__c = oDetailTypification.ISSM_CaseClosedSendLetter__c,
                CS_Automatic_Closing_Case__c = oDetailTypification.CS_Automatic_Closing_Case__c,
                Expected_Completion_Date__c = fechaCierre,
                ContactId = idContact,
                Aplica_encuesta__c = oDetailTypification.CS_Apply_Survey__c,
                IdRecordTypeCaseForce__c = recType
            );
            
            /* Se inserta el caso */
            system.debug('oNewCase: ' + oNewCase);
            Insert oNewCase;
            idNewCase = oNewCase.Id;
                      
            /* Se genera la tarea solo para Honduras y el caso no debe estar asignado a la cola */
            if(!isQueueAssigned && userCountry == System.Label.CS_Country_Honduras)
                GenerateTask(idNewCase, oDetailTypification);
            
            /* Asignacion de escalamiento a los usuarios */
            if(isUser)
                oAssignmentUser.AssignEscalationUser(idNewCase, IdOwnerStd);
            /* Asignacion de escalamientos para interlocutores */
            if(isInterlocutor)
            {
                if(filteredQuery == ISSM_Constants_cls.TELVENTA)
                    oAssignmentInterlocutor.AssignEscalationTelventa(idNewCase, filteredQuery);
                else if(filteredQuery == ISSM_Constants_cls.PREVENTA)
                    oAssignmentInterlocutor.AssignEscalationPreVenta(idNewCase, filteredQuery);
                else if(filteredQuery == ISSM_Constants_cls.CREDITO)
                    oAssignmentInterlocutor.AssignEscalationCredit(idNewCase, filteredQuery);
                else if(filteredQuery == ISSM_Constants_cls.EQUIPO_FRIO)
                    oAssignmentInterlocutor.AssignEscalationCoolers(idNewCase, filteredQuery, IdOwnerStd);
                else if (filteredQuery == ISSM_Constants_cls.TELECOBRANZA)
                    oAssignmentInterlocutor.AssignEscalationTellecolection(idNewCase, filteredQuery);    
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEGENERATION_CLASS.GenerateNewCase Message: ' + ex.getMessage());   
            System.debug('CS_CASEGENERATION_CLASS.GenerateNewCase Cause: ' + ex.getCause());   
            System.debug('CS_CASEGENERATION_CLASS.GenerateNewCase Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEGENERATION_CLASS.GenerateNewCase Stack trace: ' + ex.getStackTraceString());
            idNewCase = null;
        }
        
        return idNewCase;
    }
    
    /**
    * Method for generate a new task
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case, Typification selected
    * @return Void
    */
    @TestVisible private void GenerateTask(string idCase, ISSM_TypificationMatrix__c oDetailTypification)
    {
        Task oNewTask = new Task();
        Date oToday = Date.today();
        string questionsFirtsContact = '';
        
        DataBase.DMLOptions oSendTaskNotification = new DataBase.DMLOptions();
        oSendTaskNotification.EmailHeader.triggerUserEmail = true;

        try
        {
           oToday = oToday.addDays((integer)oDetailTypification.CS_Dias_Primer_Contacto__c);
            
           oNewTask.Subject = System.Label.CS_Subject_Task;
           oNewTask.OwnerId = IdOwnerStd;
           oNewTask.ActivityDate = oToday;
           oNewTask.WhatId = idCase;
           oNewTask.Status = System.Label.CS_Status_Task;

           questionsFirtsContact = System.Label.CS_Mensaje_Primer_Contacto + '\n' + GetQuestionsFirstContact();
           oNewTask.Description = questionsFirtsContact;
            
           DataBase.Insert(oNewTask, oSendTaskNotification);
        }
        catch(Exception ex)
        {
            System.debug('HONES_CASEGENERATION_CS_CLASS.GenerateTask TASK Message: ' + ex.getMessage());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.GenerateTask TASK Cause: ' + ex.getCause());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.GenerateTask TASK Line number: ' + ex.getLineNumber());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.GenerateTask TASK Stack trace: ' + ex.getStackTraceString());
        }
    }
    
    /**
    * Method for get a detail of typification
    * @author: jose.l.vargas.lara@accenture.com
    * @param Typification Level 1, typification level 2, typification level 3, typification level 4, User country
    * @return Object with detail of typification
    */
    public ISSM_TypificationMatrix__c GetTypificationDetail(string typificationN1, string typificationN2, string typificationN3, string typificationN4, string userCountry)
    {
       ISSM_TypificationMatrix__c oDetailTypification = new ISSM_TypificationMatrix__c();
       
        oDetailTypification =  [SELECT Id, ISSM_CaseRecordType__c, ISSM_Entitlement__c, ISSM_AssignedTo__c, ISSM_OwnerQueue__c, ISSM_StatusAssignQueue__c, ISSM_OwnerUser__c, ISSM_IdQueue__c, ISSM_Email1CommunicationLevel3__c,
                                   ISSM_Email1CommunicationLevel4__c, ISSM_Email2CommunicationLevel3__c, ISSM_Email2CommunicationLevel4__c, ISSM_Email3CommunicationLevel3__c, ISSM_Email3CommunicationLevel4__c, 
                                   ISSM_Email4CommunicationLevel4__c, ISSM_Priority__c, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, 
                                   ISSM_Countries_ABInBev__c, CS_Dias_Primer_Contacto__c, ISSM_CaseClosedSendLetter__c, CS_Automatic_Closing_Case__c, CS_Days_to_End__c, CS_Apply_Survey__c, 
                                   Survey__c //New field for survey
                            FROM ISSM_TypificationMatrix__c 
                            WHERE ISSM_TypificationLevel1__c =: typificationN1 
                            AND ISSM_TypificationLevel2__c =: typificationN2 
                            AND ISSM_TypificationLevel3__c =: typificationN3 
                            AND ISSM_TypificationLevel4__c =: typificationN4
                            AND ISSM_Countries_ABInBev__c =: userCountry LIMIT 1];
        this.userCountry = userCountry;
       return oDetailTypification;
    }
    
    /**
    * Method for assign the owner case
    * @author: jose.l.vargas.lara@accenture.com
    * @param typifiction detail for the case, Accpun detail
    * @return Void
    */
    public void GetOwnerCase(ISSM_TypificationMatrix__c oTypificationCase, Account oDetailAccount)
    {
        string idQueueWithOutOwner = '';
        isQueueAssigned = true;
        User oSubstituteUser = new User();
                
        /* Se obtiene el Id de la cola para casos sin Owner */
        idQueueWithOutOwner = GetIdQueueWithOutOwner(oTypificationCase.ISSM_OwnerQueue__c);
        System.debug(idQueueWithOutOwner);
        /* Asignacion de caso a un usuario */
        if(oTypificationCase.ISSM_AssignedTo__c == ISSM_Constants_cls.USER)
        {
            isInterlocutor = false;
            /* Se valida que la tipificacion tenga un usuario, si no cuenta con usuario, se asigna a la cola */
            if(oTypificationCase.ISSM_OwnerUser__c != null && ((string)oTypificationCase.ISSM_OwnerUser__c) != '')
            {
                 /* Se Valida si el usuario se encuentra de vacaciones */
                 oSubstituteUser = GetUserVacation(oTypificationCase.ISSM_OwnerUser__c);
                 if(oSubstituteUser.CS_On_Vacation__c)
                 {
                     /* Si el usuario esta de vacaciones se asigna el id del usuario substituto */
                     IdOwnerStd = oSubstituteUser.CS_User_Replacement__c;
                     IdOwnerForce = oSubstituteUser.CS_User_Replacement__c;
                 }
                 else
                 {
                     IdOwnerStd = oTypificationCase.ISSM_OwnerUser__c;
                     IdOwnerForce =  oTypificationCase.ISSM_OwnerUser__c;
                 }
                 
                 isInterlocutor = false;
                 isQueueAssigned = false;
                 isUser = true;
            }
        }
        else if(oTypificationCase.ISSM_AssignedTo__c == ISSM_Constants_cls.QUEUE) /* Asignacion a la cola WithOutOwner */
        {
            IdOwnerStd = idQueueWithOutOwner;
            IdOwnerForce = oDetailAccount.OwnerId;
            isInterlocutor = false;
            isUser = false;
        }
        else  /* Asignacion a los interlocutores */
        {         
              GetInterlocutorType(oTypificationCase.ISSM_AssignedTo__c);  
              switch on filteredQuery
              {
                  when 'Telventa' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorTelventa(oTypificationCase.ISSM_AssignedTo__c, oDetailAccount.Id, filteredQuery); }  
                  when 'Preventa' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorPreVenta(oTypificationCase.ISSM_AssignedTo__c, oDetailAccount.Id, filteredQuery); }
                  when 'Credito' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorCredit(oTypificationCase.ISSM_AssignedTo__c, oDetailAccount.Id, filteredQuery); }
                  when 'Cooler' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorCoolers(oTypificationCase.ISSM_AssignedTo__c, oDetailAccount.Id, filteredQuery); }
                  when 'Telecobranza' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorTellecolection(oTypificationCase.ISSM_AssignedTo__c, oDetailAccount.Id, filteredQuery); }
              }
                    
             /* Si no se asigna un interlocutor se manda el caso a la cola */
             if(IdOwnerStd == null || IdOwnerStd == '')
             {
                 IdOwnerStd = idQueueWithOutOwner;
                 IdOwnerForce = oDetailAccount.OwnerId;
                 isInterlocutor = false;
                 isUser = false;
             }
             else
             {
                oSubstituteUser = GetUserVacation(IdOwnerStd);
                if(oSubstituteUser.CS_On_Vacation__c)
                {
                    IdOwnerStd = oSubstituteUser.CS_User_Replacement__c;
                    IdOwnerForce = oSubstituteUser.CS_User_Replacement__c;
                }
                else
                    IdOwnerForce = IdOwnerStd;
                isUser = false;
                isQueueAssigned = false;
                isInterlocutor = true;
             }         
         }
     }
        
    /**
    * Method for get the id of queue
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Queue Owner
    * @return String with tHe id of queue
    */
    private string GetIdQueueWithOutOwner(string idQueueOwner)
    {
        ISSM_AppSetting_cs__c oQueueWithOutOwner = new ISSM_AppSetting_cs__c();
        string idQueue = '';
        
        if(userCountry == System.Label.CS_Country_Honduras)
        {
             oQueueWithOutOwner = [SELECT ISSM_IdQueueWithoutOwner__c FROM ISSM_AppSetting_cs__c WHERE ISSM_IdQueueWithoutOwner__c != '' LIMIT 1];
             idQueue = oQueueWithOutOwner.ISSM_IdQueueWithoutOwner__c;
            
        }
        else if(userCountry == System.Label.CS_Country_El_Salvador && idQueueOwner == ISSM_Constants_cls.QUEUE_NAME_AGENTE_SAC)
        {
             oQueueWithOutOwner = [SELECT Id_Queue_SACAgent_SV__c FROM ISSM_AppSetting_cs__c WHERE Id_Queue_SACAgent_SV__c != '' LIMIT 1];
             idQueue = oQueueWithOutOwner.Id_Queue_SACAgent_SV__c;
            
        }
        else if(userCountry == System.Label.CS_Country_El_Salvador)
        {
             oQueueWithOutOwner = [SELECT Id_Queue_SV__c FROM ISSM_AppSetting_cs__c WHERE Id_Queue_SV__c != '' LIMIT 1];
             idQueue = oQueueWithOutOwner.Id_Queue_SV__c;
            
        }
        else if(userCountry == System.Label.CS_Country_Panama)
        {
             oQueueWithOutOwner = [SELECT Id_QueueWithoutOwner_PA__c FROM ISSM_AppSetting_cs__c WHERE Id_QueueWithoutOwner_PA__c != '' LIMIT 1];
             idQueue = oQueueWithOutOwner.Id_QueueWithoutOwner_PA__c;
            
        }
        
        return idQueue;       
    }
    
    /**
    * Method for get account detail
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id account
    * @return Account with the detail
    */
    @TestVisible private Account GetAccountDetail(string idAccount)
    {
        Account oDetailAccount = new Account();
        
        oDetailAccount = [
                             SELECT ONTAP__Email__c, OwnerId, ONTAP__Street__c, ONTAP__Neighborhood__c, ONTAP__Municipality__c
                             FROM Account
                             WHERE Id =: idAccount 
                             LIMIT 1
                         ];
        
        return oDetailAccount;
    }
        
    /**
    * Method for get the id of record type
    * @author: jose.l.vargas.lara@accenture.com
    * @param Record Type Name
    * @return string with the id of record type 
    */
    public string GetRecordTypeId(string recordTypeName)
    {
        string idRecordType = '';
        RecordType oDetailRT = new RecordType();
        oDetailRT = [SELECT Id FROM RecordType WHERE DeveloperName =: recordTypeName LIMIT 1];
        idRecordType = oDetailRT.Id;
        
        return idRecordType;
    }
    
    /**
    * Method for get the first contact question
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return string with the question 
    */
    @TestVisible private string GetQuestionsFirstContact()
    {
        string questionFirstContact = '';
        List<CS_Preguntas_Primer_Contacto__mdt> lstQuestions = new List<CS_Preguntas_Primer_Contacto__mdt>();
        lstQuestions = [SELECT Pregunta__c FROM CS_Preguntas_Primer_Contacto__mdt];
       
        for(CS_Preguntas_Primer_Contacto__mdt oQuestion : lstQuestions)
        {
            questionFirstContact = questionFirstContact + oQuestion.Pregunta__c + '\n';
        }
              
        return questionFirstContact;
    }
    
    /**
    * Method for get the user country
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return string with te user country 
    */
    private static String GetUserCountry()
    {
        User oUserInfo = new User();
        oUserInfo = [SELECT Country__c FROM USER WHERE Id =: UserInfo.getUserId()];
        
        return oUserInfo.Country__c;
    }
    
    /**
    * Method for get the type of interlocutor
    * @author: jose.l.vargas.lara@accenture.com
    * @param Role to assign the case
    * @return Void
    */
    @TestVisible private void GetInterlocutorType(string assignedTo)
    {
        List<CS_Interlocutores__mdt> lstInterlocutores = new List<CS_Interlocutores__mdt>();
        
        try
        {
            lstInterlocutores = [  
                                    SELECT CS_Tipo_Interlocutor__c 
                                    FROM CS_Interlocutores__mdt 
                                    WHERE CS_Interlocutor_ENG__c =: assignedTo 
                                ];
            
            if(lstInterlocutores.size() == 0)
            {
                lstInterlocutores = [  
                                        SELECT CS_Tipo_Interlocutor__c 
                                        FROM CS_Interlocutores__mdt 
                                        WHERE CS_Interlocutor_SPN__c =: assignedTo
                                    ];

            }
                    
            filteredQuery = lstInterlocutores[INDEX0].CS_Tipo_Interlocutor__c;
        }
        catch(Exception ex)
        {
            System.debug('HONES_CASEGENERATION_CS_CLASS.GetInterlocutorType  Message: ' + ex.getMessage());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.GetInterlocutorType  Cause: ' + ex.getCause());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.GetInterlocutorType  Line number: ' + ex.getLineNumber());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.GetInterlocutorType  Stack trace: ' + ex.getStackTraceString());
        }
    }
    
    /**
    * Method for get the user Vacation
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Owner
    * @return User object with the susbtitute user
    */
    @TestVisible private User GetUserVacation(string idUser)
    {
        User oUserSubstitute = new User();
        oUserSubstitute = [SELECT CS_On_Vacation__c, CS_User_Replacement__c
                           FROM User
                           WHERE Id =: idUser];
        
        return oUserSubstitute;
    }
    
    /**
    * Method for get the primary contact
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Account
    * @return String with Id Contact
    */
    private string GetContactAccount(string idAccount)
    {
        string idContact = null;
        Contact oIdContact = new Contact();
        integer countContact = 0;
        
        countContact = [SELECT Count() FROM Contact WHERE CS_Contacto_Principal__c = true AND AccountId =: idAccount];
        if(countContact > 0)
        {
            oIdContact = [SELECT ID FROM Contact WHERE CS_Contacto_Principal__c = true AND AccountId =: idAccount];
            idContact = (string)oIdContact.Id;
        }
        return idContact;
    }
}