/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASECLOSE_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 28/12/2018           Jose Luis Vargas       Crecion de la clase para cierre de casos
 */

public class CS_CaseClose_Class 
{
    public CS_CaseClose_Class(){}
    
    public class PickListValuesSolution
    {
        @AuraEnabled
        public string label {get; set;}
        @AuraEnabled
        public string value {get; set;}
        
        public PickListValuesSolution(string Label, string Value)
        {
            this.label = Label;
            this.value = Value;
        }
    }
    
    /**
    * Method for consulting  the detail of case
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id of case
    * @return Case with detail
    */
    @AuraEnabled
    public static Case GetDetailCase(string idCase)
    {
        Case oDetailCase = new Case();
                    oDetailCase = [SELECT CaseNumber, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, ISSM_CaseForceNumber__c 
                       FROM Case WHERE Id =: idCase];
        
        return oDetailCase;
    }
    
    /**
    * Method for consulting the name of account
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id of case
    * @return String with the name associate to case
    */
    @AuraEnabled
    public static string GetDetailAccount(string idCase)
    {
        Account oDetailAccount = new Account();
        Case oDetailCase = new Case();
        
        oDetailCase = [SELECT AccountID FROM Case WHERE Id =: idCase];
        oDetailAccount = [SELECT Name FROM Account WHERE Id =: oDetailCase.AccountID];
        return oDetailAccount.Name;
    }
    
    /**
    * Method for consulting the question for the closure case
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id of case
    * @return String with the questions
    */
    @AuraEnabled
    public static string GetQuestionCloseCase(string idCase)
    {
        string questionReturn = '';
        string userCountry = '';
        string userLanguage = '';
        
        List<CS_Preguntas_Maestro_Soluciones__mdt> lstQuestions = new List<CS_Preguntas_Maestro_Soluciones__mdt>();
        userCountry = GetUserCountry();
        userLanguage = UserInfo.getLanguage();
        lstQuestions = [SELECT Pregunta_Maestro_Solucion_Eng__c, Pregunta_Maestro_Solucion_SP__c FROM CS_Preguntas_Maestro_Soluciones__mdt WHERE Pais__c =: userCountry];
              
        switch on userLanguage
        {
            when 'en_US'
            {
                for(CS_Preguntas_Maestro_Soluciones__mdt oQuestion : lstQuestions)
                {
                   questionReturn = questionReturn + oQuestion.Pregunta_Maestro_Solucion_Eng__c + '\n';
                }
            }
            when else
            {
                for(CS_Preguntas_Maestro_Soluciones__mdt oQuestion : lstQuestions)
                {
                   questionReturn = questionReturn + oQuestion.Pregunta_Maestro_Solucion_SP__c + '\n';
                }
            }
        }
       
        if(userCountry == System.Label.CS_Country_El_Salvador)
        {
             string typification = '';
             string CaseDate = '';
             Case oDetailCase = new Case();
             oDetailCase = [SELECT CaseNumber, CreatedDate, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c
                            FROM Case
                            WHERE Id =: idCase];
             typification = oDetailCase.ISSM_TypificationLevel1__c + ' | ' + oDetailCase.ISSM_TypificationLevel2__c + ' | ' + oDetailCase.ISSM_TypificationLevel3__c + ' | ' + oDetailCase.ISSM_TypificationLevel4__c;
             CaseDate = oDetailCase.CreatedDate.format('dd/MM/yyyy');
             questionReturn = questionReturn.replace('{CaseNumber}', oDetailCase.CaseNumber).replace('{Fecha}', CaseDate).replace('{Tipificacion}', typification); 
        }
        
        return questionReturn;
    }
    
    /**
    * Method for consulting the options for Solution Master
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return List with the option for country
    */
    @AuraEnabled
    public static List<PickListValuesSolution> GetSolutionMasterOptions()
    {
        string userLanguage = '';
        string userCountry = '';
        List<CS_Maestro_Soluciones__mdt> lstOptionMaster = new List<CS_Maestro_Soluciones__mdt>();
        List<PickListValuesSolution> lstPickListValues = new  List<PickListValuesSolution>();
        
        userLanguage = UserInfo.getLanguage();
        userCountry = GetUserCountry();
        lstOptionMaster = [SELECT   Valor_Maestro_Solucion_SP__c, Valor_Maestro_Solucion_Eng__c FROM CS_Maestro_Soluciones__mdt WHERE Pais__c =: userCountry];
        lstPickListValues.add(new PickListValuesSolution('',''));
        
        switch on userLanguage
        {
            when 'en_US'
            {
                for(CS_Maestro_Soluciones__mdt oMasterSolution : lstOptionMaster)
                {
                   lstPickListValues.add(new PickListValuesSolution(oMasterSolution.Valor_Maestro_Solucion_Eng__c, oMasterSolution.Valor_Maestro_Solucion_Eng__c));
                }
            }
            when else
            {
                for(CS_Maestro_Soluciones__mdt oMasterSolution : lstOptionMaster)
                {
                    lstPickListValues.add(new PickListValuesSolution(oMasterSolution.Valor_Maestro_Solucion_SP__c, oMasterSolution.Valor_Maestro_Solucion_SP__c));
                }
            }
        }
        
        return lstPickListValues;
    }
    
    /**
    * Method for close a case
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case, Id Case Force, Solution Description and Option of solution master
    * @return Boolean indicates if case is close
    */
    @AuraEnabled
    public static boolean CloseCase(string idCase, string idCaseForce, string solutionDescription, string optionMasterSolution, String recType)
    {
        boolean caseClosed = true;
        string idMilestone = '';
        Case oUpdateCase = new Case();
        ONTAP__Case_Force__c oUpdateCaseForce = new ONTAP__Case_Force__c();
        CaseMilestone oUpdateMilestone = new CaseMilestone();
        CS_RO_Cooler_Settings__c settings = CS_RO_Cooler_Settings__c.getOrgDefaults();
        
        try
        {
            /* Llenando objeto de caso standard */
            oUpdateCase.Id = idCase;
            oUpdateCase.ISSM_SolutionNote__c = solutionDescription;
            oUpdateCase.CS_Solution_Master__c = optionMasterSolution;
            oUpdateCase.Status = System.Label.CS_Estatus_Caso_Cerrado;
            oUpdateCase.IsStopped = true;
            
            System.debug('Record Type de caso para cierre de correo: ' + recType);
           
            if(recType.equals(settings.CS_RT_Cooler_Repair__c))
            {
                RecordType recTyp = ([SELECT id FROM RecordType WHERE DeveloperName =:settings.CS_RT_RO_Cooler_Repair__c]);
                oUpdateCase.RecordTypeId = recTyp.Id;
            }
            else if(recType.equals(settings.CS_RT_General_Case__c))
            {
                RecordType recTyp = ([SELECT id FROM RecordType WHERE DeveloperName =:settings.CS_RT_RO_General_Case__c]);
                oUpdateCase.RecordTypeId = recTyp.Id;
            }
            else if(recType.equals(settings.CS_RT_Generate_Client__c))
            {
                RecordType recTyp = ([SELECT id FROM RecordType WHERE DeveloperName =:settings.CS_RT_RO_Generate_Client__c]);
                oUpdateCase.RecordTypeId = recTyp.Id;                
            }
            else if(recType.equals(settings.CS_RT_Installation_Cooler__c))
            {
                RecordType recTyp = ([SELECT id FROM RecordType WHERE DeveloperName =:settings.CS_RT_RO_Installation_Cooler__c]);
                oUpdateCase.RecordTypeId = recTyp.Id;
            }
            else if(recType.equals(settings.CS_RT_Retirement_Cooler__c))
            {
                RecordType recTyp = ([SELECT id FROM RecordType WHERE DeveloperName =:settings.CS_RT_RO_Retirement_Cooler__c]);
                oUpdateCase.RecordTypeId = recTyp.Id;
            }
            else if(recType.equals(settings.CS_RT_Tracing_Case__c))
            {
                RecordType recTyp = ([SELECT id FROM RecordType WHERE DeveloperName =:settings.CS_RT_RO_Tracing_Case__c]);
                oUpdateCase.RecordTypeId = recTyp.Id;
            }
            
            /* Llenando informacion del caso force */
            oUpdateCaseForce.Id = idCaseForce;
            oUpdateCaseForce.ONTAP__Status__c = System.Label.CS_Estatus_Caso_Cerrado;
            
            /* Llenando informacion del milestone */
            idMilestone = GetCaseMilestone(idCase);
            if(idMilestone != null && idMilestone != '')
            {
                oUpdateMilestone.Id = idMilestone;
                oUpdateMilestone.CompletionDate = Datetime.now();
                Update oUpdateMilestone;   
            }
           
            /* Se actualia el caso standard */
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update oUpdateCase;
            
            /* Se actualiza el caso sforce*/
            ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
            Update oUpdateCaseForce;
                       
        }
        catch(Exception ex)
        {
            System.debug('CS_CASECLOSE_CLASS.CloseCase Message: ' + ex.getMessage());   
            System.debug('CS_CASECLOSE_CLASS.CloseCase Cause: ' + ex.getCause());   
            System.debug('CS_CASECLOSE_CLASS.CloseCase Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASECLOSE_CLASS.CloseCase Stack trace: ' + ex.getStackTraceString());
            caseClosed = false;
        }
        
        return caseClosed;
    }
    
    /**
    * Method for get user country
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return String with the user country
    */
    private static String GetUserCountry()
    {
        User oUserInfo = new User();
        oUserInfo = [SELECT Country__c FROM USER WHERE Id =: UserInfo.getUserId()];      
        return oUserInfo.Country__c;
    }
    
    /**
    * Method for get the milestone asociate to case
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case
    * @return String with de Milestone Id
    */
    private static string GetCaseMilestone(string idCase)
    {
        CaseMilestone oMilestone = new CaseMilestone();
        integer existsMilestone = 0;
        
        existsMilestone = [SELECT COUNT() FROM CaseMilestone WHERE CASEID =: idCase];
        if(existsMilestone > 0)
            oMilestone = [SELECT ID From CaseMilestone WHERE CASEID =: idCase];
        
        if(oMilestone != null && oMilestone.Id != null)
            return (string)oMilestone.Id;
        else 
            return null;
    }
}