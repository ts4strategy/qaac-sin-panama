/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_CaseAssetAccount_Class.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 28/01/ 2018     Debbie Zacarias	      Creation of the class used for control Lightning component for account assets
* 11/04/2019      Jose Luis Vargas		  Change methods for new flow cooler
*/

public class CS_CaseAssetAccount_Class 
{
	/**
	* method for find brands of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
    * @param serialNumber Serial Number
	* @return List<String> that controls the picklist values
	*/    
    @AuraEnabled
    public static ONTAP__Account_Asset__c getAssetInfo(String idCase, string serialNumber)
    {
        ONTAP__Account_Asset__c acc = new ONTAP__Account_Asset__c();
        
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            
            acc = [SELECT ONTAP__Account__c, ONTAP__Brand__c, HONES_Asset_Model__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c
                   FROM ONTAP__Account_Asset__c 
                   WHERE ONTAP__Account__c =: caso.Accountid
                   AND ONTAP__Serial_Number__c =: serialNumber LIMIT 1];
            
            acc.ONTAP__Brand__c = (acc.ONTAP__Brand__c != NULL && acc.ONTAP__Brand__c != '' ? acc.ONTAP__Brand__c : Label.CS_Opcion_Sin_Informacion);
            acc.HONES_Asset_Model__c = (acc.HONES_Asset_Model__c != NULL && acc.HONES_Asset_Model__c != '' ? acc.HONES_Asset_Model__c : Label.CS_Opcion_Sin_Informacion);
            acc.ONTAP__Asset_Description__c = (acc.ONTAP__Asset_Description__c != NULL && acc.ONTAP__Asset_Description__c != '' ? acc.ONTAP__Asset_Description__c : Label.CS_Opcion_Sin_Informacion);
            acc.HONES_EquipmentNumber__c = (acc.HONES_EquipmentNumber__c != NULL && acc.HONES_EquipmentNumber__c != '' ? acc.HONES_EquipmentNumber__c : Label.CS_Opcion_Sin_Informacion);
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETBRAND Stack trace: ' + ex.getStackTraceString());
        }
        
        return acc;
    }
    
    /**
	* method for find Serial numbers of assets for account cases
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @return List<String> that controls the picklist values
	*/
    @AuraEnabled
    public static List<String> getSerialNumber(String idCase)
    {
        List<String> pickListValuesList = new List<String>();
        List<ONTAP__Account_Asset__c> acc = new List<ONTAP__Account_Asset__c>();
        
        try
        {
            Case caso = ([SELECT id, AccountId  FROM Case WHERE id =:idCase]);
            acc = [SELECT ONTAP__Serial_Number__c FROM ONTAP__Account_Asset__c WHERE ONTAP__Account__c =: caso.AccountId];
            
            if(acc.size() > 0)
            {
                pickListValuesList.add('');
                for(ONTAP__Account_Asset__c item : acc)
                {
                    if(!pickListValuesList.contains(item.ONTAP__Serial_Number__c))
                        pickListValuesList.add(item.ONTAP__Serial_Number__c);
                }
            }
            else
                pickListValuesList.add(Label.CS_Opcion_Sin_Informacion);
                
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.GETSERIALNUMBER Stack trace: ' + ex.getStackTraceString());
        } 
        
        return pickListValuesList;
    }
            
    /*
	* method for find Info of assets for current case
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case
	* @return Case that get the info
	*/
    @AuraEnabled
    public static List<Object> SetAssetCaseInfo(String idCase)
    {
        List<Object> returnList = new List<Object>();
        Case caso;
        ONTAP__Account_Asset__c asset;
        
        try
        {
            caso = ([SELECT id, Status, AccountId, ISSM_BrandProduct__c, ISSM_CaseReason__c, Equipment_model__c, ISSM_SerialNumber__c, CS_Serial_number_of_the_equipment__c, ISSM_Count__c, CS_skuProduct__c FROM Case WHERE id =:idCase]);
            if(caso.ISSM_SerialNumber__c != null && caso.CS_skuProduct__c != null)
            {
                 asset = ([SELECT id, ONTAP__Account__c,ONTAP__Brand__c,ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                           FROM ONTAP__Account_Asset__c 
                           WHERE ONTAP__Serial_Number__c =: caso.ISSM_SerialNumber__c 
                           AND HONES_Asset_Model__c =: caso.Equipment_model__c 
                           AND ONTAP__Brand__c =: caso.ISSM_BrandProduct__c
                           AND ONTAP__Asset_Description__c =: caso.ISSM_CaseReason__c
                           AND ONTAP__Account__c =: caso.AccountId
                          LIMIT 1]);
                
                returnList.add(caso);
                returnList.add(asset);    
            }
            else 
            {
                returnList = null;
            }     
        }
        catch(Exception ex)
        {
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.SetAssetCaseInfo Stack trace: ' + ex.getStackTraceString());            
        }
                
        return returnList;        
    }
    
    /**
	* method for update current case with the information passed trough GetDescription method 
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case for update
	* @param serialNumber for update
    * @param quantity asset number to retirement
	* @return Boolean if record was updated
	*/
    @AuraEnabled
    public static Boolean UpdateCase(String idCase, string serialNumber, integer quantity)
    {
        Boolean success = true;
        Case oCaseUpdate = new Case();
        ONTAP__Case_Force__c oCaseForceUpdate = new ONTAP__Case_Force__c();
        
        try
        {            
            Case caso = ([SELECT Id, ISSM_CaseForceNumber__c, AccountId FROM Case WHERE id =: idCase]);           
            ONTAP__Account_Asset__c acc = ([SELECT Id, ONTAP__Brand__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:SerialNumber 
                                            AND ONTAP__Account__c=:caso.AccountId
                                            LIMIT 1]);
            
            oCaseUpdate.Id = idCase;
            oCaseUpdate.ISSM_BrandProduct__c = acc.ONTAP__Brand__c;
            oCaseUpdate.Equipment_model__c = acc.HONES_Asset_Model__c;
            oCaseUpdate.ISSM_SerialNumber__c = acc.ONTAP__Serial_Number__c;
            oCaseUpdate.CS_Serial_number_of_the_equipment__c = acc.HONES_EquipmentNumber__c;
            oCaseUpdate.ISSM_CaseReason__c = acc.ONTAP__Asset_Description__c;
            oCaseUpdate.CS_skuProduct__c = acc.V360_MaterialNumber__c;
            oCaseUpdate.ISSM_Count__c = quantity;
            
            oCaseForceUpdate.Id = caso.ISSM_CaseForceNumber__c;
            oCaseForceUpdate.CS_Account_Asset__c = acc.Id;
            oCaseForceUpdate.ONTAP__Quantity__c = quantity;
            
            
            ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
            Update oCaseForceUpdate;
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update oCaseUpdate;
            
            /* Se envia el caso a SAP */
            CS_Case_Coolers_Callout_Class.sendRequest(caso.id);
            
        }
        catch(Exception ex)
        {
            success = false;
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASE Stack trace: ' + ex.getStackTraceString());
        } 
        
        return success;
    }
    
    /**
	* method for update current case with the information passed trough GetDescription method 
	* @author: d.zacarias.cantillo@accenture.com
	* @param idCase id of current case for update
	* @param SerialNumber for update
	* @param EquipmentNumber for update
	* @return Boolean if record was updated
	*/
    @AuraEnabled
    public static boolean UpdateCaseAsset(string idCase, string SerialNumber)
    {
        boolean success = true;
        Case oCaseUpdate = new Case();
        ONTAP__Case_Force__c oCaseForceUpdate = new ONTAP__Case_Force__c();
        
        try
        {            
            Case caso = ([SELECT Id, ISSM_CaseForceNumber__c, AccountId FROM Case WHERE id =:idCase]);
                        
            ONTAP__Account_Asset__c acc = ([SELECT Id, ONTAP__Brand__c, ONTAP__Asset_Description__c, HONES_EquipmentNumber__c, HONES_Asset_Model__c, ONTAP__Serial_Number__c, V360_MaterialNumber__c
                                            FROM ONTAP__Account_Asset__c 
                                            WHERE ONTAP__Serial_Number__c=:SerialNumber 
                                            AND ONTAP__Account__c=:caso.AccountId
                                            LIMIT 1]);
            
            oCaseUpdate.Id = idCase;
            oCaseUpdate.ISSM_BrandProduct__c = acc.ONTAP__Brand__c;
            oCaseUpdate.Equipment_model__c = acc.HONES_Asset_Model__c;
            oCaseUpdate.ISSM_SerialNumber__c = acc.ONTAP__Serial_Number__c;
            oCaseUpdate.CS_Serial_number_of_the_equipment__c = acc.HONES_EquipmentNumber__c;
            oCaseUpdate.ISSM_CaseReason__c = acc.ONTAP__Asset_Description__c;
            oCaseUpdate.CS_skuProduct__c = acc.V360_MaterialNumber__c;
            
            oCaseForceUpdate.Id = caso.ISSM_CaseForceNumber__c;
            oCaseForceUpdate.CS_Account_Asset__c = acc.Id;           
            
            ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
            Update oCaseForceUpdate;
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update oCaseUpdate;
        }
        catch(Exception ex)
        {
            success = false;
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Message: ' + ex.getMessage());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Cause: ' + ex.getCause());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASEASSETACCOUNT_CLS.UPDATECASEASSET Stack trace: ' + ex.getStackTraceString());
        } 
        return success;
    }
}