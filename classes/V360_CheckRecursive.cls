/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_CheckRecursive.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 18/12/2018     Gerardo Martinez        Creation of methods.
*/
public class V360_CheckRecursive {
    private static boolean run = true;
    /**
    * Returns true the first time that is called in a single transaction, the second time returns false
    * in that way we can ensure that a method is not recursive
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }else{
            return run;
        }
    }
}