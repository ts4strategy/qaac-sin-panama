/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_REASSIGMENT_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 23/01/2019        Jose Luis Vargas          Crecion de la clase para asignacion de casos
 */

public class CS_Case_Reassigment_Class 
{
    public CS_Case_Reassigment_Class() {}  
    
    public class PickListValuesAssigned
    {
        @AuraEnabled
        public string label {get; set;}
        @AuraEnabled
        public string value {get; set;}
        
        public PickListValuesAssigned(string Label, string Value)
        {
            this.label = Label;
            this.value = Value;
        }
    }
    
    /**
    * Method for get the users in lookup
    * @author: jose.l.vargas.lara@accenture.com
    * @param searchKeyWord with the filter, Object name
    * @return List with the results of query
    */
    @AuraEnabled
    public static List<sObject> fetchLookUpValues(String searchKeyWord, String ObjectName) 
    {
        string searchKey = searchKeyWord + '%';
        string userCountry = '';
        List <sObject> returnList = new List <sObject>();
        User oCountryUser = [SELECT Country__c FROM USER WHERE Id =: UserInfo.getUserId()];
        userCountry = string.valueOf(oCountryUser.Country__c) ;
        
        string sQuery =  'SELECT Id, Name FROM ' + ObjectName + ' WHERE Name LIKE: searchKey AND Country__c =: userCountry Order by Name Limit 5 ';
        System.debug(sQuery);
        List <sObject> lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) 
        {
            returnList.add(obj);
        }
        return returnList;
    }
    
    /**
    * Method for get case detail
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case 
    * @return Object Case with the detail
    */
    @AuraEnabled
    public static Case GetCaseInfo(string idCase)
    {
        Case oDetailCase = new Case();
        oDetailCase = [SELECT CaseNumber, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, ISSM_CaseForceNumber__c FROM Case WHERE Id =: idCase];
        
        return oDetailCase;
    }
    
    /**
    * Method for get the name of account
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id of case
    * @return String with the name associate to case
    */
    @AuraEnabled
    public static string GetDetailAccount(string idCase)
    {
        Account oDetailAccount = new Account();
        Case oDetailCase = new Case();
        
        oDetailCase = [SELECT AccountID FROM Case WHERE Id =: idCase];
        oDetailAccount = [SELECT Name FROM Account WHERE Id =: oDetailCase.AccountID];
        return oDetailAccount.Name;
    }
    
   /**
    * Method for get the iterlocutors
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return List with the interlocutors
    */
    @AuraEnabled
    public static List<PickListValuesAssigned> GetCaseAssigned()
    {
        List<PickListValuesAssigned> lstAssignedValues = new List<PickListValuesAssigned>();
        List<CS_Assigned_Interlocutor__c> lstAssigned = new List<CS_Assigned_Interlocutor__c>();
        string userLanguage = UserInfo.getLanguage();
        
        lstAssigned = [SELECT CS_Interlocutor_ENG__c, CS_Interlocutor_SPN__c FROM CS_Assigned_Interlocutor__c];
        lstAssignedValues.add(new PickListValuesAssigned('', ''));
              
        switch on userLanguage
        {
            when 'en_US'
            {
                for(CS_Assigned_Interlocutor__c oInterlocutor : lstAssigned)
                {
                    lstAssignedValues.add(new PickListValuesAssigned(oInterlocutor.CS_Interlocutor_ENG__c, oInterlocutor.CS_Interlocutor_ENG__c));
                }
            }
            when else
            {
                for(CS_Assigned_Interlocutor__c oInterlocutor : lstAssigned)
                {
                    lstAssignedValues.add(new PickListValuesAssigned(oInterlocutor.CS_Interlocutor_SPN__c, oInterlocutor.CS_Interlocutor_SPN__c));
                }
            }
        }
        return lstAssignedValues;
    }
    
    /**
    * Method for get the interlocutor Id and Name
    * @author: jose.l.vargas.lara@accenture.com
    * @param Interlocutor and Id Case
    * @return User Object
    */
    @AuraEnabled
    public static User GetInterlocutorName(string assignedTo, string idCase)
    {
        string IdOwnerStd = '';
        string userName = '';
        string filteredQuery = '';
        
        Case oCase = new Case();
        User oDetailUser = new User();
        CS_CASEASSIGNMENT_CLASS oAssignmentInterlocutor = new CS_CASEASSIGNMENT_CLASS();
                
        try
        {
            /* Se obtiene el id de la cuenta del caso */
            oCase = [SELECT AccountID FROM Case WHERE Id =: idCase];
            
            /* Se obtiene el tipo de interlocutor */
            filteredQuery = GetInterlocutorType(assignedTo);
            switch on filteredQuery
            {
                when 'Telventa' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorTelventa(assignedTo, oCase.AccountId, filteredQuery); }  
                when 'Preventa' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorPreVenta(assignedTo, oCase.AccountId, filteredQuery); }
                when 'Credito' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorCredit(assignedTo, oCase.AccountId, filteredQuery); }
                when 'Cooler' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorCoolers(assignedTo, oCase.AccountId, filteredQuery); }
                when 'Telecobranza' { IdOwnerStd = oAssignmentInterlocutor.GetCaseInterlocutorTellecolection(assignedTo, oCase.AccountId, filteredQuery); }
            }
            
            if(IdOwnerStd != null && IdOwnerStd != '')
            {
               oDetailUser = [SELECT Id, Name FROM User WHERE Id =: IdOwnerStd];
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName  Stack trace: ' + ex.getStackTraceString());
            oDetailUser = new User();
        }
        
        return oDetailUser;
    }
    
    /**
    * Method for assign the owner to case
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case, Id Case Force, Id Case Owner, assign type
    * @return Boolean with de status process
    */
    @AuraEnabled
    public static boolean UpdateOwnerCase(string idCase, string idCaseForce, string idOwner, string assignedTo)
    {
        Case oUpdateOwnerCase = new Case();
        User oUserVacation = new User();
        ONTAP__Case_Force__c oUpdateOwnerCaseForce = new ONTAP__Case_Force__c();
        CS_CASEASSIGNMENT_CLASS oAssignmentInterlocutor = new CS_CASEASSIGNMENT_CLASS();
        CS_CASE_USER_ESCALATION_CLASS oAssignmentUser = new CS_CASE_USER_ESCALATION_CLASS();
        boolean estatusProceso = true;
        string filteredQuery = '';
        
        try
        {
            /* Se valida si el owner asignado se encuentra de vacaciones */
            oUserVacation = GetUserVacation(idOwner);
            if(oUserVacation.CS_On_Vacation__c)
                idOwner = oUserVacation.CS_User_Replacement__c;
            
            /* Se realiza la consulta del tipo de interlocutor */
            if(assignedTo == ISSM_Constants_cls.USER)
                filteredQuery =  ISSM_Constants_cls.USER;
            else
                filteredQuery = GetInterlocutorType(assignedTo);
            
            /* Se llena el Caso standard */
            oUpdateOwnerCase.Id = idCase;
            oUpdateOwnerCase.OwnerId = idOwner;
            oUpdateOwnerCase.ISSM_OwnerCaseForce__c = idOwner;
            oUpdateOwnerCase.ISSM_FirstUserOwner__c = idOwner;
            oUpdateOwnerCase.ISSM_OwnerEmailSent__c = false;
            
            /* Se llena el caso force */
            if(idCaseForce != null)
            {
                oUpdateOwnerCaseForce.Id = idCaseForce;
                oUpdateOwnerCaseForce.OwnerId = idOwner;
                /* Se actualiza el caso sforce*/
                ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
                Update oUpdateOwnerCaseForce;
            }
            
            /* Se actualia el caso standard */
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update oUpdateOwnerCase;
                                   
            /* Se actualizan los escalamientos para el caso de acuerdo al tipo de asignacion */
            if(filteredQuery == ISSM_Constants_cls.TELVENTA)
                oAssignmentInterlocutor.AssignEscalationTelventa(idCase, filteredQuery);
            else if(filteredQuery == ISSM_Constants_cls.PREVENTA)
                oAssignmentInterlocutor.AssignEscalationPreVenta(idCase, filteredQuery);
            else if(filteredQuery == ISSM_Constants_cls.CREDITO)
                oAssignmentInterlocutor.AssignEscalationCredit(idCase, filteredQuery);
            else if(filteredQuery == ISSM_Constants_cls.EQUIPO_FRIO)
                oAssignmentInterlocutor.AssignEscalationCoolers(idCase, filteredQuery, idOwner);
            else if (filteredQuery == ISSM_Constants_cls.TELECOBRANZA)
                oAssignmentInterlocutor.AssignEscalationTellecolection(idCase, filteredQuery);
            else
                oAssignmentUser.AssignEscalationUser(idCase, idOwner);
            
            GenerateTask(idCase, idOwner);
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase  Stack trace: ' + ex.getStackTraceString());
            estatusProceso = false;
        }
        
        return estatusProceso;
    }
    
    /**
    * Method for get the type of interlocutor
    * @author: jose.l.vargas.lara@accenture.com
    * @param Role to assign the case
    * @return String
    */
    private static string GetInterlocutorType(string assignedTo)
    {
        string filteredQuery = '';
        CS_Interlocutores__mdt oQuerType  = new  CS_Interlocutores__mdt();
        List<CS_Interlocutores__mdt> lstInterlocutores = new List<CS_Interlocutores__mdt>();
        
        try
        {
            lstInterlocutores = [  
                                    SELECT CS_Tipo_Interlocutor__c 
                                    FROM CS_Interlocutores__mdt 
                                    WHERE CS_Interlocutor_ENG__c =: assignedTo 
                                ];
            
            if(lstInterlocutores.size() == 0)
            {
                lstInterlocutores = [  
                                        SELECT CS_Tipo_Interlocutor__c 
                                        FROM CS_Interlocutores__mdt 
                                        WHERE CS_Interlocutor_SPN__c =: assignedTo
                                    ];

            }
                    
            filteredQuery = lstInterlocutores[0].CS_Tipo_Interlocutor__c;
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorType  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorType  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorType  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GetInterlocutorType  Stack trace: ' + ex.getStackTraceString());
        }
        
        return filteredQuery;
    }
    
    /**
    * Method for generate a new task
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Case, Id Owner
    * @return Void
    */    
    private static void GenerateTask(string idCase, string idOwner)
    {
        Task oNewTask = new Task();
        Case oDetailCase = new Case();
        ISSM_TypificationMatrix__c oTypificationMatrix = new ISSM_TypificationMatrix__c();
        Date oToday = Date.today();
        string questionsFirtsContact = '';
        
        DataBase.DMLOptions oSendTaskNotification = new DataBase.DMLOptions();
        oSendTaskNotification.EmailHeader.triggerUserEmail = true;
        oSendTaskNotification.EmailHeader.triggerOtherEmail = true;
        try
        {
            oDetailCase = [SELECT HONES_Case_Country__c, ISSM_TypificationNumber__c FROM Case WHERE Id =: idCase];
            if(oDetailCase.HONES_Case_Country__c == System.Label.CS_Country_Honduras)
            {
                oTypificationMatrix = [SELECT CS_Dias_Primer_Contacto__c FROM ISSM_TypificationMatrix__c WHERE Id =: oDetailCase.ISSM_TypificationNumber__c]; 
                oToday = oToday.addDays((integer)oTypificationMatrix.CS_Dias_Primer_Contacto__c);
                
                oNewTask.Subject = System.Label.CS_Subject_Task;
                oNewTask.OwnerId = idOwner;
                oNewTask.ActivityDate = oToday;
                oNewTask.WhatId = idCase;
                oNewTask.Status = System.Label.CS_Status_Task;

                questionsFirtsContact = System.Label.CS_Mensaje_Primer_Contacto + '\n' + GetQuestionsFirstContact();
                oNewTask.Description = questionsFirtsContact;
            
                DataBase.Insert(oNewTask, oSendTaskNotification);
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_REASSIGMENT_CLASS.GenerateTask  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GenerateTask  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GenerateTask  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_REASSIGMENT_CLASS.GenerateTask  Stack trace: ' + ex.getStackTraceString());
        }
    }
    
        /**
    * Method for get the first contact question
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return string with the question 
    */
    private static string GetQuestionsFirstContact()
    {
        string questionFirstContact = '';
        List<CS_Preguntas_Primer_Contacto__mdt> lstQuestions = new List<CS_Preguntas_Primer_Contacto__mdt>();
        lstQuestions = [SELECT Pregunta__c FROM CS_Preguntas_Primer_Contacto__mdt];
       
        for(CS_Preguntas_Primer_Contacto__mdt oQuestion : lstQuestions)
        {
            questionFirstContact = questionFirstContact + oQuestion.Pregunta__c + '\n';
        }
              
        return questionFirstContact;
    }
    
    /**
    * Method for get the user Vacation
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id Owner
    * @return User object with the susbtitute user
    */
    private static User GetUserVacation(string idUser)
    {
        User oUserSubstitute = new User();
        oUserSubstitute = [SELECT CS_On_Vacation__c, CS_User_Replacement__c
                           FROM User
                           WHERE Id =: idUser];
        return oUserSubstitute;
    }
}