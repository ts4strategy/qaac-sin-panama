/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_Prospect_Callout_Class.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 17/04/2019      Jose Luis Vargas        Creation of the class for send prospect to mulesoft
 *                                             
*/
public class CS_Prospect_Callout_Class 
{
    /**
    * method for send to mulesoft the prospects 
    * @author: jose.l.vargas.lara@accenture.com
    * @param lstProspectId List with Id of prospect
    * @return Void
    */  
    @future(callout=true)
    public static void SendProspectMuleSoft(List<Id> lstProspectId)
    {
        string jsonProspect = '';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        CS_Prospect_Settings__c prospectSetting = CS_Prospect_Settings__c.getOrgDefaults();
        List<Account> lstProspects = new List<Account>();
        List<Case> lstProspectCase = new List<Case>();
        Integer statusCode = Integer.valueOf(Label.CS_Status_Code);
        
        Map<string, object> bodyResponse = new Map<string, object>();
        Map<string, object> mpResponse = new Map<string, object>();
        List<Object> lstResponseProspect = new List<Object>();
        
        string numeroIdentificacion = '';
        string mensajeError;
        boolean codigoError;
       
        try
        {              
            /* Se realiza la consulta del detalle de los prospectos */
            lstProspects = [SELECT Id, CS_Header_Id__c, RecordTypeId, CreatedById, CreatedBy.Name, CreatedBy.Email, V360_ReferenceClient__r.ONTAP__Latitude__c, V360_ReferenceClient__r.ONTAP__Longitude__c,
                                   V360_ReferenceClient__r.ONTAP__SAP_Number__c, CS_Tipo_Identificacion__c, ISSM_ProspectType__c, HONES_IdentificationNumber__c, CS_Identification_Number_2__c,  
                                   CS_Identification_Number_3__c, ONTAP__LegalName__c, Name, CS_Last_Name__c, CS_Second_Last_Name__c, CS_Tipo_Via__c, ONTAP__Street__c, ONTAP__Street_Number__c,
                                   CS_Poblacion__c, CS_Distrito__c, CS_Tipo_Poblacion__c, ONTAP__Mobile_Phone__c, Phone, CS_Birthday__c, ONTAP__Latitude__c, ONTAP__Longitude__c, CS_Channel_SubChannel__c,
                                   CS_Prospect_Class__c
                            FROM Account
                            WHERE Id IN : lstProspectId];
            
            /* Se realiza la consulta de los casos generados de los prospectos */
            lstProspectCase = [SELECT Id, CS_Send_To_Mule__c, CS_Response_MdWeb__c, Account.HONES_IdentificationNumber__c FROM Case Where AccountId IN : lstProspectId];
            
            /* Se genera el Json y se prepara el rquest a MuleSoft */
            jsonProspect = GenerateJsonProspect(lstProspects);
            request.setEndpoint(prospectSetting.Prospect_Url__c);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8'); 
            request.setHeader('bearer', prospectSetting.Prospect_Token__c);
            request.setBody(jsonProspect);

            /* Se envia la peticion a mulesoft */
            response = http.send(request);
            
            /* Se valida si la respuesta de mulesoft es correcta  Estatus 200 */
            if(response.getStatusCode() == statusCode)
            {
                /* Se obtiene la respuesta por cada prospecto enviado a MDWeb */
                bodyResponse  = (Map<string, object>)Json.deserializeUntyped(response.getBody());
                lstResponseProspect = (List<Object>)bodyResponse.get(Label.CS_Body_Response);

                /* Se actualiza el estatus de la peticion en el caso generado por cada prospecto */                
                for(object oResponse : lstResponseProspect)
                {
                    mpResponse = (Map<String,Object>)oResponse;
                    numeroIdentificacion = string.valueOf(mpResponse.get(Label.CS_Numero_Identificacion));
                    mensajeError = string.valueOf(mpResponse.get(Label.CS_Mensaje_Error));
                    codigoError = boolean.valueOf(mpResponse.get(Label.CS_Codigo_Error));
                        
                    for(Case oCaseProspect : lstProspectCase)
                    {
                        if(oCaseProspect.Account.HONES_IdentificationNumber__c  == numeroIdentificacion)
                        {
                            oCaseProspect.CS_Send_To_Mule__c = codigoError;
                            ocaseProspect.CS_Response_MdWeb__c = (codigoError ? Label.CS_Mensaje_Exitoso : mensajeError);
                            break;
                        }
                    }
                }              
           }
           else /* Si el estatus es diferente de 200 se asigna mensaje de error a los prospectos enviados */
           {
               for(Case prospectCase : lstProspectCase)
               {
                   prospectCase.CS_Send_To_Mule__c = false;
                   prospectCase.CS_Response_MdWeb__c = Label.CS_Mensaje_Error_Prospecto + ' ' + Label.CS_Mensaje_Error_Http + ' : ' + String.valueOf(response.getStatusCode());
               }
           }
            
           /*Se actualiza el caso */
           ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
           Update lstProspectCase; 
       }
        catch(Exception ex)
        {
            /* En caso de excepcion se asigna el mensaje de error */
            for(Case prospectCase : lstProspectCase)
            {
                prospectCase.CS_Send_To_Mule__c = false;
                prospectCase.CS_Response_MdWeb__c = Label.CS_Mensaje_Error_Prospecto + ' ' + ex.getMessage();
            }
            
            /*Se actualiza el caso */
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update lstProspectCase; 
            
            System.debug('CS_Prospect_Callout_Class.SendProspectMuleSoft  Message: ' + ex.getMessage());   
            System.debug('CS_Prospect_Callout_Class.SendProspectMuleSoft  Cause: ' + ex.getCause());   
            System.debug('CS_Prospect_Callout_Class.SendProspectMuleSoft  Line number: ' + ex.getLineNumber());   
            System.debug('CS_Prospect_Callout_Class.SendProspectMuleSoft  Stack trace: ' + ex.getStackTraceString());
        }
    }
    
    /**
    * method to generate Json with prospect data
    * @author: jose.l.vargas.lara@accenture.com
    * @param lstProspects List with Data Prospect
    * @return Void
    */  
    private static string GenerateJsonProspect(List<Account> lstProspects)
    {
        string jsonProspect = '';
        CS_Prospect_Request_Class oProspectJson = new CS_Prospect_Request_Class();
        List<CS_Prospect_Request_Class> lstProspectJson = new List<CS_Prospect_Request_Class>();
        Id idRecordTypeHN = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        Id idRecordTypeSV = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_SV).getRecordTypeId();

        try
        {
            for(Account oProspect : lstProspects)
            {
                oProspectJson.Header_Id = oProspect.CS_Header_Id__c;
                oProspectJson.Country = (oProspect.RecordTypeId == idRecordTypeHN ? Label.CS_CountryCodeHN : Label.CS_CountryCodeSV);
                oProspectJson.Employee_Id_Create = oProspect.CreatedById;
                oProspectJson.Employee_Name_Create = oProspect.CreatedBy.Name;
                oProspectJson.Employee_Email_Create = oProspect.CreatedBy.Email;
                oProspectJson.Survey_Date = DateTime.now().format('dd/MM/yyyy');
                oProspectJson.Survey_Type = (oProspectJson.Country == Label.CS_CountryCodeHN  ? Label.CS_Survey_HN : Label.CS_Survey_SV);
                oProspectJson.Survey_Type_Id = oProspectJson.Header_Id + oProspectJson.Country;
                oProspectJson.Latitude = (oProspect.ONTAP__Latitude__c != null ? oProspect.ONTAP__Latitude__c : oProspect.V360_ReferenceClient__r.ONTAP__Latitude__c);
                oProspectJson.Longitude = (oProspect.ONTAP__Longitude__c != null ? oProspect.ONTAP__Longitude__c : oProspect.V360_ReferenceClient__r.ONTAP__Longitude__c);
                oProspectJson.Customer_referee = oProspect.V360_ReferenceClient__r.ONTAP__SAP_Number__c;
                oProspectJson.Status = Label.CS_Status_Prospecto;
                oProspectJson.AC_CambioRazonSocial = Label.CS_Cambio_Razon_Social;
                oProspectJson.AC_TipoIdentificacion = oProspect.CS_Tipo_Identificacion__c;
                oProspectJson.AC_TipoPersona = oProspect.ISSM_ProspectType__c;
                oProspectJson.AC_NumeroIdentificacion = oProspect.HONES_IdentificationNumber__c;
                oProspectJson.AC_NumeroIdentificacion2 = oProspect.CS_Identification_Number_2__c;
                oProspectJson.AC_NumeroIdentificacion3 = oProspect.CS_Identification_Number_3__c;
                oProspectJson.AC_NombreComercial = oProspect.ONTAP__LegalName__c;
                oProspectJson.AC_NombreCliente_PrimerNombre = oProspect.Name;
                oProspectJson.AC_NombreCliente_SegundoNombre = '';
                oProspectJson.AC_NombreCliente_ApPaterno = oProspect.CS_Last_Name__c;
                oProspectJson.AC_NombreCliente_ApMaterno = oProspect.CS_Second_Last_Name__c;
                oProspectJson.AC_TipoVia = oProspect.CS_Tipo_Via__c;
                oProspectJson.AC_NombreVia = oProspect.ONTAP__Street__c;
                oProspectJson.AC_Nro = oProspect.ONTAP__Street_Number__c;
                oProspectJson.AC_Poblacion = oProspect.CS_Poblacion__c;
                oProspectJson.AC_Distrito = oProspect.CS_Distrito__c;
                oProspectJson.AC_TipoPoblacion = oProspect.CS_Tipo_Poblacion__c;
                oProspectJson.AC_TelefonoMovil = oProspect.ONTAP__Mobile_Phone__c;
                oProspectJson.AC_TelefonoFijo = oProspect.Phone;
                oProspectJson.AC_FechaNacimiento = string.valueOf(oProspect.CS_Birthday__c);
                oProspectJson.AC_Canal_Subcanal_Local = oProspect.CS_Channel_SubChannel__c;
                oProspectJson.AC_Clase_Cliente = oProspect.CS_Prospect_Class__c;
                oProspectJson.AC_External_Id = oProspect.Id;
                               
                lstProspectJson.add(oProspectJson);
                oProspectJson = new CS_Prospect_Request_Class();
            }
                
            /* Se genera el Json */
            jsonProspect  = System.Json.serialize(lstProspectJson, true);
        }
        catch(Exception ex)
        {
            System.debug('CS_Prospect_Callout_Class.GenerateJsonProspect  Message: ' + ex.getMessage());   
            System.debug('CS_Prospect_Callout_Class.GenerateJsonProspect  Cause: ' + ex.getCause());   
            System.debug('CS_Prospect_Callout_Class.GenerateJsonProspect  Line number: ' + ex.getLineNumber());   
            System.debug('CS_Prospect_Callout_Class.GenerateJsonProspect  Stack trace: ' + ex.getStackTraceString());
        }
        
        return jsonProspect;
    }
}