/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_CallAndVisits_CONTRO.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

public class HONES_ONCALL_CallAndVisits_CONTRO {
    
    /**
    * Method to get all Flexible data  by userId  
    * Contact suport By: g.martinez.cabral@accenture.com
    * Created By: Luis Arturo Parra
    * @param String recordId
    * @return List<Id>
    */    
    @AuraEnabled
    public static List<Id> getFlexibleDataById(String recordId){
        
        List<Id> AccId = new List<Id>();
        ONCALL__Call__c acc=[SELECT ONCALL__POC__c 
                             FROM ONCALL__Call__c WHERE Id =:recordId];    
        AccId.add(acc.ONCALL__POC__c);
        
        return AccId;
    }
    
    /**
    * Method to get all Info of Call by userId  
    * Contact suport By: g.martinez.cabral@accenture.com
    * Created By: Luis Arturo Parra
    * @param String recordId
    * @return List<ONCALL_CallsVisits_Wrapper>
    */
    @AuraEnabled
    public static List<ONCALL_CallsVisits_Wrapper> getInfoCall(String recordId){
        
        List<ONCALL_CallsVisits_Wrapper> calls_visits = new List<ONCALL_CallsVisits_Wrapper>();
        
        //get Account country
        List<Account> account = new List<Account>();
        account = [Select Id, ONTAP__ExternalKey__c
                   From Account
                   Where Id =: recordId 
                   Limit 1];
        String account_country = account[0].ONTAP__ExternalKey__c.left(2);
        
        //get the number of records for call
        List<Calls_Visits_Component__mdt> configuration = new List <Calls_Visits_Component__mdt>();
        configuration = [Select Country__c, Calls_Number__c, Visits_Number__c
                         From Calls_Visits_Component__mdt
                         Where Country__c =: account_country];
        Integer calls_quantity = Integer.valueOf(configuration[0].Calls_Number__c);
        Integer visits_quantity = Integer.valueOf(configuration[0].Visits_Number__c);
        system.debug('calls_quantity: ' + calls_quantity);
        system.debug('visits_quantity: ' + visits_quantity);
        //Only calls of telesales
        //String oncall_calls_rt = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get(GlobalStrings.TELESALES).getRecordTypeId();
        //Using getRecordTypeInfosByDeveloperName to avoid extra translations
        String oncall_calls_rt = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.TELESALES_CALL_RECORDTYPE).getRecordTypeId();

        //get last calls
        List<ONCALL__Call__c> calls_history =([ 
            SELECT  Id,Name, ONCALL__Date__c, ONCALL__Call_Type__c, RecordTypeId,
            (SELECT Id, ONTAP__Amount_Total__c, RecordTypeId FROM ONCALL__Orders__r)
            FROM ONCALL__Call__c 
            WHERE ONCALL__POC__c =:recordId 
            AND RecordTypeId =:oncall_calls_rt 
            ORDER BY ONCALL__Date__c DESC NULLS LAST 
            LIMIT : calls_quantity]);
        system.debug('recordId: ' + recordId);
        system.debug('calls_history: ' + calls_history);
        System.debug('calls_history size: ' + calls_history.size());
        //get last visits
        List<Event> visits_history = ([SELECT Id, AccountId, Subject, ActivityDate 
                                                    FROM Event 
                                                    WHERE AccountId =:recordId AND
                                       					  Subject Like '%Visit%'
                                       				ORDER BY ActivityDate DESC 
                                                	NULLS LAST
                                                    LIMIT : visits_quantity]);
        System.debug('visits_history size: ' + visits_history.size());
        
        List<String> visits_history_id = new List<String>();
        for(Event eve : visits_history){
            visits_history_id.add(eve.Id);
        }
        
        //get related orders
        List<ONTAP__Order__c> related_orders = new List<ONTAP__Order__c>([Select Id, Name, ONTAP__Event_Id__c, RecordTypeId, ONTAP__Amount_Total__c
                                                                            From ONTAP__Order__c
                                                                            Where ONTAP__Event_Id__c in : visits_history_id]);
        
        Map<Event, List<ONTAP__Order__c>> event_orders = new Map<Event, List<ONTAP__Order__c>>();
        for(Event aux_event : visits_history){
            List<ONTAP__Order__c> orderList = new List<ONTAP__Order__c>();
            for(ONTAP__Order__c aux_order : related_orders){                   	
                if(aux_event.id == aux_order.ONTAP__Event_Id__c){
                	orderList.add(aux_order);    
                }                               
            }
            event_orders.put(aux_event, orderList);
        }                     
        
        //mix calls and visits
        for(ONCALL__Call__c call : calls_history){
            ONCALL_CallsVisits_Wrapper cv_wrap = new ONCALL_CallsVisits_Wrapper();
            cv_wrap.recordId = call.Id;
            cv_wrap.objectName = call.Name;
            cv_wrap.mean = 'ONCALL';
            cv_wrap.type = call.ONCALL__Call_Type__c;            
            cv_wrap.datum = call.ONCALL__Date__c;
            if(call.ONCALL__Orders__r != null){
                cv_wrap.amount = calcAmount(call.ONCALL__Orders__r, account_country);
            }
            
			calls_visits.add(cv_wrap);
        }
        
        for(Event visit : event_orders.keySet()){
            ONCALL_CallsVisits_Wrapper cv_wrap = new ONCALL_CallsVisits_Wrapper();
            cv_wrap.recordId = visit.Id;
            //cv_wrap.objectName = visit.Name;
            cv_wrap.mean = 'ONTAP';
            cv_wrap.datum = visit.ActivityDate;
            if(event_orders.get(visit) != null){
                cv_wrap.type = 'Ord';
                cv_wrap.amount = calcAmount(event_orders.get(visit), account_country);
            } else{
                cv_wrap.type = 'No Ord';
            } 
            
            calls_visits.add(cv_wrap);          
        }
        sorter(calls_visits, 0, calls_visits.size()-1);
        System.debug('calls_visits: ' + calls_visits);
        //falta acomodarlos por fecha
 
        return calls_visits;        
    }
    
    static Integer partition(List<ONCALL_CallsVisits_Wrapper> arr, Integer low, Integer high) 
    { 
        ONCALL_CallsVisits_Wrapper pivot = arr[high];
        Integer i = (low-1);  
        for (Integer j=low; j<high; j++) 
        {  
            if (arr[j].datum <= pivot.datum) 
            { 
                i++;
                
                ONCALL_CallsVisits_Wrapper temp = arr[i]; 
                arr[i] = arr[j]; 
                arr[j] = temp; 
            } 
        } 
  
        ONCALL_CallsVisits_Wrapper temp = arr[i+1]; 
        arr[i+1] = arr[high]; 
        arr[high] = temp; 
  
        return i+1; 
    } 

    static void sorter(List<ONCALL_CallsVisits_Wrapper> arr, Integer low, Integer high) 
    {
        if (low < high) 
        {
            Integer pi = partition(arr, low, high); 
  
            sorter(arr, low, pi-1); 
            sorter(arr, pi+1, high);
        }
    }

    
    /**
    * Method to calculate the amount of the orders of that day
    * Contact suport By: g.martinez.cabral@accenture.com
    * Created By: Luis Arturo Parra
    * @param List<ONTAP__Orders__c>
    * @return Decimal Amount
    */
    public static Decimal calcAmount(List<ONTAP__Order__c> orders_list, String account_country){              

        //get recordtypes of orders 
        List<String> orders_mdt_sum = new List<String>();
        List<String> orders_mdt_min = new List<String>();
        List<String> orders_rt_sum = new List<String>();
        List<String> orders_rt_min = new List<String>();
        
        List<Orders_Bh__mdt> orders_mtd = new List<Orders_Bh__mdt>();
        orders_mtd = [Select DeveloperName, Min__c, Sum__c, Country__c  
                      From Orders_Bh__mdt 
                      Where Country__c =: account_country                       		
                      Limit 1];
        orders_mdt_sum = orders_mtd[0].Sum__c.split(',');
        orders_mdt_min = orders_mtd[0].Min__c.split(',');
        
        for(String aux1 : orders_mdt_sum){
            orders_rt_sum.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux1).getRecordTypeId());
        }
        
        for(String aux2 : orders_mdt_min){
            orders_rt_min.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux2).getRecordTypeId());
        }
        
        Decimal orders_sum = 0;
        Decimal orders_min = 0;
        
        for(ONTAP__Order__c order : orders_list){
            for(String rt : orders_rt_sum){
                if(order.RecordTypeId == rt){
                    orders_sum += order.ONTAP__Amount_Total__c == NULL? 0 : order.ONTAP__Amount_Total__c;
                }
            }
        }
        
        for(ONTAP__Order__c order : orders_list){
            for(String rt : orders_rt_min){
                if(order.RecordTypeId == rt){
                    orders_min += order.ONTAP__Amount_Total__c == NULL? 0 : order.ONTAP__Amount_Total__c;
                }
            }
        }
        
        Decimal respuesta = orders_sum - orders_min;
        
        return (respuesta);
    }
       
    /**
    * Method to get all Information of the visits
    * Contact suport By: g.martinez.cabral@accenture.com
    * Created By: Luis Arturo Parra
    * @param String recordId
    * @return List<ONTAP__Tour_Visit__c>
    */    
    @AuraEnabled
    public static List<ONTAP__Tour_Visit__c> getInfoVisit(String recordId){
        
        List<ONTAP__Tour_Visit__c> visit =[SELECT Name FROM ONTAP__Tour_Visit__c WHERE ONTAP__Account__c =:recordId
                                           LIMIT 3];    
        
        return visit;        
    }

}