/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_CallAndVisits_CONTRO_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

@isTest
private class HONES_ONCALL_CallAndVisits_CONTRO_Test{
    
    /**
* Test method for set parameters and test all calls and visits
* Created By: Luis Arturo Parra Rosas
* Suport contact:  g.martinez.cabral@accenture.com    */
    
    
    @isTest static void test_getFlexibleDataById_UseCase1(){
        HONES_ONCALL_CallAndVisits_CONTRO obj01 = new HONES_ONCALL_CallAndVisits_CONTRO();
        Account acc = new Account(Name='Test');
        insert acc;
        ONCALL__Call__c call= new ONCALL__Call__c();
        call.Name='test';
        call.ONCALL__POC__c = acc.Id;
        insert call;
        HONES_ONCALL_CallAndVisits_CONTRO.getFlexibleDataById(call.Id);
    }
    /**
* Test to get all info calls by Userid    
*Created By: Luis Arturo Parra Rosas
* Suport contact:  g.martinez.cabral@accenture.com 
* Edited By: r.navarrete.lozano@accenture.com
* @param void
* @return void*/

    
    @isTest static void test_getInfoCall_UseCase1(){
        HONES_ONCALL_CallAndVisits_CONTRO obj01 = new HONES_ONCALL_CallAndVisits_CONTRO();
        Account account=new Account(ONTAP__ExternalKey__c='SVCG');
        account.Name='test';
        insert account;
        
        String oncall_calls_rt = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.TELESALES_CALL_RECORDTYPE).getRecordTypeId();
        List<ONCALL__Call__c> calls_history = new List<ONCALL__Call__c>();
        ONCALL__Call__c occ = new ONCALL__Call__c(Name='Test OnCallCall', RecordTypeId =oncall_calls_rt,ONCALL__POC__c=account.Id);
        calls_history.add(occ);
        insert calls_history;
        
        DateTime timeActivity = DateTime.now();
        List<Event> visits_history = new List<Event>();
        //Event new_evt1 = new Event(Description='Event Test 1',DurationInMinutes=15,ActivityDateTime=dateTime.parse('12/27/2009 12:00 AM'), Subject='The Visit 1',WhatId=account.Id);
        Event new_evt1 = new Event(Description='Event Test 1',DurationInMinutes=15,ActivityDateTime=timeActivity, Subject='The Visit 1',WhatId=account.Id);
		visits_history.add(new_evt1);
        insert visits_history;
        
        List<ONTAP__Order__c> orderList = new List<ONTAP__Order__c>();
        String recordSchema = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('SV_Finished_Product').getRecordTypeId();
        ONTAP__Order__c ord1 = new ONTAP__Order__c(ONTAP__Amount_Total__c=20);
        ONTAP__Order__c ord2 = new ONTAP__Order__c(ONTAP__Amount_Total__c=20, ONTAP__Event_Id__c=new_evt1.Id, ONCALL__OnCall_Account__c=account.Id ,  RecordTypeId=recordSchema);
        String recordSchema1 = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('SV_Return_of_Empties').getRecordTypeId();
        ONTAP__Order__c ord3 = new ONTAP__Order__c(ONTAP__Amount_Total__c=20, RecordTypeId=recordSchema1, ONCALL__OnCall_Account__c=account.Id, ONTAP__Event_Id__c=new_evt1.Id);
        orderList.add(ord1);
        orderList.add(ord2);
        orderList.add(ord3);
        insert orderList;
        
        
        
        HONES_ONCALL_CallAndVisits_CONTRO.getInfoCall(account.Id);
    }
    

    /**
* Test to get all info visit by Userid    
*Created By: Luis Arturo Parra Rosas
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_getInfoVisit_UseCase1(){
        HONES_ONCALL_CallAndVisits_CONTRO obj01 = new HONES_ONCALL_CallAndVisits_CONTRO();
        HONES_ONCALL_CallAndVisits_CONTRO.getInfoVisit('test data');
    }
    /**
* Test to get all info of Felxible Data Orders by Userid    
*Created By: Luis Arturo Parra Rosas
* Suport contact:  g.martinez.cabral@accenture.com 
* @param void
* @return void
*/
    @isTest static void test_getFlexibleDataOrder_UseCase1(){
        HONES_ONCALL_CallAndVisits_CONTRO obj01 = new HONES_ONCALL_CallAndVisits_CONTRO();
      
    }
   
    /**
     * Created By: r.navarrete.lozano@accenture.com
     * @param List<ONCALL_CallsVisits_Wrapper>
     * @return Integer
     */
    /*@isTest static void test_partition_UseCase1() {
        List<ONCALL_CallsVisits_Wrapper> arr = new List<ONCALL_CallsVisits_Wrapper>();
        Integer low = 5;
        Integer high = 20;
        
        //HONES_ONCALL_CallAndVisits_CONTRO.partition(arr, low, high);
    }*/
    
    /**
     * Method to test the amount of the orders of that day
     * Created By: r.navarrete.lozano@accenture.com
     * @param List<ONTAP__Orders__c>
     * @return Decimal Amount
     */
    @isTest static void test_calcAmount_UseCase1() {
        List<ONTAP__Order__c> orderList = new List<ONTAP__Order__c>();
        String recordSchema1 = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get('SV_Finished_Product').getRecordTypeId();
        ONTAP__Order__c ord1 = new ONTAP__Order__c(ONTAP__Amount_Total__c=20, RecordTypeId=recordSchema1);
        insert orderList;
        String accountCountry = 'SV';
        
        HONES_ONCALL_CallAndVisits_CONTRO.calcAmount(orderList, accountCountry);
    }
}