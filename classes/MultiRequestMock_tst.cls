/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Mock class to handle multi-request transactions

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    25/08/2017 Daniel Peñaloza            Created class
*******************************************************************************/

@isTest
public class MultiRequestMock_tst implements HttpCalloutMock {
    /**
     * Mapa de objetos Mock identificados por endpoint
     */
    protected Map<String, HttpCalloutMock> mapResponses;

    /**
     * Constructor sin parámetros
     */
    public MultiRequestMock_tst() {
        this.mapResponses = new Map<String, HttpCalloutMock>();
    }

    /**
     * Constructor con parámetros
     * @param  mapResponses Mapa de objetos mock de respuesta
     */
    public MultiRequestMock_tst(Map<String, HttpCalloutMock> mapResponses) {
        this.mapResponses = mapResponses;
    }

    /**
     * Respuesta falsa para el llamado a servicio
     * @param  request Objeto Request
     * @return         Objeto Response
     */
    public HttpResponse respond(HttpRequest request) {
        HttpCalloutMock responseMock = this.mapResponses.get(request.getEndpoint());

        if (responseMock != null) {
            return responseMock.respond(request);
        } else {
            throw new ResponseMockException('Invalid Mock');
        }
    }

    /**
     * Agregar un nuevo mock al mapa
     * @param endpoint     Endpoint identificador del objeto Mock
     * @param responseMock Objeto mock de respuesta
     */
    public void addRequestMock(String endpoint, HttpCalloutMock responseMock) {
        this.mapResponses.put(endpoint, responseMock);
    }

    /**
     * Excepción personalizada para generación de Mock de respuesta
     */
    public class ResponseMockException extends Exception {}
}