/* ----------------------------------------------------------------------------
* AB InBev :: OnTap - OnCall
* ----------------------------------------------------------------------------
* Clase: ONCALL_orderToJSON.apxc
* Versión: 1.0.0.0
* 
* Clase destinada a la creación de un objeto Orden a Json 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto      						Descripción
* 19/12/ 2018     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Creación de la clase  
*/
public class ONCALL_orderToJSON {
    /*
    * Method to generate JSON that of the order
    * Created By:heron.zurita@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Modify Date 2019-03-20
    * @params Id ids
    * @return void
    */
    public void createJson(Set<Id> setIdOrders){   
		system.debug('**** createJSON: (batch - scheduler) ****');
        Map<Id,ONTAP__Order__c> mapOrders = 
            new Map<Id,ONTAP__Order__c>([
                SELECT Id, ISSM_OriginText__c, Order_Reason__c, Name, ISSM_PaymentMethod__c, ONTAP__DeliveryDate__c, ONTAP__Amount_Total__c,
                ONTAP__Amount_Sub_Total_Product__c, ONTAP__SAPCustomerId__c, ONTAP__SalesOgId__c, ONTAP__DocumentationType__c 
                FROM ONTAP__Order__c 
                WHERE Id IN: setIdOrders]);
        
        Set<String> setSAPCIds = new Set<String>();
        for(ONTAP__Order__c ord : MapOrders.values())
            setSAPCIds.add(ord.ONTAP__SAPCustomerId__c);
        
        // Replaced ONTAP__SAP_Number__c with ONTAP__Codigo_del_cliente__c for searching on Account.
        // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
        // drs@avx
        List<Account> listAccount = 
            new List<Account>([
                SELECT Id, HONES_Term__c, ONTAP__SAP_Number__c, ONTAP__Codigo_del_cliente__c
                FROM Account 
                WHERE ONTAP__Codigo_del_cliente__c IN: setSAPCIds]);
        Map<String,String> payment_term = new Map<String, String>();
        if(!listAccount.isEmpty()){
            for(Account acc : listAccount){
                if(acc.HONES_Term__c != null){
                	payment_term.put(acc.ONTAP__SAP_Number__c, acc.HONES_Term__c.left(4));   
                }            	
            }            
        }
        /*
        List<Id> ordersId_List = new List<Id>();
        for(ONTAP__Order__c c:orders){
        ordersId_List.add(c.id);
        } 
        */
        
        List<Free_Good_Condcode__mdt> concode_free_good = new List <Free_Good_Condcode__mdt>();
        concode_free_good = [SELECT DeveloperName,Label,MasterLabel FROM Free_Good_Condcode__mdt Limit 1];
        String free_good = concode_free_good[0].MasterLabel;        
        
        List<ONTAP__Order_Item__c> totalOrderItems = 
            new List<ONTAP__Order_Item__c>([
                SELECT ONTAP__Discounts__c, ONCALL_pfn_reason__c, ONTAP__Cond_Applied__c, 
                ONTAP__CustomerOrder__c, ONTAP__ProductId__c, ONTAP__Product_ID2__c, POSEX__c, 
                ONTAP__ActualQuantity__c, ISSM_Uint_Measure_Code__c, Add_Empties__c, ONTAP__ItemProduct__c 
                FROM ONTAP__Order_Item__c 
                WHERE ONTAP__CustomerOrder__c IN: mapOrders.keySet()]);
        List<ONTAP__Order__c> totalOrderPartners = 
            new List<ONTAP__Order__c>([
                SELECT id, Order_Reason__c, ONTAP__SalesOgId__c, ISSM_PaymentMethod__c, ONTAP__OrderAccount__r.ONTAP__Credit_Condition__c, 
                ONTAP__OrderAccount__r.ONTAP__SalesOgId__c, ONTAP__OwnerSAPUserId__c, ONTAP__SAPCustomerId__c 
                FROM ONTAP__Order__c 
                WHERE Id IN: mapOrders.keySet()]);
        
        Map<Id, List<ONTAP__Order_Item__c>> orderItem_map = new Map<Id, List<ONTAP__Order_Item__c>>();
        for(ONTAP__Order_Item__c c : totalOrderItems){
            Id key2 = c.ONTAP__CustomerOrder__c;
            if(!orderItem_map.containsKey(key2)){
                orderItem_map.put(key2, new List<ONTAP__Order_Item__c>());
                orderItem_map.get(key2).add(c);
            }else{
                orderItem_map.get(key2).add(c);
            }
        }
        
        Map<Id, List<ONTAP__Order__c>> order_map = new Map<Id, List<ONTAP__Order__c>>();       
        for(ONTAP__Order__c i : totalOrderPartners){
            Id key = i.Id;
            if(!order_map.containsKey(key)){
                order_map.put(key,new List<ONTAP__Order__c>());
                order_map.get(key).add(i);
            }else{
                order_map.get(key).add(i);
            }
        }
        
        //List<HONES_ONCALL_ProductsJson> jsonOrder_list = new List<HONES_ONCALL_ProductsJson>();                        
        Map<String, JSONGenerator> mapOrderGen = new Map<String,JSONGenerator>();
        
        for(ONTAP__Order__c order : mapOrders.values()){
            System.debug('Order before JSON' + order);
            HONES_ONCALL_ProductsJson jsonOrder = new HONES_ONCALL_ProductsJson();
            // Construcción del JSON de Orden ===============================================================================================
            jsonOrder.iwHeader = new HONES_ONCALL_HeaderList();            
            jsonOrder.iwHeader.sfdcId = String.valueOf(order.Id);
            jsonOrder.iwHeader.orderType = order.ONTAP__DocumentationType__c;
            jsonOrder.iwHeader.salesOrg = String.valueOf(order.ONTAP__SalesOgId__c);
            if(order.ONTAP__DeliveryDate__c != null){
                Date theDate = date.newInstance(order.ONTAP__DeliveryDate__c.year(), order.ONTAP__DeliveryDate__c.month(), order.ONTAP__DeliveryDate__c.day());
                jsonOrder.iwHeader.deliveryDate = String.valueOf(theDate);
            }
            
            if(order.ISSM_PaymentMethod__c== GlobalStrings.CREDIT){
                jsonOrder.iwHeader.paymentMethod = payment_term.get(order.ONTAP__SAPCustomerId__c); // Acc. HONES_Term__c.left(4)
            }
            else if(order.ISSM_PaymentMethod__c== GlobalStrings.CASH){
                jsonOrder.iwHeader.paymentMethod = GlobalStrings.CASHVALUE; // 0001
            }
            
            jsonOrder.iwHeader.orderReason = String.valueOf(order.Order_Reason__c);
            jsonOrder.iwHeader.customerOrder = String.valueOf(order.ISSM_OriginText__c);
            
            List<ONTAP__Order_Item__c> prod = new List<ONTAP__Order_Item__c>(orderItem_map.get(order.Id));
            jsonOrder.items = new List<HONES_ONCALL_ItemsList>();
            jsonOrder.deals = new List<HONES_ONCALL_DealsList>();
            
            for(ONTAP__Order_Item__c j : prod){
			System.debug('Order Item before JSON: ' + j);
            System.debug(j.ONTAP__ItemProduct__c);    
                if(j.ONTAP__ItemProduct__c == null && String.valueOf(j.ONTAP__Cond_Applied__c) != null){ // ES UN DEAL?
                    
                    HONES_ONCALL_DealsList deal = new HONES_ONCALL_DealsList();
                    
                    //if(String.valueOf(j.ONTAP__Cond_Applied__c) != null) deal.condcode = String.valueOf(j.ONTAP__Cond_Applied__c);
                    //else deal.condcode = free_good;
                    deal.condcode = String.valueOf(j.ONTAP__Cond_Applied__c);
                    deal.amount = 	String.valueOf(j.ONTAP__Discounts__c);
                    deal.posex = 	String.valueOf(j.POSEX__c);
                    deal.pronr = 	String.valueOf(j.ONTAP__ProductId__c);
                    System.debug('Deal before JSON: ' + deal);
                    jsonOrder.deals.add(deal);
                    
                }else{
                    //aqui hay que validar la parte de PFN - PBE para saber si se manda o no el envase
                    //if(order.recordtype == PFN || order.recordtype == PBE) -> no se añaden al json
                    //else se añaden
                    HONES_ONCALL_ItemsList item = new HONES_ONCALL_ItemsList();
                    if(j.ONTAP__ProductId__c != null) {
                        item.sku = String.valueOf(j.ONTAP__ProductId__c); 
                    }else{
                        item.sku = String.valueOf(j.ONTAP__Product_ID2__c);
                    }
                    item.quantity = String.valueOf(j.ONTAP__ActualQuantity__c);
                    if(j.ISSM_Uint_Measure_Code__c == GlobalStrings.BOTTLE || j.ISSM_Uint_Measure_Code__c == GlobalStrings.UNIT_MEASURE_UNITS){
                        item.measurecode = GlobalStrings.UNIT_MEASURE_UNITS;
                    }
                    else{
                        item.measurecode = '';   
                    }
                    item.empties = String.valueOf(j.Add_Empties__c);                    
                    item.posex = String.valueOf(j.POSEX__c);
                    item.orderReason = j.ONCALL_pfn_reason__c;
                    System.debug('item before JSON: ' + item);
                    jsonOrder.items.add(item);
                    
                }
            }
            
            List<ONTAP__Order__c> partner = new List<ONTAP__Order__c>(order_map.get(order.Id));
            ONTAP__Order__c partnerObj = partner.get(0);
            
            jsonOrder.partner = new List<HONES_ONCALL_PartnerList>();       	
            
            HONES_ONCALL_PartnerList part = new HONES_ONCALL_PartnerList();
            part.role = GlobalStrings.ORDER_ACCOUNT_SAPROLE;
            part.numb = String.valueOf(partnerObj.ONTAP__SAPCustomerId__c);
            part.itmnumber = '';
            jsonOrder.partner.add(part); 
            /* Removed drs@avx --- No need for SAP User Id in Order Creation
             * 
            HONES_ONCALL_PartnerList part2 = new HONES_ONCALL_PartnerList();
            part2.role = GlobalStrings.ORDER_SALER_SAPROLE;
            part2.numb = String.valueOf(partnerObj.ONTAP__OwnerSAPUserId__c);
            part2.itmnumber = '';
            jsonOrder.partner.add(part2);
            *
			*/
            JSONGenerator generator = JSON.createGenerator(true);
            generator.writeObject(jsonOrder);
            System.debug('jsonOrder ' + jsonOrder);
            mapOrderGen.put(String.valueOf(order.Id), generator);
            //ONCALL_SendJson st = new ONCALL_SendJson();                        
        }
        //ONCALL_SendJson.send_Json(generator.getAsString().replaceAll('\\n',''), String.valueOf(order.Id), );
        ONCALL_SendJson.send_Json(mapOrderGen);
    }
    
        /*
    * Method to generate JSON that of the order
    * Created By:heron.zurita@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Modify Date 2019-05-07
    * @params Id ids
    * @return void
    */
    @future(callout=true)
    public static void createJsonOnCall(Set<Id> setIdOrders){   
        system.debug('**** createJSON: (callout=true) ****');
        Map<Id,ONTAP__Order__c> mapOrders = 
            new Map<Id,ONTAP__Order__c>([
                SELECT Id, ISSM_OriginText__c, Order_Reason__c, Name, ISSM_PaymentMethod__c, ONTAP__DeliveryDate__c, ONTAP__Amount_Total__c,
                ONTAP__Amount_Sub_Total_Product__c, ONTAP__SAPCustomerId__c, ONTAP__SalesOgId__c, ONTAP__DocumentationType__c
                FROM ONTAP__Order__c 
                WHERE Id IN: setIdOrders]);
        
        Set<String> setSAPCIds = new Set<String>();
        for(ONTAP__Order__c ord : MapOrders.values()){
            setSAPCIds.add(ord.ONTAP__SAPCustomerId__c);
        }
        
        
        // Replaced ONTAP__SAP_Number__c with ONTAP__Codigo_del_cliente__c for searching on Account.
        // ONTAP__SAP_Number__c is not indexed in current ONTAP Package and will cause a non-selective query with +200,000 records
        // drs@avx
        List<Account> listAccount =  
            new List<Account>([
                SELECT Id, HONES_Term__c, ONTAP__SAP_Number__c, ONTAP__Codigo_del_cliente__c
                FROM Account 
                WHERE ONTAP__Codigo_del_cliente__c IN: setSAPCIds]);
        Map<String,String> payment_term = new Map<String, String>();
        if(!listAccount.isEmpty()){
            for(Account acc : listAccount){
                if(acc.HONES_Term__c != null){
                	payment_term.put(acc.ONTAP__SAP_Number__c, acc.HONES_Term__c.left(4));   
                }            	
            }            
        }        
        
        List<Free_Good_Condcode__mdt> concode_free_good = new List <Free_Good_Condcode__mdt>();
        concode_free_good = [SELECT DeveloperName,Label,MasterLabel FROM Free_Good_Condcode__mdt Limit 1];
        String free_good = concode_free_good[0].MasterLabel;        
        
        List<ONTAP__Order_Item__c> totalOrderItems = 
            new List<ONTAP__Order_Item__c>([
                SELECT ONTAP__Discounts__c, ONCALL_pfn_reason__c, ONTAP__Cond_Applied__c,
                ONTAP__CustomerOrder__c, ONTAP__ProductId__c, ONTAP__Product_ID2__c, POSEX__c,
                ONTAP__ActualQuantity__c, ISSM_Uint_Measure_Code__c,Add_Empties__c, ONTAP__ItemProduct__c 
                FROM ONTAP__Order_Item__c 
                WHERE ONTAP__CustomerOrder__c IN: mapOrders.keySet()]);
        List<ONTAP__Order__c> totalOrderPartners = 
            new List<ONTAP__Order__c>([
                SELECT id, Order_Reason__c, ONTAP__SalesOgId__c, ISSM_PaymentMethod__c, ONTAP__OrderAccount__r.ONTAP__Credit_Condition__c, 
                ONTAP__OrderAccount__r.ONTAP__SalesOgId__c, ONTAP__OwnerSAPUserId__c, ONTAP__SAPCustomerId__c 
                FROM ONTAP__Order__c 
                WHERE Id IN: mapOrders.keySet()]);
        
        Map<Id, List<ONTAP__Order_Item__c>> orderItem_map = new Map<Id, List<ONTAP__Order_Item__c>>();
        for(ONTAP__Order_Item__c c : totalOrderItems){
            Id key2 = c.ONTAP__CustomerOrder__c;
            if(!orderItem_map.containsKey(key2)){
                orderItem_map.put(key2, new List<ONTAP__Order_Item__c>());
                orderItem_map.get(key2).add(c);
            }else{
                orderItem_map.get(key2).add(c);
            }
        }
        
        Map<Id, List<ONTAP__Order__c>> order_map = new Map<Id, List<ONTAP__Order__c>>();       
        for(ONTAP__Order__c i : totalOrderPartners){
            Id key = i.Id;
            if(!order_map.containsKey(key)){
                order_map.put(key,new List<ONTAP__Order__c>());
                order_map.get(key).add(i);
            }else{
                order_map.get(key).add(i);
            }
        }
                       
        Map<String, JSONGenerator> mapOrderGen = new Map<String,JSONGenerator>();
        
        for(ONTAP__Order__c order : mapOrders.values()){
            System.debug('order before JSON: ' + order);
            HONES_ONCALL_ProductsJson jsonOrder = new HONES_ONCALL_ProductsJson();
            
            jsonOrder.iwHeader = new HONES_ONCALL_HeaderList();            
            jsonOrder.iwHeader.sfdcId = String.valueOf(order.Id);
            jsonOrder.iwHeader.orderType = order.ONTAP__DocumentationType__c;
            jsonOrder.iwHeader.salesOrg = String.valueOf(order.ONTAP__SalesOgId__c);
            if(order.ONTAP__DeliveryDate__c != null){
                Date theDate = date.newInstance(order.ONTAP__DeliveryDate__c.year(), order.ONTAP__DeliveryDate__c.month(), order.ONTAP__DeliveryDate__c.day());
                jsonOrder.iwHeader.deliveryDate = String.valueOf(theDate);
            }
            
            if(order.ISSM_PaymentMethod__c== GlobalStrings.CREDIT){
                jsonOrder.iwHeader.paymentMethod = payment_term.get(order.ONTAP__SAPCustomerId__c); // Account - HONES TERM LEFT 4
            }
            else if(order.ISSM_PaymentMethod__c== GlobalStrings.CASH){
                jsonOrder.iwHeader.paymentMethod = GlobalStrings.CASHVALUE; // 0001
            }
            
            jsonOrder.iwHeader.orderReason = String.valueOf(order.Order_Reason__c);
            jsonOrder.iwHeader.customerOrder = String.valueOf(order.ISSM_OriginText__c);
            
            List<ONTAP__Order_Item__c> prod = new List<ONTAP__Order_Item__c>(orderItem_map.get(order.Id));
            jsonOrder.items = new List<HONES_ONCALL_ItemsList>();
            jsonOrder.deals = new List<HONES_ONCALL_DealsList>();
            
            for(ONTAP__Order_Item__c j : prod){
			System.debug('PROD: Item or Deal ' + j);
            System.debug(j.ONTAP__ItemProduct__c);    
                if(j.ONTAP__ItemProduct__c == null && String.valueOf(j.ONTAP__Cond_Applied__c) != null){
                    
                    HONES_ONCALL_DealsList deal = new HONES_ONCALL_DealsList();
                    
                    //if(String.valueOf(j.ONTAP__Cond_Applied__c) != null) deal.condcode = String.valueOf(j.ONTAP__Cond_Applied__c);
                    //else deal.condcode = free_good;
                    deal.condcode = String.valueOf(j.ONTAP__Cond_Applied__c);
                    deal.amount = String.valueOf(j.ONTAP__Discounts__c);
                    deal.posex = String.valueOf(j.POSEX__c);
                    deal.pronr = String.valueOf(j.ONTAP__ProductId__c);
                    System.debug('Deal before JSON: ' + deal);
                    jsonOrder.deals.add(deal);
                    
                }else{
                    //aqui hay que validar la parte de PFN - PBE para saber si se manda o no el envase
                    //if(order.recordtype == PFN || order.recordtype == PBE) -> no se añaden al json
                    //else se añaden
                    HONES_ONCALL_ItemsList item = new HONES_ONCALL_ItemsList();
                    if(j.ONTAP__ProductId__c != null) {
                        item.sku = String.valueOf(j.ONTAP__ProductId__c); 
                    }else{
                        item.sku = String.valueOf(j.ONTAP__Product_ID2__c);
                    }
                    item.quantity = String.valueOf(j.ONTAP__ActualQuantity__c);
                    if(j.ISSM_Uint_Measure_Code__c == GlobalStrings.BOTTLE || j.ISSM_Uint_Measure_Code__c == GlobalStrings.UNIT_MEASURE_UNITS){ // Bottle || UN
                        item.measurecode = GlobalStrings.UNIT_MEASURE_UNITS; // UN
                    }
                    else{
                        item.measurecode = '';   
                    }
                    item.empties = String.valueOf(j.Add_Empties__c);                    
                    item.posex = String.valueOf(j.POSEX__c);
                    item.orderReason = j.ONCALL_pfn_reason__c;
                    System.debug('Item before JSON: ' + item);
                    jsonOrder.items.add(item);
                    
                }
            }
            
            List<ONTAP__Order__c> partner = new List<ONTAP__Order__c>(order_map.get(order.Id));
            ONTAP__Order__c partnerObj = partner.get(0);
            
            jsonOrder.partner = new List<HONES_ONCALL_PartnerList>();       	
            
            HONES_ONCALL_PartnerList part = new HONES_ONCALL_PartnerList();
            part.role = GlobalStrings.ORDER_ACCOUNT_SAPROLE;
            part.numb = String.valueOf(partnerObj.ONTAP__SAPCustomerId__c);
            part.itmnumber = '';
            jsonOrder.partner.add(part); 
            
            /* Removed drs@avx --- No need for SAP User Id in Order Creation
             * 
            HONES_ONCALL_PartnerList part2 = new HONES_ONCALL_PartnerList();
            part2.role = GlobalStrings.ORDER_SALER_SAPROLE;
            part2.numb = String.valueOf(partnerObj.ONTAP__OwnerSAPUserId__c);
            part2.itmnumber = '';
            jsonOrder.partner.add(part2);
			*
			*/
            
            JSONGenerator generator = JSON.createGenerator(true);
            generator.writeObject(jsonOrder);
            System.debug('jsonOrder ' + jsonOrder);
            mapOrderGen.put(String.valueOf(order.Id), generator);                   
        }        
        ONCALL_SendJson.send_Json(mapOrderGen);
    }
}