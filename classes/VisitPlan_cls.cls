/****************************************************************************************************
    General Information
    -------------------
    author: Joseph Ceron
    email: jceron@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger Class for validate account choosen have 

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       10-07-2017        Joseph Ceron (JC)            Creation Class
****************************************************************************************************/
public with sharing class VisitPlan_cls {


    public VisitPlan_cls() {
    }

    public void ProcessVisitPLan(List<VisitPlan__c> LstVisitPlan)
    {
        Map<Id,List<VisitPlan__c>> MapOrganice = DateOrganice(LstVisitPlan);
        Map<Id,List<VisitPlan__c>> MapQryVisit = Qry_VisitPlansRoute(LstVisitPlan);
        for(VisitPlan__c oVistPlanNew : LstVisitPlan)
        {
            Boolean BLnErrorInsert = false;
            if(MapQryVisit.containsKey(oVistPlanNew.Route__c) && MapQryVisit.get(oVistPlanNew.Route__c).size() != 0)
            {
                for(VisitPlan__c oVisExit : MapQryVisit.get(oVistPlanNew.Route__c))
                {
                    if((oVistPlanNew.Monday__c == oVisExit.Monday__c && oVistPlanNew.Monday__c == true) ||
                       (oVistPlanNew.Tuesday__c == oVisExit.Tuesday__c && oVistPlanNew.Tuesday__c == true)||
                       (oVistPlanNew.Wednesday__c == oVisExit.Wednesday__c && oVistPlanNew.Wednesday__c == true)||
                       (oVistPlanNew.Thursday__c == oVisExit.Thursday__c && oVistPlanNew.Thursday__c == true)||
                       (oVistPlanNew.Friday__c == oVisExit.Friday__c && oVistPlanNew.Friday__c == true)||
                       (oVistPlanNew.Saturday__c == oVisExit.Saturday__c && oVistPlanNew.Saturday__c == true)||
                       (oVistPlanNew.Sunday__c == oVisExit.Sunday__c && oVistPlanNew.Sunday__c == true))
                    {
                        if(!ValidateRecordDate(oVistPlanNew,oVisExit) )
                        {
                            oVistPlanNew.addError(label.NoPlanVisit);
                            BLnErrorInsert = true;
                        }
                    }
                }
            }
            else
            {
                MapQryVisit.put(oVistPlanNew.Route__c,new List<VisitPlan__c>{oVistPlanNew});
            }
            if(!BLnErrorInsert)
                MapQryVisit.get(oVistPlanNew.Route__c).add(oVistPlanNew);
        }

    }

    public void ProcessVisitPLanUpdate(List<VisitPlan__c> LstVisitPlan, List<VisitPlan__c> LstVisitPlanold)
    {
        Map<Id,List<VisitPlan__c>> MapOrganice = DateOrganice(LstVisitPlan);
        Map<Id,List<VisitPlan__c>> MapQryVisit = Qry_VisitPlansRouteUpda(LstVisitPlan);
        for(VisitPlan__c oVistPlanNew : LstVisitPlan)
        {
            Boolean BLnErrorInsert = false;
            if(MapQryVisit.containsKey(oVistPlanNew.Route__c) && MapQryVisit.get(oVistPlanNew.Route__c).size() != 0)
            {
                for(VisitPlan__c oVisExit : MapQryVisit.get(oVistPlanNew.Route__c))
                {
                    if((oVistPlanNew.Monday__c == oVisExit.Monday__c && oVistPlanNew.Monday__c == true) ||
                       (oVistPlanNew.Tuesday__c == oVisExit.Tuesday__c && oVistPlanNew.Tuesday__c == true)||
                       (oVistPlanNew.Wednesday__c == oVisExit.Wednesday__c && oVistPlanNew.Wednesday__c == true)||
                       (oVistPlanNew.Thursday__c == oVisExit.Thursday__c && oVistPlanNew.Thursday__c == true)||
                       (oVistPlanNew.Friday__c == oVisExit.Friday__c && oVistPlanNew.Friday__c == true)||
                       (oVistPlanNew.Saturday__c == oVisExit.Saturday__c && oVistPlanNew.Saturday__c == true)||
                       (oVistPlanNew.Sunday__c == oVisExit.Sunday__c && oVistPlanNew.Sunday__c == true))
                    {
                        if(!ValidateRecordDate(oVistPlanNew,oVisExit) )
                        {
                            oVistPlanNew.addError(label.NoPlanVisit);
                            BLnErrorInsert = true;
                        }
                    }
                }
            }
            else
            {
                MapQryVisit.put(oVistPlanNew.Route__c,new List<VisitPlan__c>{oVistPlanNew});
            }
            if(!BLnErrorInsert)
                MapQryVisit.get(oVistPlanNew.Route__c).add(oVistPlanNew);
        }

    }

    public Map<Id,List<VisitPlan__c>> DateOrganice(List<VisitPlan__c> LstVisitPlan)
    {
        Map<Id,List<VisitPlan__c>> MapOrganice = new Map<Id,List<VisitPlan__c>>();
        for(VisitPlan__c oVisit : LstVisitPlan)
        {
            if(MapOrganice.containsKey(oVisit.Route__c))
            {
                MapOrganice.get(oVisit.Route__c).add(oVisit);
            }
            else
            {
                MapOrganice.put(oVisit.Route__c,new List<VisitPlan__c>{oVisit});
            }
        }
        return MapOrganice;
    }

    public Map<Id,List<VisitPlan__c>> Qry_VisitPlansRoute(List<VisitPlan__c> LstVisitPlan)
    {
        Set<Id> setIdRoute = new Set<Id>();
        for(VisitPlan__c oVisit : LstVisitPlan)
        {
            setIdRoute.add(oVisit.Route__c);
        }
        Map<Id,List<VisitPlan__c>> MapMpVisitaPlanes = new Map<Id,List<VisitPlan__c>>();
        List<VisitPlan__c> LstVisitPlanQry = [    select Id,
                                                        Name,
                                                        VisitPlanType__c,
                                                        ExecutionDate__c,
                                                        EffectiveDate__c,
                                                        Monday__c,
                                                        Tuesday__c,
                                                        Wednesday__c,
                                                        Thursday__c,
                                                        Friday__c,
                                                        Saturday__c,
                                                        Sunday__c,
                                                        Route__c
                                                    From 
                                                        VisitPlan__c
                                                    where
                                                        Route__c in : setIdRoute
                                                ];
        for(VisitPlan__c oVisitQry : LstVisitPlanQry)
        {

            if(MapMpVisitaPlanes.containsKey(oVisitQry.Route__c))
            {
                MapMpVisitaPlanes.get(oVisitQry.Route__c).add(oVisitQry);
            }
            else
            {
                MapMpVisitaPlanes.put(oVisitQry.Route__c,new List<VisitPlan__c>{oVisitQry});
            }
        }
        return MapMpVisitaPlanes;
    }

    public Map<Id,List<VisitPlan__c>> Qry_VisitPlansRouteUpda(List<VisitPlan__c> LstVisitPlan)
    {
        Set<Id> setIdRoute = new Set<Id>();
        Set<Id> setIdVisitPlan = new Set<Id>();

        for(VisitPlan__c oVisit : LstVisitPlan)
        {
            setIdRoute.add(oVisit.Route__c);
            setIdVisitPlan.add(oVisit.Id);
        }
        Map<Id,List<VisitPlan__c>> MapMpVisitaPlanes = new Map<Id,List<VisitPlan__c>>();
        List<VisitPlan__c> LstVisitPlanQry = [    select Id,
                                                        Name,
                                                        VisitPlanType__c,
                                                        ExecutionDate__c,
                                                        EffectiveDate__c,
                                                        Monday__c,
                                                        Tuesday__c,
                                                        Wednesday__c,
                                                        Thursday__c,
                                                        Friday__c,
                                                        Saturday__c,
                                                        Sunday__c,
                                                        Route__c
                                                    From 
                                                        VisitPlan__c
                                                    where
                                                        Route__c in : setIdRoute
                                                    ANd
                                                        Id not in : setIdVisitPlan
                                                ];
        for(VisitPlan__c oVisitQry : LstVisitPlanQry)
        {

            if(MapMpVisitaPlanes.containsKey(oVisitQry.Route__c))
            {
                MapMpVisitaPlanes.get(oVisitQry.Route__c).add(oVisitQry);
            }
            else
            {
                MapMpVisitaPlanes.put(oVisitQry.Route__c,new List<VisitPlan__c>{oVisitQry});
            }
        }
        return MapMpVisitaPlanes;
    }

    public Boolean ValidateRecordDate(VisitPlan__c oVistPlanNew, VisitPlan__c oVisitPlanExist)
    {
        if((oVistPlanNew.ExecutionDate__c < oVisitPlanExist.EffectiveDate__c && oVistPlanNew.EffectiveDate__c < oVisitPlanExist.EffectiveDate__c && oVistPlanNew.ExecutionDate__c < oVisitPlanExist.ExecutionDate__c && oVistPlanNew.EffectiveDate__c < oVisitPlanExist.ExecutionDate__c) || 
            (oVistPlanNew.ExecutionDate__c > oVisitPlanExist.ExecutionDate__c && oVistPlanNew.EffectiveDate__c > oVisitPlanExist.ExecutionDate__c && oVistPlanNew.ExecutionDate__c > oVisitPlanExist.EffectiveDate__c && oVistPlanNew.EffectiveDate__c > oVisitPlanExist.EffectiveDate__c))
            return true;
        else
            return false;
    }
    
    /*Andres Garrido*/
    public void manageAggregateAccount(map<Id, VisitPlan__c> mapVisitPlan){
    	map<Id, AccountByVisitPlan__c> mapAccByVP = new map<Id, AccountByVisitPlan__c>([
    		Select 	Id, VisitPlan__c
    		From	AccountByVisitPlan__c
    		Where	VisitPlan__c = :mapVisitPlan.keySet()
    	]);
    	
    	AccountByVisitPlanOperations_cls.manageAggregateAccount(mapAccByVP, false);
    }

}