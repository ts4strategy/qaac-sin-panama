/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONCALL_SendJson.apxc
 * Versión: 1.0.0.0
 * 
 * Clase destinada para el envío de órdenes a SAP
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 19/12/ 2018     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Creación de la clase  
 */
public class ONCALL_SendJson implements Database.AllowsCallouts{
	
    /**
    * Method that modify deserealize the JSON 
    * Created By: heron.zurita@accenture.com
    * @param String json_order,String orderID
    * @return void
    */
    //@Future(callout=true)
   /* public static void send_Json(String json_order, String orderID){
        string json = json_order;
        string orderSFDCID = orderID;                            
        
        List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt WHERE DeveloperName = 'Create_SAP_Order'];
        
        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint(endpoint[0].URL__c);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json;charset=utf-8');
        httpReq.setHeader('bearer', endpoint[0].Token__c);
        httpReq.setBody(json);
        System.debug('json sent: ' +json);
        HTTPResponse authresp = new HTTPResponse();
        authresp = h.send(httpReq);
        System.debug('response status: ' +authresp);
        
        if(authresp.getStatusCode() == 200){
            System.debug('Success: ' + authresp.getBody());
            
            ONCALL_SAP_OrderResponse listToSave = ONCALL_SAP_OrderResponse.parse(authresp.getBody());
            
            ONTAP__Order__c order = new  ONTAP__Order__c();
            order.id = orderSFDCID;
			order.ONCALL__SAP_Order_Number__c = listToSave.id;
            if(listToSave.etReturn!=null){
                if(listToSave.etReturn.Item.type_Z!=null&&listToSave.etReturn.Item.message!=null){
                    order.ONCALL__SAP_Order_Response__c = listToSave.etReturn.Item.type_Z + ' - ' + listToSave.etReturn.Item.message;
                }
            }            
			
            order.ONCALL__OnCall_Status__c = 'Closed';
            System.debug('order to update: ' + order);
            update order;
            
        } else{
            System.debug('Connection Error message: ' + authresp.getBody());
        }
    }*/
    
    /**
    * Method that modify deserealize the JSON 
    * Created By: heron.zurita@accenture.com
    * Modify By: gabriel.e.garcia@accenture.com
    * Modify Date 2019-03-20
    * @param String json_order,String orderID
    * @return void
    */
    public static void send_Json(Map<String, JSONGenerator> mapOrderGen){
        
        List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt WHERE DeveloperName = 'Create_SAP_Order'];
        List<ONTAP__Order__c> listOrder = new List<ONTAP__Order__c>();            
        for(String key : mapOrderGen.keySet()){
            String json = mapOrderGen.get(key).getAsString().replaceAll('\\n','');        	   
            
            ONTAP__Order__c order = new  ONTAP__Order__c(ONCALL__SAP_Order_Response__c='');                
            order.id = key;
            order.ISSM_OrderJSONRequest__c = json; //Added 2019-AUG-05 - drs@avx. For tracing purposes, we store the JSON Request sent to the CreateOrder webservice
            
            Http h = new Http();
            HttpRequest httpReq = new HttpRequest();
            system.debug('endpoint.URL: ' + endpoint[0].URL__c);
            httpReq.setEndpoint(endpoint[0].URL__c);
            httpReq.setMethod('POST');
            httpReq.setHeader('Content-Type','application/json;charset=utf-8');
            httpReq.setHeader('bearer', endpoint[0].Token__c);
            httpReq.setBody(json);
            System.debug('ORDER CREATE json request : ' +json);
            HTTPResponse authresp = new HTTPResponse();
            try
            {
            	authresp = h.send(httpReq);
                System.debug('response status: ' +authresp);                        
            
                if(authresp.getStatusCode() == 200){
                    System.debug('ORDER CREATE json response : ' + authresp.getBody());
                    order.ISSM_OrderJSONResponse__c = authresp.getBody(); //Added 2019-AUG-05 - drs@avx. For tracing purposes, we store the JSON response we receive from the CreateOrder webservice
                    ONCALL_SAP_OrderResponse listToSave = ONCALL_SAP_OrderResponse.parse(authresp.getBody());                                
                    
                    if(listToSave.orderId != null && listToSave.orderId != ''){
                        order.ONCALL__SAP_Order_Number__c = listToSave.orderId;
                        for(ONCALL_SAP_OrderResponse.cls_etReturn etR: listToSave.etReturn ){ // ADDED, now we receive a list of messages drs@avx
                            if (etR.message.contains(order.ONCALL__SAP_Order_Number__c)) {
                                order.ONCALL__SAP_Order_Response__c = etR.type_Z + ' - ' + etR.message;
                            }
                        }
                    
                    } else { // we store every message received. Watch for 255 limit drs@avx
                        for(ONCALL_SAP_OrderResponse.cls_etReturn etR: listToSave.etReturn ){ // ADDED, now we receive a list of messages drs@avx
							order.ONCALL__SAP_Order_Response__c += etR.type_Z + ' - ' + etR.message + '; ';
                        }
                    }
                    /*if(String.isEmpty(order.ONCALL__SAP_Order_Number__c)){
                        order.ONCALL__OnCall_Status__c = 'Draft';
                    }*/
                    
                    /*if(listToSave.etReturn[0].type_Z!=null && listToSave.etReturn[0].message!=null){
					order.ONCALL__SAP_Order_Response__c = listToSave.etReturn[0].type_Z + ' - ' + listToSave.etReturn[0].message;
					}*/ //removed because now we receive a list of messages // drs@avx
                                
                    //order.ONCALL__OnCall_Status__c = 'Closed';
                    System.debug('Order to update: ' + order);
                    listOrder.add(order);
                    //update order;
                } else { // Different from a 200 OK
                    order.ONCALL__SAP_Order_Response__c = 
                        authresp != null && authresp.getStatus() != null 
                        ? authresp.getStatus() 
                        : 'Error to process transaction';
                    listOrder.add(order);
                    System.debug('Connection Error message: ' + authresp.getBody());
                }
            }
            catch (Exception ex)
            {
                System.debug('Exception - Error en peticion: ' + ex);
                order.ONCALL__SAP_Order_Response__c = 'Error: ' + ex.getTypeName() + ', ' + ex.getMessage() ;    
                listOrder.add(order);
            }                        
        }
        
        if(!listOrder.isEmpty()) 
        {
            ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
            update listOrder;
        }
    }
    
}