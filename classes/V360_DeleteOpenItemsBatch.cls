/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteOpenItemsBatch.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteOpenItemsBatch implements Database.Batchable<sObject>{
    // Persistent variables

    global Boolean hasErrors {get; private set;}
	/**
    * Clean the class.
    * @author: c.leal.beltran@accenture.com 
    */
    global V360_DeleteOpenItemsBatch()
    {  
        this.hasErrors = false;
    }
    
    /**
   * Execute the Query for the account open items objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return Database.QueryLocator
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Id V360_OpenItemRT = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_V360_RECORDTYPE_NAME).getRecordTypeId();
        
        system.debug('Processing start method'); 
        // Query string for batch Apex
        String query = ''; 
        Date todayDate = System.Today();
        if(System.Test.isRunningTest()){
           query += 'SELECT Id FROM ONTAP__OpenItem__c WHERE RecordTypeId=:V360_OpenItemRT'; 
        }else{
           query += 'SELECT Id FROM ONTAP__OpenItem__c WHERE RecordTypeId=:V360_OpenItemRT AND LastModifiedDate<:todayDate';
        }
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
    /**
   * Method for delete all the account open items objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<SObject> objlist
   * @return void
   */
    global void execute(Database.BatchableContext d, List<SObject> objlist){
        delete objlist;
        DataBase.emptyRecycleBin(objlist);
    }
    
    /**
   * Method finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}