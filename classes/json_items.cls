/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: json_items.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Herón Zurita           Creation of model.
*/

/**
    * Attributes for model ItemObj
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */ 

public class json_items {
    
   		public String sku;
		public String quantity;
		public String materialProduct;
    	public String unit;
    	public Boolean selectedbyUser;
}