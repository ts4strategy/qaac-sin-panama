/* ----------------------------------------------------------------------------
 * AB InBev :: 360 ViewM
 * ----------------------------------------------------------------------------
 * Clase: V360_AccountTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 * 24/04/2019     Jose Luis Vargas 		  Generate Method for test prospect generation
 */
@isTest
public class V360_AccountTriggerHandler_Test 
{   
   /**
    * Test method for create and update accounts
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @testsetup
    public static void AccountCreateAndUpdate(){
        
        Test.startTest();
        V360_LibraryFieldsList__c libraryObj = new V360_LibraryFieldsList__c(Name='Name',V360_Code__c='Test',V360_Field__c='Name',
                                                                             V360_Value__c='2'); 
        insert libraryObj;
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        
        User u = [SELECT ID FROM User LIMIT 1];
        
        List<Account> listAccount = new List<Account>();
        
        /*
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId, ONCALL__Country__c='HN',type=GlobalStrings.PRESALES);
        insert og;
        
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id, ONCALL__Country__c='SV',type=GlobalStrings.PRESALES);
        insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id, ONCALL__Country__c='HN',type=GlobalStrings.PRESALES);
        insert cg;
        Account sg =  new Account(Name='SG',ONTAP__SAP_Number__c='T00001',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, 
                                  ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE,ParentId=cg.Id, ONCALL__Country__c='HN',type=GlobalStrings.PRESALES);
        insert sg;
		*/
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId, ONCALL__Country__c='HN',type=GlobalStrings.PRESALES, ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE);
        listAccount.add(og);
        //insert og;
        
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id, ONCALL__Country__c='SV',type=GlobalStrings.PRESALES, ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE);
        listAccount.add(off);
        //insert off;
        
        //Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id, ONCALL__Country__c='HN',type=GlobalStrings.PRESALES, ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE);
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=off.Id, ONCALL__Country__c='SV', ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE);
        listAccount.add(cg);
        //insert cg;
        
        Account sg =  new Account(Name='SG',ONTAP__SAP_Number__c='T00001',ONTAP__ExternalKey__c='SG',RecordTypeId=clientGroupdevRecordTypeId, 
                                  ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE,ParentId=cg.Id, ONCALL__Country__c='HN');
        listAccount.add(sg);
        //insert sg;
        
        Account szz =  new Account(Name='SZ',V360_CustomerReferenceNumber__c = sg.ONTAP__SAP_Number__c,ONTAP__ExternalKey__c='SZ',
                                   ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE,RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id, ONCALL__Country__c='SV',
                                   type=GlobalStrings.PRESALES);
        listAccount.add(szz);
        //insert szz;
		
        
        Account ab =  new Account(Name='AB',V360_CustomerReferenceNumber__c = sg.ONTAP__SAP_Number__c,ONTAP__ExternalKey__c='',
                                   ONTAP__SalesOgId__c =GlobalStrings.HONDURAS_ORG_CODE,RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id, ONCALL__Country__c='SV',
                                   type=GlobalStrings.PRESALES);
        listAccount.add(ab);
        //insert ab;
        
        insert listAccount;

        szz.ONTAP__SAP_Number__c = 'T0000991';
        szz.V360_Director__c=u.Id;
        update szz;

        ab.ONTAP__SAP_Number__c = 'T0000992';
        ab.V360_Manager__c=u.Id;
        update ab;

        
        V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id);
        insert sz;
        
        List<Account> listAccount2 = new List<Account>();
        
        Account acc = new Account(Name='1',V360_SalesZoneAssignedBDR__c=sz.Id,V360_SalesZoneAssignedTelesaler__c=sz.Id,V360_SalesZoneAssignedPresaler__c=sz.Id,V360_SalesZoneAssignedCredit__c=sz.Id,V360_SalesZoneAssignedTellecolector__c=sz.Id,V360_SalesZoneAssignedCoolers__c=sz.Id);
        listAccount2.add(acc);
        //insert acc;
        //update acc;
        
        Account og1 =  new Account(Name='OG',ONTAP__ExternalKey__c='OG1',RecordTypeId=salesOrgRecordTypeId);
        listAccount2.add(og1);
        //insert og1;
        Account off1 =  new Account(Name='OF',ONTAP__ExternalKey__c='OF1',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);
        listAccount2.add(off1);
        //insert off1;
        Account cg1 =  new Account(Name='CG',ONTAP__ExternalKey__c='CG1',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);
        listAccount2.add(cg1);
        //insert cg1;
        Account sg1 =  new Account(Name='SG',ONTAP__ExternalKey__c='SG1',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id);
        listAccount2.add(sg1);
        //insert sg1;
        Account szz1 =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ1',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
        listAccount2.add(szz1);
        //insert szz1;
        
        insert listAccount2;
        
        List<Account> accs=[SELECT ID FROM Account];
        System.debug('ACCS'+accs);
        
     	List<Account> listUpdateAcc = new List<Account>();
        
        szz.type=GlobalStrings.TELESALES;
        listUpdateAcc.add(szz);
        //update szz;
        
        ab.Type=GlobalStrings.TELESALES;
        listUpdateAcc.add(ab);
        //update ab;
        
        sg.ONTAP__SAP_Number__c = 'T0000993';
        sg.V360_TeamLead__c=u.Id;
        sg.type=GlobalStrings.TELESALES;
        listUpdateAcc.add(sg);
        //update sg;
        
       	off.ONTAP__SAP_Number__c='T0000994';
       	off.V360_Director__c=u.Id;
        off.Type=GlobalStrings.TELESALES;
        listUpdateAcc.add(off);
        //update off;
        
        cg.ONTAP__SAP_Number__c='T000095';
        cg.V360_Manager__c=u.Id;
        cg.Type=GlobalStrings.TELESALES;
        listUpdateAcc.add(cg);
        //update cg;
        update listUpdateAcc;
        
        User u2 = [SELECT ID FROM User WHERE ID !=:u.id LIMIT 1];
        
        off.V360_Director__c=u2.Id;
        off.type=GlobalStrings.PRESALES;
        //update off;
        
		sg.V360_TeamLead__c=u2.Id;
        sg.type=GlobalStrings.PRESALES;
        //update sg;
        
        cg.V360_Manager__c=u2.Id;
        cg.type=GlobalStrings.PRESALES;
        //update cg;
        
        update listUpdateAcc;
        
        Test.stopTest();
        
    }
    
    /**
    * Test method for check_scenario
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void check_scenario()
    {
        
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        User u = [SELECT ID FROM User LIMIT 1];
         List<Account> ListAcc = [SELECT ID,Name,ONTAP__SAP_Number__c FROM Account WHERE Name='SZ'];
        Account szz =  ListAcc.get(0);
        
        V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.id,V360_type__c='Presales');
        insert sz;
        Account acc = new Account(Name='1',ONCALL__KATR10__c='H1',ParentId=szz.Id,RecordTypeId=salesZonedevRecordTypeId);
        acc.ONTAP__SAP_Number__c='Test1';
        acc.V360_ReferenceClient__c=szz.Id;
        acc.ONTAP__Delivery_Delay_Code__c='24 hrs.';
        acc.V360_SalesZoneAssignedBDR__c=sz.Id;
        acc.V360_SalesZoneAssignedTelesaler__c=sz.Id;
        acc.V360_SalesZoneAssignedPresaler__c=sz.Id;
        acc.V360_SalesZoneAssignedCoolers__c=sz.Id;
        acc.V360_SalesZoneAssignedCredit__c=sz.Id;
        acc.V360_SalesZoneAssignedTellecolector__c=sz.Id;
        acc.ONCALL__Country__c=GlobalStrings.HONDURAS_COUNTRY_CODE;
        insert acc;
    }
    
    /**
    * Test method for Prospects
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest
    public static void Test_Prospect()
    {
        Id RecordTypeProspect = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        User oUserTest = new User();
        oUserTest.Country__c = 'Honduras';
        oUserTest.Username= 'juan@acc.com'; 
        oUserTest.LastName= 'Sawuan'; 
        oUserTest.Email= 'juan@acc.com'; 
        oUserTest.Alias= 'Juanis'; 
        oUserTest.CommunityNickname = 'jusuw';
        oUserTest.TimeZoneSidKey= 'America/Bogota'; 
        oUserTest.LocaleSidKey= 'es';
        oUserTest.EmailEncodingKey = 'ISO-8859-1';
        oUserTest.AboutMe = 'yas';
        oUserTest.LanguageLocaleKey = 'es';
        oUserTest.ProfileId= userinfo.getProfileId();
        insert oUserTest;
        
        /* Se inserta la tipificacion para el alta de prospecto */
        ISSM_TypificationMatrix__c oTypification = new ISSM_TypificationMatrix__c();
        oTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oTypification.ISSM_TypificationLevel1__c = 'SAC';
        oTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oTypification.ISSM_TypificationLevel3__c = 'Clientes';
        oTypification.ISSM_TypificationLevel4__c = 'Generacion de Cliente';
        oTypification.CS_Days_to_End__c = 5;
        oTypification.CS_OT_Type_of_request__c = 'Cliente Nuevo';
        oTypification.ISSM_CaseRecordType__c = 'CS_RT_Generate_Client';
        oTypification.ISSM_AssignedTo__c = 'User';
        oTypification.ISSM_OwnerUser__c = oUserTest.Id;
        Insert oTypification;
        
        
        /* Se inserta una cuenta */
        Account oAccount = new Account( Name='AccountTest', ONTAP__Latitude__c= '25.65653', ONTAP__Longitude__c='-100.39177');
        Insert oAccount;
        
        /* Se inserta el prospecto */
        Account oProspect = new Account();
        oProspect.Name = 'Prospect Test';
        oProspect.CS_Last_Name__c = 'Test';
        oProspect.CS_Second_Last_Name__c = 'Test';
        oProspect.ONTAP__LegalName__c = 'Tiendas Unidas';
        oProspect.ISSM_ProspectType__c = 'PN';
        oProspect.CS_Tipo_Identificacion__c = '00';
        oProspect.HONES_IdentificationNumber__c = '90181891';
        oProspect.ONTAP__Mobile_Phone__c = '5549001010';
        oProspect.CS_Tipo_Via__c = 'CL';
        oProspect.ONTAP__Street__c = 'Romero';
        oProspect.ONTAP__Street_Number__c = '12';
        oProspect.CS_Tipo_Poblacion__c = 'Urbano';
        oProspect.CS_Poblacion__c = '06 - Choluteca';
        oProspect.CS_Distrito__c = 'ALUBAREN';
        oProspect.V360_ReferenceClient__c = oAccount.Id;
        oProspect.RecordTypeId = RecordTypeProspect;
        
        Insert oProspect;
    }
}