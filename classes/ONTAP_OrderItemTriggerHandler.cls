/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONTAP_OrderItemTriggerHandler.apxc
 * Versión: 1.0.0.0
 *  
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 03/01/ 2019     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Creación
 */

public class ONTAP_OrderItemTriggerHandler extends TriggerHandlerCustom{
	private Map<Id, ONTAP__Order_Item__c> newMap;
    private Map<Id, ONTAP__Order_Item__c> oldMap;
    private List<ONTAP__Order_Item__c> newList;
    private List<ONTAP__Order_Item__c> oldList;
    
    public ONTAP_OrderItemTriggerHandler() {
        this.newMap = (Map<Id, ONTAP__Order_Item__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__Order_Item__c>) Trigger.oldMap;
         this.newList = (List<ONTAP__Order_Item__c>) Trigger.new;
        this.oldList = (List<ONTAP__Order_Item__c>) Trigger.old;
        
    }
     public override void afterInsert(){
     	//ONTAP_updateOrderCount.sumCounter(this.newlist);
        
        
    }
     public override void afterUpdate(){

    }
  
    public override void beforeInsert(){
        
    }
    
    public override void beforeUpdate(){
        
    }

}