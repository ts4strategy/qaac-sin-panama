/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CLOSECASEEMAIL_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * 
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 12/02/2019           Gabriel E Garcia       Close case by email 
 */

global class CS_CloseCaseEmail_Class implements Messaging.InboundEmailHandler 
{    
    /**
    * Method handleInboundEmail
    * @author: gabriel.e.garcia@accenture.com
    * @param Email and envolpe email
    * @return result with success or fail status
    */
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env)
    {        
        // Create an InboundEmailResult object for returning the result of the 
        // Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        String bodyEmail= '';
        String emailAddress = '';
        String bodytext = '';
        String bodycase = '';
        List<String> listcases = new List<String>();
        
        // Store the email plain text into the local variable 
        bodyEmail = email.plainTextBody;
        emailAddress = email.fromAddress;
        
        if(bodyEmail.indexOf('#') > -1)
            bodytext = bodyEmail.substring(bodyEmail.indexOf('#') + 1);
        else
        {
            result.success = false;
            result.message = System.Label.No_case_finded;
            return result;
        }                
        
        bodycase = bodytext.substring(0, bodytext.indexOf('\n'));
        bodycase = bodycase.remove(' ');
        bodyEmail = System.Label.Closed_By + ': ' + emailAddress + ' ' + bodyEmail.replace('\n', ' ');         
        listcases = bodycase.split(';');     
        
        // Try to look up list of case with status field through a list of cases numbers.
        // If there occurre a error, will be notified to de user,
        // If there are not problem update statu of cases.
        
        try
        { 
            Boolean isCloseCaseSuccess = false;
            List<Case> listCasesQ = [
                                        SELECT Id, CaseNumber, ISSM_CaseForceNumber__c, Status, RecordType.DeveloperName
                                        FROM Case 
                                        WHERE CaseNumber IN: listCases
                                    ];
            
            if(!listCasesQ.isEmpty())
               isCloseCaseSuccess = CS_CaseClose_Class.CloseCase(listCasesQ[0].Id, listCasesQ[0].ISSM_CaseForceNumber__c, bodyEmail, System.Label.CS_Close_Case_Email,  ListCasesQ[0].RecordType.DeveloperName); 
            else
            {
                result.success = isCloseCaseSuccess;
                result.message = System.Label.No_case_finded;
                return result;
            }           
                                                
            if(isCloseCaseSuccess)            
                result.success = isCloseCaseSuccess;
            else
            {
                result.success = isCloseCaseSuccess;
                result.message = System.Label.No_case_finded;
            }
        }
        catch (Exception ex) 
        {
            System.debug('CS_CloseCaseEmail_Class.handleInboundEmail Message: ' + ex.getMessage());   
            System.debug('CS_CloseCaseEmail_Class.handleInboundEmail Cause: ' + ex.getCause());   
            System.debug('CS_CloseCaseEmail_Class.handleInboundEmail Line number: ' + ex.getLineNumber());   
            System.debug('CS_CloseCaseEmail_Class.handleInboundEmail Stack trace: ' + ex.getStackTraceString());
            
            result.success = false;
            result.message = 'Error : ' + ex;
        }

        return result;
    }
}