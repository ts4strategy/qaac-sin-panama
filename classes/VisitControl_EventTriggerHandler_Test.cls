/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_EventTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
private class VisitControl_EventTriggerHandler_Test
{
    /**
    * Test method before insert 
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_beforeInsert_Retrace()
    {
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        Id routeRTypeId = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Visit_Control_Route').getRecordTypeId();
        
        User u = new User
        (
            ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN LIMIT 1].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Bogota',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'es',
            LocaleSidKey = 'es_US',
            Country__c = 'Honduras',
            ManagerId = UserInfo.getUserId()
        );
        insert u;  
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);  insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);  insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);  insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id);  insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);  insert szz;

		V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.Id,V360_Type__c='Presales');  insert sz;
        
        Account acc = new Account(Name ='1', V360_SalesZoneAssignedBDR__c = sz.Id, V360_SalesZoneAssignedTelesaler__c = sz.Id,V360_SalesZoneAssignedPresaler__c = sz.Id,
                                  V360_SalesZoneAssignedCredit__c = sz.Id, V360_SalesZoneAssignedTellecolector__c = sz.Id,V360_SalesZoneAssignedCoolers__c = sz.Id);        
           
        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id, Supervisor__c = u.id, VisitControl_AgentByZone__c = sz.Id, ServiceModel__c ='Presales', RecordTypeId = routeRTypeId);  insert rt;
        
        VisitControl_RetraceAndAnticipateRoute__c visitcontrol_retraceandanticipateroute_Obj2 = new VisitControl_RetraceAndAnticipateRoute__c
        (VisitControl_Route__c = rt.id, VisitControl_DateToSkip__c = Date.today(), VisitControl_RetraceOrAnticipate__c = 'Retrace', VisitControl_Deleted__c = false);
        Insert visitcontrol_retraceandanticipateroute_Obj2;        

        VisitPlan__c vp= New VisitPlan__c(Route__c = rt.Id, ExecutionDate__c = Date.today(), EffectiveDate__c = Date.today() + 7);  insert vp;
        
        ONTAP__Tour__c tt = New ONTAP__Tour__c(ONTAP__TourId__c = 'T-000001160',ONTAP__TourDate__c = Date.today(), VisitPlan__c = vp.Id);  insert tt;
        
        Event event_Obj = new Event(StartDateTime = system.now(), EndDateTime=System.now()+1, Subject ='Visit', VisitList__c = tt.Id );  Insert event_Obj;
    }
    
    /**
    * Test method before insert 
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_beforeInsert_Anticipate()
    {
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        Id routeRTypeId = Schema.SObjectType.ONTAP__Route__c.getRecordTypeInfosByDeveloperName().get('Visit_Control_Route').getRecordTypeId();
        
        User u = new User
        (
            ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN LIMIT 1].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Bogota',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'es',
            LocaleSidKey = 'es_US',
            Country__c = 'Honduras',
            ManagerId = UserInfo.getUserId()
        );
        insert u;   
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);  insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);  insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);  insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id);  insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);  insert szz;

		V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.Id,V360_Type__c='Presales');  insert sz;
        
        Account acc = new Account(Name ='1', V360_SalesZoneAssignedBDR__c = sz.Id, V360_SalesZoneAssignedTelesaler__c = sz.Id,V360_SalesZoneAssignedPresaler__c = sz.Id,
                                  V360_SalesZoneAssignedCredit__c = sz.Id, V360_SalesZoneAssignedTellecolector__c = sz.Id,V360_SalesZoneAssignedCoolers__c = sz.Id);        
           
        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id, Supervisor__c = u.id, VisitControl_AgentByZone__c = sz.Id, ServiceModel__c ='Presales', RecordTypeId = routeRTypeId, RetraceRoute__c = true);  
        insert rt;
        
        VisitControl_RetraceAndAnticipateRoute__c visitcontrol_retraceandanticipateroute_Obj = new VisitControl_RetraceAndAnticipateRoute__c
        (VisitControl_Route__c = rt.id, VisitControl_DateToSkip__c = Date.today(), VisitControl_RetraceOrAnticipate__c = 'Anticipate', VisitControl_Deleted__c = false);
        Insert visitcontrol_retraceandanticipateroute_Obj;        

        VisitPlan__c vp= New VisitPlan__c(Route__c = rt.Id, ExecutionDate__c = Date.today(), EffectiveDate__c = Date.today() + 7);  insert vp;
        
        ONTAP__Tour__c tt = New ONTAP__Tour__c(ONTAP__TourId__c = 'T-000001160',ONTAP__TourDate__c = Date.today(), VisitPlan__c = vp.Id);  insert tt;
        
        Event event_Obj = new Event(StartDateTime = system.now(), EndDateTime=System.now()+1, Subject ='Visit', VisitList__c = tt.Id );  Insert event_Obj;
    }
}