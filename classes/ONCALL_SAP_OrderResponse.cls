/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: ONCALL_SAP_OrderResponse.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Herón Zurita           Creation of Entities.
* 30/Jun/2019		Daniel Rosales				Modified to fit new response 
*/

public class ONCALL_SAP_OrderResponse {
	//https://www.adminbooster.com/tool/json2apex
    public String orderId;				//0000100011
	public cls_etReturn [] etReturn;
	public class cls_etReturn  {
		public String type_Z;			//S
		public String number_Z;			//233
		public String message;			//SALES_HEADER_IN procesado con Éxito
	}
    public static ONCALL_SAP_OrderResponse parse(String json){
        json = json.replace('"type":', '"type_Z":');
        json = json.replace('"number":', '"number_Z":');
        system.debug('json replaced:' + json);
		return (ONCALL_SAP_OrderResponse) System.JSON.deserialize(json, ONCALL_SAP_OrderResponse.class);
	}
    
    // OLD CODE BELOW
    // COMMENTED - SUBSTITUED BY CODE ABOVE
    /**
    * Structure of Entity Item
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    /*
	public class Item {
		public String type_Z {get;set;} // in json: type
		public String number_Z {get;set;} // in json: number
		public String message {get;set;} 

		public Item(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'type') {
							type_Z = parser.getText();
						} else if (text == 'number') {
							number_Z = parser.getText();
						} else if (text == 'message') {
							message = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Item consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public String id {get;set;} 
	public EtReturn etReturn {get;set;} 
	*/
    
    /**
    * Constructor of the class 
    * Created By: heron.zurita@accenture.com
    * @param JSONParser parser
    * @return void
    */ 
    /*
	public ONCALL_SAP_OrderResponse(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'id') {
						id = parser.getText();
					} else if (text == 'etReturn') {
						etReturn = new EtReturn(parser);
					} else {
						System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	*/
    
    
    /**
    * Estructure of the Entity EtReturn 
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */ 
	/*
    public class EtReturn {
		public Item item {get;set;} 

		public EtReturn(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						if (text == 'item') {
							item = new Item(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'EtReturn consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	*/
    /**
    * Constructor of the class 
    * Created By: heron.zurita@accenture.com
    * @param String json
    * @return void
    */ 
	/*
	public static ONCALL_SAP_OrderResponse parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new ONCALL_SAP_OrderResponse(parser);
	}
	*/
    /**
    * Method to consume the objects inside de Json (token iteration)
    * Created By: heron.zurita@accenture.com
    * @param JSONParser json
    * @return void
    */ 
    /*
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	*/
}