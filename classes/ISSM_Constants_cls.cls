public with sharing class ISSM_Constants_cls 
{
	public static final String USER = System.label.ISSM_AssignCaseUser;
    public static final String QUEUE = System.label.ISSM_AssignCaseQueue;
    public static final String SUPERVISOR =System.label.ISSM_AssignCaseSupervisor;
    public static final String BILLINGMANAGER = System.label.ISSM_AssignCaseBillingManager;
    public static final String TRADEMARKETING = System.label.ISSM_AssignCaseTradeMarketing; 
    public static final String MODELORAMA = System.label.ISSM_AssignCaseModelorama;
    public static final String REPARTO = System.label.ISSM_AssignCaseReparto;
    public static final String BOSSREFRIGERATION = System.label.ISSM_AssignCaseBossRefrigeration;
        
    public static final string TELVENTA = 'Telventa';
    public static final string PREVENTA = 'Preventa';
    public static final string CREDITO = 'Credito';
    public static final string EQUIPO_FRIO = 'Cooler';
    public static final string TELECOBRANZA = 'Telecobranza';
    
    public static final string QUEUE_NAME_AGENTE_SAC = 'SV_SacAgent';
        
    public static final string DIRECTOR_TELVENTA = 'Director Telventa';
    public static final string TELESALES_DIRECTOR = 'TeleSales Director';
    public static final string GERENTE_TELVENTA = 'Gerente Telventa';
    public static final string TELESALES_MANAGER = 'Telesales Manager';
    public static final string SUPERVISOR_TELVENTA = 'Supervisor Telventa';
    public static final string TELESALES_SUPERVISOR = 'Telesales Supervisor';
    
    public static final string DIRECTOR_VENTA = 'Director Ventas';
    public static final string SALES_DIRECTOR = 'Sales Director';
    public static final string GERENTE_VENTA = 'Gerente Ventas';
    public static final string SALES_MANAGER = 'Sales Manager';
    public static final string SUPERVISOR_VENTA = 'Supervisor Ventas';
    public static final string SALES_SUPERVISOR = 'Sales Supervisor';
       
    public static final string DIRECTOR_EQUIPO_FRIO = 'Director Equipo Frio';
    public static final string COLD_DIRECTOR = 'Cold Director';
    public static final string GERENTE_EQUIPO_FRIO = 'Gerente Equipo Frio';
    public static final string COLD_MANAGER = 'Cold Manager';
    public static final string SUPERVISOR_EQUIPO_FRIO = 'Supervisor Equipo Frio';
    public static final string COLD_SUPERVISOR = 'Cold Supervisor';
    
    public static final string DIRECTOR_TELECOBRANZA = 'Director Telecobranza';
    public static final string TELLECOLECTION_DIRECTOR = 'Tellecolection Director';
    public static final string GERENTE_TELECOBRANZA = 'Gerente Telecobranza';
    public static final string TELLECOLECTION_MANAGER = 'Tellecolection Manager';
    public static final string SUPERVISOR_TELECOBRANZA = 'Supervisor Telecobranza';
    public static final string TELLECOLECTION_SUPERVISOR = 'Tellecolection Supervisor';
    
    public static final string DIRECTOR_CREDITO = 'Director Credito';
    public static final string CREDIT_DIRECTOR = 'Credit Director';
    public static final string GERENTE_CREDITO = 'Gerente Credito';
    public static final string CREDIT_MANAGER = 'Credit Manager';
    public static final string SUPERVISOR_CREDITO = 'Supervisor Credito';
    public static final string CREDIT_SUPERVISOR = 'Credit Supervisor';   
}