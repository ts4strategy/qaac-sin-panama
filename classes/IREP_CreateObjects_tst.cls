@isTest
public class IREP_CreateObjects_tst{

	public static list<Account> createAccount(String strRecordTypeId, Integer intAccounts){
		RecordType objRT = [Select Id From RecordType Where RecordType.DeveloperName = :strRecordTypeId Limit 1][0];

		list<Account> lstAccounts = new list<Account>();

		for(Integer i=0; i<intAccounts; i++){
			Account objAcc = new Account();
			objAcc.RecordTypeId = objRT.Id;
			objAcc.Name = 'Account Test ' + i;
			objAcc.ONTAP__SAPCustomerId__c = '9999999999'+i;

			lstAccounts.add(objAcc);
		}
		return lstAccounts;
	}


	public static User createUser(){
		Profile profile                     = [Select Id From Profile where Name = 'iRep - Driver' Limit 1];
        UserRole rol                         = [Select Id From UserRole Where DeveloperName = 'iRep_GestorDatos' Limit 1];

        User UsuarioRol                          = new User();
        UsuarioRol.Alias                    = 'standt';
        UsuarioRol.Email                    = 'standarduser@testorg.com';
        UsuarioRol.EmailEncodingKey         = '';
        UsuarioRol.LastName                 = 'Testing';
        UsuarioRol.LanguageLocaleKey        = 'en_US';
        UsuarioRol.LocaleSidKey             = 'en_US';
        UsuarioRol.ProfileId                = profile.Id;
        UsuarioRol.TimeZoneSidKey           = 'America/Los_Angeles';
        UsuarioRol.UserName                 = 'standarduser' + Integer.valueOf( ( Math.random() * 100 ) ) + '_'+Integer.valueOf( ( Math.random() * 10 ) ) +'@testorg.com';
        UsuarioRol.EmailEncodingKey         = 'UTF-8';
        UsuarioRol.IsActive 				= true;
        UsuarioRol.UserRoleId               = rol.Id;

        return UsuarioRol;
	}

	public static ONTAP__Tour__c createTour(String strIdCarrier){
		ONTAP__Tour__c objTour = new ONTAP__Tour__c();
		objTour.ONTAP__TourId__c = 'AV0123';
		objTour.ONTAP__RouteId__c = 'AV0123';
		objTour.ONTAP__VehicleId__c = 'AV0123';
		objTour.ONTAP__DriverId__c = 'AV0123';
		objTour.IREP_ExternalKey__c = 'CO123';
		objTour.ONTAP__TourDate__c = Date.today();
		objTour.ONTAP__TourStatus__c = 'Not Started';
		objTour.ONTAP__IsActive__c = true;
		objTour.IREP_Carrier__c = strIdCarrier;

		return objTour;
	}

	public static list<ONTAP__Tour_Visit__c> createTourVisits(String strTourId, Integer intTourVisits, list<String> lstAccountIds){
		list<ONTAP__Tour_Visit__c> lstTourVisits = new list<ONTAP__Tour_Visit__c>();
		for(Integer i=0; i<intTourVisits; i++){
			ONTAP__Tour_Visit__c objTourVisit = new ONTAP__Tour_Visit__c();
			objTourVisit.ONTAP__Account__c = lstAccountIds[i];
			objTourVisit.ONTAP__Tour__c = strTourId;
			objTourVisit.ONTAP__WindowStartTime__c = Datetime.now();
			objTourVisit.ONTAP__WindowEndTime__c = Datetime.now() + 120;
			objTourVisit.ONTAP__SequenceId__c = i;
			objTourVisit.ONTAP__TourVisitKey__c = 'IREP'+i;
			objTourVisit.ONTAP__TourVisitStatus__c = 'Pending';
			lstTourVisits.add(objTourVisit);
		}

		return lstTourVisits;
	}

	public static list<ONTAP__Order__c> createOrders(list<String> lstTourVisitIds, list<String> lstAccounts, Integer intNumOrders){
		list<ONTAP__Order__c> lstOrders = new list<ONTAP__Order__c>();
		for(Integer i=0; i<lstTourVisitIds.size(); i++){
			for(Integer j=0; j<intNumOrders; j++){
				ONTAP__Order__c objOrder = new ONTAP__Order__c();
				objOrder.ONTAP__SAP_Order_Number__c = 'AVX'+i+''+j;
				objOrder.ONTAP__TourVisit__c = lstTourVisitIds[i];
				objOrder.ONTAP__OrderAccount__c = lstAccounts[i];
				objOrder.ONTAP__OrderStatus__c = 'Open';

				lstOrders.add(objOrder);
			}
		}
		return lstOrders;
	}


	public static list<ONTAP__Order_Item__c> createOrderItems(list<String> lstOrderIds, Integer intNumOrderItems){
		list<ONTAP__Order_Item__c> lstOrderItems = new list<ONTAP__Order_Item__c>();

		for(Integer i=0; i<intNumOrderItems; i++){
			ONTAP__Order_Item__c objOrderItem = new ONTAP__Order_Item__c();
			objOrderItem.ONTAP__CustomerOrder__c = lstOrderIds[i];
			objOrderItem.ONTAP__ActualQuantity__c = 10;
			objOrderItem.ONTAP__Base_Total__c = 100000;
			objOrderItem.ONTAP__UnitPrice__c = 100000;
			objOrderItem.ONTAP__PlanQuantity__c = 15;
			lstOrderItems.add(objOrderItem);
		}

		return lstOrderItems;
	}

	public static ONTAP__AssignedVehicle__c createAssignedVehicle(String strTourId){
		ONTAP__AssignedVehicle__c objAssignedVehicle = new ONTAP__AssignedVehicle__c();
		objAssignedVehicle.ONTAP__Tour__c = strTourId;
		objAssignedVehicle.IREP_BeginMile__c = 10;
		objAssignedVehicle.IREP_EndMile__c = 100;

		return objAssignedVehicle;
	}


	public static void createFullTour(Integer intTourVisits, Integer intOrders, Integer intOrderItems){
		 User objUser;

		 User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {

			objUser =  IREP_CreateObjects_tst.createUser();
			insert objUser;

		}

		ONTAP__Tour__c objTour = IREP_CreateObjects_tst.createTour(objUser.Id);
		insert objTour;

		ONTAP__AssignedVehicle__c objAssignedVehicle = IREP_CreateObjects_tst.createAssignedVehicle(objTour.Id);
		insert objAssignedVehicle;

		list<Account> lstAcc = IREP_CreateObjects_tst.createAccount('Account', intTourVisits);
		insert lstAcc;

		list<String> lstAccIds = new list<String>();
		for(Account objAcc : lstAcc)
			lstAccIds.add(objAcc.Id);

		list<ONTAP__Tour_Visit__c> lstTV = IREP_CreateObjects_tst.createTourVisits(objTour.Id, intTourVisits, lstAccIds);
		insert lstTV;

		list<String> lstTVIds = new list<String>();
		for(ONTAP__Tour_Visit__c objTV : lstTV)
			lstTVIds.add(objTV.Id);

		list<ONTAP__Order__c> lstOrders = IREP_CreateObjects_tst.createOrders(lstTVIds, lstAccIds, intOrders);
		insert lstOrders;

		list<String> lstOrderIds = new list<String>();
		for(ONTAP__Order__c objOrder : lstOrders)
			lstOrderIds.add(objOrder.Id);

		list<ONTAP__Order_Item__c> lstOrderItems = IREP_CreateObjects_tst.createOrderItems(lstOrderIds, intOrderItems);
		insert lstOrderItems;
	}
}