/* ----------------------------------------------------------------------------
* AB InBev :: 
* ----------------------------------------------------------------------------
* Clase: LookUpAccountCloseSale_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019    Oscar Garcia Martinez      Creation of methods.
* 17/01/2019        Luis Parra             Creation of methods.
* 18/01/2019        Heron Zurita           Creation of methods.
* 22/01/2019        Carlos Leal            Creation of methods.
*/
@isTest
private class LookUpAccountCloseSale_Test{
    
    /**
    * Test method for set start values
    * Created By: o.a.garcia.martinez@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void setupTestData(){
        test.startTest();
        json_items jonItem = new json_items();
        List<RecordType> records = [Select Id,Name from RecordType where SobjectType='Account'];
        List<Account> listAcc = new List<Account>();
        List<ONTAP__Product__c> listProduct = new List<ONTAP__Product__c>();
        List<ONCALL__Call__c> listCall = new List<ONCALL__Call__c>();
        List<HONES_ProductPerClient__c> listProductPerClient = new List<HONES_ProductPerClient__c>();
        List<ISSM_ProductByOrg__c> listProductByOrg = new List<ISSM_ProductByOrg__c>();
        for(Integer j=0;j<5;j++){            
            ONTAP__Product__c product = new ONTAP__Product__c();
            Account acc = new Account();
            acc.Name ='Test';
            acc.ONTAP__Credit_Condition__c= 'Credit';
            acc.ONCALL__KATR4__c = 'C1';
            acc.RecordTypeId= records[0].Id;
            acc.ONTAP__Delivery_Delay_Code__c = '24 hrs.';
            acc.ONTAP__SalesOgId__c=GlobalStrings.HONDURAS_ORG_CODE;
            //acc.ONTAP__ExternalKey__c='XX911'+j;
            acc.ONTAP__ExternalKey__c='SVHBF'+j;
            listAcc.add(acc);
            listProduct.add(product);            
            
        }
        insert listAcc;
        insert listProduct;
        System.debug('accounts : '+listAcc);
        
        for(Integer j=0;j<5;j++){            
            ONCALL__Call__c call = new ONCALL__Call__c();            
            call.ONCALL__Primary_Order__c = ('Test'+String.valueOf(j));
            call.ONCALL__POC__c = listAcc[0].Id;
            //product.ONCALL__Customer_Product_Name__c = ('Test'+String.valueOf(j));
            //product.ONTAP__ProductCode__c ='Test';            
            listCall.add(call);
            
            HONES_ProductPerClient__c productPer = new HONES_ProductPerClient__c();
            productPer.SAP_Account_ID__c = String.valueOf(j);
            productPer.SAP_SKU_ID__c = String.valueOf(j);
            productPer.HONES_RelatedClient__c=listAcc[0].Id;
            listProductPerClient.add(productPer);            
            
            ISSM_ProductByOrg__c tesProdByOrg = new ISSM_ProductByOrg__c();
            tesProdByOrg.ISSM_AssociatedProduct__c=listproduct[0].Id;
            tesProdByOrg.SAP_SKU_UD__c=String.valueOf(j);
            tesProdByOrg.Country_Code__c='SV';
        	listProductByOrg.add(tesProdByOrg);                    
        }  
                
        insert listCall;
        insert listProductPerClient;
        insert listProductByOrg;
        
        test.stopTest();
    }
    
    /**
    * Test method for fetch lookups values for contact
    * Created By: luis.arturo.parra@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_fetchLookUpValuesContact_UseCase1(){
        
       	LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
        List<wrapperClass> product = new List<wrapperClass>();
		wrapperClass wrp = new wrapperClass();        
        wrp.productType = 'Empties Introduction';        
        wrp.total=1.5;
        wrp.subtotal=1.5;
        wrp.tax=0;
        wrp.discount=-0.27;
        wrp.sku='000000000000009888';
        wrp.unitPrice=83.3;
        wrp.productMetadataType='Bottle';
        product.add(wrp);
        Set<String> sku_prdtWP = new Set<String>();
        Map<String, List<String>> picktosend_mapWP = new Map<String, List<String>>(); 
        LookUpAccountCloseSale.WrapperInitVariables wpVariables = new LookUpAccountCloseSale.WrapperInitVariables();
        wpVariables.sku_prdtWP = sku_prdtWP;
        wpVariables.picktosend_mapWP = picktosend_mapWP;
        String JSONStringProduct = JSON.serialize(wpVariables);
       	List<Account> account_p = [Select Id, Name From Account where ONCALL__KATR4__c = 'C1' Limit 1];                                               
       	LookUpAccountCloseSale.fetchLookUpValuesContact('Test',account_p[0].Id,JSONStringProduct);        
    }
    
    @isTest static void test_paymentMethod_UseCase1(){
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale(); 
        List<Account> account_p = [SELECT Id,Name,ONTAP__Credit_Amount__c FROM Account WHERE ONTAP__Credit_Condition__c ='Credit'];       
        LookUpAccountCloseSale.paymentMethod(account_p[0].Id);
    }
    
    /**
    * Test method for fetch lookups values for pbe
    * Created By: heron.zurita@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_fetchLookUpValuesPbe_UseCase1(){
       LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
       //Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorgenOrder());
 	   
       ONTAP__Product__c product = new ONTAP__Product__c();
       product.ONTAP__ProductCode__c='9908';
       product.ONTAP__MaterialProduct__c='Material_2000';
       product.ONTAP__ProductType__c='FERT';
       insert product;
        
                

       List<Account> account_p = [Select Id, Name From Account where ONCALL__KATR4__c = 'C1' Limit 1];
       List<ISSM_ProductByOrg__c> prodOrd= [SELECT Id,Country_Code__c,ISSM_AssociatedProduct__c,SAP_SKU_UD__c FROM ISSM_ProductByOrg__c WHERE Country_Code__c= 'SV'];
       List<ONTAP__Product__c> prd = [Select Id,ONTAP__ProductCode__c,ONTAP__MaterialProduct__c,ONTAP__ProductType__c  FROM ONTAP__Product__c WHERE Id=:account_p[0].Id];
       List<HONES_ProductPerClient__c> exclClient = [SELECT HONES_RelatedClient__c,Id,SAP_Account_ID__c,SAP_SKU_ID__c FROM HONES_ProductPerClient__c];
       
		
       List<wrapperClass> producto = new List<wrapperClass>();
	   
       wrapperClass wrp = new wrapperClass();  
       wrp.productCode='9908';
       wrp.materialProduct='Test';
       wrp.productType='GOOD_SHAPE_PRODUCT';
       wrp.quantity=1.56;
       //wrp.listaPBE=;
       wrp.pbetag='PBE';
       producto.add(wrp);
       String JSONStringProduct = JSON.serialize(producto);       
      

        
       LookUpAccountCloseSale.fetchLookUpValuesPbe('Test',account_p[0].Id);  
    }
    
    /**
    * Test method for fetch lookups values for pfn
    * Created By: o.a.garcia.martinez@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_fetchLookUpValuesPfn_UseCase1(){
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
        List<Account> account_p = [Select Id, Name From Account where ONCALL__KATR4__c = 'C1' Limit 1];                                               
        ISSM_ProductByOrg__c tesProdByOrg = new ISSM_ProductByOrg__c(Country_Code__c = '');
        insert tesProdByOrg;         
        LookUpAccountCloseSale.fetchLookUpValuesPfn('Test',account_p[0].Id);             
        
    }
    
    /**
    * Test method for look if is available
    * Created By: luis.arturo.parra@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_disponible_UseCase1(){
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
        LookUpAccountCloseSale.listValues = new List <String>();
        LookUpAccountCloseSale.listValues2 = new List <String>();
        List<Account> account_p = [Select Id, Name From Account where ONCALL__KATR4__c = 'C1' Limit 1];                                               
        LookUpAccountCloseSale.disponible(account_p[0].Id);        
    }
    
    /**
    * Test method for get the id of the related account
    * Created By: heron.zurita@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_getId_UseCase1(){
        
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
        List<ONCALL__Call__c> lstAcc = [SELECT Id,ONCALL__POC__c, ONCALL__POC__r.Name, ONCALL__POC__r.ONTAP__SAP_Number__c, ONCALL__POC__r.ONTAP__Credit_Condition__c, ONCALL__POC__r.ONTAP__Delivery_Delay_Code__c FROM ONCALL__Call__c ];
        LookUpAccountCloseSale.getId(lstAcc[0].Id);
    }
    
    /**
    * Test method for get the hour delay of the related account
    * Created By: o.a.garcia.martinez@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_getHour_UseCase1(){
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
        List<ONCALL__Call__c> lstAcc = [SELECT Id,ONCALL__POC__c, ONCALL__POC__r.Name, ONCALL__POC__r.ONTAP__SAP_Number__c, ONCALL__POC__r.ONTAP__Credit_Condition__c, ONCALL__POC__r.ONTAP__Delivery_Delay_Code__c FROM ONCALL__Call__c ];
        LookUpAccountCloseSale.getHour(lstAcc[0].Id);   
    }

    /**
    * Test method for the callout price
    * Created By: luis.arturo.parra@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_callourPrice_UseCase1(){
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale(); 
        List<ONCALL__Call__c> lstAcc = [SELECT Id,ONCALL__POC__c FROM ONCALL__Call__c ];        
        List<json_deals> deals = new List<json_deals>();   
            
        json_deals wrpprom = new json_deals();
        wrpprom.pronr='000000000000009888';
        wrpprom.condcode='YSDM';
        wrpprom.posex='1';
        wrpprom.amount=-1; 
        deals.add(wrpprom);
        String JSONStringProm = JSON.serialize(deals);
        system.debug('json'+JSONStringProm);
        List<wrapperClass> product = new List<wrapperClass>();
        wrapperClass wrp = new wrapperClass();        
        wrp.sku='000000000000009888';
        wrp.productType ='Add Empties';        
        wrp.total=1.5;
        wrp.subtotal=1.5;
        wrp.tax=0;
        wrp.discount=-0.27;
        product.add(wrp);
        
        wrapperClass wrap_item = new wrapperClass();
        ONCALL__Call__c acc = lstAcc.get(0);
        String JSONStringProduct = JSON.serialize(product);
        String JSONStringDeal = '[{"description":"PRUEBA MALTA DC","optional":true,"pay":"CREDIT","pronr":"249032"}]';
        Test.startTest();   
        Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorgenOrder());     
        LookUpAccountCloseSale.callourPrice(JSONStringProm,acc.ONCALL__POC__c,JSONStringProduct, JSONStringDeal); 
        Test.stopTest();       
    }
    
    /*
    * Test method for get the picklist values
    * Created By: heron.zurita@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    
    @isTest static void test_pickvalues_UseCase1(){
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
        LookUpAccountCloseSale.listValues = new List <String>();
        LookUpAccountCloseSale.listValues2 = new List <String>();
        LookUpAccountCloseSale.pickvalues();
    }*/
    
    /**
    * Test method for get the order
    * Created By: o.a.garcia.martinez@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void  TestOrder(){   
        
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale(); 
        Account SalesOrgId = new Account();
        SalesOrgId.ONTAP__SalesOgId__c='CS01'; 
        SalesOrgId.ONTAP__SAP_Number__c = '0011820869';
        SalesOrgId.ONCALL__KATR4__c='C1';
        SalesOrgId.Name='Test';
        insert SalesOrgId;
        String acc=SalesOrgId.Id;

        ONCALL__Call__c obj02= new ONCALL__Call__c();
        obj02.Name='Test';
        obj02.ONCALL__POC__c = acc;
        insert obj02;
        List <ONCALL__Call__c> callObj=[SELECT Id,Name from ONCALL__Call__c where Id=:obj02.Id];
          
        List<json_deals> deals = new List<json_deals>();  
        
        json_deals wrpprom = new json_deals();
        wrpprom.pronr='248856';
        wrpprom.condcode='YSDM';
        wrpprom.posex='1';
        wrpprom.amount=-1; 
        deals.add(wrpprom);
        String JSONStringProm = JSON.serialize(deals);
        
        ONTAP__Product__c prod= new ONTAP__Product__c();
        insert prod;
        
        List<wrapperClass> product = new List<wrapperClass>();
        wrapperClass wrp = new wrapperClass();        
        //wrp.productType = 'Empties Introduction';        
        wrp.productType = 'Finished Product';        
        wrp.total=1.5;
        wrp.subtotal=1.5;
        wrp.tax=0;
        wrp.discount=-0.27;
        wrp.sku='000000000000009888';
        wrp.unitPrice=83.3;
        wrp.productMetadataType='Bottle';
        product.add(wrp);
        String JSONStringProduct = JSON.serialize(product);
        Date orig = Date.today(); 
        //DateTime dtConverted = DateTime.valueOf(orig.format('yyyy-MM-dd HH:mm:ss'));
        String dayString = formatDate(orig);
        ONCALL__Call__c callObject = callObj.get(0);
     SYSTEM.debug('1-'+JSONStringProduct);SYSTEM.debug('2-'+callObject);SYSTEM.debug('3-'+acc);SYSTEM.debug('4-'+JSONStringProm);
             //LookUpAccountCloseSale.genOrder(JSONStringProduct,callObj,acc,JSONStringProm,dayString); 
    }
    
    /**
    * Test method for set the date format
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    public static String formatDate(Date d) {
        return d.year() + '-' + d.month() + '-' + d.day();
    }
    
    /**
    * Test method for get the order amount
    * Created By: luis.arturo.parra@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void  calculateOrderAmount(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorgenOrder());
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale(); 
        List <wrapperClass> wrapp= new List <wrapperClass>();
        
        wrapperClass wrp = new wrapperClass();
        wrp.productType = 'Add Empties';
        wrp.productType = 'Finished Product';
        wrp.productType = 'Gifts';
        wrp.productType = 'Empties Introduction';
        wrp.total=1.5;
        wrp.subtotal=1.5;
        wrp.tax=0;
        wrp.discount=-0.27;
        
        wrapp.add(wrp);
        LookUpAccountCloseSale.calculateOrderAmount(wrapp);          
    }
    
    /**
    * Test method for get the values of the amountin the order
    * Created By: heron.zurita@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void  calculateOrderAmountelsetest(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorgenOrder());
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale(); 
        List <wrapperClass> wrapp= new List <wrapperClass>();
        
        wrapperClass wrp = new wrapperClass();
        
        wrp.productType = 'Emp';
        wrp.total=1.5;
        wrp.subtotal=1.5;
        wrp.tax=0;
        wrp.discount=-0.27;
        
        wrapp.add(wrp);
        
        LookUpAccountCloseSale.calculateOrderAmount(wrapp);          
    }
    
     /**
    * Test method for get the values of the amountin the order
    * Created By: heron.zurita@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    
    @isTest static void test_genOrder(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorgenOrder());
        LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale(); 
        
        List<RecordType> records = [Select Id,Name from RecordType where SobjectType='Account'];
        Account acc = new Account();
        acc.Name ='Test9999';
        acc.ONCALL__KATR4__c = 'C1';
        acc.RecordTypeId= records[0].Id;
        
        acc.ONTAP__Delivery_Delay_Code__c = '24 hrs.';
        acc.ONTAP__SalesOgId__c=GlobalStrings.EL_SALVADOR_ORG_CODE;
        //acc.ONTAP__ExternalKey__c='XX911';
        acc.ONTAP__ExternalKey__c='HNHBF';
        insert acc;
        
        List<Account> account_p = [Select Id, Name From Account where ONCALL__KATR4__c = 'C1' Limit 1];                                               
        ISSM_ProductByOrg__c tesProdByOrg = new ISSM_ProductByOrg__c(Country_Code__c = '');
        insert tesProdByOrg;         
               
        ONTAP__Product__c producto = new ONTAP__Product__c();
        producto.ONCALL__Customer_Product_Name__c = 'Test';
        producto.ONTAP__ProductCode__c ='Test';            
       	insert producto ;
        
        ONTAP__Order__c order = new ONTAP__Order__c();
        order.ONTAP__OrderAccount__c=acc.Id;
        order.ISSM_PaymentMethod__c='Cash';
        order.ONTAP__DeliveryDate__c=Date.newInstance(2018, 11,4);
        order.ONTAP__Amount_Total__c =0;
        order.ONTAP__Amount_Sub_Total_Product__c =0;
        order.ONTAP__Amount_Taxes__c =0;
        insert order;
        
        String fecha=string.valueOfGmt(order.ONTAP__DeliveryDate__c);
        
        ONTAP__Order_Item__c oi=new ONTAP__Order_Item__c();
        oi.ONTAP__CustomerOrder__c=order.Id;
        //---------------------------------------------
        oi.ONTAP__ProductId__c = '000000000000009908';
        oi.ONTAP__ItemProduct__c = producto.Id;
        oi.ONTAP__ActualQuantity__c = 1.0;
        oi.ONTAP__Amount_Line_Total__c = 1.0;
        oi.ONTAP__LineAmount__c = 1.0;
        oi.ONTAP__TaxValue__c = 1.0;
        oi.ONTAP__Amount_Tax_Total__c = 1.0;
        oi.ONTAP__UnitPrice__c = 1.0;
        oi.ONTAP__UnitPriceFull__c = 1.0;
        oi.Add_Empties__c = 'X';
        oi.ONTAP__Discounts__c = 1.0;
        oi.POSEX__c = 1;
        oi.ONCALL_pfn_reason__c = 'test';
        oi.ISSM_Uint_Measure_Code__c = 'test';
        String recordtypes = 'SV_Order_Item';
        String recordtypeItemid = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByDeveloperName().get(recordtypes).getRecordTypeId();
        oi.recordTypeId = recordtypeItemid;
        //---------------------------------------------
        insert  oi;
        System.debug('------------------------');
        System.debug('ORDER ITEM: ' + oi);
        System.debug('------------------------');
        
        ONCALL__Call__c call = new ONCALL__Call__c();            
        call.ONCALL__Primary_Order__c ='Test';
        call.ONCALL__POC__c = acc.Id;
        insert call;
        
        /*List<wrapperClass>wrap1=new List<wrapperClass>();
        wrapperClass wr=new wrapperClass();
        wr.promosku='000000000000009888';
        wr.promoNamedescription='';
        wr.promoDescTosend='';
        wrap1.add(wr);*/
        
        List<json_deals> deals = new List<json_deals>();   
        json_deals wrpprom = new json_deals();
        //----------
        wrpprom.scaleid = 'test';
        wrpprom.percentage = 1.0;
        wrpprom.description = 'test';
        //----------
        wrpprom.pronr='248856';
        wrpprom.condcode='YSDM';
        wrpprom.posex='1';
        wrpprom.amount=-1; 
        deals.add(wrpprom);
        String JSONStringProm = JSON.serialize(deals);
       
        List<ONCALL__Call__c> lstAcc = [SELECT Id,ONCALL__POC__c FROM ONCALL__Call__c ];   
        List<ONTAP__Order__c> lsOrder=[Select Id,ONTAP__OrderAccount__c,ISSM_PaymentMethod__c,ONTAP__DeliveryDate__c,ONTAP__Amount_Total__c,ONTAP__Amount_Sub_Total_Product__c FROM ONTAP__Order__c];
        //List<ONTAP__Order_Item__c>lsOrderI=[];
        ONTAP__Order__c o=lsOrder.get(0);
        ONCALL__Call__c ca=lstAcc.get(0);
        
        List<wrapperClass> product = new List<wrapperClass>();
		wrapperClass wrp = new wrapperClass();   
        wrp.productType = GlobalStrings.ORDER_ADD_EMPTIES; 
        //wrp.productType = GlobalStrings.HOD_ORDER_ADD_EMPTIES;
        /*wrp.productType = GlobalStrings.HOD_ORDER_ADD_EMPTIES;
        wrp.productType = GlobalStrings.SP_ORDER_ADD_EMPTIES;*/
        //----------
        wrp.tipo = 'test';
        wrp.selectedByUser = true;
        wrp.position = '1';
        wrp.quantity = 1.0;
        wrp.pronr = 'test';
        wrp.promosku = 'test';
        wrp.promoNamedescription = 'test';
        wrp.promoDescTosend = 'test';
        wrp.productRecord = 'test';
        //----------
        wrp.total=1.5;
        wrp.subtotal=1.5;
        wrp.tax=0;
        wrp.discount=-0.27;
        wrp.sku='000000000000009888';
        wrp.unitPrice=83.3;
        wrp.productMetadataType='Bottle';
        product.add(wrp);
        System.debug('------------------------');
        System.debug('WRAPPERCLASS: ' + product);
        System.debug('------------------------');
        String JSONStringProduct = JSON.serialize(product);
        System.debug('------------------------');
        System.debug('JSONStringProm: ' + JSONStringProm);
        System.debug('lstAcc: ' + lstAcc);
        System.debug('o.ONTAP__OrderAccount__c: ' + o.ONTAP__OrderAccount__c);
        System.debug('JSONStringProduct: ' + JSONStringProduct);
        System.debug('fecha: ' + fecha);
        System.debug('order.ISSM_PaymentMethod__c: ' + order.ISSM_PaymentMethod__c);
        System.debug('------------------------');
        
        //------------
        List<Order_Types__mdt> docs = new List<Order_Types__mdt>();
        //Map<String, Order_Types__mdt> mapOrdTypes = new Map<String, Order_Types__mdt>();
        Order_Types__mdt otm = new Order_Types__mdt(ID_SAP__c='ZSV7',
                                                     Es__c='HOD Añadir Envases',
                                                     Order_Item_Record_Type__c='SV_Order_Item',
                                                     MasterLabel='HOD Add Empties',
                                                     Countries__c='HN',
                                                     Atr_4__c='C1');
        //mapOrdTypes.put('test',otm);
        docs.add(otm);
        System.debug('------------------------');
        System.debug('DOCS: ' + docs);
        //System.debug('mapOrdTypes: ' + mapOrdTypes);
        System.debug('------------------------');
		//------------
        
        LookUpAccountCloseSale.genOrder(JSONStringProduct,lstAcc,o.ONTAP__OrderAccount__c,JSONStringProm,fecha ,order.ISSM_PaymentMethod__c);       
        
    }
    
    
    @isTest static void test_initFetchLookUp(){
    	LookUpAccountCloseSale obj01 = new LookUpAccountCloseSale();
        List<RecordType> records = [Select Id,Name from RecordType where SobjectType='Account'];

        Account acc = new Account();
        acc.ONCALL__KATR4__c = 'C1';
        //acc.ONTAP__ExternalKey__c = 'XX911';
        acc.ONTAP__ExternalKey__c = 'SVHBF';
       	List<Account> account_p = [Select Id, Name From Account where ONCALL__KATR4__c = 'C1' Limit 1];  
        LookUpAccountCloseSale.initFetchLookUp(account_p[0].Id);
    }
}