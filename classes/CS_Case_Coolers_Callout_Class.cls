/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Clase: CS_CASE_COOLERS_CALLOUT_cls.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 23/01/2019           Debbie Zacarias       Creacion de la clase para Callout 
*/

public class CS_Case_Coolers_Callout_Class 
{
    /**
    * Method for send a request to SAP
    * @author: jose.l.vargas.lara@accenture.com
    * @param List with case id
    * @return Void
    */
    @future(callout=true)
    public static void sendRequest(List<string> lstIdCase)
    {   
        List<Case> lstCaseDetail = new List<Case>();
        List<Case> lstCaseUpdate = new  List<Case>();
        string numeroPedido = '';
        
        CS_CoolerSettings__c cool = CS_CoolerSettings__c.getOrgDefaults();
        End_Point__mdt mdt = ([SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt Where DeveloperName = 'Create_SAP_Order']);
        
        string countryESV = Label.CS_Country_El_Salvador;
        string countryHN = Label.CS_Country_Honduras;
        string endPoint = cool.URL__c;
        string jsonString  = '';
        string errorMess = '';
        string messageStatus = '';
        string successMessage = '';
        integer attempt = 0;
        Integer attmpLab = Integer.valueOf(Label.CS_Cooler_Attemps);
        Integer statusCode = Integer.valueOf(Label.CS_Status_Code);
        boolean flagAttempt = false;
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
                
        Map<String, Object> bodyResponse = new Map<String, Object>();
        Map<String, Object> etReturn = new Map<String, Object>();
        Map<String, Object> item = new MAp<String, Object>();
               
        try
        {
            /* Se realiza la consulta del detalle de los casos con los id */
            lstCaseDetail = [SELECT Id, ISSM_NumeroSAP__c, AccountId, CS_skuProduct__c,ISSM_InterncalComments__c, CS_Coolers_Logs__c, HONES_Case_Country__c, RecordType.DeveloperName, ISSM_Count__c 
                             FROM Case 
                             WHERE Id IN :  lstIdCase 
                             AND CS_Send_To_Mule__c = false 
                             AND (HONES_Case_Country__c =: countryHN OR HONES_Case_Country__c =: countryESV) 
                             AND CS_skuProduct__c != null];
            
            for(Case oSendCase : lstCaseDetail)
            {
                jsonString = generateJSONResponse(oSendCase);            
                request.setEndpoint(endPoint);
                request.setMethod('POST');
                request.setHeader('Content-Type', 'application/json;charset=UTF-8'); 
                request.setHeader('bearer', mdt.Token__c);
                request.setBody(jsonString);
                
                while(attempt < attmpLab && !flagAttempt)
                {
                    response = http.send(request);
                    if(response.getStatusCode() == statusCode)
                    {
                        bodyResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());                   
                        if(bodyResponse.get('id') == '')
                        {
                            errorMess = Label.CS_ERROR_CASE;
                            etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                            item = (Map<String,Object>) etReturn.get('item');
                            messageStatus = (String)item.get('message');

                            oSendCase.CS_Coolers_Logs__c = errorMess + ' : ' + messageStatus;              
                            oSendCase.CS_Send_To_Mule__c = false;
 
                           attempt++;
                           flagAttempt = false;
                        }
                        else
                        { 
                            etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                            item = (Map<String,Object>) etReturn.get('item');
                            successMessage = (String)item.get('message');
                            numeroPedido = (string)bodyResponse.get('id');
                            oSendCase.CS_Send_To_Mule__c = true;
                            oSendCase.CS_Id_Pedido_SAP__c = numeroPedido;
                            oSendCase.CS_Coolers_Logs__c = successMessage + ' Pedido: ' + numeroPedido;

                            attempt+=2;
                            flagAttempt = true; 
                         }                    
                     }
                     else
                     {
                         oSendCase.CS_Coolers_Logs__c = String.valueOf(response.getStatusCode());
                         attempt++;
                         flagAttempt = false;
                     }
                    
                     if(!lstCaseUpdate.Contains(oSendCase))
                         lstCaseUpdate.add(oSendCase);
                 }
             }
            
             /* Se actualiza la informacion de los casos */
             ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
             Update lstCaseUpdate;
            
             /* Se envia correo electrónico para los casos de el salvador */
            SendEmailInstall(lstIdCase);
         }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest_Trigger  Stack trace: ' + ex.getStackTraceString());
        }
    }
       
    /**
    * method that send cases requests for coolers.
    * @author: d.zacarias.cantillo@accenture.com
    * @param Case that is sent to WS
    * @param Integer that controls attemps to send
    * @return Integer that controls attemps to send
    */ 
    @future(callout=true)
    public static void sendRequest(String caseCooler)
    { 
        //Variables        
        List<case> lstId = new List<Case>();
        string numeroPedido = '';
        
        CS_CoolerSettings__c cool = CS_CoolerSettings__c.getOrgDefaults();
        End_Point__mdt mdt = ([SELECT DeveloperName,MasterLabel,Token__c,URL__c FROM End_Point__mdt Where DeveloperName = 'Create_SAP_Order']);     
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        
        Map<String, Object> bodyResponse = new Map<String, Object>();
        Map<String, Object> etReturn = new Map<String, Object>();
        Map<String, Object> item = new MAp<String, Object>();
        
        String countryESV = Label.CS_Country_El_Salvador;
        String countryHN = Label.CS_Country_Honduras;
        String endPoint = cool.URL__c;
        String messageStatus = '';        
        String jsonString = '';
        String errorMess = '';
        String successMessage = '';
            
        Boolean flagAttempt = false;
        Integer attempt = 0; 
        Integer attmpLab = Integer.valueOf(Label.CS_Cooler_Attemps);
        Integer statusCode = Integer.valueOf(Label.CS_Status_Code);            
        
        try
        {
            Case casoForSend = ([SELECT Id, ISSM_NumeroSAP__c, AccountId, CS_skuProduct__c,ISSM_InterncalComments__c, CS_Coolers_Logs__c, HONES_Case_Country__c, RecordType.DeveloperName, ISSM_Count__c
                                 FROM Case 
                                 WHERE Id=:caseCooler 
                                 AND CS_Send_To_Mule__c = false 
                                 AND (HONES_Case_Country__c =:countryHN OR HONES_Case_Country__c =:countryESV) 
                                 AND CS_skuProduct__c != null]); 
            
            jsonString = generateJSONResponse(casoForSend);            
            request.setEndpoint(endPoint);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json;charset=UTF-8'); 
            request.setHeader('bearer', mdt.Token__c);
            request.setBody(jsonString);
            
            while(attempt < attmpLab && !flagAttempt)
            {
                response = http.send(request);
                
                if(response.getStatusCode() == statusCode)
                {
                    bodyResponse = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());                   
                    if(bodyResponse.get('id') == '')
                    {
                        errorMess = Label.CS_ERROR_CASE;
                        etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                        item = (Map<String,Object>) etReturn.get('item');
                        messageStatus = (String)item.get('message');                
                        casoForSend.CS_Coolers_Logs__c = errorMess + ' : ' + messageStatus;                     
                        casoForSend.CS_Send_To_Mule__c = false;
                        
                        flagAttempt = false;  
                        attempt++;
                    }
                    else
                    { 
                        numeroPedido = (string)bodyResponse.get('id');
                        etReturn = (Map<String, Object>) bodyResponse.get('etReturn');
                        item = (Map<String,Object>) etReturn.get('item');
                        successMessage = (String)item.get('message');
                        casoForSend.CS_Send_To_Mule__c = true;
                        casoForSend.CS_Id_Pedido_SAP__c = numeroPedido;
                        casoForSend.CS_Coolers_Logs__c = successMessage + ' Pedido: ' + numeroPedido;
                        
                        flagAttempt = true;
                        attempt+=2;
                    }                    
                }
                else
                {
                    casoForSend.CS_Coolers_Logs__c = Label.CS_Mensaje_Error_Http + ' : ' + String.valueOf(response.getStatusCode());
                    flagAttempt = false;	
                    attempt++;
                }
            }
            lstId.add(casoForSend);
            
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            ISSM_TriggerManager_cls.inactivate('CALLOUT');
            update lstId;
            
            /* Se envia correo electrónico con el formato de consigna, solo para El Salvador */
            SendEmailInstall(caseCooler);
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.sendRequest  Stack trace: ' + ex.getStackTraceString());
        }
    }  
    
    /**
    * method to generate the json to send.
    * @author: d.zacarias.cantillo@accenture.com
    * @param Case that is serialized to json
    * @return String json generated
    */   
    public static String generateJSONResponse(Case caseCooler)
    {
        String countryESV = Label.CS_Country_El_Salvador;
        String countryHN = Label.CS_Country_Honduras;
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
        
        CS_CoolerSettings__c cool = CS_CoolerSettings__c.getOrgDefaults();
        Map<String,Object> mapToSerialize = new Map<String,Object>();
        String json = '';
        
        CS_Cooler_Request_Class partner = new CS_Cooler_Request_Class();
        Map<String, List<Object>> mapPartner = new Map<String, List<Object>>();
        
        partner.role = cool.role__c;
        partner.numb = caseCooler.ISSM_NumeroSAP__c;
        partner.itmNumber = '';        
        
        mapPartner.put('partner',new List<Object>{partner});
        
        CS_Cooler_Request_Class iwHeader = new CS_Cooler_Request_Class();
        iwHeader.sfdcId = caseCooler.Id;
        if(caseCooler.HONES_Case_Country__c.equals(countryHN))
        {
            if(caseCooler.RecordType.DeveloperName.equals(rtInstallationCooler))
            {
                iwHeader.orderType = cool.orderTypeHNInstall__c;
            }
            else if(caseCooler.RecordType.DeveloperName.equals(rtRetirementCooler))
            {
                iwHeader.orderType = cool.orderTypeHNRetire__c;
            }
            
        }
        else if(caseCooler.HONES_Case_Country__c.equals(countryESV))
        {
            if(caseCooler.RecordType.DeveloperName.equals(rtInstallationCooler))
            {
                iwHeader.orderType = cool.orderTypeESVInstall__c;
            }
            else if(caseCooler.RecordType.DeveloperName.equals(rtRetirementCooler))
            {
                iwHeader.orderType = cool.orderTypeESVRetire__c;
            }
            
        }
        iwHeader.salesOrg = '';
        iwHeader.deliveryDate = '';
        iwHeader.paymentMethod = cool.paymentMethod__c;                     
        iwHeader.orderReason = '';
        iwHeader.customerOrder = cool.customOrder__c;
        
        CS_Cooler_Request_Class items = new CS_Cooler_Request_Class();
        items.sku = caseCooler.CS_skuProduct__c;
        items.position = '';
        items.quantity = (Integer)caseCooler.ISSM_Count__c;
        items.measurecode = cool.measurecode__c;
        items.empties = '';
        
        Map<String, List<Object>> mapItems = new Map<String, List<Object>>();
        mapItems.put('items',new List<Object>{items});        
        CS_Cooler_Request_Class obj4 = new CS_Cooler_Request_Class();
        Map<String, List<Object>> mapDeals = new Map<String, List<Object>>();
        mapDeals.put('deals', new List<Object>());
        
        mapToSerialize.put('iwHeader',iwHeader);        
        mapToSerialize.putAll(mapPartner);
        mapToSerialize.putAll(mapItems);
        mapToSerialize.putAll(mapDeals);
        
        json = SYSTEM.JSON.serialize(mapToSerialize,true);
        return json;
    }
    
        /**
    * method to generate a format cooler install and send mail to owner case
    * @author: jose.l.vargas.lara@accenture.com
    * @param List with Case Id
    * @return Void
    */
    private static void SendEmailInstall(List<string> lstCases)
    {
        string messageBody = '';
        string fileName = '';
        string messageSubject = '';
        Blob pageData;
        
        List<Case> lstCaseSend = new List<Case>();
        
        try
        {
            lstCaseSend = [SELECT Id, CaseNumber, HONES_Case_Country__c, CS_Id_Pedido_SAP__c, CS_Send_To_Mule__c, RecordType.DeveloperName, Owner.Email, CS_Fecha_Creacion_Caso_FRM__c,
                                  Account.ONTAP__SAP_Number__c, Account.Name, Account.ONTAP__Street__c, Account.Phone, Account.ONTAP__Neighborhood__c, Account.ONTAP__Municipality__c,
                                  Account.HONES_AddressReference__c, Account.ONTAP__LegalName__c, CS_AccountChannel__c, CS_AccountSubChannel__c, Account.V360_VisitFrequency__c,
                                  Subject, Description, Account.V360_SalesZone__c, Account.V360_SalesZoneAssignedCoolers__r.V360_User__r.Name, CS_skuProduct__c, Equipment_model__c,
                                  Account.Owner.ONTAP__SAPUserId__c, Account.Owner.Name, Account.V360_SalesZoneAssignedPresaler__r.Name, Account.Owner.MobilePhone, ISSM_CaseForceNumber__r.Id
                           FROM Case WHERE Id IN : lstCases];

            for(Case oCaseSend : lstCaseSend)
            {
                        
                if(oCaseSend.HONES_Case_Country__c == Label.CS_Country_El_Salvador && oCaseSend.CS_Id_Pedido_SAP__c != null && oCaseSend.CS_Id_Pedido_SAP__c != '' && oCaseSend.CS_Send_To_Mule__c 
                   && oCaseSend.RecordType.DeveloperName == Label.CS_RT_Installation_Cooler)
                {
                    PageReference pageFormatCoolers = new PageReference('/apex/CS_FormatoCoolers_Page');
                    
                    pageFormatCoolers.getParameters().put('id', oCaseSend.Id);
                    pageFormatCoolers.getParameters().put('idSAP', oCaseSend.CS_Id_Pedido_SAP__c);
                    pageFormatCoolers.getParameters().put('FechaCreacion', oCaseSend.CS_Fecha_Creacion_Caso_FRM__c );
                    pageFormatCoolers.getParameters().put('CodigoCliente', oCaseSend.Account.ONTAP__SAP_Number__c);
                    pageFormatCoolers.getParameters().put('NombreCliente', oCaseSend.Account.Name);
                    pageFormatCoolers.getParameters().put('Direccion', oCaseSend.Account.ONTAP__Street__c);
                    pageFormatCoolers.getParameters().put('Telefono', oCaseSend.Account.Phone);
                    pageFormatCoolers.getParameters().put('Colonia', oCaseSend.Account.ONTAP__Neighborhood__c);
                    pageFormatCoolers.getParameters().put('Municipio', oCaseSend.Account.ONTAP__Municipality__c);
                    pageFormatCoolers.getParameters().put('Referencia', oCaseSend.Account.HONES_AddressReference__c);
                    pageFormatCoolers.getParameters().put('NombreLegal', oCaseSend.Account.ONTAP__LegalName__c);
                    pageFormatCoolers.getParameters().put('Canal', oCaseSend.CS_AccountChannel__c);
                    pageFormatCoolers.getParameters().put('SubCanal', oCaseSend.CS_AccountSubChannel__c);
                    pageFormatCoolers.getParameters().put('FrecuenciaVisita', oCaseSend.Account.V360_VisitFrequency__c);
                    pageFormatCoolers.getParameters().put('Subject', oCaseSend.Subject);
                    pageFormatCoolers.getParameters().put('Description', oCaseSend.Description);
                    pageFormatCoolers.getParameters().put('ZonaVentas', oCaseSend.Account.V360_SalesZone__c);
                    pageFormatCoolers.getParameters().put('TeamLead', oCaseSend.Account.V360_SalesZoneAssignedCoolers__r.V360_User__r.Name);
                    pageFormatCoolers.getParameters().put('Modelo', oCaseSend.CS_skuProduct__c);
                    pageFormatCoolers.getParameters().put('Material', oCaseSend.Equipment_model__c);
                    pageFormatCoolers.getParameters().put('UserId', oCaseSend.Account.Owner.ONTAP__SAPUserId__c);
                    pageFormatCoolers.getParameters().put('OwnerName', oCaseSend.Account.Owner.Name);
                    pageFormatCoolers.getParameters().put('PreVenta', oCaseSend.Account.V360_SalesZoneAssignedPresaler__r.Name);
                    pageFormatCoolers.getParameters().put('Mobile', oCaseSend.Account.Owner.MobilePhone);
                
                    if(!Test.isRunningTest())
                        pageData = pageFormatCoolers.getContentAsPDF();
                    else
                        pageData = Blob.valueOf('Ejecucion de la clase de prueba');
                                        
                    fileName = 'Solicitud_Consignacion_EF_' + oCaseSend.CaseNumber + '.pdf';
                    Attachment oAttachmentPdf = new Attachment( ParentId = oCaseSend.Id, Body = pageData, Name = fileName);
                    Insert oAttachmentPdf;
                    
                    Attachment oAttachmentPdfForce = new Attachment(ParentId = oCaseSend.ISSM_CaseForceNumber__r.Id, Body = pageData, Name = fileName);
                    Insert oAttachmentPdfForce;
                
                    /* Envio de correo electronico */
                    Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment();
                    mailAtt.setBody(pageData);
                    mailAtt.setContentType('application/pdf');
                    mailAtt.setFileName(fileName);
                
                    Messaging.SingleEmailMessage mess = new Messaging.SingleEmailMessage();
                    messageSubject = Label.CS_Subject_Formato.replace('{CaseNumber}', oCaseSend.CaseNumber);
                    mess.setSubject(messageSubject);
                    mess.setToAddresses(new String[]{oCaseSend.Owner.Email});
                    
                    messageBody = Label.CS_Body_Formato.replace('{CaseNumber}', oCaseSend.CaseNumber);
                    mess.setPlainTextBody(messageBody);
                    mess.setFileAttachments(new Messaging.EmailFileAttachment[]{mailAtt});
                    Messaging.sendEmail(new Messaging.Email[]{mess}, false);
                }
            }      
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Stack trace: ' + ex.getStackTraceString());   
        }
    }
    
    /**
    * method to generate a format cooler install and send mail to owner case
    * @author: jose.l.vargas.lara@accenture.com
    * @param idCaso with Case Id
    * @return Void
    */
    private static void SendEmailInstall(string idCaso)
    {
        Case oCaseSend = new Case();
        string messageBody = '';
        string fileName = '';
        string messageSubject = '';
         Blob pageData;
        
        try
        {
            oCaseSend = [SELECT Id, CaseNumber, HONES_Case_Country__c, CS_Id_Pedido_SAP__c, CS_Send_To_Mule__c, RecordType.DeveloperName, Owner.Email, CS_Fecha_Creacion_Caso_FRM__c,
                                  Account.ONTAP__SAP_Number__c, Account.Name, Account.ONTAP__Street__c, Account.Phone, Account.ONTAP__Neighborhood__c, Account.ONTAP__Municipality__c,
                                  Account.HONES_AddressReference__c, Account.ONTAP__LegalName__c, CS_AccountChannel__c, CS_AccountSubChannel__c, Account.V360_VisitFrequency__c,
                                  Subject, Description, Account.V360_SalesZone__c, Account.V360_SalesZoneAssignedCoolers__r.V360_User__r.Name, CS_skuProduct__c, Equipment_model__c,
                                  Account.Owner.ONTAP__SAPUserId__c, Account.Owner.Name, Account.V360_SalesZoneAssignedPresaler__r.Name, Account.Owner.MobilePhone, ISSM_CaseForceNumber__r.Id
                         FROM Case WHERE Id =: idCaso];
                        
            if(oCaseSend.HONES_Case_Country__c == Label.CS_Country_El_Salvador && oCaseSend.CS_Id_Pedido_SAP__c != null && oCaseSend.CS_Id_Pedido_SAP__c != '' && oCaseSend.CS_Send_To_Mule__c 
               && oCaseSend.RecordType.DeveloperName == Label.CS_RT_Installation_Cooler)
            {
                PageReference pageFormatCoolers = new PageReference('/apex/CS_FormatoCoolers_Page');
                
                pageFormatCoolers.getParameters().put('id', oCaseSend.Id);
                pageFormatCoolers.getParameters().put('idSAP', oCaseSend.CS_Id_Pedido_SAP__c);
                pageFormatCoolers.getParameters().put('FechaCreacion', oCaseSend.CS_Fecha_Creacion_Caso_FRM__c );
                pageFormatCoolers.getParameters().put('CodigoCliente', oCaseSend.Account.ONTAP__SAP_Number__c);
                pageFormatCoolers.getParameters().put('NombreCliente', oCaseSend.Account.Name);
                pageFormatCoolers.getParameters().put('Direccion', oCaseSend.Account.ONTAP__Street__c);
                pageFormatCoolers.getParameters().put('Telefono', oCaseSend.Account.Phone);
                pageFormatCoolers.getParameters().put('Colonia', oCaseSend.Account.ONTAP__Neighborhood__c);
                pageFormatCoolers.getParameters().put('Municipio', oCaseSend.Account.ONTAP__Municipality__c);
                pageFormatCoolers.getParameters().put('Referencia', oCaseSend.Account.HONES_AddressReference__c);
                pageFormatCoolers.getParameters().put('NombreLegal', oCaseSend.Account.ONTAP__LegalName__c);
                pageFormatCoolers.getParameters().put('Canal', oCaseSend.CS_AccountChannel__c);
                pageFormatCoolers.getParameters().put('SubCanal', oCaseSend.CS_AccountSubChannel__c);
                pageFormatCoolers.getParameters().put('FrecuenciaVisita', oCaseSend.Account.V360_VisitFrequency__c);
                pageFormatCoolers.getParameters().put('Subject', oCaseSend.Subject);
                pageFormatCoolers.getParameters().put('Description', oCaseSend.Description);
                pageFormatCoolers.getParameters().put('ZonaVentas', oCaseSend.Account.V360_SalesZone__c);
                pageFormatCoolers.getParameters().put('TeamLead', oCaseSend.Account.V360_SalesZoneAssignedCoolers__r.V360_User__r.Name);
                pageFormatCoolers.getParameters().put('Modelo', oCaseSend.CS_skuProduct__c);
                pageFormatCoolers.getParameters().put('Material', oCaseSend.Equipment_model__c);
                pageFormatCoolers.getParameters().put('UserId', oCaseSend.Account.Owner.ONTAP__SAPUserId__c);
                pageFormatCoolers.getParameters().put('OwnerName', oCaseSend.Account.Owner.Name);
                pageFormatCoolers.getParameters().put('PreVenta', oCaseSend.Account.V360_SalesZoneAssignedPresaler__r.Name);
                pageFormatCoolers.getParameters().put('Mobile', oCaseSend.Account.Owner.MobilePhone);
                
                if(!Test.isRunningTest())
                    pageData = pageFormatCoolers.getContentAsPDF();
                else
                    pageData = Blob.valueOf('Ejecucion de la clase de prueba');
                                        
                fileName = 'Solicitud_Consignacion_EF_' + oCaseSend.CaseNumber + '.pdf';
                Attachment oAttachmentPdf = new Attachment( ParentId = oCaseSend.Id, Body = pageData, Name = fileName);
                Insert oAttachmentPdf;
                    
                Attachment oAttachmentPdfForce = new Attachment(ParentId = oCaseSend.ISSM_CaseForceNumber__r.Id, Body = pageData, Name = fileName);
                Insert oAttachmentPdfForce;
                
                /* Envio de correo electronico */
                Messaging.EmailFileAttachment mailAtt = new Messaging.EmailFileAttachment();
                mailAtt.setBody(pageData);
                mailAtt.setContentType('application/pdf');
                mailAtt.setFileName(fileName);
                
                Messaging.SingleEmailMessage mess = new Messaging.SingleEmailMessage();
                messageSubject = Label.CS_Subject_Formato.replace('{CaseNumber}', oCaseSend.CaseNumber);
                mess.setSubject(messageSubject);
                mess.setToAddresses(new String[]{oCaseSend.Owner.Email});
                    
                messageBody = Label.CS_Body_Formato.replace('{CaseNumber}', oCaseSend.CaseNumber);
                mess.setPlainTextBody(messageBody);
                mess.setFileAttachments(new Messaging.EmailFileAttachment[]{mailAtt});
                Messaging.sendEmail(new Messaging.Email[]{mess}, false);                  
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Message: ' + ex.getMessage());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Cause: ' + ex.getCause());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Line number: ' + ex.getLineNumber());   
            System.debug('CS_CASE_COOLERS_CALLOUT_cls.SendEmailInstall  Stack trace: ' + ex.getStackTraceString());   
        }
    }
}