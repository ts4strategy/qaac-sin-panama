/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_TRIGGER_CASE_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 08/01/2019           Jose Luis Vargas       Crecion de la clase para testing de la clase CS_TRIGGER_CASE_CLASS 
 */

@isTest
private class CS_Trigger_Case_Class_Test 
{
    /**
    * Method for test the method afterInsert
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_afterInsert()
    {
        ISSM_MappingFieldCase__c oNewSetting = new ISSM_MappingFieldCase__c();
        oNewSetting.Name = 'AccountId';
        oNewSetting.ISSM_APICaseForce__c = 'ONTAP__Account__c';
        oNewSetting.Active__c = true;
        Insert oNewSetting;
        
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        CS_Trigger_Case_Class oTrigger = new CS_Trigger_Case_Class();
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        insert oNewCase;
        
        oTrigger.afterInsert();
    }
    
    /**
    * Method for test the method afterUpdate
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_afterUpdate()
    {
        ISSM_MappingFieldCase__c oNewSetting = new ISSM_MappingFieldCase__c();
        oNewSetting.Name = 'AccountId';
        oNewSetting.ISSM_APICaseForce__c = 'ONTAP__Account__c';
        oNewSetting.Active__c = true;
        Insert oNewSetting;
        
        oNewSetting = new ISSM_MappingFieldCase__c();
        oNewSetting.Name = 'ISSM_OwnerCaseForce__c';
        oNewSetting.ISSM_APICaseForce__c = 'OwnerId';
        oNewSetting.Active__c = true;
        Insert oNewSetting;
        
        Account oNewAccount = new Account(Name = 'Account Test');   
        insert oNewAccount;
        
        CS_Trigger_Case_Class oTrigger = new CS_Trigger_Case_Class();
        
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypification.ISSM_TypificationLevel4__c = 'Reparación';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Dias_Primer_Contacto__c = 2;
        oNewTypification.CS_Days_to_End__c = 0.0;
        Insert oNewTypification;
        
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        Insert oNewCaseForce;
        
        RecordType RecordTyp = ([SELECT id FROM RecordType WHERE DeveloperName = 'CS_RT_Installation_Cooler']);
        RecordType RecordTyp1 = ([SELECT id FROM RecordType WHERE DeveloperName = 'CS_RT_Retirement_Cooler']);
                
        Case oNewCase = new Case();
        oNewCase.Accountid = oNewAccount.Id;
        oNewCase.ISSM_CaseForceNumber__c = oNewCaseForce.Id;
        oNewCase.Description = 'Test Description';
        oNewCase.Status = 'New';
        oNewCase.Subject = 'Test Subject';
        oNewCase.ISSM_TypeSponsorship__c = 'Queue';
        oNewCase.HONES_Case_Country__c = 'Honduras';
        oNewCase.ISSM_TypificationNumber__c = oNewTypification.Id;
        insert oNewCase;
        
        Case oCase = new Case();
        oCase.Accountid = oNewAccount.Id;
        oCase.ISSM_CaseForceNumber__c = oNewCaseForce.Id;
        oCase.Description = 'Test Description';
        oCase.Status = 'New';
        oCase.Subject = 'Test Subject';
        oCase.ISSM_TypeSponsorship__c = 'Queue';
        oCase.HONES_Case_Country__c = 'Honduras';
        oCase.ISSM_TypificationNumber__c = oNewTypification.Id;
        oCase.RecordTypeId = RecordTyp.id;
        insert oCase;
        
        Case oCaseCall = new Case();
        oCaseCall.Accountid = oNewAccount.Id;
        oCaseCall.ISSM_CaseForceNumber__c = oNewCaseForce.Id;
        oCaseCall.Description = 'Test Description';
        oCaseCall.Status = 'New';
        oCaseCall.Subject = 'Test Subject';
        oCaseCall.ISSM_TypeSponsorship__c = 'Queue';
        oCaseCall.HONES_Case_Country__c = 'Honduras';
        oCaseCall.ISSM_TypificationNumber__c = oNewTypification.Id;
        oCaseCall.RecordTypeId = RecordTyp1.id;
        insert oCaseCall;
        
        ISSM_TriggerManager_cls.Activate();   
        Case oUpdateCase = new Case();
        oUpdateCase.Id = oNewCase.Id;
        oUpdateCase.Subject = 'Test Subject';
        Update oUpdateCase;
        
        Case oUpdateCase1 = new Case();
        oUpdateCase1.Id = oCase.Id;
        oUpdateCase1.CS_skuProduct__c = 'Test sku';
        oUpdateCase1.CS_Send_To_Mule__c = false;
        Update oUpdateCase1;
        
        Case oUpdateCaseCall = new Case();
        oUpdateCaseCall.Id = oCaseCall.Id;
        oUpdateCaseCall.CS_skuProduct__c = 'Test sku 1';
        oUpdateCaseCall.CS_Send_To_Mule__c = false;
        Update oUpdateCaseCall;
        
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        oTrigger.afterUpdate();
    }
}