/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase:CS_CASEGENERATION_CLASS_Test.apxc
 * Versión: 1.0.0.0
 * 
 * 
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 15/01/2019    Heron Zurita	  heron.zurita@accenture.com     Creación de la clase  
 */
@isTest
private class CS_CaseGeneration_Class_Test 
{
	/*
     * Method that insert all values the Account and generate tipification matrix both from Honduras and from Salvador
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/    
    @testSetup 
    static void setup() 
    {
        //GENERAL ACCONT FOR TESTING 
        Account acc=new Account();
        acc.Name='test1234';
        acc.ONTAP__Email__c='email@mail.com';
        acc.ONTAP__Street__c='street';
        acc.ONTAP__Neighborhood__c='neigh';
        acc.ONTAP__Municipality__c='municipal';
        insert acc;   
     	

        //user for Honduras
        User oUpdateUserOne = new User();
        oUpdateUserOne.Country__c = 'Honduras';
        oUpdateUserOne.Username= 'juan@acc.com'; 
        oUpdateUserOne.LastName= 'Sawuan'; 
        oUpdateUserOne.Email= 'juan@acc.com'; 
        oUpdateUserOne.Alias= 'Juanis'; 
        oUpdateUserOne.CommunityNickname = 'jusuw';
        oUpdateUserOne.TimeZoneSidKey= 'America/Bogota'; 
        oUpdateUserOne.LocaleSidKey= 'es';
        oUpdateUserOne.EmailEncodingKey = 'ISO-8859-1';
        oUpdateUserOne.AboutMe = 'yas';
        oUpdateUserOne.LanguageLocaleKey = 'es';
        oUpdateUserOne.ProfileId= userinfo.getProfileId();
        insert oUpdateUserOne;
        
        
        //user for Salvador
        User oUpdateUsertwo = new User();
        oUpdateUsertwo.CS_On_Vacation__c=true;
        oUpdateUsertwo.CS_User_Replacement__c=oUpdateUserOne.Id;
        oUpdateUsertwo.Country__c = 'El Salvador';
        oUpdateUsertwo.Username= 'juans@acc.com'; 
        oUpdateUsertwo.LastName= 'swabns'; 
        oUpdateUsertwo.Email= 'juans@acc.com'; 
        oUpdateUsertwo.Alias= 'Juanis'; 
        oUpdateUsertwo.CommunityNickname = 'jusuws';
        oUpdateUsertwo.TimeZoneSidKey= 'America/Bogota'; 
        oUpdateUsertwo.LocaleSidKey= 'es';
        oUpdateUsertwo.EmailEncodingKey = 'ISO-8859-1';
        oUpdateUsertwo.AboutMe = 'yas';
        oUpdateUsertwo.LanguageLocaleKey = 'es';
        oUpdateUsertwo.ProfileId= userinfo.getProfileId();

        insert oUpdateUsertwo;
          
        acc.OwnerId=oUpdateUsertwo.Id;
        update acc;
       
        //Generate appsettings with userid
        ISSM_AppSetting_cs__c oCola = new ISSM_AppSetting_cs__c();
        oCola.ISSM_IdQueueWithoutOwner__c = oUpdateUsertwo.Id;
        oCola.Id_Queue_SV__c = oUpdateUsertwo.Id;
        oCola.Id_Queue_SACAgent_SV__c = oUpdateUsertwo.Id;

        Insert oCola;

        //Generate typification matrix Honduras
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypification.ISSM_TypificationLevel4__c = 'Reparación';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Dias_Primer_Contacto__c = 2;
        oNewTypification.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypification.ISSM_AssignedTo__c = 'User';
        oNewTypification.ISSM_OwnerUser__c = oUpdateUsertwo.Id ;
        oNewTypification.ISSM_Priority__c = 'Medium';
        oNewTypification.CS_Automatic_Closing_Case__c = true;
        oNewTypification.ISSM_OwnerQueue__c = 'WithOut Owner';
        oNewTypification.ISSM_IdQueue__c = '';         
        oNewTypification.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypification.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypification.CS_Days_to_End__c=3;
        Insert oNewTypification;
        
        
        //Generate typification matrix 
        ISSM_TypificationMatrix__c oNewTypificationS = new ISSM_TypificationMatrix__c();
        oNewTypificationS.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypificationS.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypificationS.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypificationS.ISSM_TypificationLevel4__c = 'Intalación';
        oNewTypificationS.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypificationS.CS_Dias_Primer_Contacto__c = 2;
        oNewTypificationS.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypificationS.ISSM_AssignedTo__c = 'Cold Team Leader';
        oNewTypificationS.ISSM_OwnerUser__c = oUpdateUserOne.Id ;
        oNewTypificationS.ISSM_Priority__c = 'Medium';
        oNewTypificationS.CS_Automatic_Closing_Case__c = true;
        oNewTypificationS.ISSM_OwnerQueue__c = 'WithOut Owner';     
        oNewTypificationS.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypificationS.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypificationS.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypificationS.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypificationS.CS_Days_to_End__c = 3;
        Insert oNewTypificationS;
        
        //Generate typification matrix 
        ISSM_TypificationMatrix__c oNewTypificationQ = new ISSM_TypificationMatrix__c();
        oNewTypificationQ.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypificationQ.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypificationQ.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypificationQ.ISSM_TypificationLevel4__c = 'Retiro';
        oNewTypificationQ.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypificationQ.CS_Dias_Primer_Contacto__c = 2;
        oNewTypificationQ.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypificationQ.ISSM_AssignedTo__c = 'Queue';
        oNewTypificationQ.ISSM_OwnerUser__c = oUpdateUserOne.Id ;
        oNewTypificationQ.ISSM_Priority__c = 'Medium';
        oNewTypificationQ.CS_Automatic_Closing_Case__c = true;
        oNewTypificationQ.ISSM_OwnerQueue__c = 'WithOut Owner';       
        oNewTypificationQ.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypificationQ.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypificationQ.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypificationQ.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypificationQ.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypificationQ.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypificationQ.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypificationQ.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypificationQ.CS_Days_to_End__c = 3;
        Insert oNewTypificationQ;     
        
        CS_CaseForce_RT__c oRecordType = new CS_CaseForce_RT__c();
        oRecordType.CS_Cooler_Installation__c = 'CS_RT_Installation_Cooler';
        oRecordType.CS_Cooler_Retirement__c = 'CS_RT_Retirement_Cooler';
        oRecordType.Ontap_HN_Cooler__c = 'CS_RT_CF_Case_Cooler';
        oRecordType.CS_CF_General_Case__c = 'CS_CF_RT_General_Case';
        oRecordType.CS_Cooler_Repair__c = 'CS_RT_Cooler_Repair';
        oRecordType.CS_General_Case__c = 'CS_RT_General_Case';
        oRecordType.Ontap_SV_Cooler__c = 'CS_RT_CF_Case_Cooler';
        Insert oRecordType;
        
        Contact oContacto = new Contact();
        oContacto.AccountId = acc.Id;
        oContacto.CS_Contacto_Principal__c = True;
        oContacto.LastName = 'Test';
        Insert oContacto;
        
        
        
        //Generate typification matrix Honduras
        ISSM_TypificationMatrix__c oNewTypificationS0 = new ISSM_TypificationMatrix__c();
        oNewTypificationS0.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypificationS0.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypificationS0.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypificationS0.ISSM_TypificationLevel4__c = 'Reparación';
        oNewTypificationS0.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypificationS0.CS_Dias_Primer_Contacto__c = 2;
        oNewTypificationS0.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypificationS0.ISSM_AssignedTo__c = 'User';
        oNewTypificationS0.ISSM_OwnerUser__c = oUpdateUsertwo.Id ;
        oNewTypificationS0.ISSM_Priority__c = 'Medium';
        oNewTypificationS0.CS_Automatic_Closing_Case__c = true;
        oNewTypificationS0.ISSM_OwnerQueue__c = 'WithOut Owner';
        oNewTypificationS0.ISSM_IdQueue__c = '';         
        oNewTypificationS0.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypificationS0.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypificationS0.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypificationS0.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypificationS0.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypificationS0.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypificationS0.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypificationS0.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypificationS0.CS_Days_to_End__c=3;
        Insert oNewTypificationS0;
                
        //Generate typification matrix 
        ISSM_TypificationMatrix__c oNewTypificationS1 = new ISSM_TypificationMatrix__c();
        oNewTypificationS1.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypificationS1.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypificationS1.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypificationS1.ISSM_TypificationLevel4__c = 'Intalación';
        oNewTypificationS1.ISSM_Countries_ABInBev__c = 'El Salvador';
        oNewTypificationS1.CS_Dias_Primer_Contacto__c = 2;
        oNewTypificationS1.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypificationS1.ISSM_AssignedTo__c = 'Cold Team Leader';
        oNewTypificationS1.ISSM_OwnerUser__c = oUpdateUserOne.Id ;
        oNewTypificationS1.ISSM_Priority__c = 'Medium';
        oNewTypificationS1.CS_Automatic_Closing_Case__c = true;
        oNewTypificationS1.ISSM_OwnerQueue__c = 'WithOut Owner';     
        oNewTypificationS1.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypificationS1.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypificationS1.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypificationS1.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypificationS1.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypificationS1.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypificationS1.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypificationS1.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypificationS1.CS_Days_to_End__c = 3;
        Insert oNewTypificationS1;
        
        //Generate typification matrix 
        ISSM_TypificationMatrix__c oNewTypificationS2 = new ISSM_TypificationMatrix__c();
        oNewTypificationS2.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypificationS2.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypificationS2.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypificationS2.ISSM_TypificationLevel4__c = 'Retiro';
        oNewTypificationS2.ISSM_Countries_ABInBev__c = 'El Salvador';
        oNewTypificationS2.CS_Dias_Primer_Contacto__c = 2;
        oNewTypificationS2.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypificationS2.ISSM_AssignedTo__c = 'Queue';
        oNewTypificationS2.ISSM_OwnerUser__c = oUpdateUserOne.Id ;
        oNewTypificationS2.ISSM_Priority__c = 'Medium';
        oNewTypificationS2.CS_Automatic_Closing_Case__c = true;
        oNewTypificationS2.ISSM_OwnerQueue__c = 'SV_WithoutOwner';       
        oNewTypificationS2.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypificationS2.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypificationS2.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypificationS2.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypificationS2.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypificationS2.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypificationS2.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypificationS2.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypificationS2.CS_Days_to_End__c = 3;
        Insert oNewTypificationS2;
        
        ISSM_TypificationMatrix__c oNewTypificationS3 = new ISSM_TypificationMatrix__c();
        oNewTypificationS3.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypificationS3.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypificationS3.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypificationS3.ISSM_TypificationLevel4__c = 'Matenimiento';
        oNewTypificationS3.ISSM_Countries_ABInBev__c = 'El Salvador';
        oNewTypificationS3.CS_Dias_Primer_Contacto__c = 2;
        oNewTypificationS3.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypificationS3.ISSM_AssignedTo__c = 'Queue';
        oNewTypificationS3.ISSM_OwnerUser__c = oUpdateUserOne.Id ;
        oNewTypificationS3.ISSM_Priority__c = 'Medium';
        oNewTypificationS3.CS_Automatic_Closing_Case__c = true;
        oNewTypificationS3.ISSM_OwnerQueue__c = 'SV_SacAgent';       
        oNewTypificationS3.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypificationS3.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypificationS3.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypificationS3.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypificationS3.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypificationS3.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypificationS3.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypificationS3.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypificationS3.CS_Days_to_End__c = 3;
        Insert oNewTypificationS3;
    }
    
    /*
     * test Method that test the generation of new cases
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/  
    @isTest 
    static void GenerateNewCase_case1() 
    {
        Account acc = [Select Id,ONTAP__Street__c,ONTAP__Neighborhood__c,ONTAP__Municipality__c From Account where Name='test1234' LIMIT 1 ];
        User us1 = [SELECT Id,Username from User where Username='juan@acc.com' LIMIT 1];
        User us2 = [SELECT Id,Username from User where Username='juans@acc.com' LIMIT 1];
        CS_CASEGENERATION_CLASS oNewCase = new CS_CASEGENERATION_CLASS();
        
        test.startTest();
        System.runAs(us1) 
        {
            oNewCase.filteredQuery = 'Cooler';
	        oNewCase.GenerateNewCase(acc.Id,'SAC','Solicitudes','Equipo Frío','Reparación','test description');
            oNewCase.GenerateNewCase(acc.Id,'SAC','Solicitudes','Equipo Frío','Instalación','test description');
            oNewCase.GenerateNewCase(acc.Id,'SAC','Solicitudes','Equipo Frío','Retiro','test description');
        }
        
        System.runAs(us2) 
        {
            oNewCase.filteredQuery = 'Cooler';
	        oNewCase.GenerateNewCase(acc.Id,'SAC','Solicitudes','Equipo Frío','Reparación','test description');
            oNewCase.GenerateNewCase(acc.Id,'SAC','Solicitudes','Equipo Frío','Instalación','test description');
            oNewCase.GenerateNewCase(acc.Id,'SAC','Solicitudes','Equipo Frío','Retiro','test description');
            oNewCase.GenerateNewCase(acc.Id,'SAC','Solicitudes','Equipo Frío','Matenimiento','test description');
        }      
        
        test.stopTest();   
    } 
}