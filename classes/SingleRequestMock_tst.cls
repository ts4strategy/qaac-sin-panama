/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Test Mock class to handle single request callout mock

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    25/08/2017 Daniel Peñaloza            Created class
*******************************************************************************/

@isTest
public class SingleRequestMock_tst implements HttpCalloutMock {

    // Instance attributes
    public Integer code  { get; set; }
    public String status { get; set; }
    public String body   { get; set; }
    public Map<String, String> mapResponseHeaders { get; set; }

    public SingleRequestMock_tst(String body) {
        this.body   = body;
        this.code   = 200;
        this.status = 'OK';
    }

    public SingleRequestMock_tst(String body, Integer code, String status) {
        this(body);
        this.code = code;
        this.status = status;
    }

    public SingleRequestMock_tst(String body, Integer code, String status, Map<String, String> mapResponseHeaders) {
        this(body, code, status);
        this.mapResponseHeaders = mapResponseHeaders;
    }

    /**
     * Send mock response
     * @param  req [description]
     * @return     [description]
     */
    public HttpResponse respond(HttpRequest req) {
        HttpResponse response = new HttpResponse();

        // Cabeceras HTTP adicionales para la respuesta
        if (mapResponseHeaders != null) {
            for (String key : this.mapResponseHeaders.keySet()) {
                response.setHeader(key, this.mapResponseHeaders.get(key));
            }
        }

        response.setBody(this.body);
        response.setStatusCode(this.code);
        response.setStatus(this.status);

        return response;
    }
}