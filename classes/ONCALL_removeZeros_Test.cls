/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: ONCALL_removeZeros_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 28/01/2019     Oscar Garcia            Creation of methods.
*/
@isTest
public class ONCALL_removeZeros_Test {
	@isTest static void test_drop_leading_zeros() {
        System.assertEquals('11111', ONCALL_removeZeros.drop_leading_zeros('11111'));
        System.assertEquals('11111', ONCALL_removeZeros.drop_leading_zeros('00000011111'));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros('abc'));
        System.assertEquals('99999', ONCALL_removeZeros.drop_leading_zeros(' 99999 '));
        System.assertEquals('999', ONCALL_removeZeros.drop_leading_zeros(' 00999 '));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros('w'));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros('00'));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros('(303) 317-2235'));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros('111 091'));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros('  '));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros(''));
        System.assertEquals(null, ONCALL_removeZeros.drop_leading_zeros(null));
    }
}