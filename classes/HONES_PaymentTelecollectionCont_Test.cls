/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase:HONES_PaymentTelecollectionController_Test.apxc
 * Versión: 1.0.0.0
 * 
 * 
 * 
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 15/01/2019    Heron Zurita	  heron.zurita@accenture.com     Creación de la clase  
 */
@isTest
private class HONES_PaymentTelecollectionCont_Test{
  /*
     * test Method which get the payment telecollection by id
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/    
  @isTest static void test_getById_UseCase1(){
   
     Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
     insert acc;
     ONCALL__Call__c call =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id);
     call.Name = 'Test';
     insert call;
    
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getById(call.Id);
  }
    
    /*
     * test Method which get the call  id
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/  
    @isTest static void test_getById_UseCase2(){
    
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getById('Test');
  }
    /*
     * test Method which get the picklist status values 
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/  
  @isTest static void test_getPickListValuesStatus_UseCase1(){
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getPickListValuesStatus();
  }
    /*
     * test Method which get the incomplete status values 
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/  
  @isTest static void test_getPickListValuesIncompleteStatus_UseCase1(){
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getPickListValuesIncompleteStatus();
  }
     /*
     * test Method which get the payment promise values 
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/ 
    
  @isTest static void test_getPickListValuesPaymentPromise_UseCase1(){
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getPickListValuesPaymentPromise();
  }
     /*
     * test Method which get the incomplete status values 
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/ 
  @isTest static void test_getPickListValuesClarificationRequest_UseCase1(){
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getPickListValuesClarificationRequest();
  }
    
     /*
     * test Method which get the clarificationType values 
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/ 
  @isTest static void test_getPickListValuesClarificationType_UseCase1(){
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getPickListValuesClarificationType();
  }
     /*
     * test Method which get the PaymentDelayReason values 
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/ 
  @isTest static void test_getPickListValuesPaymentDelayReason_UseCase1(){
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.getPickListValuesPaymentDelayReason();
  }
    
    /*
     * test Method that test generation of new cases of call
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/ 
    
  @isTest static void test_insertTelecobranza_UseCase1(){
      Account acc = new Account();
      acc.Name='test1';
      insert acc;
      
       Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Type__c.getDescribe();
            List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
            Map<String, String> Call_StatusPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : Call_StatusPick_list) {
                Call_StatusPick_values.put(v.getLabel(),v.getValue());
            }
      
      
      ONCALL__Call__c call = new ONCALL__Call__c();
      call.Name = 'Test';
      call.ONCALL__Call_Status__c = 'Complete';
      call.ONCALL__POC__c = acc.Id;
      
      call.ISSM_PaymentPromise__c = 'Yes';
      call.ISSM_ClarificationRequest__c ='Yes';
      call.ISSM_ClarificationType__c = Call_StatusPick_values.get(Label.PAYMENT_PROMISE);
      call.ISSM_CompromisedAmount__c = 10;
      call.ISSM_PaymentPromiseDate__c = Date.valueOf('2013-08-01');
      call.ISSM_PaymentDelayReason__c = 'ZAR';
      
      insert call;
      
      ONCALL__Call__c newUpdate = [SELECT Name, ISSM_PaymentPromise__c, ISSM_ClarificationRequest__c,ISSM_ClarificationType__c,ISSM_CompromisedAmount__c, ISSM_PaymentPromiseDate__c, ISSM_PaymentDelayReason__c, ONCALL__Call_Status__c, ISSM_Other_Reason__c FROM ONCALL__Call__c WHERE Name = 'Test' Limit 1];
      
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.insertTelecobranza(newUpdate.id,acc.Id,newUpdate.ISSM_PaymentPromise__c,newUpdate.ISSM_ClarificationRequest__c,newUpdate.ISSM_ClarificationType__c,newUpdate.ISSM_CompromisedAmount__c,String.valueOf(newUpdate.ISSM_PaymentPromiseDate__c),newUpdate.ISSM_PaymentDelayReason__c,newUpdate.ONCALL__Call_Status__c,'test data','');
  }
    
    @isTest static void test_insertTelecobranza_UseCase2(){
      Account acc = new Account();
      acc.Name='test1';
      insert acc;
        
        Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Type__c.getDescribe();
            List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
            Map<String, String> Call_StatusPick_values = new Map<String,String>();
            for( Schema.PicklistEntry v : Call_StatusPick_list) {
                Call_StatusPick_values.put(v.getLabel(),v.getValue());
            }
        
        Schema.DescribeFieldResult Call_StatusPick_s = ONCALL__Call__c.ONCALL__Call_Status__c.getDescribe();
            List<Schema.PicklistEntry> Call_StatusPick_list_s = Call_StatusPick_s.getPicklistValues();
            Map<String, String> Call_StatusPick_values_s = new Map<String,String>();
            for( Schema.PicklistEntry v : Call_StatusPick_list_s) {
                Call_StatusPick_values_s.put(v.getLabel(),v.getValue());
            }
      
      ONCALL__Call__c call = new ONCALL__Call__c();
      call.Name = 'Test Fail';
      call.ONCALL__Call_Status__c = Call_StatusPick_values_s.get(Label.INCOMPLETE_HEAD);
      system.debug('Comprobar estatus: ' + Call_StatusPick_values_s.get(Label.INCOMPLETE_HEAD));
      call.ONCALL__POC__c = acc.Id;
     
        
      call.ISSM_PaymentPromise__c = 'No';
      call.ISSM_ClarificationRequest__c ='Yes';
      call.ISSM_ClarificationType__c =  Call_StatusPick_values.get(Label.PAYMENT_PROMISE);
      call.ISSM_CompromisedAmount__c = 10;
      call.ISSM_PaymentPromiseDate__c = Date.valueOf('2013-08-01');
      call.ISSM_PaymentDelayReason__c = 'ZAR';
        
      insert call;
      
      ONCALL__Call__c newUpdate = [SELECT Name, ISSM_PaymentPromise__c, ISSM_ClarificationRequest__c,ISSM_ClarificationType__c,ISSM_CompromisedAmount__c, ISSM_PaymentPromiseDate__c, ISSM_PaymentDelayReason__c, ONCALL__Call_Status__c, ISSM_Other_Reason__c FROM ONCALL__Call__c WHERE Name = 'Test Fail' Limit 1];
      
    HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    HONES_PaymentTelecollectionController.insertTelecobranza(newUpdate.id,acc.Id,newUpdate.ISSM_PaymentPromise__c,newUpdate.ISSM_ClarificationRequest__c,newUpdate.ISSM_ClarificationType__c,newUpdate.ISSM_CompromisedAmount__c,String.valueOf(newUpdate.ISSM_PaymentPromiseDate__c),newUpdate.ISSM_PaymentDelayReason__c, newUpdate.ONCALL__Call_Status__c,'test data','');
  }
    
     /*
     * test Method that returns values call
     * Created By:heron.zurita@accenture.com
     * @params  void
     * @return void 
	*/ 
    
  @isTest static void test_infoCall_UseCase1(){
  	HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
    Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
    insert acc;
    ONCALL__Call__c call =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id);
    call.Name = 'Test';
    insert call;
    HONES_PaymentTelecollectionController.infoCall(call.Id);
  }
    @isTest static void test_getDeliveryDateHours(){
    	HONES_PaymentTelecollectionController obj01 = new HONES_PaymentTelecollectionController();
        Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
     	insert acc;
     	ONCALL__Call__c call =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id);
   		call.Name='Test';
    	call.ISSM_PaymentPromise__c='Yes';
        insert call;
        HONES_PaymentTelecollectionController.getDeliveryDateHours(call.Id);
        
    }
}