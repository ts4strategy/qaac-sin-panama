/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: MockHttpResponseGeneratorgenOrder.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 07/01/2019           Heron Zurita           Creation of methods.
*/

@isTest
global class MockHTTPResponseGeneratorgenOrder implements HttpCalloutMock {
     /*
     * Method that create HTTPResponse where verify the correct answer 
     * Created By:heron.zurita@accenture.com
     * @param HTTPRequest req
     * @return HTTPResponse 
*/
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and
        // return response.
        
        //System.assertEquals('https://abco-priceengine.herokuapp.com/dealslist', req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(200);             
        //res.setBody('{"outputMessages":[{"text":"el sku 9887 no es válido","type":"WARNING","key":"W"}],"order":{"country":"SV","accountId":"0011820881","salesOrg":"CS01","paymentMethod":"CASH","total":1.5,"subtotal":1.5,"tax":0,"discount":-0.27,"empties":{"totalAmount":0,"minimumRequired":0,"extraAmount":0},"items":[{"sku":"000000000000009888","quantity":1,"freeGood":false,"discount":-0.27,"tax":0,"subtotal":1.5,"total":1.5,"price":1.5,"unitPrice":1.5}]}}');
        res.setBody('{"outputMessages":[{"text":"el sku 9887 no es válido","type":"WARNING","key":"W"}],"order":{"country":"SV","accountId":"0011820881","salesOrg":"CS01","paymentMethod":"CASH","total":1.5,"subtotal":1.5,"tax":0,"discount":-0.27,"empties":{"totalAmount":0,"minimumRequired":0,"extraAmount":0},"items":[{"sku":"000000000000009888","quantity":1,"freeGood":false,"discount":-0.27,"tax":0,"subtotal":1.5,"total":1.5,"price":1.5,"unitPrice":1.5,"applieddeals": [{"pronr": "test", "condcode": "test", "amount": 1.0, "percentage": 1.0, "posex": "test", "scaleid": "test"}]}]}}');    
        System.debug('Respuesta'+res);
        
        return res;
    }
    
    
}