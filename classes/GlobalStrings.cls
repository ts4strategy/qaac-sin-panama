/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: GlobalStrings.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */

public class GlobalStrings {
    
    /** 
    * Obtaining custom label fields.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */    
    
    public static final String BDR =  LABEL.BDR;
    public static final String TELESALES =  LABEL.TELESALES;
    public static final String TELESALES_CALL_RECORDTYPE =  LABEL.TELESALES_CALL_RECORDTYPE;
    public static final String PRESALES = LABEL.PRESALES;
    public static final String TELECOLLECTION =  LABEL.TELECOLLECTION;
    public static final String TELECOLLECTION_VALIDATION =  LABEL.TELECOLLECTION_VALIDATION;
    public static final String CREDIT =  LABEL.CREDIT;
    public static final String ONCALLCREDIT =  LABEL.OnCall_Credit;
    public static final String ONCALLCASH =  LABEL.OnCall_Cash;
    public static final String CREDITVALUE =  LABEL.CREDITVALUE;
    public static final String CASH =  LABEL.CASH;
    public static final String CASHVALUE =  LABEL.CASHVALUE;
    public static final String DEBIT =  LABEL.DEBIT;
    public static final String COOLERS = LABEL.COOLERS;
    
    public static final String ACCOUNT_RECORDTYPE_NAME = LABEL.ACCOUNT_RECORDTYPE_NAME;
    public static final String SALES_ZONE_RECORDTYPE_NAME = LABEL.SALES_ZONE_RECORDTYPE_NAME;
    public static final String CLIENT_GROUP_RECORDTYPE_NAME = LABEL.CLIENT_GROUP_RECORDTYPE_NAME;
    public static final String SALES_GROUP_RECORDTYPE_NAME = LABEL.SALES_GROUP_RECORDTYPE_NAME;
    public static final String SALES_OFFICE_RECORDTYPE_NAME = LABEL.SALES_OFFICE_RECORDTYPE_NAME;
    public static final String SALES_ORG_RECORDTYPE_NAME = LABEL.SALES_ORG_RECORDTYPE_NAME;
    public static final String RECORDTYPE_VISITCONTROL_ROUTE = LABEL.RECORDTYPE_VISITCONTROL_ROUTE;
    public static final String ACCOUNT_PROSPECT_RECORDTYPE_NAME = LABEL.ACCOUNT_PROSPECT_RECORDTYPE_NAME;
    
    public static final String OPEN_ITEMS_V360_RECORDTYPE_NAME = LABEL.OPEN_ITEMS_V360_RECORDTYPE_NAME;
    public static final String EMPTY_BALANCE_V360_RECORDTYPE_NAME = LABEL.EMPTY_BALANCE_V360_RECORDTYPE_NAME;
    public static final String ASSETS_V360_RECORDTYPE_NAME = LABEL.ASSETS_V360_RECORDTYPE_NAME;
    public static final String KPI_V360_RECORDTYPE_NAME = LABEL.KPI_V360_RECORDTYPE_NAME;
    public static final String INVOICE_V360_RECORDTYPE_NAME = LABEL.INVOICE_V360_RECORDTYPE_NAME;
    public static final String INVOICE_ITEM_V360_RECORDTYPE_NAME= LABEL.INVOICE_ITEM_V360_RECORDTYPE_NAME;
    public static final String COVERAGE_RECORDTYPE_NAME = LABEL.COVERAGE_RECORDTYPE_NAME;
    
    public static final String ORDER_SALER_SAPROLE = LABEL.ORDER_SALER_SAPROLE;
    public static final String ORDER_ACCOUNT_SAPROLE = LABEL.ORDER_ACCOUNT_SAPROLE;
    
    public static final String ORDER_MULESOFT_ENDPOINT = [Select URL__c from End_Point__mdt where DeveloperName = :LABEL.ORDER_MULESOFT_ENDPOINT].URL__c; 
    
    public static final String CANAL_MODERNO = LABEL.CANAL_MODERNO;
    public static final String USER_LANGUAGE = LABEL.Language_Info;
    
    public static final String ORDERS_BH = LABEL.ORDERS_BH;
    public static final String OPEN_ITEMS_IDENTIFIER = LABEL.OPEN_ITEMS_IDENTIFIER;
    
    public static final String HOD_OUT_OF_REGULATION_PRODUCT = LABEL.HOD_OUT_OF_REGULATION_PRODUCT;
    public static final String HOD_GOOD_SHAPE_PRODUCT = LABEL.HOD_GOOD_SHAPE_PRODUCT;  
    public static final String HOD_ORDER_GIFTS = LABEL.HOD_ORDER_GIFTS;
    public static final String HOD_ORDER_SEND_EMPTIES = LABEL.HOD_ORDER_SEND_EMPTIES;
    public static final String HOD_ORDER_EMPTIES = LABEL.HOD_ORDER_EMPTIES;
    public static final String HOD_ORDER_ADD_EMPTIES = LABEL.HOD_ORDER_ADD_EMPTIES;
    public static final String HOD_ORDER_FINISHED_PRODUCT = LABEL.HOD_ORDER_FINISHED_PRODUCT;
    
    public static final String SP_OUT_OF_REGULATION_PRODUCT = LABEL.SP_OUT_OF_REGULATION_PRODUCT;
    public static final String SP_GOOD_SHAPE_PRODUCT = LABEL.SP_GOOD_SHAPE_PRODUCT;  
    public static final String SP_ORDER_GIFTS = LABEL.SP_ORDER_GIFTS;
    public static final String SP_ORDER_SEND_EMPTIES = LABEL.SP_ORDER_SEND_EMPTIES;
    public static final String SP_ORDER_EMPTIES = LABEL.SP_ORDER_EMPTIES;
    public static final String SP_ORDER_ADD_EMPTIES = LABEL.SP_ORDER_ADD_EMPTIES;
    public static final String SP_ORDER_FINISHED_PRODUCT = LABEL.SP_ORDER_FINISHED_PRODUCT;
    public static final String SP_ORDER_EMPTIES_INTRODUCTION = LABEL.SP_ORDER_EMPTIES_INTRODUCTION;
    
    public static final String ORDER_ADD_EMPTIES = LABEL.ORDER_ADD_EMPTIES;
    public static final String ORDER_GIFTS = LABEL.ORDER_GIFTS;
    public static final String ORDER_FINISHED_PRODUCT = LABEL.ORDER_FINISHED_PRODUCT;
    public static final String ORDER_EMPTIES_INTRO = LABEL.ORDER_EMPTIES_INTRO;    
    public static final String OUT_OF_REGULATION_PRODUCT = LABEL.OUT_OF_REGULATION_PRODUCT;
    public static final String GOOD_SHAPE_PRODUCT = LABEL.GOOD_SHAPE_PRODUCT;    
    public static final String ORDER_SEND_EMPTIES = LABEL.ORDER_SEND_EMPTIES;
    public static final String ORDER_EMPTIES = LABEL.ORDER_EMPTIES;
    public static final String ORDER_CO2 = LABEL.ORDER_CO2; // Daniel Rosales - drs@avx - Gap Órdenes CO2 Panamá - 2019-06-06
    
    public static final String UNIT_MEASURE_BOXES = LABEL.UNIT_MEASURE_BOXES;
    public static final String UNIT_MEASURE_UNITS = LABEL.UNIT_MEASURE_UNITS;
    
    public static final String DEAL_CONDITION_METADATA = LABEL.DEAL_CONDITION_METADATA; 
        
    public static final String ACCOUNT_CREDIT_CONDITION_CREDIT = LABEL.ACCOUNT_CREDIT_CONDITION_CREDIT; 
    public static final String ACCOUNT_CREDIT_CONDITION_CASH = LABEL.ACCOUNT_CREDIT_CONDITION_CASH;
    //do not translate
    public static final String ISSM_Cash = LABEL.ISSM_Cash;
    public static final String ISSM_Credit = LABEL.ISSM_Credit;
    
    public static final String OPEN_ITEMS_RT = Label.OPEN_ITEMS_RT;
    
    public static final String VISIT_CONTROL_ROUTE = LABEL.VISIT_CONTROL_ROUTE;
    
    public static final String HONDURAS_ORG_CODE 	= LABEL.HONDURAS_ORG_CODE;
    public static final String EL_SALVADOR_ORG_CODE = LABEL.EL_SALVADOR_ORG_CODE;
    public static final String PANAMA_ORG_CODE 		= LABEL.PANAMA_ORG_CODE; //added drs@avx
    
    public static final String HONDURAS_COUNTRY_CODE 	= LABEL.HONDURAS_COUNTRY_CODE;
    public static final String EL_SALVADOR_COUNTRY_CODE = LABEL.EL_SALVADOR_COUNTRY_CODE;
    public static final String PANAMA_COUNTRY_CODE 		= LABEL.PANAMA_COUNTRY_CODE; // added drs@avx
    
    public static final String BDR_TOUR_RECORDTYPE =  LABEL.BDR_TOUR_RECORDTYPE;
    public static final String TELESALES_TOUR_RECORDTYPE =  LABEL.TELESALES_TOUR_RECORDTYPE;
    public static final String PRESALES_TOUR_RECORDTYPE = LABEL.PRESALES_TOUR_RECORDTYPE;
    
    
    public static final String FLEXIBLEDATA_RECORDTYPE_NAME = LABEL.FLEXIBLEDATA_RECORDTYPE;
    
    //public static final String INTEGRATION_USER = LABEL.INTEGRATION_USER;
    
    public static final String BOTTLE = LABEL.BOTTLE;
    
    public static final String TYPIF_NUMB1 = LABEL.TYPIF_NUMB1;
    public static final String TYPIF_NUMB2 = LABEL.TYPIF_NUMB2;
    public static final String TYPIF_NAME1 = LABEL.TYPIF_NAME1;
    public static final String TYPIF_NAME2 = LABEL.TYPIF_NAME2;
    public static final String VISITCONTROL_ANTICIPATE = LABEL.VisitControl_Anticipate;
    public static final String VISITCONTROL_RETRACE = LABEL.VisitControl_Retrace;
       
    //telecollection variables
    public static final Date d = System.today() + 1;
    public static final Datetime dt = (DateTime)d;
    public static final String dayOfWeek = dt.format('EEEE');
    public static final String campaign = 'TC' + dt.format('ddMMyyyy') + '_1';
    public static final String callname= 'C' + dt.format('ddMMyyyy_');
    
    //Deal freegood
    public static final String deal_PFNReason = LABEL.ONCALL_Deal_PFNReason;
        
        
}