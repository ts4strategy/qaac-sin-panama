global class IREP_DeleteSObject_bch implements Database.Batchable<sObject>, Database.Stateful {
	
	String strQuery;
	Integer intStep;
	
	
	global IREP_DeleteSObject_bch(String strSObjectParam, String strDaysParam, String strRecordTypes, Integer intStepParam) {
		this.intStep = intStepParam;
		strQuery = 'Select Id From '+strSObjectParam+' Where CreatedDate < LAST_N_DAYS:'+strDaysParam;
		System.debug('\nQuery ===> '+strQuery);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(strQuery);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		delete scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		Datetime dtHoraActual       =   System.now();
        Datetime dtNextExecution    =   dtHoraActual.addSeconds(5);
        
        this.intStep += 1;
        
        String strTime  =   dtNextExecution.second() +' ';
        strTime         +=  dtNextExecution.minute() +' ';
        strTime         +=  dtNextExecution.hour() +' ';
        strTime         +=  dtNextExecution.day() +' ';
        strTime         +=  dtNextExecution.month()+' ';
        strTime         +=  '? ';
        strTime         +=  dtNextExecution.year()+' ';

        String strDeleteSObject    =   'IREP_DeleteSObject_sch-'+this.intStep;
        
        //try{
                
            for ( CronTrigger ct : [SELECT Id FROM CronTrigger WHERE CronJobDetail.Name =: strDeleteSObject] ) { 
            	System.abortJob(ct.Id); 
            }
            
            String jobId = System.schedule(strDeleteSObject, strTime, new IREP_DeleteSObject_sch(this.intStep));

        //}catch(Exception e){ System.debug('\n ERROR DELETING RECORDS ===========>>>> '+e.getMessage()); }
		
	}
	
}