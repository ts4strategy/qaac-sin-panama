/****************************************************************************************************
General Information
-------------------
author: Nelson Sáenz Leal
email: nsaenz@avanxo.com
company: Avanxo Colombia
Project: ISSM DSD
Customer: AbInBev Grupo Modelo
Description: Test Class for CreateVisitPlan_ctr

Information about changes (versions)
-------------------------------------
Number    Dates             Author                       Description
------    --------          --------------------------   -----------
1.0       10-08-2017        Nelson Sáenz Leal (NSL)  Creation Class
****************************************************************************************************/
@isTest
private class CreateVisitPlan_tst
{
    public static ONTAP__Route__c objONTAPRoute;
    public static AccountByVisitPlan__c objISSMAccByVisitPlan;
    public static Account objAccount;
    public static AccountByRoute__c objAccxRoute;
    public static User u;
    public static Account objAccount2;
    public static AccountTeamMember objAccountTeamMember;
    public static AccountTeamMember objAccountTeamMember2;
    public static ONTAP__Vehicle__c objONTAPVehicle;
    
    public static VisitPlan__c objISSMVisitPlan;
    public static VisitPlan__c objISSMVisitPlan1;
    public static VisitPlan__c objISSMVisitPlan2;
    public static VisitPlan__c objISSMVisitPlan3;
    public static VisitPlan__c objISSMVisitPlan4;
    public static VisitPlan__c objISSMVisitPlan5;
    public static VisitPlan__c objISSMVisitPlan6;
    public static VisitPlan__c objISSMVisitPlan7;
    public static VisitPlan__c objISSMVisitPlan8;
    
    //@testSetup
    static void createData()
    {
        u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ManagerId = UserInfo.getUserId()
        );
        insert u;
        
        list<RecordType> lstRT = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' OR SobjectType = 'ONTAP__Route__c'];
        map<String, RecordType> mapRT = new map<String, RecordType>();
        for(RecordType rt : lstRT)
            mapRT.put(rt.DeveloperName, rt);
        
        
        objAccount2                         = new Account();
        objAccount2.RecordTypeId            = mapRT.get('SalesOffice').Id;
        objAccount2.Name                    = 'CMM Test';
        objAccount2.StartTime__c            = ' 02:00';
        objAccount2.EndTime__c              = ' 01:00';
        objAccount2.ONTAP__SalesOffId__c   = 'FZ08';
        insert objAccount2;
        
        
        
        objAccount                        = new Account();
        objAccount.RecordTypeId                   = mapRT.get('Account').Id;
        objAccount.Name                           = 'CMM Dolores FZ08';
        objAccount.StartTime__c                   = ' 01:00';
        objAccount.EndTime__c                     = '01:00';
        objAccount.ISSM_SalesOffice__c            = objAccount2.Id;
        objAccount.ONCALL__Order_Block__c         = null;
        objAccount.ONCALL__SAP_Deleted_Flag__c    = null;
        objAccount.ONTAP__SalesOgId__c        = '3116';
        objAccount.ONTAP__SalesOffId__c        = 'FZ08';
        insert objAccount;
        
        objAccountTeamMember  = new AccountTeamMember();
        objAccountTeamMember.AccountId                  = objAccount.Id;
        objAccountTeamMember.TeamMemberRole         = 'Preventa';
        objAccountTeamMember.UserId                     = u.Id;
        insert objAccountTeamMember;
        
        objAccountTeamMember2     = new AccountTeamMember();
        objAccountTeamMember2.AccountId                     = objAccount.Id;
        objAccountTeamMember2.TeamMemberRole            = 'Supervisor de Ventas';
        objAccountTeamMember2.UserId                            = u.Id;
        insert objAccountTeamMember2;
        
        
        objONTAPVehicle       = new ONTAP__Vehicle__c();
        objONTAPVehicle.ONTAP__VehicleId__c     = '112345-23';
        objONTAPVehicle.ONTAP__VehicleName__c   = 'Zurdo Movil';
        objONTAPVehicle.ONTAP__SalesOffice__c = objAccount2.id;
        insert objONTAPVehicle;
        
        
        objONTAPRoute                       = new ONTAP__Route__c();
        objONTAPRoute.RecordTypeId      = mapRT.get('Sales').Id;
        objONTAPRoute.ServiceModel__c       = 'Presales';
        objONTAPRoute.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute.RouteManager__c       = u.Id;
        objONTAPRoute.Supervisor__c         = u.id;
        objONTAPRoute.Vehicle__c            = objONTAPVehicle.id;
        insert objONTAPRoute;
        
        objISSMVisitPlan1                    =   new VisitPlan__c();
        objISSMVisitPlan1.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan1.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan1.Saturday__c        =   false;
        objISSMVisitPlan1.Wednesday__c       =   false;
        objISSMVisitPlan1.Thursday__c        =   false;
        objISSMVisitPlan1.Tuesday__c         =   false;
        objISSMVisitPlan1.Friday__c          =   false;
        objISSMVisitPlan1.Monday__c          =   false;
        objISSMVisitPlan1.VisitPlanId__c     =   '1';
        objISSMVisitPlan1.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan1;
        
        objISSMVisitPlan2                    =   new VisitPlan__c();
        objISSMVisitPlan2.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan2.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan2.Saturday__c        =   false;
        objISSMVisitPlan2.Wednesday__c       =   false;
        objISSMVisitPlan2.Thursday__c        =   false;
        objISSMVisitPlan2.Tuesday__c         =   false;
        objISSMVisitPlan2.Friday__c          =   false;
        objISSMVisitPlan2.Monday__c          =   false;
        objISSMVisitPlan2.VisitPlanId__c     =   '2';
        objISSMVisitPlan2.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan2;
        
        objISSMVisitPlan3                    =   new VisitPlan__c();
        objISSMVisitPlan3.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan3.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan3.Saturday__c        =   false;
        objISSMVisitPlan3.Wednesday__c       =   false;
        objISSMVisitPlan3.Thursday__c        =   false;
        objISSMVisitPlan3.Tuesday__c         =   false;
        objISSMVisitPlan3.Friday__c          =   false;
        objISSMVisitPlan3.Monday__c          =   false;
        objISSMVisitPlan3.VisitPlanId__c     =   '3';
        objISSMVisitPlan3.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan3;
        
        objISSMVisitPlan4                    =   new VisitPlan__c();
        objISSMVisitPlan4.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan4.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan4.Saturday__c        =   false;
        objISSMVisitPlan4.Wednesday__c       =   false;
        objISSMVisitPlan4.Thursday__c        =   false;
        objISSMVisitPlan4.Tuesday__c         =   false;
        objISSMVisitPlan4.Friday__c          =   false;
        objISSMVisitPlan4.Monday__c          =   false;
        objISSMVisitPlan4.VisitPlanId__c     =   '4';
        objISSMVisitPlan4.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan4;
        
        objISSMVisitPlan5                    =   new VisitPlan__c();
        objISSMVisitPlan5.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan5.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan5.Saturday__c        =   false;
        objISSMVisitPlan5.Wednesday__c       =   false;
        objISSMVisitPlan5.Thursday__c        =   false;
        objISSMVisitPlan5.Tuesday__c         =   false;
        objISSMVisitPlan5.Friday__c          =   false;
        objISSMVisitPlan5.Monday__c          =   false;
        objISSMVisitPlan5.VisitPlanId__c     =   '5';
        objISSMVisitPlan5.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan5;
        
        objISSMVisitPlan6                    =   new VisitPlan__c();
        objISSMVisitPlan6.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan6.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan6.Saturday__c        =   false;
        objISSMVisitPlan6.Wednesday__c       =   false;
        objISSMVisitPlan6.Thursday__c        =   false;
        objISSMVisitPlan6.Tuesday__c         =   false;
        objISSMVisitPlan6.Friday__c          =   false;
        objISSMVisitPlan6.Monday__c          =   false;
        objISSMVisitPlan6.VisitPlanId__c     =   '6';
        objISSMVisitPlan6.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan6;
        
        objISSMVisitPlan7                    =   new VisitPlan__c();
        objISSMVisitPlan7.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan7.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan7.Saturday__c        =   false;
        objISSMVisitPlan7.Wednesday__c       =   false;
        objISSMVisitPlan7.Thursday__c        =   false;
        objISSMVisitPlan7.Tuesday__c         =   false;
        objISSMVisitPlan7.Friday__c          =   false;
        objISSMVisitPlan7.Monday__c          =   false;
        objISSMVisitPlan7.Sunday__c          =   false;
        objISSMVisitPlan7.VisitPlanId__c     =   '7';
        objISSMVisitPlan7.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan7;
        
        objISSMVisitPlan8                    =   new VisitPlan__c();
        objISSMVisitPlan8.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan8.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan8.Saturday__c        =   false;
        objISSMVisitPlan8.Wednesday__c       =   false;
        objISSMVisitPlan8.Thursday__c        =   false;
        objISSMVisitPlan8.Tuesday__c         =   false;
        objISSMVisitPlan8.Friday__c          =   false;
        objISSMVisitPlan8.Monday__c          =   false;
        objISSMVisitPlan8.VisitPlanId__c     =   '8';
        objISSMVisitPlan8.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan8;
        
        objAccxRoute                        =   new AccountByRoute__c();
        objAccxRoute.Route__c               =   objONTAPRoute.Id;
        objAccxRoute.Account__c             =   objAccount.Id;
        objAccxRoute.Saturday__c            =   true;
        objAccxRoute.Wednesday__c           =   true;
        objAccxRoute.Thursday__c            =   true;
        objAccxRoute.Tuesday__c             =   true;
        objAccxRoute.Friday__c              =   true;
        objAccxRoute.Monday__c              =   true;
        insert objAccxRoute;
        
        objISSMAccByVisitPlan =   new AccountByVisitPlan__c();
        objISSMAccByVisitPlan.Account__c            =   objAccount.Id;
        objISSMAccByVisitPlan.VisitPlan__c          =   objISSMVisitPlan1.Id;
        objISSMAccByVisitPlan.Sequence__c           =   1;
        objISSMAccByVisitPlan.WeeklyPeriod__c       =   '1';
        objISSMAccByVisitPlan.LastVisitDate__c      =   null;
        insert objISSMAccByVisitPlan;
    }
    
    @isTest static void test_method_one()
    {
        createData();
        
        CreateVisitPlan_ctr.ObjWrapper objWrapper = new CreateVisitPlan_ctr.ObjWrapper();
        objWrapper.blnActive = true;
        objWrapper.objVPxAcc = objISSMAccByVisitPlan;
        objWrapper.strError = '0';
        objWrapper.strSAPId = '1';
        
        //List<CreateVisitPlan_ctr.ObjWrapper> lstObjWrapper = new List<CreateVisitPlan_ctr.ObjWrapper>();
        //lstObjWrapper.add(objWrapper);
        
        Test.startTest();
        PageReference pageRef      = Page.CreateVisitPlan_pag;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objISSMVisitPlan1.Id));
        ApexPages.StandardController sc                            = new ApexPages.StandardController(objISSMVisitPlan1);
        CreateVisitPlan_ctr testAccPlan = new CreateVisitPlan_ctr(sc);
        System.AssertEquals(testAccPlan.selectAllItems(), null);
        testAccPlan.cancel();
        testAccPlan.saveVisitPlan();
        testAccPlan.deleteCrossReference();
        testAccPlan.refreshData();
        Test.stopTest();
    }
    
    @isTest static void test_method_two()
    {
        createData();
        
        objISSMVisitPlan2.EffectiveDate__c  =   null;
        objISSMVisitPlan2.ExecutionDate__c  =   null;
        update objISSMVisitPlan2;
        
        Test.startTest();
        PageReference pageRef      = Page.CreateVisitPlan_pag;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objISSMVisitPlan2.Id));
        ApexPages.StandardController sc                            = new ApexPages.StandardController(objISSMVisitPlan2);
        CreateVisitPlan_ctr testAccPlan = new CreateVisitPlan_ctr(sc);
        testAccPlan.saveVisitPlan();
        Test.stopTest();
    }
    
    @isTest static void test_method_three()
    {
        createData();
        objAccxRoute.Saturday__c            =   true;
        objAccxRoute.Wednesday__c           =   false;
        objAccxRoute.Thursday__c            =   false;
        objAccxRoute.Tuesday__c             =   false;
        objAccxRoute.Friday__c              =   false;
        objAccxRoute.Monday__c              =   false;
        objAccxRoute.Sunday__c              =   true;
        update objAccxRoute;
        DELETE objISSMVisitPlan2;
        Test.startTest();
        PageReference pageRef      = Page.CreateVisitPlan_pag;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objISSMVisitPlan3.Id));
        ApexPages.StandardController sc                            = new ApexPages.StandardController(objISSMVisitPlan3);
        CreateVisitPlan_ctr testAccPlan = new CreateVisitPlan_ctr(sc);
        testAccPlan.saveVisitPlan();
        Test.stopTest();
    }
    
    @isTest static void test_method_four()
    {
        createData();
        objAccxRoute.Saturday__c            =   true;
        objAccxRoute.Wednesday__c           =   false;
        objAccxRoute.Thursday__c            =   true;
        objAccxRoute.Tuesday__c             =   false;
        objAccxRoute.Friday__c              =   false;
        objAccxRoute.Monday__c              =   false;
        update objAccxRoute;
        Test.startTest();
        PageReference pageRef      = Page.CreateVisitPlan_pag;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objISSMVisitPlan4.Id));
        ApexPages.StandardController sc                            = new ApexPages.StandardController(objISSMVisitPlan4);
        CreateVisitPlan_ctr testAccPlan = new CreateVisitPlan_ctr(sc);
        testAccPlan.strIdVisitPlan = null;
        testAccPlan.deleteCrossReference();
        testAccPlan.saveVisitPlan();
        Test.stopTest();
    }
    @isTest static void test_method_six()
    {
        createData();
        objAccxRoute.Saturday__c            =   true;
        objAccxRoute.Wednesday__c           =   false;
        objAccxRoute.Thursday__c            =   false;
        objAccxRoute.Tuesday__c             =   false;
        objAccxRoute.Friday__c              =   true;
        objAccxRoute.Monday__c              =   false;
        objAccxRoute.Sunday__c              =   true;
        update objAccxRoute;
        Test.startTest();
        PageReference pageRef      = Page.CreateVisitPlan_pag;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objISSMVisitPlan5.Id));
        ApexPages.StandardController sc                            = new ApexPages.StandardController(objISSMVisitPlan5);
        CreateVisitPlan_ctr testAccPlan = new CreateVisitPlan_ctr(sc);
        testAccPlan.strIdVisitPlan = null;
        testAccPlan.deleteCrossReference();
        testAccPlan.saveVisitPlan();
        Test.stopTest();
    }
    @isTest static void test_method_seven()
    {
        createData();
        objAccxRoute.Saturday__c            =   false;
        objAccxRoute.Wednesday__c           =   false;
        objAccxRoute.Thursday__c            =   false;
        objAccxRoute.Tuesday__c             =   false;
        objAccxRoute.Friday__c              =   false;
        objAccxRoute.Monday__c              =   false;
        objAccxRoute.Sunday__c              =   false;
        update objAccxRoute;
        
        Test.startTest();
        PageReference pageRef      = Page.CreateVisitPlan_pag;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objISSMVisitPlan6.Id));
        ApexPages.StandardController sc                            = new ApexPages.StandardController(objISSMVisitPlan6);
        CreateVisitPlan_ctr testAccPlan = new CreateVisitPlan_ctr(sc);
        testAccPlan.strIdVisitPlan = null;
        testAccPlan.deleteCrossReference();
        testAccPlan.saveVisitPlan();
        Test.stopTest();
    }
    
    @isTest static void test_method_eight()
    {
        createData();
        objAccxRoute.Saturday__c            =   false;
        objAccxRoute.Wednesday__c           =   false;
        objAccxRoute.Thursday__c            =   false;
        objAccxRoute.Tuesday__c             =   false;
        objAccxRoute.Friday__c              =   false;
        objAccxRoute.Monday__c              =   false;
        objAccxRoute.Sunday__c              =   false;
        update objAccxRoute;
        //delete objISSMAccByVisitPlan;
        Test.startTest();
        PageReference pageRef      = Page.CreateVisitPlan_pag;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objISSMVisitPlan7.Id));
        ApexPages.StandardController sc                            = new ApexPages.StandardController(objISSMVisitPlan7);
        CreateVisitPlan_ctr testAccPlan = new CreateVisitPlan_ctr(sc);
        testAccPlan.getAccountsRoute();
        Test.stopTest();
    }
    
}