/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_FlexibleDataTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
public class V360_FlexibleDataTriggerHandler_Test {
    
    /**
   	* Method for test the before insert flexible data.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    @isTest static void test_beforeInsert_UseCase1(){
        V360_FlexibleDataTriggerHandler obj01 = new  V360_FlexibleDataTriggerHandler();
        V360_FlexibleData__c obj02= new V360_FlexibleData__c();
        Account acc = new Account(name='1234567890',ONTAP__SAP_Number__c='1234567890');
        insert acc;
        obj02.SAP_Account_ID__c = '1234567890';
        obj02.V360_Type__c = 'ts';
        obj02.V360_Value__c = 'ts';
        insert obj02;
    }
    
    /**
   	* Method for test the before update flexible data.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    @isTest static void test_beforeUpdate_UseCase1(){
        V360_FlexibleDataTriggerHandler obj01 = new  V360_FlexibleDataTriggerHandler();
        V360_FlexibleData__c obj02= new V360_FlexibleData__c();
        obj02.SAP_Account_ID__c = 'ts';
        obj02.V360_Type__c = 'ts';
        obj02.V360_Value__c = 'ts';
        insert obj02;
        update obj02;
    }
    
    /**
   	* Method for test relate an Account to flexible data.
   	* @author: g.martinez.cabral@accenture.com
   	* @param void
   	* @return void
   	*/
    @isTest static void test_relateAccountsToEmpties_UseCase1(){
        V360_FlexibleDataTriggerHandler obj01 = new  V360_FlexibleDataTriggerHandler();
        List<v360_FlexibleData__c> newlist = new List<v360_FlexibleData__c>();
        V360_FlexibleData__c obj02= new V360_FlexibleData__c();
        obj02.SAP_Account_ID__c = 'ts';
        obj02.V360_Type__c = 'ts';
        obj02.V360_Value__c = 'ts';
        newlist.add(obj02);
        //V360_FlexibleDataTriggerHandler.relateAccountsToFlexibleData( newlist);
    }
    
}