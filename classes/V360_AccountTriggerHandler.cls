/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_AccountTriggerHandler.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 18/12/2018     Gerardo Martinez        Creation of methods.
* 19/12/2018     Carlos Leal             Creation of methods.
*/
public without sharing class V360_AccountTriggerHandler extends TriggerHandlerCustom 
{
    private Map<Id, Account> newMap;
    private Map<Id, Account> oldMap;
    private List<Account> newList;
    private List<Account> oldList;
    private Boolean IsChangingUsers;
    
    /**
    * Constructor which takes the global trigger vars and adde them to the class.
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public V360_AccountTriggerHandler() 
    {
        this.newMap = (Map<Id, Account>) Trigger.newMap;
        this.oldMap = (Map<Id, Account>) Trigger.oldMap;
        this.newList = (List<Account>) Trigger.new;
        this.oldList = (List<Account>) Trigger.old;
    }
    
    /**
    * method wich start every time before record is inserted 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public override void beforeInsert()
    {
        //This line copy the interlocutors of the customer of reference and validates for Honduras or El Salvador, Runs for prospects
        system.debug('Acc Before Insert');
        copyCustomerReferenceFields(this.newList);
        
        //This line updates the new hierarchy account key field concatenating the id of the parent plus the id of the current record, also is validated for Honduras and El Salvador only.
        List<Account> listObj =PreventExecutionUtil.validateAccountWithoutIdByContry(this.newList);
        syncSalesStructureWithParentBeforeInsert(listObj);
        
        //Validates to run only once the insert for customers 
        if(V360_CheckRecursive.runOnce())
        {
            //Validate if the customer is from El Salvador or Hunduras then assing the interlocutors
            List<Account> listObj2 =PreventExecutionUtil.validateAccountWithoutIdByORG(this.newList);
        	syncCustomerToSalesStructure(listObj2);
            List<Account> newList = PreventExecutionUtil.validateAccountWithIdByORGBeforeInsert(this.newList);
            V360_InterlocutorTools.AssignInterlocutorBeforeUpdate(newList);            
			
            normalizeCreditOrCashAttribute(listObj2);
        	translatecodes(listObj2);
        }
    }
    
    /**
    * method wich start every time before record is updated 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public override void beforeUpdate()
    {
        //Validates and Syncronize Hiearchy Accounts if the records received belongs to the hierarchy structure.
        Map<Id,Account> newMapValid = PreventExecutionUtil.validateAccountWithIdByContry(this.newMap);
        system.debug('newMapValid: '+ newMapValid );
        syncSalesStructureWithParentBeforUpdate(newMapValid);
        
        //Validates to run only once the updates for customers 
        if(V360_CheckRecursive.runOnce())
        {
            //Validate if the customer is from El Salvador or Hunduras then assing the interlocutors
        	Map<Id,Account> newMapValid2 = PreventExecutionUtil.validateAccountWithIdByORG(this.newMap);
        	syncCustomerToSalesStructure(newMapValid2.values());
            V360_InterlocutorTools.AssignInterlocutorBeforeUpdate(newMapValid2.values());
            normalizeCreditOrCashAttribute(newMapValid2.values());
            //Validate if the customer is from El Salvador or Hunduras then validates if a field has change and translate the fields if something has changed
            List<Account> accountValidToTranslate = PreventExecutionUtil.prevalidateBeforeToTranslate(newMapValid2,this.oldMap);
            translatecodes(accountValidToTranslate);
        }
    }
    
    /**
    * method wich start every time after record is updated 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */  
    public override void afterUpdate()
    {
        Map<Id,Account> newMapValid = PreventExecutionUtil.validateAccountWithIdByContry (this.newMap);
        Map<Id,Account> oldMapValid = PreventExecutionUtil.validateAccountWithIdByContry (this.oldMap);
        updateAccountHierarchy(newMapValid,oldMapValid);
    }
    
    /**
    * method wich start every time after record is Insert 
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return Void
    */  
    public override void afterInsert()
    {   
        ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
        List<ONTAP__Case_Force__c> lstCaseForce = new List<ONTAP__Case_Force__c>();
        List<ONTAP__Case_Force__c> lstCaseForceUpdate = new List<ONTAP__Case_Force__c>();
        Set<string> sCaseNumber = new Set<string>();
        List<Case> lstCase = new List<Case>();
        List<Account> lstProspect = new List<Account>();
        List<Id> lstIdProspect = new List<Id>();
        List<Account> lstProspectUpdate = new List<Account>();
        CS_Prospect_Settings__c prospectSetting = CS_Prospect_Settings__c.getOrgDefaults();
        
        try
        {
            Id idRecordTypeHN = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
            Id idRecordTypeSV = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_SV).getRecordTypeId();
            
            /* Se obtienen los registros que estan asociado a los record type Prospect_HN y Prospect_SV */
            for(Account oProspect : this.newList)
            {
                if(oProspect.RecordTypeId == idRecordTypeHN ||  oProspect.RecordTypeId == idRecordTypeSV)
                {
                    lstProspect.add(oProspect);
                    lstIdProspect.add(oProspect.Id);
                }
            }
            
            /* Se valida si existen propsectos a procesar */
            if(lstProspect.size() > 0)
            {
                Id idRecordTypeCaseForce = Schema.SObjectType.ONTAP__Case_Force__c.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_CF_Prospectos).getRecordTypeId();
                /* Se generan los casos por cada prospecto */
                for(Account oCaseProspect : lstProspect)
                {
                    oNewCaseForce.ONTAP__Account__c = oCaseProspect.Id;
                    oNewCaseForce.ONTAP__Subject__c = Label.CS_Case_Subject_Prospecto;
                    oNewCaseForce.ONTAP__Description__c = Label.CS_Descripcion_Caso_Prospecto;
                    oNewCaseForce.CS_Country_Code__c = (oCaseProspect.RecordTypeId == idRecordTypeHN ? Label.CS_CountryCodeHN : Label.CS_CountryCodeSV);
                    oNewCaseForce.CS_Country__c = (oCaseProspect.RecordTypeId == idRecordTypeHN ? Label.CS_Country_Honduras : Label.CS_Country_El_Salvador);
                    oNewCaseForce.CS_OT_Type_of_request__c = Label.CS_Tipo_Solicitud_Prospecto;
                    oNewCaseForce.RecordTypeId = idRecordTypeCaseForce;
                    oNewCaseForce.ONTAP__Status__c = Label.CS_Estatus_Case_Force;
                    lstCaseForce.add(oNewCaseForce);
                    oNewCaseForce = new ONTAP__Case_Force__c();
                }
            
                /* Se insertan lo casos */
                Insert lstCaseForce;
            
                /* Se actualiza el Owner del caso force */
                /* Se obtiene la informacion de los casos force insertados */
                lstCaseForceUpdate = [SELECT Id, ISSM_CaseNumber__c FROM ONTAP__Case_Force__c WHERE Id In : lstCaseForce];
                for(ONTAP__Case_Force__c ocfUpdate : lstCaseForceUpdate)
                {
                    sCaseNumber.add(ocfUpdate.ISSM_CaseNumber__c);
                }
                
                /* Se consultan los casos standard insertados */
                lstCase = [SELECT Id, ISSM_CaseForceNumber__c, OwnerId, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c FROM Case WHERE CaseNumber IN : sCaseNumber];
                lstCaseForceUpdate = new List<ONTAP__Case_Force__c>();
                oNewCaseForce = new ONTAP__Case_Force__c();
                for(Case oCase : lstCase)
                {
                    oNewCaseForce.Id = oCase.ISSM_CaseForceNumber__c;
                    oNewCaseForce.OwnerId = oCase.OwnerId;
                    oNewCaseForce.ISSM_TypificationLevel1__c = oCase.ISSM_TypificationLevel1__c;
                    oNewCaseForce.ISSM_TypificationLevel2__c = oCase.ISSM_TypificationLevel2__c;
                    oNewCaseForce.ISSM_TypificationLevel3__c = oCase.ISSM_TypificationLevel3__c;
                    oNewCaseForce.ISSM_TypificationLevel4__c = oCase.ISSM_TypificationLevel4__c;
                    lstCaseForceUpdate.add(oNewCaseForce);
                    oNewCaseForce = new ONTAP__Case_Force__c();
                }
                
                ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
                Update lstCaseForceUpdate;
                
                /* Se actualiza la longitud y latitud del prospecto */
                lstProspectUpdate = [SELECT Id, V360_ReferenceClient__r.ONTAP__Latitude__c, V360_ReferenceClient__r.ONTAP__Longitude__c, ONTAP__Latitude__c, ONTAP__Longitude__c
                                     FROM Account
                                     WHERE Id IN : lstIdProspect];
                
                for(Account oAccountUpdate : lstProspectUpdate)
                {
                    if(oAccountUpdate.ONTAP__Latitude__c == null || oAccountUpdate.ONTAP__Latitude__c == '')
                        oAccountUpdate.ONTAP__Latitude__c = oAccountUpdate.V360_ReferenceClient__r.ONTAP__Latitude__c;
                    
                    if(oAccountUpdate.ONTAP__Longitude__c == null || oAccountUpdate.ONTAP__Longitude__c == '')
                        oAccountUpdate.ONTAP__Longitude__c = oAccountUpdate.V360_ReferenceClient__r.ONTAP__Longitude__c;
                    
                    oAccountUpdate.CS_Prospect_Create_Date__c = Date.today();
                }
                
                Update lstProspectUpdate;
                
                /* Se hace el envio de prospectos a MuleSoft si la bandera esta activa */
                if(prospectSetting.CS_Send_MdWeb__c)
                    CS_Prospect_Callout_Class.SendProspectMuleSoft(lstIdProspect);
            }
        }
        catch(Exception ex)
        {
            System.debug('V360_AccountTriggerHandler.afterInsert Message: ' + ex.getMessage());   
            System.debug('V360_AccountTriggerHandler.afterInsert Cause: ' + ex.getCause());   
            System.debug('V360_AccountTriggerHandler.afterInsert Line number: ' + ex.getLineNumber());   
            System.debug('V360_AccountTriggerHandler.afterInsert Stack trace: ' + ex.getStackTraceString());  
        }
    }
   
    /**
    * method reasing each interlocutor to a new customer or  re-calculates the account hierarchy
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */  
    public void updateAccountHierarchy(Map<Id,Account> newMapValid,Map<Id,Account> oldMapValid)
    {
        List<Account> finalList = New List<Account>();
        finalList.addAll(changeDirectorForAlCustomers(newMapValid,oldMapValid));
        finalList.addAll(changeManagerForAlCustomers(newMapValid,oldMapValid));
        finalList.addAll(changeTeamLeadForAlCustomers(newMapValid,oldMapValid));
        finalList.addAll(changeTeleSupervisorForAlCustomers(newMapValid,oldMapValid));
        finalList.addAll(changeTelesalesManagerForAlCustomers(newMapValid,oldMapValid));
        finalList.addAll(changeTeleDirectorForAlCustomers(newMapValid,oldMapValid));
        
        if (finalList.size()>0 && !TriggerExecutionControl_cls.hasAlreadyDone('V360_AccountTriggerHandler','SyncSalesStructureWithChild'))
        {
            System.enqueueJob(new V360_QueueSalesStructureUpdate(finalList));
            TriggerExecutionControl_cls.setAlreadyDone('V360_AccountTriggerHandler','SyncSalesStructureWithChildInterlocutors');   
        }
        else if(!TriggerExecutionControl_cls.hasAlreadyDone('V360_AccountTriggerHandler','SyncSalesStructureWithChildInterlocutors'))
        {
            syncSalesStructureWithChildsafterUpdate(newMapValid,oldMapValid);
            TriggerExecutionControl_cls.setAlreadyDone('V360_AccountTriggerHandler',' SyncSalesStructureWithChild');
        }
    }
    
    /**
    * method wich change director for the customer if is reasigned into  a Sales Office (account hierarchy)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return cutomers to modify
    */
    public List<Account> changeDirectorForAlCustomers(Map<Id, Account> newMap,Map<Id, Account> oldMap)
    {
        SET<Id> AccountsId = New Set<Id>();
        List<Account> accResult = new List<Account>();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        
        for( Id accountId : newMap.keySet())
        {
            if(newMap!=NULL && oldMap!=NULL && oldMap.get( accountId )!=NULL && newMap.get( accountId )!=NULL  && oldMap.get( accountId ).V360_Director__c !=newMap.get( accountId ).V360_Director__c 
               && salesOfficeRecordTypeId ==newMap.get( accountId ).RecordTypeId  && newMap.get( accountId ).type == GlobalStrings.PRESALES)
            {
                AccountsId.add(accountId);  
            }
        }
        
        if(AccountsId.size()>0)
        {
            for(List<Account> lst : [SELECT Id, V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.Parent.Parent.ParentId, V360_Director__c FROM Account WHERE V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.Parent.Parent.ParentId IN: AccountsId])
            {
                for (Account acc : lst)
                {
                    acc.V360_Director__c = newMap.get(acc.V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.Parent.Parent.ParentId ).V360_Director__c;
                    accResult.add(acc);
                }
            }
        }
        
        return accResult;
    } 
    
    /**
    * method wich change manager for the customer if is reasigned into  a Sales group (account hierarchy)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return cutomers to modify
    */
    public List<Account> changeManagerForAlCustomers(Map<Id, Account> newMap,Map<Id, Account> oldMap)
    {
        SET<Id> AccountsId = New Set<Id>();
        List<Account> accResult = new List<Account>();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        
        for(Id accountId : newMap.keySet())
        {
            if(newMap!=NULL && oldMap!=NULL && oldMap.get( accountId )!=NULL && newMap.get( accountId )!=NULL && oldMap.get( accountId ).V360_Manager__c !=newMap.get( accountId ).V360_Manager__c 
               && salesOfficeRecordTypeId ==newMap.get( accountId ).RecordTypeId && newMap.get( accountId ).type == GlobalStrings.PRESALES)
            {
                AccountsId.add(accountId);  
            }
        }
        
        if(AccountsId.size()>0)
        {
            for(List<Account> lst : [SELECT Id, V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.Parent.ParentId, V360_Manager__c FROM Account WHERE V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.Parent.ParentId IN: AccountsId])
            {
                for (Account acc : lst)
                {
                    acc.V360_Manager__c = newMap.get(acc.V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.Parent.ParentId ).V360_Manager__c;
                    accResult.add(acc);
                }
            }           
        }
        
        return accResult;
    }
    
    /**
    *method wich change supervisors for the customer if is reasigned into  a Client group (account hierarchy)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return cutomers to modify
    */
    public List<Account> changeTeamLeadForAlCustomers(Map<Id, Account> newMap,Map<Id, Account> oldMap)
    {
        SET<Id> AccountsId = New Set<Id>();
        List<Account> accResult = new List<Account>();
        
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        
        for(Id accountId : newMap.keySet())
        {
            if(newMap!=NULL && oldMap!=NULL && oldMap.get( accountId )!=NULL && newMap.get( accountId )!=NULL && oldMap.get( accountId ).V360_TeamLead__c !=newMap.get( accountId ).V360_TeamLead__c 
               && salesOfficeRecordTypeId ==newMap.get( accountId ).RecordTypeId && newMap.get( accountId ).type == GlobalStrings.PRESALES)
            {
                AccountsId.add(accountId);  
            }
        }
        
        if(AccountsId.size()>0)
        {           
            for(List<Account> lst : [SELECT Id, V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.ParentId, V360_TeamLead__c FROM Account WHERE V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.ParentId IN: AccountsId])
            {
                for (Account acc : lst)
                {
                    acc.V360_TeamLead__c = newMap.get(acc.V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.ParentId ).V360_TeamLead__c;
                    accResult.add(acc);
                }
            }           
        }
        
        return accResult;
    }
    
    /**
    * method wich change supervisors for the customer if is reasigned into  a Client group (account hierarchy)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return cutomers to modify
    */
    public List<Account> changeTeleSupervisorForAlCustomers(Map<Id, Account> newMap,Map<Id, Account> oldMap)
    {
        SET<Id> AccountsId = New Set<Id>();
        List<Account> accResult = new List<Account>();
        
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        
        for( Id accountId : newMap.keySet())
        {
            if(newMap!=NULL && oldMap!=NULL && oldMap.get( accountId )!=NULL && newMap.get( accountId )!=NULL && oldMap.get( accountId ).V360_TeamLead__c !=newMap.get( accountId ).V360_TeamLead__c 
               && salesOfficeRecordTypeId ==newMap.get( accountId ).RecordTypeId && newMap.get( accountId ).type == GlobalStrings.TELESALES)
            {
                AccountsId.add(accountId);  
            }
        }
        
        if(AccountsId.size()>0)
        {
            for(List<Account> lst : [SELECT Id, V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.ParentId, V360_TelesalesSupervisor__c FROM Account WHERE V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.ParentId IN: AccountsId])
            {
                for (Account acc : lst)
                {
                    acc.V360_TelesalesSupervisor__c = newMap.get(acc.V360_SalesZoneAssignedPresaler__r.V360_SalesZone__r.ParentId ).V360_TeamLead__c;
                    accResult.add(acc);
                }
            }
        }

        return accResult;
    }
    
    /**
    * method wich change director of telesales for the customer if is reasigned into  a Sales Office (account hierarchy)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return cutomers to modify
    */
    public List<Account> changeTeleDirectorForAlCustomers(Map<Id, Account> newMap,Map<Id, Account> oldMap)
    {
        SET<Id> AccountsId = New Set<Id>();
        List<Account> accResult = new List<Account>();
        
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        
        for( Id accountId : newMap.keySet())
        {
            if( newMap!=NULL && oldMap!=NULL && oldMap.get( accountId )!=NULL && newMap.get( accountId )!=NULL && oldMap.get( accountId ).V360_Director__c !=newMap.get( accountId ).V360_Director__c 
               && salesOfficeRecordTypeId ==newMap.get( accountId ).RecordTypeId && newMap.get( accountId ).type == GlobalStrings.TELESALES)
            {
                AccountsId.add(accountId);  
            }
        }
        
        if(AccountsId.size()>0)
        {
            for(List<Account> lst : [SELECT Id, V360_SalesZoneAssignedTelesaler__r.V360_SalesZone__r.Parent.Parent.ParentId, V360_TelesalesDirector__c FROM Account WHERE V360_SalesZoneAssignedTelesaler__r.V360_SalesZone__r.Parent.Parent.ParentId IN: AccountsId])
            {
                for (Account acc : lst)
                {
                    acc.V360_TelesalesDirector__c = newMap.get(acc.V360_SalesZoneAssignedTelesaler__r.V360_SalesZone__r.Parent.Parent.ParentId).V360_Director__c;
                    accResult.add(acc);
                }
            }
        }
        
        return accResult;
    }
    
    /**
    * method wich change manager of telesales for the customer if is reasigned into  a Sales group (account hierarchy)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return cutomers to modify
    */
    public List<Account> changeTelesalesManagerForAlCustomers(Map<Id, Account> newMap,Map<Id, Account> oldMap)
    {
        SET<Id> AccountsId = New Set<Id>();
        List<Account> accResult = new List<Account>();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        
        for( Id accountId : newMap.keySet())
        {
            if(newMap!=NULL && oldMap!=NULL && oldMap.get( accountId )!=NULL && newMap.get( accountId )!=NULL && oldMap.get( accountId ).V360_Manager__c !=newMap.get( accountId ).V360_Manager__c 
               && salesOfficeRecordTypeId ==newMap.get( accountId ).RecordTypeId && newMap.get( accountId ).type == GlobalStrings.TELESALES)
            {
                AccountsId.add(accountId);  
            }
        }
        
        if(AccountsId.size()>0)
        {          
            for(List<Account> lst : [SELECT Id, V360_SalesZoneAssignedTelesaler__r.V360_SalesZone__r.Parent.ParentId, V360_TelesalesManager__c FROM Account WHERE V360_SalesZoneAssignedTelesaler__r.V360_SalesZone__r.Parent.ParentId IN: AccountsId])
            {
                for (Account acc : lst)
                {
                    acc.V360_TelesalesManager__c = newMap.get(acc.V360_SalesZoneAssignedTelesaler__r.V360_SalesZone__r.Parent.ParentId ).V360_Manager__c;
                    accResult.add(acc);
                }
            }           
        }
        
        return accResult;
    }
    
    /**
    * This Method sync the account Hierarchy Key With Sales Structure, if change or you add any sales office, sales org, client group, sales zone, sales group (before update)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return void
    */
    public void syncSalesStructureWithParentBeforUpdate(Map<Id, Account> newMap)
    {
        SET<Id> recordTypeList = New SET<Id>();
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesOrgRecordTypeId);
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesOfficeRecordTypeId);
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(clientGroupdevRecordTypeId);
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesGroupdevRecordTypeId);
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesZonedevRecordTypeId);
        
        Set<Id> parentAcc = new Set<Id>();
        for (Account acc : newMap.values())
        {	
            if(recordTypeList.contains(acc.RecordTypeId) && acc.ParentId !=NULL)
            {
                parentAcc.add(acc.ParentId);
            }
        }
        
        if(parentAcc.size()>0)
        {
            Map<Id,Account> parentMap = New Map<Id,Account>([SELECT Id,V360_HierarchyKey__c, ONTAP__ExternalKey__c, Parent.ONTAP__ExternalKey__c, Parent.V360_HierarchyKey__c FROM Account WHERE RecordTypeId IN :recordTypeList AND Id IN:parentAcc]);            
            
            for (Account acc : newList)
            {
                Account auxParent = parentMap.get(acc.ParentId);
                if(auxParent!=NULL && auxParent.V360_HierarchyKey__c!=NULL && acc.ONTAP__ExternalKey__c!= NULL)
                {
                    acc.V360_HierarchyKey__c = auxParent.V360_HierarchyKey__c+acc.ONTAP__ExternalKey__c;
                }
                else if(auxParent!=NULL && auxParent.Parent.ONTAP__ExternalKey__c != NULL &&acc.ONTAP__ExternalKey__c!=NULL)
                {
                    acc.V360_HierarchyKey__c = auxParent.ONTAP__ExternalKey__c+acc.ONTAP__ExternalKey__c;   
                }
                else if(auxParent==NULL&& acc.ONTAP__ExternalKey__c!= NULL)
                {
                    acc.V360_HierarchyKey__c = acc.ONTAP__ExternalKey__c;
                }
            }
        }
    }
    
    
    /**
    * This Method sync the account Hierarchy Key With Sales Structure, if change or you add any sales office, sales org, client group, sales zone, sales group (before insert)
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified
    * @return void
    */    
    public void syncSalesStructureWithParentBeforeInsert(List<Account> newList)
    {
        SET<Id> recordTypeList = New SET<Id>();
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesOrgRecordTypeId);
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesOfficeRecordTypeId);
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(clientGroupdevRecordTypeId);
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesGroupdevRecordTypeId);
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesZonedevRecordTypeId);
        Set<Id> parentAcc = new Set<Id>();
        
        for (Account acc : newList)
        {          
            if(acc.ParentId != NULL&& recordTypeList.contains(acc.RecordTypeId))
            { 
                parentAcc.add(acc.ParentId);
            }
        }
        
        if(parentAcc.size()>0)
        {
            Map<Id,Account> parentMap = New Map<Id,Account>([SELECT Id,V360_HierarchyKey__c, ONTAP__ExternalKey__c, Parent.ONTAP__ExternalKey__c, Parent.V360_HierarchyKey__c FROM Account WHERE RecordTypeId IN :recordTypeList AND Id IN:parentAcc]);            
            
            for (Account acc : newList)
            {
                Account auxParent = parentMap.get(acc.ParentId);
                
                if(auxParent!=NULL && auxParent.V360_HierarchyKey__c!=NULL && acc.ONTAP__ExternalKey__c!= NULL)
                {
                    acc.V360_HierarchyKey__c = auxParent.V360_HierarchyKey__c+acc.ONTAP__ExternalKey__c;
                    
                }
                else if(auxParent!=NULL && auxParent.Parent.ONTAP__ExternalKey__c != NULL &&acc.ONTAP__ExternalKey__c!=NULL)
                {
                    acc.V360_HierarchyKey__c = auxParent.ONTAP__ExternalKey__c+acc.ONTAP__ExternalKey__c;   
                    
                }
                else if(auxParent==NULL&& acc.ONTAP__ExternalKey__c!= NULL)
                {
                    acc.V360_HierarchyKey__c = acc.ONTAP__ExternalKey__c;
                    
                }
            }
        }
    }
    
    /**
    * This Method force sync the child accounts, with the account Hierarchy of their parents, if change or you add any sales office, sales org, client group, sales zone, sales group
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified, old values of the account modified
    * @return void
    */
    public void syncSalesStructureWithChildsafterUpdate(Map<Id, Account> newMap,Map<Id, Account> oldMap)
    {
        SET<Id> recordTypeList = New SET<Id>();
        SET<Id> parents = New SET<Id>();
        
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesOrgRecordTypeId);
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesOfficeRecordTypeId);
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(clientGroupdevRecordTypeId);
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesGroupdevRecordTypeId);
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        recordTypeList.add(salesZonedevRecordTypeId);
               
        for(Id AccId : newMap.keySet())
        {
            if ((newMap.get(AccId).V360_HierarchyKey__c != NULL || newMap.get(AccId).ParentId != NULL) && recordTypeList.contains(newMap.get(accId).recordTypeId) 
                 && (newMap.get(AccId).V360_HierarchyKey__c != oldMap.get(AccId).V360_HierarchyKey__c || oldMap.get( AccId).ParentId != newMap.get(AccId ).ParentId))
            {
                parents.add(AccId);
            }
        }
        
        if(parents.size()>0)
        {
            List<Account> accResult = [SELECT Id,V360_HierarchyKey__c, ONTAP__ExternalKey__c, ParentId  FROM Account WHERE RecordTypeId IN :recordTypeList AND ParentId IN:parents];
            if(accResult.size()>0)
            { 
                System.enqueueJob(new V360_QueueSalesStructureUpdate(accResult));
            }      
        }       
    }
        
    /**
    * This Method sugets a sales zone per sales model (BDR, Telesales, Presales), if the customer hierarchy match with te account hierarchy created in salesforce
    * @author: g.martinez.cabral@accenture.com
    * @param New values of account modified
    * @return void
    */
    public void syncCustomerToSalesStructure(List<Account> newList)
    {
        SET<String> keysByCustomer = new SET<String>();
        
        for(Account acc: newList)
        {
            /*if(acc.ONTAP__SalesOgId__c !=NULL && acc.ONTAP__SalesOffId__c != NULL && acc.V360_SalesZone__c !=NULL && acc.V360_CustomerGroup__c != NULL&& acc.V360_SalesGroup__c!= NULL)
            {
                keysByCustomer.add(acc.ONTAP__SalesOgId__c+acc.ONTAP__SalesOffId__c+acc.V360_SalesGroup__c+acc.V360_CustomerGroup__c+acc.V360_SalesZone__c);
            }*/
            if(acc.parent.V360_HierarchyKey__c !=NULL )
            {
                keysByCustomer.add(acc.Parent.V360_HierarchyKey__c);
            }
        }
      
        for(List<V360_SalerPerZone__c> lst: [SELECT Id, V360_Type__c, V360_HierarchyKey__c FROM V360_SalerPerZone__c WHERE V360_HierarchyKey__c IN: keysByCustomer])
        {
            for(V360_SalerPerZone__c zn: lst)
            {
                for (Account customer:newList)
                {                   
                    //String key = customer.ONTAP__SalesOgId__c+customer.ONTAP__SalesOffId__c+customer.V360_SalesGroup__c+customer.V360_CustomerGroup__c+customer.V360_SalesZone__c;
                    String key = customer.Parent.V360_HierarchyKey__c;
                    if(key!=NULL && zn.V360_HierarchyKey__c!=NULL &&zn.V360_Type__c!=NULL && zn.V360_HierarchyKey__c == key)
                    {
                        if(zn.V360_Type__c == GlobalStrings.BDR && customer.V360_SalesZoneAssignedBDR__c==NULL)
                        {                            
                            customer.V360_SalesZoneAssignedBDR__c=zn.Id;
                        }
                        else if(zn.V360_Type__c == GlobalStrings.PRESALES && customer.V360_SalesZoneAssignedPresaler__c==NULL)
                        {
                            customer.V360_SalesZoneAssignedPresaler__c=zn.Id;
                        }
                    }
                }
            }
        }
    } 
    
    /**
    * relate a prospect tu a customer reference
    * @author: c.leal.beltran@accenture.com
    * @param New values of account modified
    * @return cvoid
    * *** 2019-07-10 : Modified Daniel Rosales - drs@avx : Exception searching on Account by ONTAP__SAP_Number__c because it is not Indexed. 
    * *** Changed to search by lookup "V360_ReferenceClient__c" and a little cleanup
    */
    public void  copyCustomerReferenceFields(List<Account> acNewList){
        Set<Id> referenceAccIds = new Set<Id>();
        Id prospectRecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ACCOUNT_PROSPECT_RECORDTYPE_NAME).getRecordTypeId();
        
        for (Account acc : acNewList){
            if(acc.RecordTypeId != NULL && acc.RecordTypeId == prospectRecordType && acc.V360_ReferenceClient__c != NULL ){
                referenceAccIds.add(acc.V360_ReferenceClient__c); } }
        
        if(referenceAccIds.size()>0){
            for(List<Account> referenceAccList : [
                SELECT ONTAP__SAP_Number__c, V360_ReferenceClient__c, ONTAP__Delivery_Delay_Code__c,
                	V360_SalesZoneAssignedBDR__c, V360_SalesZoneAssignedTelesaler__c, V360_SalesZoneAssignedPresaler__c,
                	V360_SalesZoneAssignedCoolers__c, V360_SalesZoneAssignedCredit__c,V360_SalesZoneAssignedTellecolector__c 
                FROM Account 
                WHERE Id IN :referenceAccIds]){
                    for(Account referecenAcc: referenceAccList){
                        for(Account prospAcc: acNewList){
                            if(prospAcc.V360_ReferenceClient__c != NULL && prospAcc.V360_ReferenceClient__c == referecenAcc.id ){
                                prospAcc.V360_SalesZoneAssignedBDR__c = referecenAcc.V360_SalesZoneAssignedBDR__c;
                            	prospAcc.V360_SalesZoneAssignedTelesaler__c =  referecenAcc.V360_SalesZoneAssignedTelesaler__c;
                            	prospAcc.V360_SalesZoneAssignedPresaler__c =  referecenAcc.V360_SalesZoneAssignedPresaler__c;
                            	prospAcc.V360_SalesZoneAssignedCoolers__c =  referecenAcc.V360_SalesZoneAssignedCoolers__c;
                            	prospAcc.V360_SalesZoneAssignedCredit__c =  referecenAcc.V360_SalesZoneAssignedCredit__c;
                            	prospAcc.V360_SalesZoneAssignedTellecolector__c =  referecenAcc.V360_SalesZoneAssignedTellecolector__c;
                            }
                        }
                    }
            }
        }
    }
    
    /**
    * translate codes in fields of a customer in salesforce
    * Created By: c.leal.beltran@accenture.com
    * @param New values of account modified
    * @return void
    */
    public void translatecodes(List<Account> lstacc)
    {
        if(lstacc!=null && lstacc.size()>0)
        {
            SET<String>AccountLibraryFields = new Set<String>();
            List<V360_LibraryFieldsList__c> library = [SELECT V360_Code__c,V360_Field__c,V360_Value__c FROM V360_LibraryFieldsList__c];
            
            Map<String, Schema.SObjectField> m = Schema.SObjectType.Account.fields.getMap();
            for(Schema.SObjectField a : m.values())
            {
                AccountLibraryFields.add(String.valueOf(a));
            }
            
            for(Account acc:lstacc)
            {
                for(V360_LibraryFieldsList__c field :library)
                {
                    if(field.V360_Field__c != NULL && field.V360_Code__c!=NULL && field.V360_Value__c!=NULL && AccountLibraryFields.contains(field.V360_Field__c) && field.V360_Code__c == acc.get(field.V360_Field__c))
                    {
                        String result=(String)acc.put(field.V360_Field__c, field.V360_Code__c + '-' + field.V360_Value__c);
                    }
                }
            }
        }       
    }
      
    /**
    * normalize the credit or cash attribute for each customer
    * Created By: c.leal.beltran@accenture.com
    * @param New values of account modified
    * @return void
    */
    public void normalizeCreditOrCashAttribute(List<Account> lstacc)
    {
        for(Account acc2:lstacc)
        {
            if(acc2.HONES_Term__c != null)
            {        
            	if(acc2.HONES_Term__c.left(4) == GlobalStrings.CREDITVALUE)
                {
                    acc2.ONTAP__Credit_Condition__c = GlobalStrings.ISSM_Cash;
                }
                else
                {
                    acc2.ONTAP__Credit_Condition__c = GlobalStrings.ISSM_Credit;
                }
            }
        } 
    }
    
    /*--------------------------------------------------------Metodos agregados para el proceso de prospectos de Honduras y El salvador --------------------------------------------------------*/
}