/* ----------------------------------------------------------------------------
* AB InBev :: Visit Control
* ----------------------------------------------------------------------------
* Clase: VisitPlanWrapper_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 21/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class VisitPlanWrapper_Test {
	
    /**
    * Test method for set Sap Order Id
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void visitWrap_Case1(){
        VisitPlanWrapper visitWrap = new VisitPlanWrapper();
        VisitPlan__c objVisitPlan = new VisitPlan__c();
        visitWrap.objVisitPlan = objVisitPlan;
        List<AccountByVisitPlan__c> lstAccounts = new List<AccountByVisitPlan__c>();
        visitWrap.lstAccounts = lstAccounts;
    }
}