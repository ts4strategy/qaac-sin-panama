/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Clase: CS_CASE_COOLERS_CALLOUT_cls_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 19/02/2019           Debbie Zacarias       Creacion de la clase test para Callout
*/
@isTest
public class CS_Case_Coolers_Callout_Class_Test 
{
    
    /**
    * Method for create data 
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void loadDataToTest()
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ISSM_TypificationMatrix__c typMat = new ISSM_TypificationMatrix__c();
        typMat.CS_Days_to_End__c = 1;
        insert typMat;
        
        ONTAP__Product__c product = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686');
        insert product;
        
        ONTAP__Product__c product1 = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005687');
        insert product1;
        
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
        
        RecordType rectypeSV = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtRetirementCooler]);        
        RecordType rectype = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtInstallationCooler]);
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Subject';
        caseTest.ISSM_TypificationNumber__c = typMat.Id;
        caseTest.HONES_Case_Country__c = 'El Salvador';
        caseTest.CS_Send_To_Mule__c = false;
        caseTest.RecordTypeId = rectype.id;
        caseTest.CS_skuProduct__c = '3005686';
        caseTest.ISSM_Count__c = 1;
        insert caseTest;
        
         Case caseTest1 = new Case();
        caseTest1.Accountid = accountTest.Id;
        caseTest1.Description = 'Test Description';
        caseTest1.Status = 'New';
        caseTest1.Subject = 'Subject';
        caseTest1.ISSM_TypificationNumber__c = typMat.Id;
        caseTest1.HONES_Case_Country__c = 'El Salvador';
        caseTest1.CS_Send_To_Mule__c = false;
        caseTest1.RecordTypeId = rectype.id;
        caseTest1.CS_skuProduct__c = '3005687';
        caseTest1.ISSM_Count__c = 1;
        insert caseTest1;
             
        
        CS_CoolerSettings__c oSettings = new CS_CoolerSettings__c();
        oSettings.customOrder__c = 'CUSE';
        oSettings.measurecode__c = 'UN';
        oSettings.orderTypeESVInstall__c = 'ZSKB';
        oSettings.orderTypeESVRetire__c = 'ZSKA';
        oSettings.orderTypeHNInstall__c = 'ZHKB';
        oSettings.orderTypeHNRetire__c = 'ZHKA';
        oSettings.paymentMethod__c = '0001';
        oSettings.quantity__c = 1;
        oSettings.role__c = 'SH';
        oSettings.URL__c = 'http://wiit-hndesv-orders-sfdc-to-sap.de-c1.cloudhub.io/generateOrders';
        Insert oSettings;
    }
    
    /**
    * Method for test when get success response
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest static void testCallout()
    {
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        Case caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005686']);
        Case caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005687']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        CS_Case_Coolers_Callout_Class.sendRequest(caso.id);
        
        List<string> lstIdCase = new List<string>();
        lstIdCase.add(caso1.id);
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);        
    }
    
    /*
    * Method for test when get error response
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest static void testCallout_caseError()
    {        
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        List<Case> caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005686']);
        List<Case> caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005687']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        if(caso.size() > 0){
        	CS_Case_Coolers_Callout_Class.sendRequest(caso[0].id);
        }
        
        List<string> lstIdCase = new List<string>();
        if(caso1.size() > 0){
        	lstIdCase.add(caso1[0].id);
        }
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);    } 
}