/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: ONCALL_SendJson_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 16/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class ONCALL_SendJson_Test {
    
    /**
    * Test method for to send a JSON
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    
    @isTest static void sendJson_test(){
        
        Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorSendOrder());
        //String jsonToSend = '{"orderId": "0225000667", "etReturn": [ {"type" : "S", "number" : "233", "message" : "SALES_HEADER_IN procesado con éxito"} ] }';
        
        Account acc = new Account(Name = 'Test Acc',ONTAP__SalesOgId__c='IS');
        insert acc;
        
        
        Set<Id> orderSet = new Set<Id>();
        ONCALL_SendJson sendJ = new ONCALL_SendJson();
        
        ONTAP__Order__c order = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id);
        order.ONCALL__SAP_Order_Item_Counter__c=1;
        order.ONCALL__Total_Order_Item_Quantity__c=1;
        order.ONTAP__DocumentationType__c='test';
        order.ISSM_PaymentMethod__c='Cash';
        order.ONTAP__DeliveryDate__c=System.today();
        order.ONCALL__SAP_Order_Number__c = '00121231283';
        insert order;  
        
        ONTAP__Product__c product= new ONTAP__Product__c();
        insert product;
        
        ONTAP__Order_Item__c orderItem = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__Cond_Applied__c = 'Deal');
        insert orderItem;
        ONTAP__Order_Item__c orderItem2 = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id, ONTAP__ItemProduct__c = product.id);
        insert orderItem2;
        
        Map<String, JSONGenerator> mapSend = new Map<String,JSONGenerator>{order.id => JSON.createGenerator(true)};

        test.startTest(); 
        orderSet.add(order.id);
       	ONCALL_SendJson.send_Json(mapSend);
        test.stopTest();
    }

}