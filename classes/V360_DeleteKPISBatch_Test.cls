/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_DeleteKPISBatch_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 19/12/2018     Carlos Leal        Creation of methods.
*/
@isTest
public class V360_DeleteKPISBatch_Test {
    
    /**
    * Method test for delete all the kpis in V360_DeleteKPISBatch.
    * @author: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void testBatch(){
        Id V360_EmptyBalanceRT = Schema.SObjectType.ONTAP__KPI__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.KPI_V360_RECORDTYPE_NAME).getRecordTypeId();

        ONTAP__KPI__c ob = new ONTAP__KPI__c(RecordTypeId=V360_EmptyBalanceRT, ONTAP__Categorie__c='Coverage',Account_Sap_Id__c='testing');
        insert ob;    
        V360_DeleteKPISBatch obj01 = new V360_DeleteKPISBatch();
        Database.executeBatch(obj01, 200);
    }
}