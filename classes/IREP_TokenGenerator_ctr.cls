public with sharing class IREP_TokenGenerator_ctr {

    public String sapId {get; set;}
    public String token {get; set;}
    public String strRecordId;
    public ONTAP__Tour_Visit__c objTourVisit = new ONTAP__Tour_Visit__c();

    public IREP_TokenGenerator_ctr() {
        //sapId = ApexPages.currentPage().getParameters().get('sapid');
        //strRecordId = ApexPages.currentPage().getParameters().get('aid');
        String strTourVisitId = ApexPages.currentPage().getParameters().get('tid');
        if(strTourVisitId == null || strTourVisitId == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.IREP_NoTourFound));
            objTourVisit = null;
        }
        else{
            objTourVisit = [
                SELECT  Id, ONTAP__TourId__c, ONTAP__Tour__r.Id, ONTAP__Tour__r.ONTAP__RouteId__c,
                        ONTAP__Tour__r.ONTAP__TourDate__c, ONTAP__Account__r.Id , ONTAP__Tour__c,
                        ONTAP__Account__r.ONTAP__SAPCustomerId__c
                FROM    ONTAP__Tour_Visit__c 
                WHERE	Id = :strTourVisitId
                LIMIT 1
            ];
            sapId = objTourVisit.ONTAP__Account__r.ONTAP__SAPCustomerId__c;
            strRecordId = objTourVisit.ONTAP__Account__r.Id;
                
            if(sapId != null && sapId != '' && sapId.length() > 10){
                sapId = sapId.substring(4, sapId.length());
            }          
        }
    }

    public PageReference generateToken(){
        System.debug('sapid: '+sapId);
        System.debug('aid: '+strRecordId);

        if(strRecordId == null || strRecordId == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.IREP_InvalidAccount));
            return null;
        }

        if(sapId == null || sapId == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.IREP_InvalidSAPNumber));
            return null;
        }
        token = generate(strRecordId, sapId);
        if(token == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.IREP_NoTourFound));
            return null;
        }
        return null;
    }

    public String generate(String customerId, String sapId){
        String strToken = '';
        String userName = UserInfo.getName();
        Integer tokx = 0;
        
        /*list<ONTAP__Tour_Visit__c> tvc = [ 
            SELECT  Id, ONTAP__TourId__c, ONTAP__Tour__r.Id, ONTAP__Tour__r.ONTAP__RouteId__c,
                    ONTAP__Tour__r.ONTAP__TourDate__c, ONTAP__Account__r.Id , ONTAP__Tour__c
            FROM    ONTAP__Tour_Visit__c 
            WHERE   ONTAP__Account__c = :customerId AND 
                    ONTAP__Tour__r.ONTAP__IsActive__c  = True  
            ORDER BY ONTAP__Tour__r.ONTAP__TourDate__c 
            DESC LIMIT 1 
        ];*/

        if (objTourVisit != null){
            String tourId = objTourVisit.ONTAP__TourId__c;
            DateTime cd = datetime.now();
            Integer sum1 = cd.year();
            Integer sum2 = cd.day()*100 + cd.month();
            System.debug('\nsum2:'+sum2);
            String v = '1';
            String r = objTourVisit.ONTAP__Tour__r.ONTAP__RouteId__c.substring(2);
            r = '0000' + r;
            r = r.substring(r.length() - 4);
            System.debug('\nr:'+r);
            String c = '0000000000' + sapId;
            c = c.substring(c.length() - 10);
            System.debug('\nc:'+c);            
            String temp = v+r+c;
            System.debug('\nr:'+r);            
            System.debug('\ntemp......'+temp);
            tokx = integer.valueOf(temp.substring(0,3))
                    + integer.valueOf(temp.substring(3,6))
                    + integer.valueOf(temp.substring(6,9))
                    + integer.valueOf(temp.substring(9,12))
                    + integer.valueOf(temp.substring(12,15))
                    + sum1 + sum2;
            System.debug('\ntokx :'+tokx );                    
            //token variable recieves the value from tokx to be show in the visualforce page
            if (tokx > 0){
                strtoken = ''+tokx;
            }
               
            ONTAP__AuthorizationToken__c objToken = new ONTAP__AuthorizationToken__c();
            objToken.IREP_ExternalKey__c = customerId + '-' + objTourVisit.ONTAP__Tour__c;
            objToken.IREP_Tour__c = objTourVisit.ONTAP__Tour__c;
            objToken.ONTAP__CustomerAccount__c = customerId;
            objToken.ONTAP__RequestedBy__c = UserInfo.getUserId();
            objToken.ONTAP__RequestedDateTime__c = DateTime.now();
            objToken.ONTAP__SAPCustomerId__c = sapId;
            objToken.ONTAP__Token__c = strtoken;
            objToken.ONTAP__RequestDescription__c = 'Token ' + objToken.ONTAP__Token__c + ' requested by ' + userName + ' for Customer ' + objToken.ONTAP__SAPCustomerId__c + ' - Date/Time: ' + objToken.ONTAP__RequestedDateTime__c + '.';
            upsert objToken IREP_ExternalKey__c;
        }
        return strToken;
    }
}