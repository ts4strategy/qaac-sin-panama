/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: wrapperClass_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

@isTest
private class wrapperClass_Test{
    
    /**
    * Test method for set values in the object
    * Created By: luis.arturo.parra@accenture.com
    * @param void
    * @return void
    */
    @isTest static void wrapperTest(){
        
        wrapperClass totest=new wrapperClass();
        totest.cantidad='cantidad';
        totest.description='descripcion';
        totest.lista=new List<String>();
        totest.listaPBE=new List<String>();
        totest.listaPFN=new List<String>();
        totest.listaPFNUnits=new List<String>();
        totest.material='material';
        totest.materialProduct='product';
        totest.optional=true;
        totest.pbereason='reason';
        totest.pbetag='tag';
        totest.pfntag='tag';
        totest.price=0.0;
        totest.productCode='code';
        totest.productMetadataType='metadata';
        totest.productRecord='record';
        totest.productType='type';
        totest.promoDescTosend='tosend';
        totest.promoNamedescription='desc';
        totest.promosku='sku';
        totest.pronr='pronr';
        totest.quantity=1.0;
        totest.sku='sku';
        totest.subtotal=83.3;
        totest.tax=83.3;
        totest.tipo='tipo';
        totest.total=38.3;
        totest.unitPrice=83.3;
       totest.pfnpbe='pfb';
        totest.addempties='ur';
        totest.discount=2.2;
        totest.listadeals=new  List<json_deals>();
 totest.orderTotal=2.2;
 totest.orderSubTotal=2.2;
 totest.orderTaxes=2.2;
 totest.orderDiscounts=2.2;
        totest.position='fndm';
    totest.condcode='test';
    totest.amount='test';
    totest.posex='test';
    totest.selectedByUser=true;    
    totest.measureCode='test';
    totest.freeGood=true;
    }

    
    
}