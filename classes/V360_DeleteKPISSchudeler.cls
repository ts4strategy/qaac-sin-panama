/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteKPISSchudeler.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteKPISSchudeler  implements schedulable
{
     /**
   * Execute a Schudeler for execute the batch for delete all the Kpis.
   * @author: c.leal.beltran@accenture.com
   * @param SchedulableContext sc
   * @return void
   */
    global void execute(SchedulableContext sc)
    {
    V360_DeleteKPISBatch c    = new V360_DeleteKPISBatch();
      database.executebatch(c,200);
    }
}