/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_TYPIFICATION_TRIGGERHANDLER_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 21/01/2019     Carlos Leal             Creation of methods.
 */
@isTest
public class CS_CaseTypification_Trigger_Class_Test 
{

    /**
    * Test method for before insert a typification c
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_beforeInsert_UseCase1()
    {
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        CS_CaseTypification_Trigger_Class obj01 = new CS_CaseTypification_Trigger_Class();
        Entitlement ent = new Entitlement();
        ent.Name = 'Test';
        ent.AccountId = acc.Id;
   	    insert ent;
        ISSM_TypificationMatrix__c typ = new ISSM_TypificationMatrix__c();
        typ.CS_Entitlement_Name__c = 'Test';
        typ.CS_Days_to_End__c = 1.00;
        insert(typ);
     }
}