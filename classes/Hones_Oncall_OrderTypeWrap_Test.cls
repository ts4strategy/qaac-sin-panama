/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: Hones_Oncall_OrderTypeWrap_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 09/04/2019           Ricardo Navarrete Lozano          Creation of methods.
*/
@isTest
public class Hones_Oncall_OrderTypeWrap_Test {
    
    /**
     * Test for picktype
     * Created By: r.navarrete.lozano@accenture.com
     * @param void
     * @return void
     */
    @isTest static void test_Hones_Oncall_OrderTypeWrap_UseCase1() {
        
        List<List<String>> picktypeList = new List<List<String>>();
        List<String> substring = new List<String>();
        substring.add('Test1');
        substring.add('Test2');
        picktypeList.add(substring);
        
        Hones_Oncall_OrderTypeWrap ot = new Hones_Oncall_OrderTypeWrap();
        ot.picktype = picktypeList;
    }
}