/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteInvoicesItemsBatch_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
public class V360_AccountAssetTriggerHandler_Test {
    
    /**
    * Method Test for inserts and updates in accounts
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
     @testSetup
  	static void setupTestData(){
        test.startTest();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        Id V360_Account_AssetRT = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ASSETS_V360_RECORDTYPE_NAME).getRecordTypeId();
          
        User u = [Select Id FROM User Limit 1];  
        Account a = New Account(ONTAP__ExternalKey__c='1234567890',Name ='ABC', RecordTypeId=salesZonedevRecordTypeId, ONTAP__SAP_Number__c = '1234567890');
        insert a;

        ONTAP__Account_Asset__c aa =  new ONTAP__Account_Asset__c(V360_SAPCustomerId__c = '1234567890',RecordTypeId =V360_Account_AssetRT);
        insert aa;
    
        update aa;
                   
        test.stopTest();
    
	}
    
    /**
    * Method Test for Client reference
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    @isTest static void test_relateAccountsToAssets_UseCase1(){
        
        List<ONTAP__Account_Asset__c> acclst = [SELECT Id, V360_SAPCustomerId__c, V360_InventoryNumber__c, ONTAP__Account__c,RecordTypeId FROM ONTAP__Account_Asset__c Limit 5];
        V360_AccountAssetTriggerHandler obj01 = new V360_AccountAssetTriggerHandler();
        List<ONTAP__Account_Asset__c> objList = PreventExecutionUtil.validateAccountAssetWithOutId(acclst);
        Set<Id> setIdsAsset = new Set<Id>();
        for(ONTAP__Account_Asset__c aass : objList){
            if(!setIdsAsset.contains(aass.Id) && aass.ONTAP__Account__c == null){setIdsAsset.add(aass.Id);}
        }
        if(!setIdsAsset.isEmpty()){
        	//V360_AccountAssetTriggerHandler.relateAccountsToAssetsAfter(setIdsAsset);
        }
    }
    
   
   
}