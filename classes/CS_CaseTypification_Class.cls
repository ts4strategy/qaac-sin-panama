/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASETYPIFICATION_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 07/11/ 2018     Jose Luis Vargas         Creacion de la clase usada para el componente de Tipificaciones
 * 29/11/2018      Jose Luis Vargas         Se agrega metodo para la generacion de un nuevo caso.
 */
public class CS_CaseTypification_Class 
{
    public CS_CaseTypification_Class() {  }
    
    public class PicklistValues 
    {
        @AuraEnabled
        public String label {get;set;} 
        @AuraEnabled
        public String value {get;set;}
        
        public PicklistValues(String label, String value)
        {
            this.label = label;
            this.value = value;
        }
    }
          
    /**
    * Method for consulting the user country
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return string with the user country
    */
    private static String GetUserCountry()
    {
        User oUserInfo = new User();
        oUserInfo = [SELECT Country__c FROM USER WHERE Id =: UserInfo.getUserId()];
        return oUserInfo.Country__c;
    }
             
    /**
    * Method for consulting the level 1 for typification for country
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return List with the typification level 1 for country
    */
    @AuraEnabled
    public static List<PicklistValues> GetTypificationN1()
    {
        List<PicklistValues> lstTypificationN1  = new List<PicklistValues>();
        AggregateResult[] arrTypificationN1;
        String userCountry = GetUserCountry();

        arrTypificationN1 = [SELECT ISSM_TypificationLevel1__c FROM ISSM_TypificationMatrix__c WHERE ISSM_Countries_ABInBev__c =: userCountry  GROUP BY ISSM_TypificationLevel1__c];
        lstTypificationN1.add(new PicklistValues('',''));
        
        for(AggregateResult ar: arrTypificationN1)
        {
          lstTypificationN1.add(new PicklistValues(String.valueOf(ar.get('ISSM_TypificationLevel1__c')), String.valueOf(ar.get('ISSM_TypificationLevel1__c'))));
        }

        return lstTypificationN1 ;
    }
    
    /**
    * Method for consulting the level 2 for typification for country
    * @author: jose.l.vargas.lara@accenture.com
    * @param Typification selected for level 1
    * @return List with the typification level 2 for country
    */
    @AuraEnabled
    public static List<PicklistValues> GetTypificationN2(string typificationNivel1)
    {
        List<PickListValues> lstTypificationN2 = new List<PickListValues>();
        AggregateResult[] arrTypificationN2;
        String userCountry = GetUserCountry();
               
        arrtypificationN2 = [SELECT ISSM_TypificationLevel2__c FROM ISSM_TypificationMatrix__c WHERE ISSM_TypificationLevel1__c =: typificationNivel1 
                             AND ISSM_TypificationLevel2__c != '' AND ISSM_Countries_ABInBev__c =: userCountry GROUP BY ISSM_TypificationLevel2__c];
        lstTypificationN2.add(new PickListValues('',''));
        
        for(AggregateResult ar: arrTypificationN2)
        {
           lstTypificationN2.add(new PicklistValues(String.valueOf(ar.get('ISSM_TypificationLevel2__c')), String.valueOf(ar.get('ISSM_TypificationLevel2__c'))));
        }
        
        return lstTypificationN2;
    }
    
    /**
    * Method for consulting the level 3 for typification for country
    * @author: jose.l.vargas.lara@accenture.com
    * @param Typification selected for level 1, typification selected for level 2
    * @return List with the typification level 3 for country
    */
    @AuraEnabled
    public static List<PickListValues> GetTypificationN3(string typificationNivel1, string typificationNivel2)
    {
        List<PickListValues> lstTypificationN3 = new List<PickListValues>();
        AggregateResult[] arrTypificationN3;
        String userCountry = GetUserCountry();
        
        arrTypificationN3 = [SELECT ISSM_TypificationLevel3__c FROM ISSM_TypificationMatrix__c WHERE ISSM_TypificationLevel1__c =: typificationNivel1 
                             AND ISSM_TypificationLevel2__c =: typificationNivel2 AND ISSM_TypificationLevel3__c != '' AND ISSM_Countries_ABInBev__c =: userCountry 
                             GROUP BY ISSM_TypificationLevel3__c];
        lstTypificationN3.add(new PickListValues('',''));
        
        for(AggregateResult ar: arrTypificationN3)
        {
           lstTypificationN3.add(new PicklistValues(String.valueOf(ar.get('ISSM_TypificationLevel3__c')), String.valueOf(ar.get('ISSM_TypificationLevel3__c'))));
        }
        
        return lstTypificationN3;
    }
    
    /**
    * Method for consulting the level 4 for typification for country
    * @author: jose.l.vargas.lara@accenture.com
    * @param Typification selected for level 1, typification selected for level 2, typification selected for level 3
    * @return List with the typification level 4 for country
    */
    @AuraEnabled
    public static List<PickListValues> GetTypificationN4(string typificationNivel1, string typificationNivel2, string typificationNivel3)
    {
        List<PickListValues> lstTypificationN4 = new List<PickListValues>();
        AggregateResult[] arrTypificationN4;
        String userCountry = GetUserCountry();
        
        arrTypificationN4 = [SELECT ISSM_TypificationLevel4__c FROM ISSM_TypificationMatrix__c WHERE ISSM_TypificationLevel1__c =: typificationNivel1 
                             AND ISSM_TypificationLevel2__c =: typificationNivel2 AND ISSM_TypificationLevel3__c =: typificationNivel3 AND ISSM_TypificationLevel4__c != '' 
                             AND ISSM_Countries_ABInBev__c =: userCountry 
                             GROUP BY ISSM_TypificationLevel4__c];
        lstTypificationN4.add(new PickListValues('',''));
        
        for(AggregateResult ar: arrTypificationN4)
        {
           lstTypificationN4.add(new PicklistValues(String.valueOf(ar.get('ISSM_TypificationLevel4__c')), String.valueOf(ar.get('ISSM_TypificationLevel4__c'))));
        }
        
        return lstTypificationN4;
    }
    
    /**
    * Method for consulting the account detail
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id account
    * @return Account whith the detail
    */
    @AuraEnabled
    public static Account GetAccountDetail(string idAccount)
    {
        Account oAccountData = new Account();
        oAccountData = [SELECT Name, ONTAP__SAPCustomerId__c, Phone, ONTAP__Secondary_Phone__c FROM Account WHERE Id =: idAccount LIMIT 1];

        return oAccountData;
    }
    
    /**
    * Method for generate a new standard case
    * @author: jose.l.vargas.lara@accenture.com
    * @param Id account, typification level 1, typification level 2, typification level 3,
    *        typification level 4, Case detail
    * @return String with id case
    */
    @AuraEnabled
    public static Case GenerateNewCase(string IdAccount, string typificationN1, string typificationN2, string typificationN3, string typificationN4, string CaseDetail)
    {
        string idNewCase = '';
        Case oDetailCase = new Case();
        
        CS_CASEGENERATION_CLASS oGenerateCase = new CS_CASEGENERATION_CLASS();     
        idNewCase = oGenerateCase.GenerateNewCase(IdAccount, typificationN1, typificationN2, typificationN3, typificationN4, CaseDetail);
        if(idNewCase != null && idNewCase != '')
            oDetailCase = [SELECT Id, CS_Automatic_Closing_Case__c FROM Case WHERE Id =: idNewCase];
                
        return oDetailCase;
    }
}