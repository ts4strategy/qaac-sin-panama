/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_TourTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
public class VisitControl_TourTriggerHandler_Test {

    /**
    * Test method for set parameters 
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
	@testSetup
  	static void setupTestData(){
        test.startTest();
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        
        User u = [Select Id FROM User Limit 1];  
        
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);
        insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id);
        insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id);
        insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id);
        insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
        insert szz;
        
        V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.id,V360_type__c='Presales');
        insert sz;
        
        Account acc = new Account(Name='1',V360_SalesZoneAssignedBDR__c=sz.Id,V360_SalesZoneAssignedTelesaler__c=sz.Id,V360_SalesZoneAssignedPresaler__c=sz.Id,V360_SalesZoneAssignedCredit__c=sz.Id,V360_SalesZoneAssignedTellecolector__c=sz.Id,V360_SalesZoneAssignedCoolers__c=sz.Id);  
        
        ONTAP__Route__c rt = New ONTAP__Route__c(RouteManager__c = u.Id,supervisor__c=u.Id,VisitControl_SalesZone__c =szz.Id, ServiceModel__c='Presales', VisitControl_ByPass__c = true,RetraceRoute__c=True);
        insert rt;  
          
        VisitPlan__c vp = New VisitPlan__c(Route__c = rt.Id, ExecutionDate__c = Date.today(), EffectiveDate__c = Date.today() + 7);
        insert vp;   
        
        ONTAP__Tour__c tt = New ONTAP__Tour__c(Route__c = rt.Id, ONTAP__TourId__c = 'T-000001160',ONTAP__TourDate__c = Date.today(), VisitPlan__c = vp.Id); 
        insert tt;
          
        VisitControl_RetraceAndAnticipateRoute__c visitcontrol_retraceandanticipateroute_Obj = new VisitControl_RetraceAndAnticipateRoute__c(VisitControl_Route__c = rt.id, VisitControl_DateToSkip__c = Date.today(), VisitControl_RetraceOrAnticipate__c = 'Anticipate', VisitControl_Deleted__c = false);
        Insert visitcontrol_retraceandanticipateroute_Obj; 
        
        VisitControl_RetraceAndAnticipateRoute__c visitcontrol_retraceandanticipateroute_Obj2 = new VisitControl_RetraceAndAnticipateRoute__c(VisitControl_Route__c = rt.id, VisitControl_DateToSkip__c = Date.today(), VisitControl_RetraceOrAnticipate__c = 'Retrace', VisitControl_Deleted__c = false);
        Insert visitcontrol_retraceandanticipateroute_Obj2;
                
        test.stopTest();
    
	}
    
    /**
    * Test to set a retrace by tour in a day    
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_retraceTourOneDay_UseCase1(){        
        List<ONTAP__Tour__c> tapObj = [SELECT VisitControl_IsRetraced__c, VisitControl_VisitplanRoute__c, ONTAP__TourDate__c, ONTAP__IsActive__c, ONTAP__RouteId__c, ONTAP__UserId__c, Route__c FROM ONTAP__Tour__c];
        List<V360_SalerPerZone__c> spzObj = [SELECT V360_User__c, V360_SalesZone__c, V360_Type__c, IsDeleted FROM V360_SalerPerZone__c];
        
        VisitControl_TourTriggerHandler obj01 = new VisitControl_TourTriggerHandler();
        VisitControl_TourTriggerHandler.retraceTourOneDay(tapObj);
    }
    
    /**
    * Test to set a anticipe by tour in a day    
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
     @isTest static void test_anticipateTourOneDay_UseCase1(){
        
        List<ONTAP__Tour__c> tapObj = [SELECT VisitControl_IsRetraced__c, VisitControl_VisitplanRoute__c, ONTAP__TourDate__c, ONTAP__IsActive__c, ONTAP__RouteId__c, ONTAP__UserId__c, Route__c FROM ONTAP__Tour__c];
        List<V360_SalerPerZone__c> spzObj = [SELECT V360_User__c, V360_SalesZone__c, V360_Type__c, IsDeleted FROM V360_SalerPerZone__c];
        
        VisitControl_TourTriggerHandler obj01 = new VisitControl_TourTriggerHandler();
        VisitControl_TourTriggerHandler.anticipateTourOneDay(tapObj);
    }
}