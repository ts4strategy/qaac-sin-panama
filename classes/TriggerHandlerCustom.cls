/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: TriggerHandlerCustom.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Gerardo Martinez        Creation of methods.
 */
public virtual class TriggerHandlerCustom {
    
    // static map of handlername, times run() was invoked
    private static Map<String, LoopCount> loopCountMap;
    private static Set<String> bypassedHandlers;
    
    // the current context of the trigger, overridable in tests
    @TestVisible
    private TriggerContext context;
    
    // the current context of the trigger, overridable in tests
    @TestVisible
    private Boolean isTriggerExecuting;
    
    // static initialization
    static {
        loopCountMap = new Map<String, LoopCount>();
        bypassedHandlers = new Set<String>();
    }
    
    // constructor
    public TriggerHandlerCustom() {
        this.setTriggerContext();
    }
    
    /***************************************
* public instance methods
***************************************/
   
    /**
    * Main method that will be called during execution 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public void run() {
        
        if(!validateRun()) return;
        
        addToLoopCount();
        
        // dispatch to the correct handler method
        if(this.context == TriggerContext.BEFORE_INSERT) {
            this.beforeInsert();
        } else if(this.context == TriggerContext.BEFORE_UPDATE) {
            this.beforeUpdate();
        } else if(this.context == TriggerContext.BEFORE_DELETE) {
            this.beforeDelete();
        } else if(this.context == TriggerContext.AFTER_INSERT) {
            this.afterInsert();
        } else if(this.context == TriggerContext.AFTER_UPDATE) {
            this.afterUpdate();
        } else if(this.context == TriggerContext.AFTER_DELETE) {
            this.afterDelete();
        } else if(this.context == TriggerContext.AFTER_UNDELETE) {
            this.afterUndelete();
        }
        
    }
    
    /**
    * Method for set the max loop counter
    * @author: g.martinez.cabral@accenture.com
    * @param Integer max
    * @return Void
    */
    public void setMaxLoopCount(Integer max) {
        String handlerName = getHandlerName();
        if(!TriggerHandlerCustom.loopCountMap.containsKey(handlerName)) {
            TriggerHandlerCustom.loopCountMap.put(handlerName, new LoopCount(max));
        } else {
            TriggerHandlerCustom.loopCountMap.get(handlerName).setMax(max);
        }
    }
    
    /**
    * Method for clear the max loop counter
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public void clearMaxLoopCount() {
        this.setMaxLoopCount(-1);
    }
    
    /***************************************
* public static methods
***************************************/
    
    /**
    * Method for use bypass a handlerName
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public static void bypass(String handlerName) {
        TriggerHandlerCustom.bypassedHandlers.add(handlerName);
    }
    
    /**
    * Method for clear the bypass of a handlerName
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public static void clearBypass(String handlerName) {
        TriggerHandlerCustom.bypassedHandlers.remove(handlerName);
    }
    
    /**
    * Method for check the bypass of a handlerName
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public static Boolean isBypassed(String handlerName) {
        return TriggerHandlerCustom.bypassedHandlers.contains(handlerName);
    }
    
    /**
    * Method for clear all bypass of a handlerName
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public static void clearAllBypasses() {
        TriggerHandlerCustom.bypassedHandlers.clear();
    }
    
    /***************************************
* private instancemethods
***************************************/
    
    /**
    * Method for set the contect in the trigger constructor
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    @TestVisible
    private void setTriggerContext() {
        this.setTriggerContext(null, false);
    }
    
    /**
    * Method for set the contect in the trigger
    * @author: g.martinez.cabral@accenture.com
    * @param String ctx, Boolean testMode
    * @return Void
    */
    @TestVisible
    private void setTriggerContext(String ctx, Boolean testMode) {
        if(!Trigger.isExecuting && !testMode) {
            this.isTriggerExecuting = false;
            return;
        } else {
            this.isTriggerExecuting = true;
        }
        
        if((Trigger.isExecuting && Trigger.isBefore && Trigger.isInsert) ||
           (ctx != null && ctx == 'before insert')) {
               this.context = TriggerContext.BEFORE_INSERT;
           } else if((Trigger.isExecuting && Trigger.isBefore && Trigger.isUpdate) ||
                     (ctx != null && ctx == 'before update')){
                         this.context = TriggerContext.BEFORE_UPDATE;
                     } else if((Trigger.isExecuting && Trigger.isBefore && Trigger.isDelete) ||
                               (ctx != null && ctx == 'before delete')) {
                                   this.context = TriggerContext.BEFORE_DELETE;
                               } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isInsert) ||
                                         (ctx != null && ctx == 'after insert')) {
                                             this.context = TriggerContext.AFTER_INSERT;
                                         } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isUpdate) ||
                                                   (ctx != null && ctx == 'after update')) {
                                                       this.context = TriggerContext.AFTER_UPDATE;
                                                   } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isDelete) ||
                                                             (ctx != null && ctx == 'after delete')) {
                                                                 this.context = TriggerContext.AFTER_DELETE;
                                                             } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isUndelete) ||
                                                                       (ctx != null && ctx == 'after undelete')) {
                                                                           this.context = TriggerContext.AFTER_UNDELETE;
                                                                       }
    }
    
    /**
    * Method for increment the loop count
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    @TestVisible
    private void addToLoopCount() {
        String handlerName = getHandlerName();
        if(TriggerHandlerCustom.loopCountMap.containsKey(handlerName)) {
            Boolean exceeded = TriggerHandlerCustom.loopCountMap.get(handlerName).increment();
            if(exceeded) {
                Integer max = TriggerHandlerCustom.loopCountMap.get(handlerName).max;
                throw new TriggerHandlerException('Maximum loop count of ' + String.valueOf(max) + ' reached in ' + handlerName);
            }
        }
    }
    
    /**
    * Method for make sure this trigger should continue to run
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Boolean
    */
    @TestVisible
    private Boolean validateRun() {
        if(!this.isTriggerExecuting || this.context == null) {
            throw new TriggerHandlerException('Trigger handler called outside of Trigger execution');
        }
        if(TriggerHandlerCustom.bypassedHandlers.contains(getHandlerName())) {
            return false;
        }
        return true;
    }
    
    /**
    * Method for get handler name
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return String
    */
    @TestVisible
    private String getHandlerName() {
        return String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
    }
    
    /***************************************
* context methods
***************************************/
    
    // context-specific methods for override
    @TestVisible
    protected virtual void beforeInsert(){}
    @TestVisible
    protected virtual void beforeUpdate(){}
    @TestVisible
    protected virtual void beforeDelete(){}
    @TestVisible
    protected virtual void afterInsert(){}
    @TestVisible
    protected virtual void afterUpdate(){}
    @TestVisible
    protected virtual void afterDelete(){}
    @TestVisible
    protected virtual void afterUndelete(){}
    
    /***************************************
* inner classes
***************************************/
    
    /**
    * Method inner class for managing the loop count per handler
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    @TestVisible
    private class LoopCount {
        private Integer max;
        private Integer count;
        
        public LoopCount() {
            this.max = 5;
            this.count = 0;
        }
        
        public LoopCount(Integer max) {
            this.max = max;
            this.count = 0;
        }
        
        public Boolean increment() {
            this.count++;
            return this.exceeded();
        }
        
        public Boolean exceeded() {
            if(this.max < 0) return false;
            if(this.count > this.max) {
                return true;
            }
            return false;
        }
        
        public Integer getMax() {
            return this.max;
        }
        
        public Integer getCount() {
            return this.count;
        }
        
        public void setMax(Integer max) {
            this.max = max;
        }
    }
    
    /**
    * Method get the count of triggers context actives 
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return enum
    */
    @TestVisible
    private enum TriggerContext {
        BEFORE_INSERT, BEFORE_UPDATE, BEFORE_DELETE,
            AFTER_INSERT, AFTER_UPDATE, AFTER_DELETE,
            AFTER_UNDELETE
            }
    
    /**
    * Exception class
    * @author: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    public class TriggerHandlerException extends Exception {}
    

}