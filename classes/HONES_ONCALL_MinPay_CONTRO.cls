/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* -----------------------------------------------------------------------------
* Clase: HONES_ONCALL_MinPay_CONTRO.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------
* Date                 User                   Description
* 19/12/2018     Luis Parra        Creation of methods.
*
* @author: l.parra@accenture.com  -Luis arturo Parra Rosas
* @author: heron.zurita@accenture.com -Heron Zurita Vazquez
-------------------------------------------------------------------------------*/
public class HONES_ONCALL_MinPay_CONTRO {
    
    /**
    * Method to get the credit limit for an specific account
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return List<Account>
    */
    @AuraEnabled
    public static List<Account> creditLimit(String recordId){
        
        List<Account> lim = new List<Account>([SELECT ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Id =: recordId]);
        
        return lim;       
    }
    
    /**
    * Method to get the account id for an specific Call
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return List<Id>
    */
    
    @AuraEnabled
    public static List<Id> getById(String recordId){
        
        List<Id> AccId = new List<Id>();
        //ONCALL__Call__c acc=[SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE Id =:recordId ];    
        //AccId.add(acc.ONCALL__POC__c);
        AccId.add(recordId);
        return AccId;
    }
    
    /**
    * Method to get the first record account id for an specific Call
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return List<Id>
    */
    
    @AuraEnabled
    public static List<Id> getFlexibleDataById(String recordId){
        
        List<Id> AccId = new List<Id>();
        ONCALL__Call__c acc=[SELECT ONCALL__POC__c FROM ONCALL__Call__c WHERE ONCALL__POC__c =:recordId LIMIT 1];    
        AccId.add(acc.ONCALL__POC__c);
        return AccId;
    }
    
    /**
    * Method to obtain the Account of an Open Item
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return List<Id>
    */
    
    @AuraEnabled
    public static List<Id> getFlexibleDataById2(String recordId){
        List<Id> AccId = new List<Id>();
        //ONTAP__OpenItem__c acc=[SELECT ONTAP__Account__c FROM ONTAP__OpenItem__c WHERE ONTAP__Account__c =:recordId LIMIT 1];    
        AccId.add(recordId);
        return AccId;
    }
    
    
    /**
    * Method to obtain the available balance of an Account 
    * Created By: heron.zurita@accenture.com  
    * @param Double recordId
    * @return Double
    */
    @AuraEnabled
    public static Double disponible(String recordId){
        System.debug('disponible recordId' + recordId);
        //get account country
        List<Account> lim = new List<Account>([SELECT ISSM_CreditLimit__c, ONTAP__ExternalKey__c FROM Account WHERE Id =:recordId]);
        String account_country = lim[0].ONTAP__ExternalKey__c.left(2);        
        
        //get recordtypes of orders that affect credit
        List<String> orders_mdt_sum = new List<String>();
        List<String> orders_mdt_min = new List<String>();
        List<String> orders_rt_sum = new List<String>();
        List<String> orders_rt_min = new List<String>();
        
        List<Orders_Bh__mdt> orders_mtd = new List<Orders_Bh__mdt>();
        orders_mtd = [Select DeveloperName, Min__c, Sum__c, Country__c  
                      From Orders_Bh__mdt 
                      Where Country__c =: account_country                       		
                      Limit 1];
        orders_mdt_sum = orders_mtd[0].Sum__c.split(',');
        orders_mdt_min = orders_mtd[0].Min__c.split(',');
        
        for(String aux1 : orders_mdt_sum){
            orders_rt_sum.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux1).getRecordTypeId());
        }
        
        for(String aux2 : orders_mdt_min){
            orders_rt_min.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux2).getRecordTypeId());
        }
      
        Double creditLimit = 0.0;
        Double amount = 0.0;
        Double sumaAmount = 0.0;
        Double totalDisponible = 0.0;
        Double orderAmount= 0.0;
        Double orderAmount_sum= 0.0;
        Double orderAmount_min= 0.0;  
        //get account credit limit
        if(lim != null){
            for(Account i : lim){
                if(i.ISSM_CreditLimit__c != null) {
                    creditLimit = i.ISSM_CreditLimit__c;
                }
                else{
                    creditLimit = 0.0;
                }
            }
        }
        else
        {
            creditLimit = 0.0;  
        }
        
        //total amount of open items
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        /*AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) FROM ONTAP__OpenItem__c WHERE ONTAP__Account__c =:recordId]; 
        if(Amou != null){
            for(AggregateResult i : Amou){
                sumaAmount = (Decimal)i.get('expr0');
                if(sumaAmount == null){
                    sumaAmount=0.0;
                }            
            }  
        }
        else{
            sumaAmount=0.0;
        }*/              
      
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =: recordId AND
                              		RecordTypeId =: rt_open_items
                             ];
        System.debug('account_open_items ' + account_open_items);
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    sumaAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    sumaAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        if(sumaAmount == null) sumaAmount = 0;
                    
		//total of todays orders, this will affect the sumaAmount       
		//Date
		DateTime hoy = System.today();
        DateTime dateaux = hoy.addDays(1);
        
        AggregateResult[] itemAmount_sum = [SELECT SUM(ONTAP__Amount_Total__c) 
                                            FROM ONTAP__Order__c 
                                            WHERE 
                                            ONTAP__OrderAccount__c =:recordId AND
                                            ISSM_PaymentMethod__c = 'Credit' AND 
                                            RecordTypeId in: orders_rt_sum AND 
                                            CreatedDate >=: hoy];
                                            //((CreatedDate >=: hoy AND CreatedDate <=: dateaux) OR (ONTAP__DeliveryDate__c >=: hoy AND ONTAP__DeliveryDate__c <=: dateaux))];                                                           
        
        if(itemAmount_sum != null){
            for(AggregateResult i : itemAmount_sum){
                orderAmount_sum = (Decimal)i.get('expr0');
                if(orderAmount_sum == null){ 
                    orderAmount_sum=0.0;
                }  
            }
        }
        else{
            orderAmount_sum=0.0;
        }
        
        AggregateResult[] itemAmount_min = [SELECT SUM(ONTAP__Amount_Total__c) 
                                            FROM ONTAP__Order__c 
                                            WHERE 
                                            ONTAP__OrderAccount__c =:recordId AND
                                            ISSM_PaymentMethod__c = 'Credit' AND 
                                            RecordTypeId in: orders_rt_min AND
                                            CreatedDate >=: hoy];
                                            //((CreatedDate >=: hoy AND CreatedDate <=: dateaux) OR (ONTAP__DeliveryDate__c >=: hoy AND ONTAP__DeliveryDate__c <=: dateaux))];                  
        if(itemAmount_min != null){
            for(AggregateResult i : itemAmount_min){
                orderAmount_min = (Decimal)i.get('expr0');
                if(orderAmount_min == null){ 
                    orderAmount_min=0.0;
                }  
            }
        }
        else{
            orderAmount_min=0.0;
        }
        
        orderAmount = orderAmount_sum - orderAmount_min;
               
        if(creditLimit == null){creditLimit = 0.0;}
        if(sumaAmount == null){sumaAmount = 0.0;}
        if(totalDisponible == null){totalDisponible = 0.0;}
        if(orderAmount == null){orderAmount = 0.0;}
        
        System.debug('creditLimit: ' +creditLimit);
        System.debug('sumaAmount: ' +sumaAmount);
        System.debug('orderAmount: ' +orderAmount);
        
        totalDisponible = creditLimit - sumaAmount; 
        
        totalDisponible = totalDisponible - orderAmount;
        
        //if (totalDisponible < 0) totalDisponible = 0;
        
        return totalDisponible;        
    }
    
     /**
    * Method to obtain the total amount of all Open Items of an Account and the Due Date is set before today 
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return Double
    */
    
    @AuraEnabled
    public static Double outOfDateCredits(String recordId){
        
        
        Double totalAmount = 0.0;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c <: System.today()	AND
                              		RecordTypeId =: rt_open_items
                             ];
        
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        /*
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
									FROM ONTAP__OpenItem__c 
									WHERE ONTAP__Account__c =:recordId AND ONTAP__DueDate__c <: System.today()];
        
        for(AggregateResult i : Amou){
            totalAmount = (Decimal)i.get('expr0');
            
        }*/
        
        if(totalAmount == null) totalAmount = 0;
        
        return totalAmount;
        
    }
    
    /**
    * Method to obtain the total amount of all Open Items of an Account and the Due Date is set before today and the Reference is Liquid
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return Double
    */
    
    @AuraEnabled
    public static Double outOfDateCreditsLiquid(String recordId){
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String liquid = !openItems_identifier.isEmpty() ? openItems_identifier[0].Liquid__c : null;
        System.debug('Los líquidos: ' + liquid);
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        if(liquid != null){
            account_open_items = [Select Id, 
                                        ONTAP__Amount__c, 
                                        ISSM_CreditDebit_F__c, 
                                        RecordTypeId,  
                                        ONTAP__SAPCustomerId__c,
                                        ISSM_Debit_Credit__c,
                                        ONTAP__Account__c
                                  From ONTAP__OpenItem__c
                                  Where 
                                        ONTAP__Account__c =:recordId AND 
                                        ONTAP__DueDate__c <: System.today()	AND
                                        RecordTypeId =: rt_open_items AND 
                                        V360_Reference__c =: liquid
                                 ];
        }
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        /*
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) FROM ONTAP__OpenItem__c WHERE (ONTAP__Account__c =:recordId AND ONTAP__DueDate__c <: System.today()) AND V360_Reference__c = 'L'];
        
        for(AggregateResult i : Amou){
            totalAmount = (Decimal)i.get('expr0');
            
        }*/                
        
        if(totalAmount == null) totalAmount = 0;
        
        return totalAmount;        
    }
    
    /**
    * Method to obtain the total amount of all Open Items of an Account and the Due Date is set before today and the Reference is Container
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return Double
    */
    
    @AuraEnabled
    public static Double outOfDateCreditsContainer(String recordId){
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String empties = !openItems_identifier.isEmpty() ? openItems_identifier[0].Empties__c : null;
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        if(empties != null){
            account_open_items = [Select Id, 
                                        ONTAP__Amount__c, 
                                        ISSM_CreditDebit_F__c, 
                                        RecordTypeId,  
                                        ONTAP__SAPCustomerId__c,
                                        ISSM_Debit_Credit__c,
                                        ONTAP__Account__c
                                  From ONTAP__OpenItem__c
                                  Where 
                                        ONTAP__Account__c =:recordId AND 
                                        ONTAP__DueDate__c <: System.today()	AND
                                        RecordTypeId =: rt_open_items AND 
                                        V360_Reference__c =: empties
                                 ];
        }
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        /*
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) FROM ONTAP__OpenItem__c WHERE (ONTAP__Account__c =:recordId AND ONTAP__DueDate__c <: System.today()) AND V360_Reference__c = 'E'];
        
        for(AggregateResult i : Amou){
            totalAmount = (Decimal)i.get('expr0');
            
        }*/
        
        if(totalAmount == null) totalAmount = 0;
        
        return totalAmount;
        
    }
    
    /**
    * Method to obtain the total amount of the Open Items of an Account where the Due Date is set today  or days after today
    * Created By: heron.zurita@accenture.com  
    * @param Double recordId
    * @return Double
    */
    
    @AuraEnabled
    public static Double dateCredits(String recordId){
                
        Double totalAmount = 0.0;
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
		
		List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >=: System.today() AND
                              		RecordTypeId =: rt_open_items
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        /*
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
                                  FROM ONTAP__OpenItem__c 
                                  WHERE ONTAP__Account__c =:recordId AND 
                                  ONTAP__DueDate__c >=: System.today()];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else
        {
            totalAmount = 0.0;
        }*/
        if(totalAmount == null){totalAmount = 0.0;}
        
        return totalAmount;     
    }
    
        /**
    * Method to obtain the amount of today Credit Orders
    * Created By: heron.zurita@accenture.com  
    * @param String recordId
    * @return Double
    */
    public static Double todayCreditOrders(String recordId){
        
        Double creditLimit = 0.0;
        Double amount = 0.0;
        Double sumaAmount = 0.0;
        Double totalDisponible = 0.0;
        Double orderAmount= 0.0;
        Double orderAmount_sum= 0.0;
        Double orderAmount_min= 0.0;  
        
        //get account country
        List<Account> lim = new List<Account>([SELECT ISSM_CreditLimit__c, ONTAP__ExternalKey__c FROM Account WHERE Id =:recordId]);
        String account_country = lim[0].ONTAP__ExternalKey__c.left(2);        
        
        //get recordtypes of orders that affect credit
        List<String> orders_mdt_sum = new List<String>();
        List<String> orders_mdt_min = new List<String>();
        List<String> orders_rt_sum = new List<String>();
        List<String> orders_rt_min = new List<String>();
        
        List<Orders_Bh__mdt> orders_mtd = new List<Orders_Bh__mdt>();
        orders_mtd = [Select DeveloperName, Min__c, Sum__c, Country__c  
                      From Orders_Bh__mdt 
                      Where Country__c =: account_country                       		
                      Limit 1];
        orders_mdt_sum = orders_mtd[0].Sum__c.split(',');
        orders_mdt_min = orders_mtd[0].Min__c.split(',');
        
        for(String aux1 : orders_mdt_sum){
            orders_rt_sum.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux1).getRecordTypeId());
        }
        
        for(String aux2 : orders_mdt_min){
            orders_rt_min.add(Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get(aux2).getRecordTypeId());
        }
        DateTime hoy = System.today();
        DateTime dateaux = hoy.addDays(1);
        
        AggregateResult[] itemAmount_sum = [SELECT SUM(ONTAP__Amount_Total__c) 
                                            FROM ONTAP__Order__c 
                                            WHERE 
                                            ONTAP__OrderAccount__c =:recordId AND
                                            ISSM_PaymentMethod__c = 'Credit' AND 
                                            RecordTypeId in: orders_rt_sum AND 
                                            CreatedDate >=: hoy];
                                            //((CreatedDate >=: hoy AND CreatedDate <=: dateaux) OR (ONTAP__DeliveryDate__c >=: hoy AND ONTAP__DeliveryDate__c <=: dateaux))];                                                           
        
        if(itemAmount_sum != null){
            for(AggregateResult i : itemAmount_sum){
                orderAmount_sum = (Decimal)i.get('expr0');
                if(orderAmount_sum == null){ 
                    orderAmount_sum=0.0;
                }  
            }
        }
        else{
            orderAmount_sum=0.0;
        }
        
        AggregateResult[] itemAmount_min = [SELECT SUM(ONTAP__Amount_Total__c) 
                                            FROM ONTAP__Order__c 
                                            WHERE 
                                            ONTAP__OrderAccount__c =:recordId AND
                                            ISSM_PaymentMethod__c = 'Credit' AND 
                                            RecordTypeId in: orders_rt_min AND
                                            CreatedDate >=: hoy];
                                            //((CreatedDate >=: hoy AND CreatedDate <=: dateaux) OR (ONTAP__DeliveryDate__c >=: hoy AND ONTAP__DeliveryDate__c <=: dateaux))];                  
        if(itemAmount_min != null){
            for(AggregateResult i : itemAmount_min){
                orderAmount_min = (Decimal)i.get('expr0');
                if(orderAmount_min == null){ 
                    orderAmount_min=0.0;
                }  
            }
        }
        else{
            orderAmount_min=0.0;
        }
        
        orderAmount = orderAmount_sum - orderAmount_min;
        
        return orderAmount;
    }
    
    
    /**
    * Method to obtain the minimum payment of an order based on the scenario which recibes the balances of the account and the payment form to calculate the minimum payment
    * Created By: heron.zurita@accenture.com  
    * @param String recordId, String payForm, Double totalOrder
    * @return Double
    */
    
    @AuraEnabled
    public static Double total(String recordId, String payForm, Double totalOrder){
        
        Double outAmount = 0.0;
        Double inAmount = 0.0;
        Double actualOrder = 0.0;
        Double limitExc = 0.0;
        Double totalOr = totalOrder;
        Double todyCreditOrders = 0.0;
        
        Double sumTotal = 0.0;
        
        String orderType = '';
        String modernChannel = '';
        Boolean isModernChannel = false;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >=: System.today() AND
                              		RecordTypeId =: rt_open_items
                             ];
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    inAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    inAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        if(inAmount == null) inAmount=0.0;
        
        List<ONTAP__OpenItem__c> account_open_items_out = new List<ONTAP__OpenItem__c>();
        
        account_open_items_out = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c <: System.today() AND
                              		RecordTypeId =: rt_open_items
                             ];
         
        if(account_open_items_out.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items_out){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    outAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    outAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        if(outAmount == null) outAmount=0.0;
        
        /*
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) 
									FROM ONTAP__OpenItem__c 
									WHERE ONTAP__Account__c =:recordId AND ONTAP__DueDate__c >=: System.today()];
        
        for(AggregateResult i : Amou){
            inAmount = (Decimal)i.get('expr0');            
            if(inAmount == null){inAmount=0.0;} 
        }
        
        AggregateResult[] AmountIn = [SELECT SUM(ONTAP__Amount__c) 
										FROM ONTAP__OpenItem__c 
										WHERE ONTAP__Account__c =:recordId AND ONTAP__DueDate__c <: System.today()];
        
        for(AggregateResult i : AmountIn){
            outAmount = (Decimal)i.get('expr0');
            if(outAmount == null){outAmount=0.0;} 
        }*/
        
        //Min payment scenarios
        Double credLimit = 0.0;
        List<Account> account = new List<Account>();
        account = [SELECT ISSM_CreditLimit__c, ONTAP__Credit_Condition__c, ONCALL__KATR6__c, Modern_channel__c FROM Account WHERE Id =: recordId Limit 1];
        if(account[0].ISSM_CreditLimit__c != null) {
            credLimit = account[0].ISSM_CreditLimit__c; // limite de credito
            isModernChannel = account[0].Modern_channel__c;
        } 
        
        if(!account.isEmpty()) {
            isModernChannel = account[0].Modern_channel__c;
        } 
        
        orderType = payForm; // Forma de pago 
        System.debug('payForm'+payForm);
        actualOrder = double.valueof(totalOr); //Saldo Total
        outAmount = outAmount < 0 ? 0 : outAmount; // PA Vencidas
        System.debug('PA Vencidas'+ outAmount);
        inAmount = inAmount < 0 ? 0 : inAmount; //PA Corriente
        System.debug('PA Corrientes'+inAmount);
        actualOrder = actualOrder < 0 ? 0 : actualOrder; //Valor de la orden
        System.debug('Saldo Total'+actualOrder);
        todyCreditOrders = todayCreditOrders(recordId);
        limitExc = credLimit - (outAmount + inAmount + actualOrder + todyCreditOrders); //Excedente
        if(limitExc > 0.0 ) limitExc = 0.0;
        else limitExc = limitExc * -1;
        System.debug('Excedente'+limitExc);
        System.debug('account[0]'+account[0]);
        //if(modernChannel != GlobalStrings.CANAL_MODERNO){ //Sub canal, si es igual Escenario 2
        if(!isModernChannel){ //Sub canal, si es igual Escenario 2    
            if(account[0].ONTAP__Credit_Condition__c == 'Credit'){ //Si forma de pago es credito esceneario 3-9 else escenario 1
            //if(account[0].ONTAP__Credit_Condition__c == GlobalStrings.ACCOUNT_CREDIT_CONDITION_CREDIT){ //Si forma de pago es credito esceneario 3-9 else escenario 1
                if(account[0].ONTAP__Credit_Condition__c == payForm){ // Si codicion de credito y forma de pago igual a credito escenario 3,4,5,7,9 else escenario 6 y 8
                    if(outAmount > 0.0){ //Si hay Pa vencidas escenarios 3 y 7 else 4,5,9
                        if(limitExc == 0.0){ 
                            //Escenario 3 OK
                            System.debug('PM 3'); 
                            sumTotal = outAmount;
                        }else if(limitExc >= outAmount){
                            //Escenario 7 OK
                            System.debug('PM 7');
                            sumTotal = Math.abs(limitExc);
                        }else if(limitExc < outAmount){
                            sumTotal = Math.abs(outAmount);
                        }
                        //Falta caso donde las PA vencidas son mayor que el excedente y es credito
                    }else{                        
                        if(limitExc == 0.0){
                            //Escenario 4 OK
                            System.debug('PM 4');
                            sumTotal = 0;
                        }
                        else if(limitExc > 0.0){
                            //Escenario 5 OK
                            System.debug('PM 5');
                            sumTotal = Math.abs(limitExc);
                        }
                        else if(limitExc > outAmount){
                            //Escenario 9 es similar a escenario 5
                            System.debug('PM 9');
                            sumTotal = Math.abs(limitExc);
                        }
                    }
                }else{
                    if(outAmount>0){
                        //Escenario 6 OK
						System.debug('PM 6');
                        sumTotal = actualOrder + outAmount;
                    } else{
                        //escenario 8 OK
						System.debug('PM 8');
                        sumTotal = actualOrder;
                    }
                }
            }else{
                //Escenario 1 OK
                System.debug('PM 1');
                sumTotal = actualOrder;
            }            
        }else{
            //Escenario 2 OK
            System.debug('2');
            sumTotal = 0;
        }
        if(sumTotal == null){sumTotal = 0.0;}
        
        return sumTotal;        
    }
    
    /**
    * Method to obtain the total amount of the Open Items of an Account where the Due Date is set today  or days after today and the Reference es Liquid
    * Created By: heron.zurita@accenture.com  
    * @param Double recordId
    * @return Double
    */
    
    @AuraEnabled
    public static Double onTimeDateCreditsLiquid(String recordId){
        
        /*
        Double totalAmount = 0.0;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) FROM ONTAP__OpenItem__c WHERE (ONTAP__Account__c =:recordId AND ONTAP__DueDate__c >: System.today()) AND V360_Reference__c = 'L'];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else
        {
            totalAmount = 0.0;
        }
        if(totalAmount == null){totalAmount = 0.0;}*/
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String liquid = !openItems_identifier.isEmpty() ? openItems_identifier[0].Liquid__c : null;
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        if(liquid != null){
        	account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >: System.today()	AND
                              		RecordTypeId =: rt_open_items AND 
                              		V360_Reference__c =: liquid
                             ];
        }
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        return totalAmount;
        
    }
    
    @AuraEnabled
    public static Double onTimeDateCreditsContainer(String recordId){
        
        /*
        Double totalAmount = 0.0;
        
        AggregateResult[] Amou = [SELECT SUM(ONTAP__Amount__c) FROM ONTAP__OpenItem__c WHERE (ONTAP__Account__c =:recordId AND ONTAP__DueDate__c >: System.today()) AND V360_Reference__c = 'E'];
        if(Amou != null){
            for(AggregateResult i : Amou){
                totalAmount = (Decimal)i.get('expr0');
                
            }
        }
        else{
            totalAmount = 0.0;                
        }
        if(totalAmount == null){totalAmount = 0.0;}*/
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();        
        Double totalAmount = 0.0;
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        openItems_identifier = [Select DeveloperName, Empties__c, Liquid__c From Open_Items_Identifier__mdt Where DeveloperName =: GlobalStrings.OPEN_ITEMS_IDENTIFIER Limit 1];        
        if(test.isRunningTest()){
            openItems_identifier.add(new Open_Items_Identifier__mdt(DeveloperName='Identifier', Empties__c='E', Liquid__c='L'));
        }
        String empties = !openItems_identifier.isEmpty() ? openItems_identifier[0].Empties__c : null;
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        
        if(empties != null){
        	account_open_items = [Select Id, 
                              		ONTAP__Amount__c, 
                              		ISSM_CreditDebit_F__c, 
                              		RecordTypeId,  
                              		ONTAP__SAPCustomerId__c,
                             		ISSM_Debit_Credit__c,
                              		ONTAP__Account__c
                              From ONTAP__OpenItem__c
                              Where 
                              		ONTAP__Account__c =:recordId AND 
                              		ONTAP__DueDate__c >: System.today()	AND
                              		RecordTypeId =: rt_open_items AND 
                              		V360_Reference__c =: empties
                             ];
        }
         
        if(account_open_items.size() > 0){
            for(ONTAP__OpenItem__c aux : account_open_items){
                if(aux.ISSM_Debit_Credit__c == 'S'){
                    totalAmount += aux.ONTAP__Amount__c;
                }else if(aux.ISSM_Debit_Credit__c == 'H'){
                    totalAmount -= aux.ONTAP__Amount__c;
                }
            }
        }
        
        return totalAmount;
        
    }
    
    
    
}