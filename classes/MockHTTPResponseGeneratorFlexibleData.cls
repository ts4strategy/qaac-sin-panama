/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: MockHTTPResponseGeneratorFlexibleData.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 22/01/2019        carlos Leal          Creation of methods.
*/
@isTest
global class MockHTTPResponseGeneratorFlexibleData implements HttpCalloutMock {
    
    /**
    * Test method for create mock for felxible data call out
    * Created By: c.leal.beltran@accenture.com@accenture.com
    * @param void
    * @return void
    */
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and
        // return response.
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(200);          
        //res.setBody('{"deals":[{"pronr":"248856","proportional":false,"optional":false,"validfrom":"2018-12-17T00:00:00.000Z","validto":"2018-12-31T00:00:00.000Z","description":"2% Descuento por 9891"},{"pronr":"248873","proportional":false,"optional":true,"validfrom":"2019-01-13T00:00:00.000Z","validto":"2019-01-13T00:00:00.000Z","description":"Prueba para Deals 3"},{"pronr":"248854","proportional":false,"optional":true,"validfrom":"2018-12-17T00:00:00.000Z","validto":"2019-12-31T00:00:00.000Z","description":"9888 CADA 15 UND 1 UNIDAD GRATIS ADICIONAL"}]}');      
        //Added drs@avx
        res.setBody('[{"promotionNumber":"249032","title":"PRUEBA MALTA DC","validfrom":"2019-02-13T00:00:00.000Z","validto":"9999-12-31T00:00:00.000Z"}]');
        System.debug('Respuesta'+res);
        return res;
    }
    
    
}