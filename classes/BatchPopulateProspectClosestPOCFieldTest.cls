@istest
public class BatchPopulateProspectClosestPOCFieldTest {
    @istest public static void testrun(){
        list<account> laccounts = new list<account>();
        test.startTest();
        id rectypeProspDO = BatchPopulateProspectClosestPOCField.getRecordtype('Prospect_DO') ;
        id rectypeAccDO = BatchPopulateProspectClosestPOCField.getRecordtype('Account_DO');
        system.assertequals(true , BatchPopulateProspectClosestPOCField.getRecordtype('Testing_Account_DO_test') ==null );
        laccounts.add(new account(name='Account_DO' , ONTAP__Longitude__c ='-69.95276',ONTAP__Latitude__c = '18.4732',recordtypeid = rectypeAccDO ));
        laccounts.add(new account(name='Account_DO2' , ONTAP__Longitude__c ='-69.3343',ONTAP__Latitude__c = '18.3222',recordtypeid = rectypeAccDO));
        laccounts.add(new account(name='Prospect_DO' , ONTAP__Longitude__c ='-69.3333',ONTAP__Latitude__c = '18.3222',recordtypeid = rectypeProspDO));
    	insert laccounts;
        database.executeBatch(new   BatchPopulateProspectClosestPOCField());
        system.assertequals(true , BatchPopulateProspectClosestPOCField.Schedule()!=null);
        system.assertequals(true , BatchPopulateProspectClosestPOCField.Schedule()==null);
        test.stopTest();
		system.assertequals(true , [select id from account where Closest_POC_Code__c =null and recordtype.name =:rectypeProspDO ].size()==0); 
    }
}