/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_AccountAsset.apxt
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                  				 Description
* 20/12/2018     g.martinez.cabral@accenture.com        Creation of Trigger which executes after delete, after insert, after update,
														before delete, before insert, before update on the object ONTAP__Account_Asset__c
*/


/**
* Constructor which call the V360_AccountAssetTriggerHandler in the following cases: after delete, after insert, after update, before delete, before insert, before update 
* @author: g.martinez.cabral@accenture.com
* @param void
* @return Void
*/
trigger V360_AccountAsset on ONTAP__Account_Asset__c (after delete, after insert, after update, before delete, before insert, before update) {
 new V360_AccountAssetTriggerHandler().run();
}