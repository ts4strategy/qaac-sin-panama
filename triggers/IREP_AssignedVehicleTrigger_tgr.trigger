trigger IREP_AssignedVehicleTrigger_tgr on ONTAP__AssignedVehicle__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete 
) {

	if (Trigger.isBefore) {
    	//call your handler.before method
    
	} else if (Trigger.isAfter) {
    	if(Trigger.isInsert){
    		IREP_AssignedVehicleTrigger_thr.updateMileageOnTour(trigger.new);
    	}
    	else if(Trigger.isUpdate){
    		list<ONTAP__AssignedVehicle__c> lstAV = new list<ONTAP__AssignedVehicle__c>();
    		for(Integer i=0; i<trigger.new.size(); i++){
    			if(
    				trigger.new[i].IREP_BeginMile__c != trigger.old[i].IREP_BeginMile__c || trigger.new[i].IREP_EndMile__c != trigger.old[i].IREP_EndMile__c
    			)
    				lstAV.add(trigger.new[i]);
    		}
    		if(!lstAV.isEmpty())
    			IREP_AssignedVehicleTrigger_thr.updateMileageOnTour(lstAV);	
    	}
    	else if(Trigger.isDelete){
    		IREP_AssignedVehicleTrigger_thr.updateMileageOnTour(trigger.old);
    	}
    	else{}
    
	}
}