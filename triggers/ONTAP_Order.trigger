/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: ONTAP_Order.apxt
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                                  Description
* 20/12/2018     g.martinez.cabral@accenture.com        Creation of Trigger
*/


/**
* Constructor which call the ONTAP_OrderTriggerHandler in the following cases: after insert,after update,before insert,  before update 
* @author: g.martinez.cabral@accenture.com
* @param void
* @return Void
*/

trigger ONTAP_Order on ONTAP__Order__c ( after insert,after update,before insert,  before update ) {
    New ONTAP_OrderTriggerHandler().run();
}