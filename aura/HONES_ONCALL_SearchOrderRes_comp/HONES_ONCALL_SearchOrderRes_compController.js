({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        //helper.getpicklist(component,recordId);
    },
    
    selectRecord : function(component, event, helper){      
        // get the selected record from list selectRecord         
        var getSelectRecord = component.get("v.oRecord");
        
        getSelectRecord.quantity = component.get("v.quantity");
        if (getSelectRecord.quantity == null || !Number.isInteger(getSelectRecord.quantity) || getSelectRecord.quantity<1 ){            
            alert($A.get("$Label.c.You_must_enter_an_amount"));            
        }else{  
            component.set("v.isLoading", true);
            component.get("v.oRecord.lista").forEach(function(item){
                if (item == component.get("v.selectedValue")){
                    getSelectRecord.productType=component.get("v.selectedValue"); 
                    getSelectRecord.selectedByUser=component.get("v.clickedbyUser");
                    console.log("getSelectRecord.selectedByUser-"+getSelectRecord.selectedByUser);                    
                    console.log("etse_-"+component.get("v.selectedValue"));
                    console.log("the product --"+getSelectRecord.productType);                        
                    // call the event   
                    var compEvent = component.getEvent("oSelectedRecordEvent");
                    console.log("inevent-"+JSON.stringify(getSelectRecord));
                    compEvent.setParams({"recordByEvent" : getSelectRecord});  
                    // fire the event  
                    compEvent.fire();
                    component.find("inputbar").set("v.value", "");
                    
                    var searchEvent = component.getEvent("oSearchRecordEvent");
                    searchEvent.setParams({"cleanAndFocus" : true});  
                    // fire the event  
                    searchEvent.fire();
                    component.set("v.isLoading", false);
                }
            });
            
        }                        
    },
    
    clear :function(component,event,heplper){        
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField");  
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        component.set("v.selectedRecord", {} );
        component.set("v.search", "slds-input__icon slds-show");                
    }
    
})