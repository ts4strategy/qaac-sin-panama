({
    doInit: function (component, event, helper) {
        console.log('search');
        component.set("v.isLoading", true);
        var recordId = component.get("v.idcall");
        //$A.get("$Label.c.CASH")
        //$A.get("$Label.c.CREDIT")
        var creditCashArray = ['Cash', 'Credit'];
        component.set("v.creditCash", creditCashArray)
        helper.gettheID(component,recordId);
        helper.gettheDate(component,recordId);
        
        
        var recordId2 = component.get("v.recordId");
        helper.getDataAccountDis(component,recordId2);
        helper.getpaymet(component,recordId2);
        helper.getInitVariablesForSearch(component,recordId2);
        //helper.searchHelper(component, event, getInputkeyWord);
        
        
    },
    
    onChangeChildCheckbox : function(component, event, helper) {
        
        var check = event.getSource().get('v.value');
        var creditCash = component.get("v.creditCash");
        
        console.log("checkbox" + check);
        //Hide button Generate Order if change value;
        component.set("v.boton", false);
        console.log("boton", component.get("v.boton"));
        if(check == true){
            var generalList=component.get("v.lookName");
            generalList.forEach(function(element) {
                element.pay=$A.get("$Label.c.OnCall_Cash");
                
            });
            component.set("v.lookName",generalList);
            var cash = component.get("v.cash");
            component.set("v.cash", true);
            //component.set("v.creditCashValues", $A.get("$Label.c.OnCall_Cash"));
            component.set("v.creditCashValues", creditCash[0]);
            console.log(JSON.stringify(generalList))
        }
        else{
            var generalList2=component.get("v.lookName");
            generalList2.forEach(function(element) {
                element.pay=$A.get("$Label.c.OnCall_Credit");
                
            });
            component.set("v.cash", false);
            //component.set("v.creditCashValues", $A.get("$Label.c.OnCall_Credit"));
            component.set("v.creditCashValues", creditCash[1]);
            component.set("v.lookName",generalList2);
            console.log('LookName: ' + JSON.stringify(generalList2));
        } 
        
    },
    
    onCheck: function (cmp, event, helper) {
        var checkCmp = cmp.find("checkbox");
        console.log(checkCmp);
    },
    
    handleContactSearchComplete : function(component, event, helper) {
        var opt = [];
        opt.push(event.getParam("pickval"));  
        component.set("v.selected", opt);
        console.log("1"+event.getParam("pickval"));                       
    }, 
    
    pickNameChanged : function(component, event, helper) {
        var newValue = event.getParam("value");
        var oldValue = event.getParam("oldValue");
        component.set("v.newvalue",newValue);
        alert("Expense name changed from '" + oldValue + "' to '" + newValue + "'");
    },
    
    focus: function (component, event, helper) {
        var forOpen = component.find("lookup");
        forOpen.focus();
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 10 Records order by createdDate DESC  
        var getInputkeyWord = '';
        //helper.searchHelper(component, event, getInputkeyWord);
    },
    
    onfocus: function (component, event, helper) {
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 10 Records order by createdDate DESC  
        var getInputkeyWord = '';
        //helper.searchHelper(component, event, getInputkeyWord);
    },
    
    keyPressController: function (component, event, helper) {
        // get the search Input keyword   
        var typingTimer = component.get("v.typingTimer");    
        //console.log('typingTimer', typingTimer);
        if(typingTimer){clearTimeout(typingTimer);}
        var getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if (getInputkeyWord != null) {
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            //helper.searchHelper(component, event, getInputkeyWord);
  			typingTimer = setTimeout(
                $A.getCallback(function() {
        			//helper.searchLocalHelper(component, event, getInputkeyWord)
                    helper.searchHelper(component, event, getInputkeyWord);
                }), 300);
            component.set("v.typingTimer", typingTimer);
            
            
        } else {
            component.set("v.listOfSearchRecords", null);
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
        
        
    },
    
    // function for clear the Record Selaction 
    clear: function (component, event, helper) {
        
        component.set("v.SearchKeyWord", null);
        component.set("v.listOfSearchRecords", null);
        component.set("v.selectedRecord", {});
        component.set("v.search", "slds-input__icon slds-show");
        
    }, 
    
    toggleOptionalTab: function (cmp) {
        cmp.set('v.isPFNVisible', !cmp.get('v.isPFNVisible'));
    },
    
    toggleOptionalTab2: function (cmp) {

    },
    
    handlePromoEvent: function (component, event, helper) {
        var selectedPromo = event.getParam("promoName");
        //var description = event.getParam("description");
        console.log("searchorders--"+selectedPromo);
        component.set('v.selectedPromo', selectedPromo);
        //component.set('v.selectedDescription', description);
        
    },
        
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent: function (component, event, helper) {
        // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");        
        var element = '';
        var validation = true;
        var newElem = true;
        var listSelectedItems  = component.get("v.lookName");   
        console.log("label ", $A.get("$Label.c.ONCALL_ORDER_SEND_EMPTIES"));
        if(listSelectedItems.length>0){ 
            var element = component.get("v.lookName");
            for(var i= 0; i< element.length; i++){
            //component.get("v.lookName").forEach(function(element) { 
                console.log("element from event: ", element[i]);
                console.log('selectedAccountGetFromEvent', selectedAccountGetFromEvent);
                if(element[i].productCode ==selectedAccountGetFromEvent.productCode && selectedAccountGetFromEvent.productMetadataType == null && selectedAccountGetFromEvent.pfntag == null && selectedAccountGetFromEvent.pbetag == null)
                {
                    newElem=false;                   
                    alert($A.get("$Label.c.You_have_already_added_this_product"));                        
                }else if((selectedAccountGetFromEvent.productType == $A.get("$Label.c.ONCALL_ORDER_EMPTIES") || selectedAccountGetFromEvent.productType.includes($A.get("$Label.c.ONCALL_ORDER_SEND_EMPTIES"))) && element[i].productType == $A.get("$Label.c.ONCALL_ORDER_ADD_EMPTIES")){                
                    newElem=false;   
                    //Solo es posible pactar envase en producto terminado
                    alert($A.get("$Label.c.ONCALL_MsgFinishProduct"));          
                    break;
                }else if((element[i].productType == $A.get("$Label.c.ONCALL_ORDER_EMPTIES") || element[i].productType.includes($A.get("$Label.c.ONCALL_ORDER_SEND_EMPTIES"))) && selectedAccountGetFromEvent.productType == $A.get("$Label.c.ONCALL_ORDER_ADD_EMPTIES")){
                    newElem=false;   
                    //Solo es posible agregar producto terminado
                    alert($A.get("$Label.c.ONCALL_MsgOnlyFinishProduct"));   
                   	break;
                }
            }
            //);  
        }
        else{
            
            listSelectedItems.push(selectedAccountGetFromEvent);
            component.set("v.lookName",listSelectedItems );  
            component.set("v.showtabOI",true);
            newElem=false;
            component.set("v.showtabOI",true);          
            
        }   
        console.log("newElem", newElem);
        if(newElem== true  && listSelectedItems.length>0){
            
            listSelectedItems.push(selectedAccountGetFromEvent);
            component.set("v.lookName",listSelectedItems );  	
            component.set("v.showtabOI",true);            
            
        }
        
    },
    
    //component.set("v.selectedRecord", selectedAccountGetFromEvent);
    //component.set("v.showtabOI", true);       
            
    selectRecord: function (component, event, helper) {
        // get the selected record from list  
        var getSelectRecord = component.get("v.oRecord");
        // call the event   
        var compEvent = component.getEvent("oSelectedRecordEvent");
        // set the Selected sObject Record to the event attribute.  
        compEvent.setParams({
            "recordByEvent": getSelectRecord
        });
        // fire the event  
        compEvent.fire();
    },
        
    inlineEditName: function (component, event, helper) {
        // show the name edit field popup 
        component.set("v.nameEditMode", true);
        // after the 100 millisecond set focus to input field   
        setTimeout(function () {
            component.find("inputId");
        }, 100);                
    },
    
    inlineEditRating: function (component, event, helper) {
        // show the rating edit field popup 
        component.set("v.ratingEditMode", true);
        // after set ratingEditMode true, set picklist options to picklist field 
        component.find("accRating").set("v.options", component.get("v.ratingPicklistOpts"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function () {
            component.find("accRating");
        }, 100);
    },
    
    onNameChange: function (component, event, helper) {
        // if edit field value changed and field not equal to blank,
        // then show save and cancel button by set attribute to true
        if (String(event.getSource().get("v.value")).trim() != '') {
            component.set("v.showSaveCancelBtn", true);
        }
    },
    
    onRatingChange: function (component, event, helper) {
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn", true);
    },
    
    closeNameBox: function (component, event, helper) {
        // on focus out, close the input section by setting the 'nameEditMode' att. as false   
        component.set("v.nameEditMode", false);
        // check if change/update Name field is blank, then add error class to column -
        // by setting the 'showErrorClass' att. as True , else remove error class by setting it False   
        if (String(event.getSource().get("v.value")).trim() == '') {
            component.set("v.showErrorClass", true);
        } else {
            component.set("v.showErrorClass", false);
        }
    },
    
    closeRatingBox: function (component, event, helper) {
        // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.ratingEditMode", false);
    },
    
    openModel: function(component, event, helper) {
        // for Display Model,set the "Opisen" attribute to "true"
        console.log('promosEnviar2 ', component.get("v.promosEnviar"));
        console.log('promosEnviar2 ', component.get("v.promosEnviar2"));
        var disponible = component.get("v.creditLimitInit");
        var lookName = component.get("v.lookName");
        var call = component.get("v.Idcuenta");
        var sum = 0;
        var creditCash = component.get("v.creditCash");
        
        console.log("v.Idcuenta", component.get("v.Idcuenta"));
        
        component.set("v.isOpen", true);
        
        lookName.forEach(function(element) {
            sum = sum + element.total;
        });
        
        console.log("call: ", call);
        console.log("disponible: ", disponible);
        console.log("Este es el total: ", sum);
        console.log("Credit Condition:", lookName[0]);
        console.log("Credit Condition:", lookName[0].pay);
        console.log("Label Credit", $A.get("$Label.c.OnCall_Credit"));
        console.log("creditCash", creditCash[1]);
        
        if((creditCash[1]).toLowerCase() == (lookName[0].pay).toLowerCase() || ($A.get("$Label.c.OnCall_Credit")).toLowerCase() == (lookName[0].pay).toLowerCase()){
            var totalCredito = lookName[0].orderTotal ? disponible - lookName[0].orderTotal : disponible;
            console.log("totalCredito", totalCredito);
          	component.set("v.creditLimit", totalCredito);            
        }else{
            component.set("v.creditLimit", disponible);
        }
        
        if(sum > disponible && ((creditCash[1]).toLowerCase() == (lookName[0].pay).toLowerCase() || ($A.get("$Label.c.OnCall_Credit")).toLowerCase() == (lookName[0].pay).toLowerCase()) ){
                component.set("v.ExcessOfLimit", true);
            }
            else{
                component.set("v.ExcessOfLimit", false);
            }
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    likenClose: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('Se ha generado la orden:)');
        component.set("v.isOpen", false);
    },
        
    deleteProd: function (component, event, helper) {
        
        var foralln = component.get("v.lookName");
        var tempList = [];            
        var i = 0;            
        var index = event.target.dataset.index;           
        component.set("v.boton", false);
        
        for (i = 0; i < foralln.length; i++) {
            if(i!=index) {
                tempList.push(foralln[i]);  
            }
        }	
        
        if( tempList.length==0){
            component.set("v.showtabOI", false);
            //component.set("v.showButtonGenerateOrder", false);
            
        }
        component.set("v.lookName",tempList);
    },
    
    deleteProm: function (component, event, helper) {
        
        var foralln = component.get("v.promosEnviar2");
        var tempList = [];            
        var i = 0;            
        var index = event.target.dataset.index;        
        
        for (i = 0; i < foralln.length; i++) {
            if(i!=index) {
                tempList.push(foralln[i]);  
            }
        }	
        if( tempList.length==0){
            /*var compEvent = component.getEvent("returnCheckBoxValue");
            compEvent.setParams({"checked" : false});  
            compEvent.fire();*/
            component.set("v.tablePromotion", false);
        }
        console.log("tempList",tempList);
        component.set("v.promosEnviar2",tempList);
    },
    
    openModel2: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },
    
    closeModel2: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen2", false);
    },
    
    likenClose2: function(component, event, helper) {
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('Se ha generado la orden:)');
        component.set("v.isOpen2", false);
    },   
    
    closeModel11: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenModel1", false);
        //component.set("v.isOpen", true);
        component.set("v.boton", false);
    },
    closeModel22: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenModel2", false);
        //component.set("v.isOpen", true);
        component.set("v.boton", false);
    },
    
    openModel1: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenModel1", true);
        component.set("v.isOpen", false);
    },
    
    openModel2: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenModel2", true);
        component.set("v.isOpen", false);
    },
        
    sendOrder: function(component, event, helper) {
        
        component.set("v.isOpen", false);
        component.set("v.boton", false);
        var recordId = component.get("v.recordId");
        helper.getResponse(component,recordId,event);
        
    },
    
    sendOrderandUpdate: function(component, event, helper) {        
        component.set("v.isOpen", false);
        var recordId = component.get("v.recordId");
        helper.getResponseupdate(component,recordId,event);                
    },
    
    changeButton: function(component, event, helper,document) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.hiddenButtonCalculate", false);
        component.set("v.hiddenButtonGenerateOrder", true);
        //component.set("v.showButtonGenerateOrder", true);
        //component.set("v.promosEnviar2", null);
        var recordId = component.get("v.recordId");
        helper.getprice(component,recordId);
        var promos = component.get("v.promosEnviar2");
        console.log("promos chengeButton ", promos);
        var lookName = component.get("v.lookName");        
    },
    
    handleSearchEvent: function(component, event, helper) {
        var value = event.getParam("cleanAndFocus");
        component.set("v.boton", false);
        //component.set("v.isLoading", true);
        if(value == true){
            var searchId = component.find("lookup").focus();
            component.set("v.SearchKeyWord", "");  
            var getInputkeyWord = '';
            helper.searchHelper(component, event, getInputkeyWord);
            //helper.searchLocalHelper(component, event, getInputkeyWord)
        }        
        //console.log("value event", value);
    },
    
    handleClickButtonCloseModal: function(component, event, helper) {
        component.set("v.ExcessOfLimit", false); 
    
    },
    disableGenerateButton: function(component, event, helper) {
        	component.set("v.boton",false);
    }
    
})