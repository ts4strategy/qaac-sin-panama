({
    searchHelper: function (component, event,getInputkeyWord) {
        // call the apex class method 
        console.log('Search F component');
        var wpVaraibles = component.get("v.initVariables");
        //console.log('wpVaraibles' , wpVaraibles);
        var recordId = component.get("v.recordId");
        var action = component.get("c.fetchLookUpValuesContact");
        
        var initList = component.get("v.listOfSearchRecordsInit");
        console.log('initList ' + initList);
        
        //if(initList && initList.length == 0) {   
        // set param to method  
            action.setParams({
                'recordId' : recordId,
                'searchKeyWord': getInputkeyWord,
                'stringWpVariables': JSON.stringify(wpVaraibles),
                'ObjectName': component.get("v.objectAPIName")
            });
            // set a callBack         
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    console.log('storeResponsehelper', storeResponse);                
                    for(var i = 0; i < storeResponse.length; i++){
                        storeResponse[i].quantity = "";
                        storeResponse[i].selectedValue = storeResponse[i].lista && storeResponse[i].lista[0] ? storeResponse[i].lista[0] : '';
                    }
                    component.set("v.listOfSearchRecordsInit", storeResponse);
                    component.set("v.listOfSearchRecords", storeResponse);
                    console.log('storeResponseSearchHelper', storeResponse);
                    component.set("v.isLoading", false);
                }
                
            });
            // enqueue the Action  
            $A.enqueueAction(action);     
        //}
    },
    
    searchLocalHelper: function (component, event, getInputkeyWord) {
        // call the apex class method 
        console.log('getInputkeyWord', getInputkeyWord);
        var listInit = component.get("v.listOfSearchRecordsInit");
        //console.log('listInit' , listInit);
        var expresion = new RegExp(getInputkeyWord, 'gi');
        var listFilter = [];
        listFilter = listInit.filter(function filterByCodeAndMat(prod) {
            //console.log('prod', prod);
            if ('productCode' in prod && 'materialProduct' in prod && (prod.productCode.match(expresion) || prod.materialProduct.match(expresion) ) ) {
                return true;
            } else {
                return false;
            }
        });
        component.set("v.listOfSearchRecords", listFilter);
        /*var myVar = setInterval(
            $A.getCallback(function() {
                if(listFilter.length > 0 ){
                    component.set("v.isLoading", false);
                }
            })
            ,500);
        clearInterval(myVar);*/
        
        //console.log('listFilter', listFilter);                
    },
    
    getInitVariablesForSearch: function(component,recordId2) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.initFetchLookUp");
        // set param to method  
        action.setParams({
            'recordId' : recordId
        });
        // set a callBack    
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('storeResponse',storeResponse);
                component.set("v.initVariables", storeResponse);
                this.searchHelper(component, event, '');
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },

    loadOptions: function (component, event, helper) {
        var options = [{
            label: '        ',
            value: '        '
        },
                       {
                           label: 'obsequio',
                           value: 'obsequio'
                       },
                       {
                           label: 'siembra',
                           value: 'siembra'
                       },
                       
                       
                      ];
                       component.set("v.statusOptions", options);
                       },
                       // fetch picklist values dynamic from apex controller 
                       fetchPickListVal: function (component, fieldName, picklistOptsAttributeName) {
                       var action = component.get("c.getselectOptions");
                       action.setParams({
                       "objObject": component.get("v.objInfoForPicklistValues"),
                       "fld": fieldName
                       });
                       var opts = [];
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v." + picklistOptsAttributeName, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    gettheID : function(cmp,recordId) {
        var action = cmp.get("c.getId");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.Idcuenta", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    gettheDate : function(cmp,recordId) {
        var action = cmp.get("c.getHour");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.date", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataAccountDis : function(cmp,recordId2) {
        var action = cmp.get("c.disponible");
        action.setParams({ recordId2 : recordId2});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                console.log("v.creditLimit" , conts);
                cmp.set("v.creditLimit", conts);
                cmp.set("v.creditLimitInit", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getResponse : function(component,recordId, event) {
        var action = component.get("c.genOrder");
        var oncall = component.get("v.Idcuenta");
        var product = component.get("v.lookName");
        var promos = component.get("v.promosEnviar2");
        var dateValue = component.get("v.date");
        var creCash = component.get("v.creditCashValues");
        
        console.log('getResponse product: ', JSON.stringify(product));
        console.log('getResponse promos: ', JSON.stringify(promos));
        /*
        if (!confirm("Products: " + JSON.stringify(product))) {
          return;
        }
        if (!confirm("Deals: " + JSON.stringify(promos))) {
          return;
        }
        */
        component.set("v.showtabOI",false);
        component.set("v.isLoading",true);
        
        action.setParams({
            product : JSON.stringify(product),
            call:oncall,
            recordId:recordId,
            promos:JSON.stringify(promos),
            dateValue:dateValue,
            creCash:creCash
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if (component.isValid() && state === "SUCCESS") {
                console.log('res---->' + JSON.stringify(response.getReturnValue()));
                var x = response.getReturnValue(); 
                component.set("v.response", x);
                // sforce.one.navigateToSObject(recordId	);                 
                //location.reload();                                
                var appEventSearch = $A.get("e.c:eventReloadComponents");
                //var appEventSearch = component.getEvent("eventSendOrd");
                //console.log('event fire from Search');
                //appEventSearch.setParams({"reloadViewBySearchOrder": true});
                appEventSearch.setParams({"reloadView": true});
                setTimeout($A.getCallback(function() {
                    console.log('event fire from search set');
                    console.log("appEventSearch ", appEventSearch);                    
                    appEventSearch.fire();
                    //Refresh variable
                    //console.log('was fire')
                    component.set("v.lookName", []);
                    component.set("v.promosEnviar2", []);
                    component.set("v.promosEnviar", []);
                    component.set("v.promosView", []);
                    component.set("v.tablePromotion",false);
                    
                    $A.get('e.force:refreshView').fire();
                    
                }), 3000);                
                
            }
            else if (state === "INCOMPLETE") {
                // do something
                component.set("v.isLoading",false);
            }
                else if (state === "ERROR") {
                    component.set("v.isLoading",false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        
    },
    getResponseupdate : function(component,recordId, event) {
        var action = component.get("c.genOrder");
        var oncall = component.get("v.Idcuenta");
        var product = component.get("v.lookName");
        var promos = component.get("v.promosEnviar2");
        var creCash = component.get("v.creditCashValues");
        
        action.setParams({
            product : JSON.stringify(product),
            call:oncall,
            recordId:recordId,
            promos:JSON.stringify(promos),
            creCash:creCash
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if (component.isValid() && state === "SUCCESS") {
                //console.log('res---->' + JSON.stringify(response.getReturnValue()));
                var x = response.getReturnValue(); 
                component.set("v.response", x);
                window.setTimeout(
                    $A.getCallback(function() {
                        
                        alert("Orden generada correctamente");				
                    }), 600
                    
                ); 	
                
                var eUrl= $A.get("e.force:navigateToURL");
                eUrl.setParams({
                    "url": 'https://abinbevcopec--wiidevacc1.lightning.force.com/lightning/o/ONCALL__Call__c/list?filterName=Recent' 
                });
                eUrl.fire();            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getprice : function(component,recordId) {        
        var action = component.get("c.callourPrice");
        var product = component.get("v.lookName");
        var promos = component.get("v.promosEnviar");   
        var deals = component.get("v.soRespDeals")
        
        console.log("getPrice promos--->", promos);
        console.log("getPrice deals--->", JSON.stringify(deals));
        
        var cash = component.get("v.cash");
        var cuenta = component.get("v.Idcuenta");
        var promosV = [];
        var promosSend = [];
        var payConditionAcc = cuenta && cuenta.length > 0 && cuenta[0].ONCALL__POC__c ? cuenta[0].ONCALL__POC__r.ONTAP__Credit_Condition__c : '';
        var qttyNotOk = false;
        
        //console.log('payConditionAcc', payConditionAcc);
        console.log("Este es el producto ----> " , product);
        product.forEach(elementProd=> {
             if (elementProd.quantity == null || !Number.isInteger(elementProd.quantity) || elementProd.quantity<1 ){       
            console.log("elementProd qtty is not valid: " + elementProd.quantity)
            	alert($A.get("$Label.c.You_must_enter_an_amount"));   
            	qttyNotOk=true;
        		}
        })
       if(qttyNotOk){
            return;
        }
            
        action.setParams({
            recordId:	recordId,
            product : 	JSON.stringify(product),
            promos: 	JSON.stringify(promos),
            deals: 		JSON.stringify(deals)
        });
        console.log("Segunda Lista ---> ",JSON.stringify(product));
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var x = response.getReturnValue();    
                //if(x && x.lenght > 0){                    
                component.set("v.boton",true);
                component.set("v.pesos",true);
                component.set("v.lookName",x);
                
                var newArray=[];
                console.log("wrap inside getprice" + JSON.stringify(x));
                var promos=x;
                var arraypromo=[];
                var arraypromotosend=[];
				
                promos.forEach(element=> {
                    if(element.listadeals != null && element.listadeals != ""){  
                    	//Se identifica deal con freegood
                        if(element.freeGood && element.listadeals && element.listadeals.length){                            
                            element.listadeals.forEach(elementdeal=> {
                    			elementdeal.freeGood = true;
                    			console.log("elementdeal " + elementdeal);
                            });
                        }                    
                        arraypromo.push(element.listadeals);
                        console.log('listadeals '+JSON.stringify(arraypromo));                        
                    }
                    //console.log('cash getPrice', cash );     
                    if(payConditionAcc == 'Credit'){
                        if(cash){
                            element.pay=$A.get("$Label.c.OnCall_Cash");
                            //console.log('element.pay getPrice', element.pay );
                        }else{
                            element.pay=$A.get("$Label.c.OnCall_Credit");   
                            // console.log('element.pay getPrice else', element.pay );
                        }        
                    }else{
                        element.pay=$A.get("$Label.c.OnCall_Cash");
                        //console.log('element.pay getPrice', element.pay );
                    }
                
            	});
                    if(arraypromo != null && arraypromo != "") {
                        arraypromo.forEach(element=> {
                            element.forEach(element2=> {
                            	newArray.push(element2);
                            	console.log('element2: '+JSON.stringify(element2));
                        	});
                        });
                        arraypromotosend = [];
                       	var objectPromod = {};
						var objectPromod2 = {};
                        newArray.forEach(element2=> {
                            //if(element2.pronr!= null && element2.condcode != null){
                            if(element2.pronr!= null){
                                var prom={
                                    promoNamedescription:element2.pronr,
                                    promoDescTosend:element2.description,
                                    condcode:element2.condcode,
                                    amount:element2.amount,
                                    posex:element2.posex,
                            		freeGood : element2.freeGood
                                };
                                
                                if(!objectPromod.hasOwnProperty(prom.promoNamedescription)){
                                    objectPromod[prom.promoNamedescription] = prom;
                                    promosV.push(prom);
                                }

								if(!objectPromod2.hasOwnProperty(prom.promoNamedescription) && (prom.condcode || prom.freeGood)){
                                    objectPromod2[prom.promoNamedescription] = prom;
                                    promosSend.push(prom);
                                }

                        		console.log("objectPromod", objectPromod2);
                                arraypromotosend.push(prom);
                                component.set("v.tablePromotion",true);
                            }
                		});                
                    console.log("arraypromotosend",arraypromotosend);
        			component.set("v.promosEnviar", arraypromotosend);  
                    component.set("v.promosEnviar2", arraypromotosend);  
                    //component.set("v.promosEnviar2", promosSend);  
                    //console.log("get" , component.get("v.promosEnviar2"));
                    //console.log("promosV",promosV);                     
                    component.set("v.promosView",promosV);  
                }
            
                if(arraypromotosend.length == 0){
                    component.set("v.tablePromotion",false);
                    var emptyArray = [];
                    component.set("v.promosEnviar",emptyArray);
    				component.set("v.promosEnviar2",emptyArray);
                }
            
                console.log('final '+JSON.stringify(arraypromotosend));
                //}
                //	else{
                //    	alert($A.get("$Label.c.No_price_available"));    
                //}
            }
        });
        $A.enqueueAction(action);
    },
    
    getpaymet : function(component,recordId2) {
        var action = component.get("c.paymentMethod");
        action.setParams({ recordId2 : recordId2});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.creditCashValues", conts);
                console.log("Value default "+ conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    }            
    
})