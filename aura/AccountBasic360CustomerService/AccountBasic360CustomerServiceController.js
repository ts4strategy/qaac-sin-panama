({
    IsOpenBasicSection : function(cmp, event, helper) {
        
        var sectionOneIsClosed = cmp.get('v.sectionOneIsClosed'); 
		var sectionOne = cmp.find('sectionOne');
		var sectionOneS1 = cmp.find('sectionOne-switch1');
        var sectionOneS2 = cmp.find('sectionOne-switch2');
        
        if(!sectionOneIsClosed){
            cmp.set('v.sectionOneIsClosed',true);
            $A.util.removeClass(sectionOne, 'slds-is-open');
            $A.util.addClass(sectionOneS1, 'toggle');
            $A.util.removeClass(sectionOneS2, 'toggle');

        }else{  
            cmp.set('v.sectionOneIsClosed',false);
            $A.util.addClass(sectionOne, 'slds-is-open');
            $A.util.removeClass(sectionOneS1, 'toggle');
            $A.util.addClass(sectionOneS2, 'toggle');
        }

	},
    
    IsOpenStructureSection : function(cmp, event, helper) {
        
        var sectionTwoIsClosed = cmp.get('v.sectionTwoIsClosed'); 
        var sectionTwo = cmp.find('sectionTwo');
        var sectionTwoS1 = cmp.find('sectionTwo-switch1');
        var sectionTwoS2 = cmp.find('sectionTwo-switch2');
        
        if(!sectionTwoIsClosed){
            cmp.set('v.sectionTwoIsClosed',true);
            $A.util.removeClass(sectionTwo, 'slds-is-open');
            $A.util.addClass(sectionTwoS1, 'toggle');
            $A.util.removeClass(sectionTwoS2, 'toggle');
            
        }else{  
            cmp.set('v.sectionTwoIsClosed',false);
            $A.util.addClass(sectionTwo, 'slds-is-open');
            $A.util.removeClass(sectionTwoS1, 'toggle');
            $A.util.addClass(sectionTwoS2, 'toggle');
        }
    }
})