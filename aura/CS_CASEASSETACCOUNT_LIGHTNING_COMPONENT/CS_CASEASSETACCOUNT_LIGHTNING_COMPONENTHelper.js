({    
    getSerialNumber: function(component)
    {
        var action = component.get("c.getSerialNumber");
        var idCase = component.get("v.RecordId");
        action.setParams({idCase: idCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                component.set("v.SerialNumbers", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    component.set("v.listSerialNumber", 'True');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
        
    getAssetInfo:function(component, SerialNumber)
    {
        var action = component.get("c.getAssetInfo");
        var idCase = component.get("v.RecordId");
        action.setParams({idCase:idCase, serialNumber:SerialNumber});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {   
                var conts = response.getReturnValue();
                component.set("v.selectedBrand", conts.ONTAP__Brand__c);
                component.set("v.selectedModel", conts.HONES_Asset_Model__c);
                component.set("v.Description", conts.ONTAP__Asset_Description__c);
                component.set("v.selectedEquipmentNumber", conts.HONES_EquipmentNumber__c);
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else
                {
                    console.log("Unknown error");
                }
            } 
        });
        
        $A.enqueueAction(action);        
    },
    
    setAssetCaseInformation: function(component, idCase)
    {
        var idCase = component.get("v.RecordId");
        var action = component.get("c.SetAssetCaseInfo");
        action.setParams({idCase: idCase});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var conts = response.getReturnValue();
                if(conts == null)
                {
                    console.log("Wihtout Asset Info");
                }
                else
                {
                    component.set("v.caseUpdated",conts);
                    component.set("v.quantity",conts[0].ISSM_Count__c);
                    component.set("v.selectedBrand",conts[1].ONTAP__Brand__c);
                    component.set("v.selectedModel",conts[1].HONES_Asset_Model__c);
                    component.set("v.Description",conts[1].ONTAP__Asset_Description__c);
                    component.set("v.selectedEquipmentNumber",conts[1].HONES_EquipmentNumber__c);
                    component.set("v.selectedSerialNumber",conts[1].ONTAP__Serial_Number__c);
                    component.set("v.listSerialNumber",'False');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    updateAndCallout: function(component, event, helper)
    {
        var idCase = component.get("v.RecordId");
        var SerialNumber = component.get("v.selectedSerialNumber");
        var quantity = component.get("v.quantity");
        var action = component.get("c.UpdateCase");
        action.setParams({idCase:idCase, serialNumber:SerialNumber, quantity:quantity});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var isUpdatedAndSend = response.getReturnValue();
                if(isUpdatedAndSend)
                {
                    component.set("v.Error", false);
                    component.set("v.showSpinner", false);
                    component.set("v.caseUpdated",true);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Enviado"));
                    component.set("v.buttonDisabled",true);
                    component.set("v.Edit", false);
                }
                else
                {
                    component.set("v.showSpinner", false);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Error"));
                    component.set("v.buttonDisable",true);
                    component.set("v.Edit",false);
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    },
    
    updateAsset: function(component, event, helper)
    {
        var idCase = component.get("v.RecordId");
        var SerialNumber = component.get("v.SerialNumber");        
        var action = component.get("c.UpdateCaseAsset");
        action.setParams({idCase:idCase, SerialNumber:SerialNumber});
        
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var isUpdatedAndSend = response.getReturnValue();
                if(isUpdatedAndSend)
                {
                    component.set("v.showSpinner", false);
                    component.set("v.caseUpdated",true);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Reparacion"));
                    component.set("v.buttonDisabled",true);
                }
                else
                {
                    component.set("v.showSpinner", false);
                    component.set("v.mensajeEnvio",$A.get("$Label.c.CS_Caso_Error"));
                    component.set("v.buttonDisable",true);
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    }
})