({
    loadData: function (component, event, helper)
    {
        setTimeout(function(){component.set("v.timer",true);}, 500);
        var idCase = component.get("v.RecordId");
        helper.getSerialNumber(component);
        helper.setAssetCaseInformation(component,idCase);       
    },
    
    onSerialNumber: function(component, event, helper)
    {
        var SerialNumber = component.find("SelectSerialNumber").get("v.value");
        component.set("v.SerialNumber", SerialNumber);
        component.set("v.Error", false);
        
        if(typeof SerialNumber != undefined && SerialNumber)
        {
            helper.getAssetInfo(component, SerialNumber);
            component.set("v.selectedSerialNumber", SerialNumber);
            component.set("v.listSerialNumber", 'False');
            component.set("v.buttonDisabled", false);
        }
    },
        
    editProduct: function(component, event, helper) 
    {
        component.set("v.listSerialNumber", 'True');
        component.set("v.buttonDisable",false);
        component.set("v.SerialNumber","");
        component.set("v.selectedSerialNumber", "");
        component.set("v.selectedBrand", "");
        component.set("v.selectedModel","");
        component.set("v.Description","");
        component.set("v.selectedEquipmentNumber","");
        component.set("v.caseUpdated",null);
        component.set("v.buttonDisabled", true);
        component.set("v.quantity", null);
        component.set("v.Error", "False")
    },
    
    handleClickButtonSend : function(component, event, helper)
    {       
        component.set("v.showSpinner", true);     
        var inputValid = true;    
        var mensajesError;
        var quanti = component.get("v.quantity");
        
        if(typeof quanti === undefined || quanti === 0 || quanti == null )
        {
            inputValid = false; 
            mensajesError = $A.get("$Label.c.CS_Cantidad_Erronea");
        }
        
        var SerialNumber = component.get("v.selectedSerialNumber");
        if(typeof SerialNumber === undefined || SerialNumber === '')
        {
            inputValid = false; 
            mensajesError = $A.get("$Label.c.CS_NumeroDeSerie_Obligatorio");
        }
        
        if(inputValid)
        {
            helper.updateAndCallout(component);
        }
        else
        {
            component.set("v.Error", "True");
            component.set("v.mensajeError",mensajesError);
            component.set("v.showSpinner", false);
        }              
    },
    
    handleClickButtonUpdate:function(component, event, helper)
    {       
        component.set("v.showSpinner", true);     
        var inputValid = true;    
        var mensajesError;
        
        if(component.get("v.listSerialNumber") ==='True')
        {
            var SerialNumber = component.find("SelectSerialNumber").get("v.value");
            if(typeof SerialNumber === undefined || SerialNumber === '')
            {
                inputValid = false; 
                mensajesError = $A.get("$Label.c.CS_NumeroDeSerie_Obligatorio");
            }
            
        }
                
        if(inputValid)
        {
            helper.updateAsset(component);
        }
        else
        {
            component.set("v.Error", "True");
            component.set("v.mensajeError",mensajesError);
            component.set("v.showSpinner", false);
        }        
        
    }    
})