({
	searchHelper: function (component, event, getInputkeyWord) {
		// call the apex class method 
		var recordId = component.get("v.recordId");
		var action = component.get("c.fetchLookUpValuesPbe");
		// set param to method  
		action.setParams({
            'recordId' : recordId,
			'searchKeyWord': getInputkeyWord,
			'ObjectName': component.get("v.objectAPIName")
		});
		// set a callBack    
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === "SUCCESS") {
				var storeResponse = response.getReturnValue();
         		component.set("v.listOfSearchRecordsInit", storeResponse);
                component.set("v.listOfSearchRecords", storeResponse);
                console.log('storeResponseSearchHelper', storeResponse);
			}

		});
		// enqueue the Action  
		$A.enqueueAction(action);

	},

    searchLocalHelper: function (component, event, getInputkeyWord) {
        // call the apex class method 
        console.log('getInputkeyWord', getInputkeyWord);
        var listInit = component.get("v.listOfSearchRecordsInit");
        console.log('listInit' , listInit);
        var expresion = new RegExp(getInputkeyWord, 'gi');
        var listFilter = [];
        if(getInputkeyWord && getInputkeyWord != ''){
            listFilter = listInit.filter(function filterByCodeAndMat(prod) {
                //console.log('prod', prod);
                if ('productCode' in prod && 'materialProduct' in prod && (prod.productCode.match(expresion) || prod.materialProduct.match(expresion) ) ) {
                    return true;
                } else {
                    return false;
                }
            });
            component.set("v.listOfSearchRecords", listFilter);   
            console.log("listFilter", listFilter);
        }else{
            component.set("v.listOfSearchRecords", listInit);  
        }             
    },
    
	loadOptions: function (component, event, helper) {
		var options = [{
				label: '        ',
				value: '        '
			},
			{
				label: 'obsequio',
				value: 'obsequio'
			},
			{
				label: 'siembra',
				value: 'siembra'
			},


		];
		component.set("v.statusOptions", options);
	},
	// fetch picklist values dynamic from apex controller 
	fetchPickListVal: function (component, fieldName, picklistOptsAttributeName) {
		var action = component.get("c.getselectOptions");
		action.setParams({
			"objObject": component.get("v.objInfoForPicklistValues"),
			"fld": fieldName
		});
		var opts = [];
		action.setCallback(this, function (response) {
			if (response.getState() == "SUCCESS") {
				var allValues = response.getReturnValue();

				if (allValues != undefined && allValues.length > 0) {
					opts.push({
						class: "optionClass",
						label: "--- None ---",
						value: ""
					});
				}
				for (var i = 0; i < allValues.length; i++) {
					opts.push({
						class: "optionClass",
						label: allValues[i],
						value: allValues[i]
					});
				}
				component.set("v." + picklistOptsAttributeName, opts);
			}
		});
		$A.enqueueAction(action);
	},
    
    gettheID : function(cmp,recordId) {
        var action = cmp.get("c.getId");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.Idcuenta", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    gettheDate : function(cmp,recordId) {
        var action = cmp.get("c.getHour");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.date", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataAccountDis : function(cmp,recordId2) {
        var action = cmp.get("c.disponible");
        action.setParams({ recordId2 : recordId2});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.creditLimit", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getResponse : function(component,recordId, event) {
		var action = component.get("c.genOrder");
        var oncall = component.get("v.Idcuenta");
        var product = component.get("v.lookName");
        action.setParams({
             product : JSON.stringify(product),
             call:oncall,
            recordId:recordId
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            //alert(state);
            if (component.isValid() && state === "SUCCESS") {
                //console.log('res---->' + JSON.stringify(response.getReturnValue()));
                var x = response.getReturnValue(); 
                component.set("v.response", x);
               // sforce.one.navigateToSObject(recordId	); 
                location.reload();
            }
           else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
	},
       getprice : function(component,recordId) {
		var action = component.get("c.callourPrice");
         var product = component.get("v.lookName");
           

            action.setParams({
            
            product : JSON.stringify(product)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            alert(state);
            if (component.isValid() && state === "SUCCESS") {
                var x = response.getReturnValue();
                console.log('Este es el precio calculado :D ' + x);
                  component.set("v.boton",true);
                 component.set("v.pesos",true);
                 component.set("v.lookName",x);
					
            }
        });
        $A.enqueueAction(action);
	},
})