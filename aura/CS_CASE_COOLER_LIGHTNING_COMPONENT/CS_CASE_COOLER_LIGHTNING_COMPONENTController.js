({
    doInit : function(component, event, helper) 
    {
        helper.getPicklistProducts(component);   
        helper.finalProducts(component);
        setTimeout(function(){component.set("v.timer",true);}, 500);
    },
    
    changeProduct: function(component, event, helper) 
    {
        var prod = component.find("selectMaterial").get("v.value");
        var quan = component.find("quantityId").get("v.value");
        component.set("v.selectedValue",prod);
        component.set("v.buttonDisable",false);        
             
        if(typeof quan === undefined || quan === 0 || quan== null)
        {
            component.set("v.mensajeError",$A.get("$Label.c.CS_Cantidad_Erronea"));            
            component.set("v.Error", "True");
            component.set("v.selectedValue", "");
            component.find("selectMaterial").set("v.value","");
        }
        else if(typeof prod != undefined || prod != '')
        {
            component.set("v.FinalValue", "True");
        	helper.changeProducts(component);
            component.set("v.Error", false);
        }
    },
    
    editProduct: function(component, event, helper) 
    {
        component.set("v.FinalValue",false);
        component.set("v.Error",false);
        component.set("v.selectedValue", "");
        component.set("v.Error",false);
        component.set("v.sendError",false);
        component.set("v.sendSuccess",false);
        component.set("v.buttonDisable",false);
        
    },
    
    handleClickButtonSend: function(component, event, helper)
    {   
        component.set("v.showSpinner", true); 
        component.set("v.Error",false);
        component.set("v.sendError",false);
        component.set("v.sendSuccess",false);
        var inputValid = true;        
        var prod = component.get("v.selectedValue");
        var quanty = component.get("v.quantity");
        
        if(typeof prod === undefined || prod === '')
        {
            inputValid = false;            
        }
        
        if(inputValid)
        {
            helper.updateAndCallout(component);            
        }
        else
        {
            component.set("v.mensajeError",$A.get("$Label.c.CS_Material_Obligatorio"));
            component.set("v.Error", "True");
            component.set("v.showSpinner", false);
            component.set("v.buttonDisable",true);
        }
        
    }
    
})