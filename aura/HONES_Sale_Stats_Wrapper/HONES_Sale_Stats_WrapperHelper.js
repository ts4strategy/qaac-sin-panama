({    
getDataMap2 : function(cmp,recordId) {
        var action = cmp.get("c.getAccountFromCall2");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.AccountRec", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
})